-- phpMyAdmin SQL Dump
-- version 4.4.1.1
-- http://www.phpmyadmin.net
--
-- Host: localhost:8889
-- Generation Time: Mar 12, 2018 at 03:19 PM
-- Server version: 5.5.42
-- PHP Version: 5.6.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `yanfomaLaravel`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'app', '2018-01-30 19:53:47', '2018-01-30 19:53:47');

-- --------------------------------------------------------

--
-- Table structure for table `checkouts`
--

CREATE TABLE `checkouts` (
  `id` int(10) unsigned NOT NULL,
  `purchased_id` int(11) NOT NULL,
  `purchased_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prix` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `checkouts`
--

INSERT INTO `checkouts` (`id`, `purchased_id`, `purchased_name`, `name`, `image`, `qty`, `prix`, `total`, `created_at`, `updated_at`) VALUES
(1, 1, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-05 21:56:54', '2018-03-05 21:56:54'),
(2, 1, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-05 21:56:54', '2018-03-05 21:56:54'),
(3, 2, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-05 22:13:36', '2018-03-05 22:13:36'),
(4, 2, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-05 22:13:36', '2018-03-05 22:13:36'),
(5, 3, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-05 22:14:03', '2018-03-05 22:14:03'),
(6, 3, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-05 22:14:03', '2018-03-05 22:14:03'),
(7, 4, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-05 22:50:37', '2018-03-05 22:50:37'),
(8, 4, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-05 22:50:37', '2018-03-05 22:50:37'),
(9, 5, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-05 22:52:30', '2018-03-05 22:52:30'),
(10, 5, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-05 22:52:30', '2018-03-05 22:52:30'),
(11, 6, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-05 22:54:17', '2018-03-05 22:54:17'),
(12, 6, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-05 22:54:17', '2018-03-05 22:54:17'),
(13, 7, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-05 22:55:13', '2018-03-05 22:55:13'),
(14, 7, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-05 22:55:13', '2018-03-05 22:55:13'),
(15, 8, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-05 22:56:37', '2018-03-05 22:56:37'),
(16, 8, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-05 22:56:37', '2018-03-05 22:56:37'),
(17, 9, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-05 22:57:35', '2018-03-05 22:57:35'),
(18, 9, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-05 22:57:35', '2018-03-05 22:57:35'),
(19, 10, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-05 23:05:15', '2018-03-05 23:05:15'),
(20, 10, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-05 23:05:15', '2018-03-05 23:05:15'),
(21, 11, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-05 23:06:42', '2018-03-05 23:06:42'),
(22, 11, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-05 23:06:42', '2018-03-05 23:06:42'),
(23, 12, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-05 23:14:12', '2018-03-05 23:14:12'),
(24, 12, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-05 23:14:12', '2018-03-05 23:14:12'),
(25, 13, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-05 23:14:40', '2018-03-05 23:14:40'),
(26, 13, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-05 23:14:40', '2018-03-05 23:14:40'),
(27, 14, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-05 23:15:09', '2018-03-05 23:15:09'),
(28, 14, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-05 23:15:09', '2018-03-05 23:15:09'),
(29, 15, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-05 23:15:21', '2018-03-05 23:15:21'),
(30, 15, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-05 23:15:21', '2018-03-05 23:15:21'),
(31, 16, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-05 23:18:14', '2018-03-05 23:18:14'),
(32, 16, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-05 23:18:14', '2018-03-05 23:18:14'),
(33, 17, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-05 23:20:30', '2018-03-05 23:20:30'),
(34, 17, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-05 23:20:30', '2018-03-05 23:20:30'),
(35, 18, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-05 23:23:41', '2018-03-05 23:23:41'),
(36, 18, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-05 23:23:41', '2018-03-05 23:23:41'),
(37, 19, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-05 23:27:00', '2018-03-05 23:27:00'),
(38, 19, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-05 23:27:00', '2018-03-05 23:27:00'),
(39, 20, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-05 23:38:53', '2018-03-05 23:38:53'),
(40, 20, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-05 23:38:53', '2018-03-05 23:38:53'),
(41, 21, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-05 23:40:22', '2018-03-05 23:40:22'),
(42, 21, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-05 23:40:22', '2018-03-05 23:40:22'),
(43, 22, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-05 23:40:52', '2018-03-05 23:40:52'),
(44, 22, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-05 23:40:52', '2018-03-05 23:40:52'),
(45, 23, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-05 23:46:49', '2018-03-05 23:46:49'),
(46, 23, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-05 23:46:49', '2018-03-05 23:46:49'),
(47, 24, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-05 23:48:04', '2018-03-05 23:48:04'),
(48, 24, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-05 23:48:04', '2018-03-05 23:48:04'),
(49, 25, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-05 23:49:28', '2018-03-05 23:49:28'),
(50, 25, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-05 23:49:28', '2018-03-05 23:49:28'),
(51, 26, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-05 23:51:12', '2018-03-05 23:51:12'),
(52, 26, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-05 23:51:12', '2018-03-05 23:51:12'),
(53, 27, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-05 23:53:14', '2018-03-05 23:53:14'),
(54, 27, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-05 23:53:14', '2018-03-05 23:53:14'),
(55, 28, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-05 23:53:32', '2018-03-05 23:53:32'),
(56, 28, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-05 23:53:32', '2018-03-05 23:53:32'),
(57, 29, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-05 23:55:40', '2018-03-05 23:55:40'),
(58, 29, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-05 23:55:40', '2018-03-05 23:55:40'),
(59, 30, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-05 23:56:27', '2018-03-05 23:56:27'),
(60, 30, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-05 23:56:27', '2018-03-05 23:56:27'),
(61, 31, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-06 00:03:51', '2018-03-06 00:03:51'),
(62, 31, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-06 00:03:51', '2018-03-06 00:03:51'),
(63, 32, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-06 00:11:54', '2018-03-06 00:11:54'),
(64, 32, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-06 00:11:54', '2018-03-06 00:11:54'),
(65, 33, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-06 00:15:02', '2018-03-06 00:15:02'),
(66, 33, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-06 00:15:02', '2018-03-06 00:15:02'),
(67, 34, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-06 00:20:04', '2018-03-06 00:20:04'),
(68, 34, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-06 00:20:04', '2018-03-06 00:20:04'),
(69, 35, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-06 00:20:16', '2018-03-06 00:20:16'),
(70, 35, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-06 00:20:16', '2018-03-06 00:20:16'),
(71, 36, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-06 00:20:37', '2018-03-06 00:20:37'),
(72, 36, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-06 00:20:37', '2018-03-06 00:20:37'),
(73, 37, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-06 00:20:47', '2018-03-06 00:20:47'),
(74, 37, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-06 00:20:47', '2018-03-06 00:20:47'),
(75, 38, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-06 00:26:55', '2018-03-06 00:26:55'),
(76, 38, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-06 00:26:55', '2018-03-06 00:26:55'),
(77, 39, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-06 00:28:33', '2018-03-06 00:28:33'),
(78, 39, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-06 00:28:33', '2018-03-06 00:28:33'),
(79, 40, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-06 00:29:18', '2018-03-06 00:29:18'),
(80, 40, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-06 00:29:18', '2018-03-06 00:29:18'),
(81, 41, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-06 00:30:01', '2018-03-06 00:30:01'),
(82, 41, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-06 00:30:01', '2018-03-06 00:30:01'),
(83, 42, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-06 00:30:41', '2018-03-06 00:30:41'),
(84, 42, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-06 00:30:41', '2018-03-06 00:30:41'),
(85, 43, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-06 00:31:40', '2018-03-06 00:31:40'),
(86, 43, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-06 00:31:40', '2018-03-06 00:31:40'),
(87, 44, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-06 00:32:50', '2018-03-06 00:32:50'),
(88, 44, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-06 00:32:50', '2018-03-06 00:32:50'),
(89, 45, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-06 00:34:44', '2018-03-06 00:34:44'),
(90, 45, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-06 00:34:44', '2018-03-06 00:34:44'),
(91, 46, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-06 00:35:29', '2018-03-06 00:35:29'),
(92, 46, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-06 00:35:29', '2018-03-06 00:35:29'),
(93, 47, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-06 00:36:08', '2018-03-06 00:36:08'),
(94, 47, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-06 00:36:08', '2018-03-06 00:36:08'),
(95, 48, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-06 00:37:15', '2018-03-06 00:37:15'),
(96, 48, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-06 00:37:15', '2018-03-06 00:37:15'),
(97, 49, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-06 00:38:58', '2018-03-06 00:38:58'),
(98, 49, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-06 00:38:58', '2018-03-06 00:38:58'),
(99, 50, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-06 00:39:34', '2018-03-06 00:39:34'),
(100, 50, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-06 00:39:34', '2018-03-06 00:39:34'),
(101, 51, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-06 00:40:28', '2018-03-06 00:40:28'),
(102, 51, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-06 00:40:28', '2018-03-06 00:40:28'),
(103, 52, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-06 00:42:37', '2018-03-06 00:42:37'),
(104, 52, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-06 00:42:37', '2018-03-06 00:42:37'),
(105, 53, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-06 00:43:34', '2018-03-06 00:43:34'),
(106, 53, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-06 00:43:34', '2018-03-06 00:43:34'),
(107, 54, 'sdnjdk', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474474', '234', '111,026,916.00', '2018-03-06 00:46:26', '2018-03-06 00:46:26'),
(108, 54, 'sdnjdk', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-06 00:46:26', '2018-03-06 00:46:26'),
(109, 51, 'sd nd nd dsmdsd', 'sjndsj', 'uploads/shop/products/15174782262.jpg', '474478', '234', '111,027,852.00', '2018-03-08 06:42:31', '2018-03-08 06:42:31'),
(110, 51, 'sd nd nd dsmdsd', 'adds', 'uploads/shop/products/15174782073.jpg', '23', '222', '5,106.00', '2018-03-08 06:42:31', '2018-03-08 06:42:31');

-- --------------------------------------------------------

--
-- Table structure for table `community`
--

CREATE TABLE `community` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `purpose` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `community`
--

INSERT INTO `community` (`id`, `name`, `slug`, `category`, `purpose`, `status`, `description`, `image`, `link`, `created_at`, `updated_at`) VALUES
(10, 'Devduni', 'devduni', 'technologie', 'dkndksd', 'ouvert', '<p>dsnjdskds</p>', 'uploads/communities/1520221604devduni.png', 'https://www.facebook.com/groups/1550813691622336/permalink/1596716203698751/?notif_id=1520071111019261&notif_t=page_group_post&ref=notif', '2018-03-04 19:46:44', '2018-03-04 19:46:44');

-- --------------------------------------------------------

--
-- Table structure for table `demande`
--

CREATE TABLE `demande` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` int(11) NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `resume` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `view` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `demande`
--

INSERT INTO `demande` (`id`, `name`, `code`, `email`, `phone`, `resume`, `message`, `view`, `created_at`, `updated_at`) VALUES
(1, 'dddkdf', 814, 'dnfdjnfd@yahoo.fr', '123', 'uploads/jobs/applications/1520302420Textes-pour-le-site-WEB_3.docx', 'dnldjndkndfdf', 'Yes', '2018-03-05 18:13:40', '2018-03-06 19:42:34'),
(2, 'sdhdsbd djhbusdds', 814, 'dbhdbhdbhjdsb@gsdhbdsjh.fr', '12345678', 'uploads/jobs/applications/1520302893-HOW-IS-IPS.docx', 'kcnkjnckjnkjcncnckjnckjnckjnv\r\nsnsdndsdsjndskjsdnds\r\ndjnjdndnfbfddf\r\ndfidf\r\nndojnjf\r\ndfbffddf\r\nbfjbf\r\nddfh\r\ndohido', 'Yes', '2018-03-05 18:21:33', '2018-03-05 18:21:56'),
(3, 'sddds', 814, 'fabioued@yahoo.fr', '1234567', 'uploads/jobs/applications/15203152511520302893-HOW-IS-IPS.docx', 'dddd', 'Yes', '2018-03-05 21:47:31', '2018-03-06 03:30:23');

-- --------------------------------------------------------

--
-- Table structure for table `introduction`
--

CREATE TABLE `introduction` (
  `id` int(10) unsigned NOT NULL,
  `language` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_10_05_165454_create_posts_table', 1),
(4, '2017_10_05_170141_create_categories_table', 1),
(5, '2017_10_10_015524_create_tags_table', 1),
(6, '2017_10_10_020032_create_post_tag_table', 1),
(7, '2017_10_10_033514_create_profiles_table', 1),
(8, '2017_10_10_195759_create_settings_table', 1),
(9, '2017_10_11_064626_create_post_translations_table', 1),
(10, '2017_10_14_050912_insert_user_id', 1),
(11, '2017_10_18_091745_create_slideshow_table', 1),
(12, '2017_10_18_110013_create_team_table', 1),
(13, '2017_10_21_063845_insert_translate_column', 1),
(14, '2017_10_21_120055_insert_translate_column_posts', 1),
(15, '2017_12_30_175008_create_testimony_table', 1),
(16, '2017_12_30_185043_create_introduction_table', 1),
(17, '2017_12_30_193657_create_upcomingEvents_table', 1),
(19, '2018_01_31_035606_create_products_table', 2),
(24, '2018_02_01_175650_create_shoppingcart_table', 3),
(25, '2018_02_17_165943_create_offers_table', 3),
(26, '2018_02_23_095518_create_community_table', 4),
(28, '2018_02_24_192100_create_demande_table', 5),
(31, '2018_03_05_085528_create_purchased_table', 6),
(32, '2018_03_06_052306_create_checkouts_table', 6);

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE `offers` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nbrPosition` int(10) unsigned NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `experience` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '0 => juste poster',
  `location` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `dateEnd` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `offers`
--

INSERT INTO `offers` (`id`, `name`, `slug`, `code`, `nbrPosition`, `description`, `category`, `experience`, `status`, `location`, `dateEnd`, `price`, `created_at`, `updated_at`) VALUES
(4, 'warty', 'warty', '814', 2, '<p>vggv</p>\r\n<p>bjbb</p>\r\n<p>bjbb</p>\r\n<p>jbbjb</p>\r\n<p>nhghhbh</p>\r\n<p>&nbsp;</p>', 'qwerty', '6', '0', 'sdfghjk', '8 March, 2018', 11234567, '2018-02-18 10:12:36', '2018-02-28 21:54:34'),
(5, 'send', 'send', '902', 6, '<p>dnd d jd sdjs dsd dsjd jds dsdhiuhdsuihsduisdsd dsibsdihdssdsd</p>', 'dsndsds', '89', '0', 'needs', '8 March, 2018', 456, '2018-02-18 10:25:49', '2018-02-28 21:54:14'),
(6, 'php controller', 'php-controller', '573', 1, '<p>sdnkddjsd</p>', 'php php', '3 ans', '0', 'djddj', '9 March, 2018', 89, '2018-02-20 18:27:57', '2018-02-28 21:54:45');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) unsigned NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `category_id` int(11) DEFAULT NULL,
  `featuredImage` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `language` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blog` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `translated` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `slug`, `content`, `category_id`, `featuredImage`, `language`, `blog`, `translated`, `deleted_at`, `created_at`, `updated_at`, `user_id`) VALUES
(1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-28 23:45:34', '2018-02-28 23:45:34', 1),
(2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-28 23:46:58', '2018-02-28 23:46:58', 1),
(3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-28 23:47:56', '2018-02-28 23:47:56', 1),
(4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-28 23:58:50', '2018-02-28 23:58:50', 1),
(5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-03-01 00:07:18', '2018-03-01 00:07:18', 1);

-- --------------------------------------------------------

--
-- Table structure for table `post_tag`
--

CREATE TABLE `post_tag` (
  `id` int(10) unsigned NOT NULL,
  `post_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `post_tag`
--

INSERT INTO `post_tag` (`id`, `post_id`, `tag_id`, `created_at`, `updated_at`) VALUES
(1, 1, 2, NULL, NULL),
(2, 2, 2, NULL, NULL),
(3, 3, 2, NULL, NULL),
(4, 4, 2, NULL, NULL),
(5, 5, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `post_translations`
--

CREATE TABLE `post_translations` (
  `id` int(10) unsigned NOT NULL,
  `post_id` int(10) unsigned NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `featuredImage` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `blog` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `language` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `translated` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `post_translations`
--

INSERT INTO `post_translations` (`id`, `post_id`, `locale`, `title`, `slug`, `content`, `category_id`, `featuredImage`, `blog`, `language`, `deleted_at`, `created_at`, `updated_at`, `translated`) VALUES
(1, 1, 'en', 'djknjkdnfjkndfjdf fj fjfd jkfd fdjkfddfjf jfd dfj kj', 'djknjkdnfjkndfjdf-fj-fjfd-jkfd-fdjkfddfjf-jfd-dfj-kj', '<p>jdsdjkndjs</p>', 1, 'uploads/1519890334Golden-bitcoin-digital-currency-1.jpg', 'post', 'english', '2018-03-01 01:16:34', '2018-02-28 23:45:34', '2018-03-01 01:16:34', '1'),
(2, 2, 'en', 'sdkldmds dsj ds dsds', 'sdkldmds-dsj-ds-dsds', '<p>Excepteur sint occaecat cupidatat proident sunt culpa qui officia deserunt mollit anmlab rum. Sed perspiciatis unde</p>', 1, 'uploads/15198904181-1-1.jpg', 'post', 'english', NULL, '2018-02-28 23:46:58', '2018-02-28 23:46:58', '1'),
(3, 3, 'en', 'jnsdjndsijsd sd ddssdsd', 'jnsdjndsijsd-sd-ddssdsd', '<p>Excepteur sint occaecat cupidatat proident sunt culpa qui officia deserunt mollit anmlab rum. Sed perspiciatis unde</p>', 1, 'uploads/15198904761-1-1.jpg', 'post', 'english', NULL, '2018-02-28 23:47:56', '2018-02-28 23:47:56', '1'),
(4, 4, 'en', 'sddsmdskds sd kissed', 'sddsmdskds-sd-kissed', '<p>sjkdsdjdssd sdnkd kd kjd dskj kdjd</p>', 1, '/private/var/folders/xs/3pqwg53j0z7_gjhvd0gn7wnm0000gn/T/phpQ0MMox', 'post', 'english', '2018-03-01 01:16:30', '2018-02-28 23:58:50', '2018-03-01 01:16:30', '1'),
(5, 5, 'en', 'le comvceo sun ds d h ssh ssh ssh ddc dh ds des Bdshd sud djhs', 'le-comvceo-sun-ds-d-h-ssh-ssh-ssh-ddc-dh-ds-des-bdshd-sud-djhs', '<p>ddnkjdnkjdsnjdsk dj dsjd dh dss d &nbsp;ddnkjdnkjdsnjdsk dj dsjd dh dss d&nbsp;ddnkjdnkjdsnjdsk dj dsjd dh dss d&nbsp;ddnkjdnkjdsnjdsk dj dsjd dh dss d&nbsp;ddnkjdnkjdsnjdsk dj dsjd dh dss dddnkjdnkjdsnjdsk dj dsjd dh dss dddnkjdnkjdsnjdsk dj dsjd dh dss dddnkjdnkjdsnjdsk dj dsjd dh dss d</p>\r\n<p>&nbsp;</p>', 1, '/private/var/folders/xs/3pqwg53j0z7_gjhvd0gn7wnm0000gn/T/php3l67bs', 'post', 'english', '2018-03-01 01:16:28', '2018-03-01 00:07:18', '2018-03-01 01:16:28', '1');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` bigint(20) NOT NULL,
  `minQty` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE `profiles` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(11) NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about` text COLLATE utf8mb4_unicode_ci,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `user_id`, `avatar`, `about`, `facebook`, `created_at`, `updated_at`) VALUES
(1, 1, 'uploads/avatar/avatar.png', ' Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus eveniet facere nisi velit voluptatem! Assumenda cupiditate deleniti enim, eum illum iste minus neque perferendis praesentium quas ratione repellendus veritatis vero?', 'facebook.com', '2018-01-30 19:50:05', '2018-01-30 19:50:05');

-- --------------------------------------------------------

--
-- Table structure for table `purchased`
--

CREATE TABLE `purchased` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pays` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ville` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `addresse` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `nbrProduits` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `view` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `purchased`
--

INSERT INTO `purchased` (`id`, `name`, `pays`, `ville`, `email`, `phone`, `addresse`, `nbrProduits`, `total`, `view`, `created_at`, `updated_at`) VALUES
(1, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'Yes', '2018-03-05 21:56:54', '2018-03-05 21:56:54'),
(2, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'Yes', '2018-03-05 22:13:36', '2018-03-05 22:13:36'),
(3, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'Yes', '2018-03-05 22:14:03', '2018-03-05 22:14:03'),
(4, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'Yes', '2018-03-05 22:50:37', '2018-03-05 22:50:37'),
(5, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'No', '2018-03-05 22:52:30', '2018-03-05 22:52:30'),
(6, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'No', '2018-03-05 22:54:17', '2018-03-05 22:54:17'),
(7, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'No', '2018-03-05 22:55:13', '2018-03-05 22:55:13'),
(8, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'No', '2018-03-05 22:56:37', '2018-03-05 22:56:37'),
(9, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'Yes', '2018-03-05 22:57:35', '2018-03-06 21:04:08'),
(10, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'Yes', '2018-03-05 23:05:15', '2018-03-06 21:01:01'),
(11, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'No', '2018-03-05 23:06:42', '2018-03-05 23:06:42'),
(12, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'No', '2018-03-05 23:14:12', '2018-03-05 23:14:12'),
(13, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'No', '2018-03-05 23:14:40', '2018-03-05 23:14:40'),
(14, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'No', '2018-03-05 23:15:09', '2018-03-05 23:15:09'),
(15, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'No', '2018-03-05 23:15:21', '2018-03-05 23:15:21'),
(16, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'No', '2018-03-05 23:18:14', '2018-03-05 23:18:14'),
(17, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'No', '2018-03-05 23:20:30', '2018-03-05 23:20:30'),
(18, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'No', '2018-03-05 23:23:41', '2018-03-05 23:23:41'),
(19, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'No', '2018-03-05 23:27:00', '2018-03-05 23:27:00'),
(20, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'No', '2018-03-05 23:38:53', '2018-03-05 23:38:53'),
(21, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'No', '2018-03-05 23:40:22', '2018-03-05 23:40:22'),
(22, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'No', '2018-03-05 23:40:52', '2018-03-05 23:40:52'),
(23, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'No', '2018-03-05 23:46:49', '2018-03-05 23:46:49'),
(24, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'No', '2018-03-05 23:48:04', '2018-03-05 23:48:04'),
(25, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'No', '2018-03-05 23:49:28', '2018-03-05 23:49:28'),
(26, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'No', '2018-03-05 23:51:12', '2018-03-05 23:51:12'),
(27, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'No', '2018-03-05 23:53:14', '2018-03-05 23:53:14'),
(28, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'No', '2018-03-05 23:53:32', '2018-03-05 23:53:32'),
(29, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'No', '2018-03-05 23:55:40', '2018-03-05 23:55:40'),
(30, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'No', '2018-03-05 23:56:27', '2018-03-05 23:56:27'),
(31, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'No', '2018-03-06 00:03:51', '2018-03-06 00:03:51'),
(32, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'No', '2018-03-06 00:11:54', '2018-03-06 00:11:54'),
(33, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'No', '2018-03-06 00:15:02', '2018-03-06 00:15:02'),
(34, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'No', '2018-03-06 00:20:04', '2018-03-06 00:20:04'),
(35, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'No', '2018-03-06 00:20:16', '2018-03-06 00:20:16'),
(36, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'No', '2018-03-06 00:20:37', '2018-03-06 00:20:37'),
(37, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'No', '2018-03-06 00:20:47', '2018-03-06 00:20:47'),
(38, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'No', '2018-03-06 00:26:55', '2018-03-06 00:26:55'),
(39, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'No', '2018-03-06 00:28:33', '2018-03-06 00:28:33'),
(40, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'No', '2018-03-06 00:29:18', '2018-03-06 00:29:18'),
(41, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'No', '2018-03-06 00:30:01', '2018-03-06 00:30:01'),
(42, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'No', '2018-03-06 00:30:41', '2018-03-06 00:30:41'),
(43, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'No', '2018-03-06 00:31:40', '2018-03-06 00:31:40'),
(44, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'No', '2018-03-06 00:32:50', '2018-03-06 00:32:50'),
(45, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'No', '2018-03-06 00:34:44', '2018-03-06 00:34:44'),
(46, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'No', '2018-03-06 00:35:29', '2018-03-06 00:35:29'),
(47, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'Yes', '2018-03-06 00:36:08', '2018-03-06 21:31:00'),
(48, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'Yes', '2018-03-06 00:37:15', '2018-03-06 21:30:51'),
(49, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'Yes', '2018-03-06 00:38:57', '2018-03-06 21:30:42'),
(50, 'sdnjdk', 'jjkdds', 'dsndks', 'fabioued@yahoo.fr', '1234567', 'dkdkdmklmdd', '2', '111,032,022.00', 'Yes', '2018-03-06 00:39:34', '2018-03-06 21:30:55'),
(51, 'sd nd nd dsmdsd', 'ndsbdhbdsds', 'njdsjdkss', 'fabioued@yahoo.fr', '1234578', 'ndjkddkndjkdnjkdnjkdnjdjdnfjkfdnfnjfknfdd\r\ndndjknjdkndjkdnjdndjndndjddd', '2', '111,032,958.00', 'Yes', '2018-03-08 06:42:31', '2018-03-08 06:44:09');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL,
  `contact_numberEn` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_numberFr` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_emailEn` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_emailFr` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `addresse` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `introductionVideo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `aboutUs` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `aboutFr` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `youtube` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `copyrightEn` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `copyrightFr` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `contact_numberEn`, `contact_numberFr`, `contact_emailEn`, `contact_emailFr`, `addresse`, `introductionVideo`, `aboutUs`, `aboutFr`, `youtube`, `facebook`, `twitter`, `copyrightEn`, `copyrightFr`, `created_at`, `updated_at`) VALUES
(1, '+226 25 66 64 60', '+226 25 66 64 60', 'ekumi@afrikeveil.org', 'info@afrikeveil.org', 'Cité Azimmo Ouaga 2000, 111 BP 1597 Ouagadougou CMS 11—Burkina Faso', 'https://www.youtube.com/embed/mObSx7bMNTE', 'AFRIK EVEIL fosters start-up development and the scaling-up of innovative and sustainable business solutions in the West African region. We combine applied research with analytical and technical competence of our diverse team of experts, to provide strategic support to small and medium enterprises through provision of management training, coaching and mentoring, networking and social media strategies.', 'Afrik Eveil est une organisation à but non lucratif, créée et gérée par des professionnels expérimentés qui se moblisent en faveur de l''entrepreneuriat comme moyen efficace de promotion de l’employabilité des jeunes en Afrique.', 'https://www.youtube.com/channel/UCMEFv8F1--9s-WcuvLXkixA', 'https://www.facebook.com/afrikeveil.org/', 'https://twitter.com/a_eveil', 'Afrik Eveil Foundation © 2017 All Copyrights Reserved.', 'Fondation Afrik Eveil © 2017 Tous Droits Réservés.', '2018-01-30 19:50:05', '2018-01-30 19:50:05');

-- --------------------------------------------------------

--
-- Table structure for table `shoppingcart`
--

CREATE TABLE `shoppingcart` (
  `identifier` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instance` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `slideshow`
--

CREATE TABLE `slideshow` (
  `id` int(10) unsigned NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(10) unsigned NOT NULL,
  `tag` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `tag`, `created_at`, `updated_at`) VALUES
(2, '2017', '2018-01-30 19:54:29', '2018-01-30 19:54:29');

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE `team` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bio` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `langue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `translated` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `testimony`
--

CREATE TABLE `testimony` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `language` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `upcomingEvents`
--

CREATE TABLE `upcomingEvents` (
  `id` int(10) unsigned NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `language` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `permission` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'superadmin',
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `admin`, `permission`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'afrik Eveil', 'afrikeveil@yahoo.fr', 1, 'superadmin', '$2y$10$U3ZwDmlbk71Qvc.JyM2YN.3hMgwsNLvu/MsbEYBqLZtlP6OP48Qwa', 'ccXNWEoSxJCPRIeZOpLhGNsbn6aLI8W9HmEtbQXMYQ6ojtkaiqCY9Ab7KNN3', '2018-01-30 19:50:05', '2018-01-30 19:50:05');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `checkouts`
--
ALTER TABLE `checkouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `community`
--
ALTER TABLE `community`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `community_slug_unique` (`slug`);

--
-- Indexes for table `demande`
--
ALTER TABLE `demande`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `introduction`
--
ALTER TABLE `introduction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `offers_slug_unique` (`slug`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_tag`
--
ALTER TABLE `post_tag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_translations`
--
ALTER TABLE `post_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `post_translations_post_id_locale_unique` (`post_id`,`locale`),
  ADD UNIQUE KEY `post_translations_slug_unique` (`slug`),
  ADD KEY `post_translations_locale_index` (`locale`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `products_slug_unique` (`slug`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchased`
--
ALTER TABLE `purchased`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shoppingcart`
--
ALTER TABLE `shoppingcart`
  ADD PRIMARY KEY (`identifier`,`instance`);

--
-- Indexes for table `slideshow`
--
ALTER TABLE `slideshow`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimony`
--
ALTER TABLE `testimony`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `upcomingEvents`
--
ALTER TABLE `upcomingEvents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `checkouts`
--
ALTER TABLE `checkouts`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=111;
--
-- AUTO_INCREMENT for table `community`
--
ALTER TABLE `community`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `demande`
--
ALTER TABLE `demande`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `introduction`
--
ALTER TABLE `introduction`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `offers`
--
ALTER TABLE `offers`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `post_tag`
--
ALTER TABLE `post_tag`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `post_translations`
--
ALTER TABLE `post_translations`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `purchased`
--
ALTER TABLE `purchased`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `slideshow`
--
ALTER TABLE `slideshow`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `team`
--
ALTER TABLE `team`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `testimony`
--
ALTER TABLE `testimony`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `upcomingEvents`
--
ALTER TABLE `upcomingEvents`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;