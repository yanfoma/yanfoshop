let CACHE_NAME    = 'my-site-cache-v1';
const urlsToCache =  [
    './',
    'https://yanfoma.com/js/vendor/modernizr-3.6.0.min.js',
    '/js/vendor/modernizr-3.6.0.min.js',
    '/js/vendor/jquery-3.3.1.min.js',
    '/js/bootstrap.min.js',
    '/js/jquery-ui.js',
    '/js/plugins.js',
    '/js/main.js',
    '/js/owl.carousel.min.js',
    '/js/owl.carousel.min.js',
    '/js/toastr.min.js',
    '/css/owl.theme.default.min.css',
    '/css/owl.carousel.min.css',
    '/css/toastr.min.css',
    '/css/style.css',
    '/css/plugins.css',
    '/css/bootstrap.min.css'
];
self.addEventListener('install', function(event) {
    // Perform install steps
    event.waitUntil(
        caches.open(CACHE_NAME)
            .then(function(cache) {
                console.log('Opened cache');
                return cache.addAll(urlsToCache);
            })
    );
});

self.addEventListener('activate', function() {
    console.log('SW activated');
});

self.addEventListener('fetch', function(event) {
    event.respondWith(
        caches.match(event.request).then(function(res) {
                if(res){
                    return res;
                } else{
                    return fetch(event.request);
                    //console.log('fetch evnt.request');
                }
            })
    );
});



// /* Import Workbox */
// importScripts('https://storage.googleapis.com/workbox-cdn/releases/3.6.1/workbox-sw.js');
//
// /* Workbox Config */
// workbox.setConfig({
//     debug: false
// });
//
// /* Cache HTML */
// workbox.routing.registerRoute(
//     /.*\.html/,
//     workbox.strategies.staleWhileRevalidate({
//         cacheName: 'cache-html',
//     })
// );
//
// /* Cache CSS */
// workbox.routing.registerRoute(
//     /.*\.css/,
//     workbox.strategies.staleWhileRevalidate({
//         cacheName: 'cache-css',
//     })
// );
//
// /* Cache JS */
// workbox.routing.registerRoute(
//     /.*\.js/,
//     workbox.strategies.staleWhileRevalidate({
//         cacheName: 'cache-js',
//     })
// );
//
// /* Cache Images */
// workbox.routing.registerRoute(
//     /.*\.(?:gif|jpeg|jpg|png|svg)/,
//     workbox.strategies.staleWhileRevalidate({
//         cacheName: 'cache-img',
//     })
// );
//
// /* Cache Fonts */
// workbox.routing.registerRoute(
//     /.*\.(?:eot|otf|ttf|woff|woff2)/,
//     workbox.strategies.staleWhileRevalidate({
//         cacheName: 'cache-font',
//     })
// );
