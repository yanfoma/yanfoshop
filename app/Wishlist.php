<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Wishlist extends Model
{
    protected $fillable = ['product_id', 'user_id','store_id'];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function product(){
        return $this->belongsTo(Product::class);
    }

    public function store(){
        return $this->belongsTo(Store::class);
    }
}
