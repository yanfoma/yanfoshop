<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tracking extends Model
{
    protected $table    = 'tracking';
    protected $fillable = ['purchasedId','tackingCode','step','date','note'];

    public function purchaseds(){
        return $this->belongsTo(\App\Purchased::class);
    }


}
