<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SlideShow extends Model
{
    //
    protected $table = 'slideshow';

    protected $fillable = ['title','image'];

    public function getImageAttribute($image){

        return asset($image);
    }

    public function store(){
        return $this->belongsTo('App\Store');
    }
}
