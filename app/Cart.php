<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Auth;

class Cart extends Model
{
    protected $table = 'cart';
    protected $fillable = ['store_id','user_id','product_id','product_name','product_slug',
    'product_qty','product_total','product_price','product_image','total'];

    public function product(){
        return $this->belongsTo('App\Product');
    }

    public function products(){
        return $this->belongsTo('App\Product');
    }

    public  function getStoreTotal($store_id)
    {
        $products = Cart::where('store_id',$store_id)->where('user_id',Auth::user()->id)->get();
        
        $total =0;
        
        foreach($products as $product){

            $total += $product->product_total;
        }
        return $total;
    }
}
