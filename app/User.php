<?php

namespace App;

use App\Notifications\VerifyEmail;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'name', 'email', 'password','admin','wa_number','profession','education','verified','token'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];


    public function verified(){
        return (($this->token === null) && $this->verified == 'yes');
    }
    public function profile(){
        return $this->hasOne('App\Profile');
    }

    public function posts(){
        return $this->hasMany('App\Post');
    }

    public function postTranslations(){
        return $this->hasMany('App\PostTranslations');
    }

    public function purchased(){

    return $this->hasMany('\App\Purchased');

    }

    public function interests() {
        return $this->belongsToMany('App\Interest');
    }

    public function has_filled_data(){
        $user = Auth::user();
        if(!is_null($user->sexe) && !is_null($user->pays) && !is_null($user->ville)  && !is_null($user->profession)  && !is_null($user->addresse) && !is_null($user->dob) && !is_null($user->education) )
            return true;
        else
            return false;
    }

    public function wishlist(){
        return $this->hasMany(Wishlist::class);
    }

    public function sendVerificationEmail(){

        $this->notify(new VerifyEmail($this));
    }
}
