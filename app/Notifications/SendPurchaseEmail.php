<?php

namespace App\Notifications;

use App\Purchased;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class SendPurchaseEmail extends Notification
{
    use Queueable;

    public $purchase;

    public function __construct(Purchased $purchase)
    {
        $this->purchase = $purchase;
    }

    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {

        $name           = $this->purchase->name;
        $purchasedId    = $this->purchase->purchasedId;
        $total          = $this->purchase->total;

        return (new MailMessage)
                    ->subject('Méthode de payment')
                    ->greeting('Bonjour !!!!')
                    ->line('Cher '.$name.' Nous avons recu votre commande no:'.$purchasedId.' Montant: '.$total.' mais elle n\'est pas confirmée. 
                    Pour confirmer la commande,  veuillez choisir une methode de paiement et bien vouloir nous envoyer un mail a info@yanfoma.tech pour continuer votre commande.')
                    ->line('Nos methodes de paiement sont les suivantes:')
                    ->line('Bitcoin')
                    ->line('Ecobank Pay')
                    ->line('Versement Bancaire Ecobank')
                    ->line('Orange Money')
                    ->line('Cash chez Kaanu')
                    ->action('En savoir plus sur les Methodes de Paiement ', route('faq'))
                    ->line('Merci bien')
                    ->line('Service Client - Yanfoma Shop');
    }

    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
