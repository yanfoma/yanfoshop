<?php

namespace App\Notifications;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class VerifyEmail extends Notification
{
    use Queueable;

    public $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->greeting('Bonjour !!!!')
                    ->line('Veuillez verifier votre compte yanfoshop en verifiant votre addresse email')
                    ->action('Verifier votre Addresse Email',route('verify', $this->user->token))
                    ->line('Bienvenue Sur YanfoSop!!!');
    }

    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
