<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Checkouts extends Model
{
    protected $table     = 'checkouts';
    protected $fillable  = ['purchased_id','purchased_name','name','image','qty','prix','total','store_id','payment'];

     public function purchased() 
     {
         return $this->belongsTo(\App\purchased::class);
     }

    public function produit($name){
         return Product::where('name',$name)->first();
    }

}
