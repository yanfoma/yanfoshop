<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $fillable = ['user_id','product_id','rating', 'rating_desc','valid'];


    public function user(){
        return $this->belongsTo(User::class);
    }
}
