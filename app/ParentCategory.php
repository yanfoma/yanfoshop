<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParentCategory extends Model
{
    protected $fillable = ['store_id','name','image'];
    public function subcategories(){
        return $this->hasMany('App\Subcategory','parent_id');
    }

    public function products() {
        return $this->hasManyThrough('App\Product'
        ,'App\Subcategory','parent_id','category_id','id');}
}
