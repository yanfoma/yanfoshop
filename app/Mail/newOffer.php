<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class newOffer extends Mailable
{
    use Queueable, SerializesModels;

    public $name,$content,$phone,$email,$services,$code,$id;

    public function __construct($name,$content,$phone,$email,$code,$id)
    {
        $this->id       = $id;
        $this->code     = $code;
        $this->name     = $name;
        $this->content  = $content;
        $this->phone    = $phone;
        $this->email    = $email;
    }

    public function build()
    {
        return $this->markdown('emails.newOffer');
    }
}
