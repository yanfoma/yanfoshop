<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ConsultatingMessage extends Mailable
{
    use Queueable, SerializesModels;

    public $name,$message,$phone,$email,$services;

    public function __construct($name,$message,$phone,$email,$services)
    {
        $this->name     = $name;
        $this->message  = $message;
        $this->phone    = $phone;
        $this->email    = $email;
        $this->services = $services;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.Consulting');
    }
}
