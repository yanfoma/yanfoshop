<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name'];

    public function products(){
        return $this->hasMany('App\Product');
    }

    public function Subcategory() {
        return $this->belongsTo('App\Subcategory');
    }

    public function Parent() {
        return $this->belongsTo('App\ParentCategory');
    }

    public function store(){
        return $this->belongsTo('App\Store');
    }

    public function postTranslations(){

        return $this->hasMany('App\PostTranslation');

    }
}
