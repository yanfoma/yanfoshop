<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;

class Product extends Model
{
    protected $table     = 'products';
    protected $fillable = ['name','slug','price','minQty','description','image'];


    public function is_liked_by_auth_user(){
        return $this->likes->where('user_id', Auth::id())->isNotEmpty();
    }
    public function is_wished_by_auth_user(){
        return $this->wishlists->where('user_id', Auth::id())->isNotEmpty();
    }

    public function store(){
        return $this->belongsTo('App\Store');
    }

    public function category(){
        return $this->belongsTo('App\Category');
    }

    public function wishlists(){

        return $this->hasMany('App\Wishlist');
    }

    public function likes(){

        return $this->hasMany('App\Like');
    }

    public function wishlist(){
        return $this->hasMany(Wishlist::class);
    }

    public function reviews(){
        return $this->hasMany(Review::class);
    }

    public function is_reviewed_by_auth_user(){
        return $this->reviews->where('user_id', Auth::id())->isNotEmpty();
    }

    public function getRating($rate){
        return Review::where('rating',$rate)->get()->count();
        //return $this->reviews->groupBy('rating');
    }

}
