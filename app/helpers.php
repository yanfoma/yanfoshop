<?php

use App\Cart;
use App\Store;
use App\Category;
/**
 * User: fabioued
 * Date: 10/29/18
 * Time: 15:09
 */

if (! function_exists('getUser')) {
    function getUser($id)
    {
         return \App\User::findOrFail($id);
    }
}

if (! function_exists('getStoreTotal')) {

  function getStoreTotal($store_id)
    {
        if(Auth::check()){
            $products = Cart::where('store_id',$store_id)->where('user_id',Auth::user()->id)->get();
            $total =0;
            foreach($products as $product){

                $total += $product->product_total;
            }
        }else{
            $total =0;

        }
        return $total;
    }
}
if (! function_exists('getCategories')) {

  function getCategories($store_id)
    {
        $categories = Category::where('store_id',$store_id)->whereHas('products')->orderBy('created_at','desc')->get();
        return $categories;
    }
}
if (! function_exists('cart')) {

  function cart($store_id)
    {
        if(Auth::check()){

            $cart          = Cart::where('user_id', Auth::user()->id)->where('store_id',$store_id)->orderBy('created_at','desc')->get();
        }else{
            $cart          = collect([]);

        }
        return $cart;
    }
}

if (! function_exists('getTotal')) {

  function getTotal()
    {
        if(Auth::check()){
            $products = Cart::where('user_id',Auth::user()->id)->get();
            $total =0;
            foreach($products as $product){

                $total += $product->product_total;
            }
        }else{
            $total =0;

        }
        return $total;
    }
}

if (! function_exists('getStoreName')) {

  function getStoreName($store_id)
    {
        return  Store::where('store_id',$store_id)->first()->name;
    }
}