<?php

namespace App\Providers;

use App\Category;
use App\Product;
use App\Settings;
use App\Store;
use App\Cart;
use Auth;
use Session;
use App\StoreCategorie;
use Jenssegers\Date\Date;
use App\SlideShow;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if($this->app->environment() === 'production'){
            $this->app['request']->server->set('HTTPS', true);
            //URL::forceScheme('https');
        }

        view()->composer("layouts/header","App\Http\ViewComposers\NotificationViewComposer");


        View::composer('errors.404', function($view)
        {
            return $view->with('settings', Settings::first());
        });

        View::composer('layouts/frontEnd/footer', function($view)
        {
            $view->with('settings', Settings::first());
        });

        View::composer('layouts/frontEnd/slideshow', function($view)
        {
            $view->with('slides', SlideShow::all());
        });

        View::composer('*', function($view)
        {

            $view->with('stores', Store::all(),'storeCategories', StoreCategorie::all());
        });

        View::composer('frontEnd/shopSingle', function($view)
        {
            $recentlyViewed          = Session::get('products.recently_viewed');
            //$recently_viewedProducts = $recentlyViewed;
            $recently_viewedProducts = Product::whereIn('id', $recentlyViewed)->orderBy('created_at','desc')->take(3)->get();
            $view->with('recently_viewedProducts',$recently_viewedProducts);
        });


        View::composer(['frontEnd/home','frontEnd/nosBoutiques'], function ($view) {
            if(Auth::check()){
                $cart          = Cart::where('user_id', Auth::user()->id)->orderBy('created_at','desc')->get();
            }else{
                $cart          = collect([]);
            }
            $view->with('cart', $cart);
        });

        View::composer(['yanfomasoko.*','mobile.*','customer.*'], function ($view) {
            if(Auth::check()){
                $cart          = Cart::where('user_id', Auth::user()->id)->orderBy('created_at','desc')->get();
            }else{
                $cart          = collect([]);
            }
            $view->with('cart', $cart);
        });
        // Set the app locale according to the URL
        //app()->setLocale(substr(request()->server('HTTP_ACCEPT_LANGUAGE'), 0, 2));


//        app()->setLocale(request()->segment(1));

        $locale=app()->getLocale();
        Date::setLocale($locale);

        Schema::defaultStringLength(191);

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
        $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
    }
    }
}
