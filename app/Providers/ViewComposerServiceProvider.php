<?php

namespace App\Providers;

use App\Category;
use App\Settings;
use App\Store;
use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
    public function boot()
    {

        View::composer('errors.404', function($view)
        {
            return $view->with('settings', Settings::first());
        });

        View::composer('layouts/frontEnd/footer', function($view)
        {
            $view->with('settings', Settings::first());
        });

        View::composer('layouts/frontEnd/footerHome', function($view)
        {
            $view->with('stores', Store::all());
        });


    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
