<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use App\Notifications\SendPurchaseEmail;
use Illuminate\Database\Eloquent\Model;

class Purchased extends Model
{
    protected $table        = 'purchased';
    protected $fillable     = ['purchasedId','name','pays','ville','email','phone','addresse','nbrProduits','total','view','payed','payedDate','user_id','recu','note','store_id','payment'];


     public function user() 
     {
         return $this->belongsTo(\App\User::class);
     }

    public function sendPurchaseEmail(){

        $this->notify(new SendPurchaseEmail($this));
    }

    public function trackings()
    {
        return $this->hasMany(Tracking::class,'purchasedId','purchasedId');
    }


    public function checkouts(){

    return $this->hasMany('\App\Checkouts');

    }


}
