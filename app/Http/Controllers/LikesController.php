<?php

namespace App\Http\Controllers;

use App\Like;
use App\Store;
use Illuminate\Http\Request;
use Auth;

class LikesController extends Controller
{
    public function like($store_slug,$id){
        $store         = Store::where('slug',$store_slug)->first();
        $store_id      = $store->id;
        Like::create([
            'product_id' => $id,
            'user_id'    => Auth::id(),
            'store_id'   => $store_id
        ]);
        return redirect()->back();
    }

    public function unlike($store_slug,$id){
        $store         = Store::where('slug',$store_slug)->first();
        $store_id      = $store->id;
        $like = Like::where('product_id', $id)->where('user_id', Auth::id())->where('store_id',$store_id)->first();
        $like->delete();
        return redirect()->back();
    }
}
