<?php

namespace App\Http\Controllers;


// use Illuminate\Pagination\Paginator ;
// use Illuminate\Pagination\LengthAwarePaginator ;
// use Illuminate\Contracts\Pagination\Presenter ;
// use Illuminate\Support\Facades\DB;
use App\Faq;
use App\Interest;
use App\Wishlist;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request ;
use App\Http\Controllers\Controller;
use JD\Cloudder\Facades\Cloudder;
use App\Profile;
use App\User;
use Auth;
use Mail;
use Session;
use EmailValidator;
Use Validator ;
Use Hash ;
Use App\Purchased;
Use App\Checkouts;

class CustomerController extends Controller
{

    public function welcome()
    {
        $interests = Interest::all();
        $user      = Auth::user();
        if($this->agent->isPhone()){
            return view('mobile.welcome',compact('user', $user,'interests',$interests));
        }else{
            return view('customer.welcome',compact('user', $user,'interests',$interests));
        }
    }

    public function adresse()
    {
        $user = Auth::user();
        if($this->agent->isPhone()){
            return view('mobile.addresse',compact('user', $user));
        }else{
            return view('customer.adresse',compact('user', $user));
        }
    }

    public function collectData()
    {
        $interests = Interest::all();
        $user      = Auth::user();
        return view('customer.collectData',compact('interests','user'));
    }

    public function password()
    {
        $user = Auth::user();
        if($this->agent->isPhone()){
            return view('mobile.password',compact('user', $user));
        }else{
            return view('customer.password',compact('user', $user));
        }
    }

    public function edit($id)
    {
        $user = User::find($id);
        if($this->agent->isPhone()){
            return view('mobile.edit')->with('user', $user);
        }else{
            return view('customer.edit')->with('user', $user);
        }
    }

    public function verify($id, Request $request){

        if(isset($request->verifyemail)){
            $user       = User::find($id);
            $validity   = EmailValidator::verify($request->verifyemail);
            if($validity->response->body['result'] == 'valid'){
                $user->verified = 'yes';
                $user->save();
                Session::flash('success', 'Votre Email est  Valide!!!');
                return back();
            }else{
                Session::flash('danger', 'Votre Email est  Invalide. Veuillez Corriger!!!');
                return back();
            }
        }
    }

    public function update($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'      => 'bail|required',
            'email'     => 'bail|required|email',
            'wa_number' => 'bail|required',
            'sexe'      => 'required',
            'dob'       => 'required',
            'interests' => 'required',
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $customer = User::findOrFail($id);
        $customer->name      = $request->name ;
        $customer->email     = $request->email ;
        $customer->wa_number = $request->full_phone;
        $customer->sexe      = $request->sexe;
        $customer->dob       = $request->dob;
        $customer->interests()->sync($request->interests);
        $customer->update() ;
        Session::flash('success', 'Vos informations ont bien été modifiées. Merci');
        return redirect()->route('customer.welcome');

    }
    public function updateData(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'sexe'          => 'required',
            'dob'           => 'required',
            'pays'          => 'bail|required',
            'interests'     => 'required',
            'education'     => 'bail|required',
            'profession'    => 'bail|required',
            'addresse'      => 'bail|required',
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $customer               = Auth::user();
        $customer->sexe         = $request->sexe;
        $customer->dob          = $request->dob;
        $customer->pays         = $request->pays;
        $customer->education    = $request->education;
        $customer->profession   = $request->profession;
        $customer->addresse     = $request->addresse;
        $customer->interests()->sync($request->interests);
        $customer->update() ;
        $customer->save();
        Session::flash('success', 'Vos informations ont bien été modifiées. Merci');
//        return redirect()->route('customer.welcome');
        return redirect()->intended();

    }
    public function updateAddresse($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'pays'      => 'bail|required',
            'ville'     => 'bail|required',
            'addresse'  => 'bail|required',
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $customer = User::findOrFail($id);
        $customer->pays      = $request->pays ;
        $customer->ville     = $request->ville ;
        $customer->addresse  = $request->addresse;
        $customer->update() ;
        Session::flash('success', 'Votre addresse a bien été modifiée. Merci');
        return redirect()->route('customer.adresse');
    }

    public function updatePassword($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password'      => 'bail|confirmed|min:8|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X]).*$/',
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $customer = User::findOrFail($id);
        $customer->password      = bcrypt($request->password);
        $customer->update() ;
        Session::flash('success', 'Votre Mot de Passe a bien été changé. Merci');
        return redirect()->route('customer.password');
    }

    public function faq()
    {
        $faqs = Faq::orderBy('created_at','asc')->get();
        return view('customer.faq',compact('faqs'))->with('user', Auth::user());
    }

    public function aide()
    {
        if($this->agent->isPhone()){
            return view('mobile.aide')->with('user', Auth::user());
        }else{
            return view('customer.aide')->with('user', Auth::user());
        }

    }

    public function wishlist(){
        $user      = Auth::user();
        $wishlists = Wishlist::where("user_id", "=", Auth::user()->id)->orderby('id', 'desc')->paginate(10);
        if($this->agent->isPhone()){
            return view('mobile.wishlist',compact('wishlists', $wishlists,'user',$user));
        }else{
            return view('customer.wishlist',compact('wishlists', $wishlists,'user',$user));
        }
    }

    public function commandeView()
    {
        $mesCommandes   = Purchased::where('user_id', '=', Auth::user()->id)->orderBy('id', 'DESC')->paginate(10);
        $commandes      = new Collection();
        foreach ($mesCommandes as $commande){
            $commandes->push($commande->checkouts);
        }
        if($this->agent->isPhone()){
            return view('mobile.commande',compact('mesCommandes'))->with('user', Auth::user());
        }else{
            return view('customer.commande',compact('mesCommandes'))->with('user', Auth::user());
        }
    }


    public function commandeDetail($id)
    {
        $detail= Checkouts::join('purchased', 'purchased.id', '=','checkouts.purchased_id')
                ->select('checkouts.name as coName','checkouts.qty','checkouts.prix','checkouts.total as coTotal','purchased.created_at as pcCreated_at','checkouts.image as image_url')
                ->where('purchased.id', '=', $id)
                 ->paginate(5);
        if($this->agent->isPhone()){
            return view('mobile.detail',compact('detail','id'))->with('user', Auth::user());
        }else{
            return view('customer.detail',compact('detail','id'))->with('user', Auth::user());
        }
    }

    public function suivreCommande($purchasedId)
    {
        $purchased = Purchased::where('purchasedId',$purchasedId)->where('user_id',Auth::user()->id)->first();

        if($this->agent->isPhone()){
            return view('mobile.suivi',compact('purchased'))->with('user', Auth::user());
        }else{
            return view('customer.suivi',compact('purchased'))->with('user', Auth::user());
        }

    }

    public function commandeConfirmation(){
        $mesCommandes   = Purchased::where('user_id', '=', Auth::user()->id)->orderBy('id', 'DESC')->paginate(10);
        $commandes      = new Collection();
        foreach ($mesCommandes as $commande){
            $commandes->push($commande->checkouts);
        }
        return view('customer.commandeConfirmation',compact('mesCommandes'))->with('user', Auth::user());
    }

    public function confirmStore(Request $request){
        $request->validate([
            'commandeCode'    => 'required',
            'dateVersement'   => 'required',
            'image'           => "required",
        ]);

        $purchased = Purchased::where('purchasedId',$request->commandeCode)->where('user_id',$request->id)->first();
        $purchased->payed       = 1;
        $purchased->payedDate   = $request->dateVersement;
        $purchased->recu        = $this->uploadFile($request);
        $purchased->note        = $request->note;
        $purchased->save();

        //send email to info@yanfoma.tech
        $name  = $purchased->name;
        $to    = "info@yanfoma.tech";
        $email = $purchased->email;
        $recu  = $purchased->recu;
        $data = [
            'to'               =>   $to,
            'name'             =>   $name,
            'purchasedId'      =>   $purchased->purchasedId,
            'total'            =>   $purchased->total,
            'payedDate'        =>   $purchased->payedDate,
            'email'            =>   $email,
            'recu'             =>   $recu
        ];

        Mail::send('emails.purchasedConfirmTransaction',$data,function($mail) use ($to,$email,$recu,$name) {
            $mail->from($email,$name);
            $mail->to($to, "yanfoShop");
            $mail->subject('Confirmation De Versement Ecobank');
            $mail->attach($recu);
        },true);

        Mail::send('emails.purchasedConfirmer',$data,function($mail) use ($to,$email,$recu,$name) {
            $mail->from($to, "yanfoShop");
            $mail->to($email, $name);
            $mail->subject('Confirmation De Versement Ecobank');
            $mail->attach($recu);
        },true);

        Session::flash('success', 'Succès!!! Nous confirmons votre versement et procéderons à votre commande. Merci');
        return redirect()->route('customer.commandeConfirmation');
    }

    private function uploadFile(Request $request) {

        $image                = $request->file('image');
        $image_name           = $image->getRealPath();
        $publicId             = $image->getClientOriginalName();
        Cloudder::upload($image_name, $publicId);
        list($width, $height) = getimagesize($image_name);
        $image_url            = Cloudder::secureShow(Cloudder::getPublicId(), ["width" => $width, "height" => $height]);
        return $image_url;
    }
}
