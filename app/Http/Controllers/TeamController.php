<?php

namespace App\Http\Controllers;

use App\Team;
use Illuminate\Http\Request;
use Session;
use File;

class TeamController extends Controller
{
    //
    public function index()
    {
        $translated     = ['translated'=>'1'];
        $noTranslated   = ['translated'=>'0'];
        $translated_en  = ['langue'=>'english','translated'=>'1'];
        $translated_fr  = ['langue'=>'french','translated'=>'1'];
        return view('admin.team.index')->with('members', Team::all())
                                       ->with('members_translated',Team::where($translated)->get())
                                       ->with('members_noTranslated',Team::where($noTranslated)->get())
                                       ->with('members_english',Team::where($translated_en)->get())
                                       ->with('members_french', Team::where($translated_fr)->get());
    }

    public function create()
    {
        return view('admin.team.create');
    }

    public function edit($id)
    {
        $member = Team::find($id);
        return view('admin.team.edit')->with('member', $member);
    }

    public function translate($id)
    {
        $member = Team::find($id);
        return view('admin.team.translate')->with('member', $member);
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name'          => 'required',
            'image'         => 'required|image',
            'position'      => 'required',
            'bio'           => 'required',
            'langue'        => 'required',
        ]);

        $image  = $request->image;

        $image_new_name = time().$image->getClientOriginalName();

        $image->move('uploads/team/', $image_new_name);

        $translated = 0;

        $member = Team::create([

            'name'        => $request->name,
            'image'       => 'uploads/team/' .$image_new_name,
            'position'    => $request->position,
            'bio'         => $request->bio,
            'langue'      => $request->langue,
            'translated'  => $translated,
        ]);

        Session::flash('success',trans('app.memberCreated'));
        //dd($request->all());

        return redirect()->route('team.index');
    }

    public function storeTranslate(Request $request, $id)
    {
        $this->validate($request,[
            'position'      => 'required',
            'bio'           => 'required',
        ]);

        $memberTeam = Team::find($id);

        $image      = $memberTeam->image;
        $name       = $memberTeam->name;
        $langue     = $memberTeam->langue;
        $translated = 1;

        //dd($langue);

        if($langue =='english'){

            $langue = 'french';
        }else{
            $langue = 'english';
        }

        //dd($request->all(),"name is: j $name","langue is $langue and image is $image",$translated);

        $member = Team::create([

            'name'        => $name,
            'image'       => $image,
            'position'    => $request->position,
            'bio'         => $request->bio,
            'langue'      => $langue,
            'translated'  => $translated,
        ]);

        $memberTeam->translated = $translated;
        $memberTeam->save();

        Session::flash('success',trans('app.public'));
        //dd($request->all());

        return redirect()->route('team.index');
    }

    public function update(Request $request, $id)
    {
        //

        $this->validate($request, [
            'name'          => 'required',
            'image'         => 'image',
            'position'      => 'required',
            'bio'           => 'required',
            'langue'        => 'required',
        ]);

        $member = Team::find($id);

        if($request->hasFile('image')){

            $path = parse_url($member->image);
            File::delete(public_path($path['path']));

            $image          = $request->image;

            $image_new_name = time().$image->getClientOriginalName();

            $image->move('uploads/team/', $image_new_name);

            $member->image = 'uploads/team/'.$image_new_name;

            $name           = $member->name;

            if($request->langue =='english'){
                $memberLangue = 'french';
            }else{
                $memberLangue = 'english';
            }

            //$where = ['name'=>$name,'langue'=>$memberLangue];

            $updateMembers = Team::all()->where('name',$name)
                                        ->where('langue',$memberLangue)->first();

            //dd("member to update is$updateMembers and member now is $member");
            $updateMembers->image = 'uploads/team/'.$image_new_name;

            $updateMembers->save();

        }


        $member->name           = $request->name;
        $member->position       = $request->position;
        $member->bio            = $request->bio;
        $member->langue         = $request->langue;

        $member->save();

        Session::flash('success',trans('app.memberUpdated'));

        return redirect()->route('team.index');
    }


    public function delete($id){

        $member = Team::find($id);

        $name   = $member->name;

        $deleteMembers = Team::all()->where('name',$name);

        //dd($deleteMembers);

        foreach ($deleteMembers as $deleteMember){
            //echo($deleteMember);
            $path = parse_url($deleteMember->image);
            File::delete(public_path($path['path']));

            $deleteMember->Delete();
        }


        Session::flash('success', trans('app.memberDeleted'));

        return redirect()->back();

    }
}
