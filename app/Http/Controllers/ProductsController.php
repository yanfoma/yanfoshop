<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Session;
use File;

class ProductsController extends Controller
{
    public function index()
    {
        $products = Product::OrderBy('id','desc')->paginate(10);
        return view('admin.products.index',compact('products'));
    }

    public function create()
    {
        return view('admin.products.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'              => "required",
            'price'             => "required",
            'minQty'            => "required",
            'description'       => "required",
            'image'             => "required",
        ]);

        $image          = $request->image;
        $image_new_name = time().$image->getClientOriginalName();

        $image->move('uploads/shop/products', $image_new_name);

        Product::create([
            'name'          => $request->name,
            'slug'          => str_slug($request->name),
            'price'         => $request->price,
            'minQty'        => $request->minQty,
            'description'   => $request->description,
            'image'         => 'uploads/shop/products/' .$image_new_name,

        ]);

        Session::flash('success','Produit Ajouté avec Succès!');

        return redirect()->route('products.index');
    }

    public function edit($id)
    {
        $product = Product::findOrFail($id);
        return view('admin.products.edit',compact('product'));
    }


    public function update(Request $request, $id)
    {
        $request->validate([
            'name'        => "required",
            'price'       => "required",
            'minQty'      => "required",
            'description' => "required",
        ]);

        $product = Product::find($id);
        $image   = $product->image;

        if($request->hasFile('image')){

            $path = parse_url($product->image);
            FIle::delete(public_path($path['path']));

            $image          = $request->image;

            $image_new_name = time().$image->getClientOriginalName();

            $image->move('uploads/shop/products/', $image_new_name);

            $product->image = 'uploads/shop/products/'.$image_new_name;
        }

        $product->name         = $request->name;
        $product->slug         = str_slug($request->name);
        $product->price        = $request->price;
        $product->minQty       = $request->minQty;
        $product->image        = $image;
        $product->description  = $request->description;

        $product->save();

        Session::flash('success', 'Produit Modifié avec Succès!');

        return redirect()->route('products.index');
    }

    public function delete($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();
        Session::flash('success','Produit Supprimé avec Succès!');
        return redirect()->route('products.index');
    }
}
