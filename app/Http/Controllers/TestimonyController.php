<?php

namespace App\Http\Controllers;

use App\Category;
use App\Store;
use App\Testimony;
use Illuminate\Http\Request;
use Session;

class TestimonyController extends Controller
{
    public function index($store_slug)
    {
        $store      = Store::where('slug',$store_slug)->first();
        $store_id   = $store->id;
        $categories = getCategories($store_id);
        $cart       = cart($store_id);
        if($this->agent->isPhone()){
            return view('mobile.testimonyIndex',compact('store','categories','cart'));
        }else{
            return view('frontEnd.testimonyIndex',compact('store','categories','cart'));
        }
    }

    public function viewAll($store_slug)
    {
        $store          = Store::where('slug',$store_slug)->first();
        $store_id       = $store->id;
        $categories     = getCategories($store_id);
        $cart           = cart($store_id);
        $testimonies    = Testimony::where('store_id',$store_id)->orderBy('created_at','desc')->get();
        if($this->agent->isPhone()){
            return view('mobile.testimonyViewAll',compact('testimonies','store','categories','cart'));
        }else{
            return view('frontEnd.testimonyViewAll',compact('testimonies','store','categories','cart'));
        }
    }

    public function upload($store_slug,Request $request)
    {
        $this->validate($request,[
            'name'      => 'required',
            'phone'     => 'required',
            'country'   => 'required',
            'email'     => 'required|email',
            'message'   => 'required'
        ]);

        $store      = Store::where('slug',$store_slug)->first();
        $store_id   = $store->id;
        $categories = getCategories($store_id);
        $cart       = cart($store_id);
        Testimony::create([
            'name'      =>  $request->name,
            'phone'     =>  $request->phone,
            'country'   =>  $request->country,
            'email'     =>  $request->email,
            'message'   =>  $request->message,
            'store_id'  =>  $store_id,
        ]);
        Session::flash('success',trans('Merci de Votre Avis!!!'));
        return redirect()->route('testimonyViewAll',$store_slug )->with('store',$store,'categories',$categories,'cart',$cart);
    }
}

