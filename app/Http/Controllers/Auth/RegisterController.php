<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Request;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Agent\Agent;

class RegisterController extends Controller
{

    use RegistersUsers;

    protected function redirectTo(){
    
     return redirect()->route('login');
  }

    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    public function showRegistration(){
        $agent = new Agent();
        if($agent->isPhone()){
            return view('mobile.register');
        }else{
            return view('auth.register');
        }
    }


    public function register(Request $request)
    {
        //dd($request->all());
        $this->validator($request->all())->validate();
        event(new Registered($user = $this->create($request->all())));
        $id = $request->get('id');
        Auth::login($user);
        return redirect()->route('customer.welcome');

    }

    protected function create(array $data)
    {
        $user = User::create([
            'name'      => $data['name'],
            'email'     => $data['email'],
            'wa_number' => $data['wa_number'],
            'password'  => bcrypt($data['password']),
            'admin'     => 0,
            'verified'  => 'no',
            'token'     => str_random(25),
        ]);

       $user->sendVerificationEmail();

        return $user;
    }
}
