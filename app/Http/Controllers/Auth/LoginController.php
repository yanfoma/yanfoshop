<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\RedirectsUsers ;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Jenssegers\Agent\Agent;

class LoginController extends Controller
{

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

    protected function authenticated(Request $request, $user)
    {
        return redirect()->route('welcome');
        //dd('hi');
    }

//     protected function authenticated(Request $request)
//     {
//         $id = $request->get('id');
//
//         $product = Product::findOrFail($id);
//         return redirect()->route('shop.single', ['slug' => $product->slug]);
//         //return redirect()->route('shop');
//     }

    public function username()
    {
        if(!is_null(Input::get('email'))) {
            return 'email';
        };
        return 'wa_number';
    }

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function logout(Request $request)
    {
        Auth::logout();
        return redirect()->back();
    }


    public function showLoginForm(){
        $agent = new Agent();
        if($agent->isPhone()){
            return view('mobile.login');
        }else{
            return view('auth.login');
        }
    }

    public function mobileLogin(){
        return view('mobile.login');
    }
    public function mobileRegister(){
        return view('mobile.register');
    }

}
