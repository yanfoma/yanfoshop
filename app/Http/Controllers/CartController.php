<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Checkouts;
use App\Purchased;
use Auth;
use Mail;
use Session;
use App\Product;
use App\Store;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function cart(){

        $cart       = Cart::where('user_id', Auth::user()->id)->orderBy('created_at','desc')->groupBy('store_id')->get();

        if($this->agent->isPhone()){
            return view('mobile.yanfomasokoCart',compact('store','categories','cart'));
        }else{
            return view('yanfomasoko.cart',compact('store','categories','cart'));
        }
    }

    public function storeCart($store_slug){
        $store      = Store::where('slug',$store_slug)->first();
        $store_id   = $store->id;
        $categories = getCategories($store_id);
        $cart       = cart($store_id);
        if($this->agent->isPhone()){
            return view('mobile.cart',compact('store','categories','cart'));
        }else{
            return view('frontEnd.cart',compact('store','categories','cart'));
        }

    }

    public function storeAddToCart($store_slug){
        $store          = Store::where('slug',$store_slug)->first();
        $store_id       = $store->id;
        $product        = Product::find(request()->product_id);
        $product_total  = request()->quantity * $product->price;
        $total          = getStoreTotal($store_id);
        $cart           = Cart::where('product_id',$product->id)->where('user_id', Auth::user()->id)->where('store_id',$store_id)->first();
        if($cart){
            $cart->product_qty   += request()->quantity;
            $cart->product_total += (request()->quantity * $product->price);
            $cart->save();
        }else {
            $cartItem = Cart::create([
                'store_id'      => $store_id,
                'user_id'       => Auth::user()->id,
                'product_id'    => $product->id,
                'product_qty'   => request()->quantity,
                'product_price' => $product->price,
                'product_total' => $product_total,
                'total' => $total,
            ]);
        }
        Session::flash('success', 'Produit Ajouté dans Votre Panier !');
        return redirect()->back();
    }

    public function storeAddDirect($store_slug,$id){
        $store          = Store::where('slug',$store_slug)->first();
        $store_id       = $store->id;
        $product        = Product::find($id);
        $product_total  = $product->minQty * $product->price;
        $total          = getStoreTotal($store_id);
        $cart           = Cart::where('product_id',$id)->where('user_id', Auth::user()->id)->where('store_id',$store_id)->first();
        if($cart){
            $cart->product_qty   += $product->minQty;
            $cart->product_total += $product->price;
            $cart->save();
        }else{
            $cartItem = cart::create([
                'store_id'      => $store_id,
                'user_id'       => Auth::user()->id,
                'product_id'    => $product->id,
                'product_qty'   => 1,
                'product_price' => $product->price,
                'product_total' => $product_total,
                'total'         => $total,
            ]);
        }
        Session::flash('success', 'Produit Ajouté dans Votre Panier !');
        return redirect()->back();
    }

    public function storeCheckout($store_slug)
    {
        $store          = Store::where('slug',$store_slug)->first();
        $store_id       = $store->id;
        $categories     = getCategories($store_id);
        $cart           = cart($store_id);
        $user           = Auth::user();
        if($cart->count() == 0)
        {
            Session::flash('info','Votre panier est vide. Veuillez ajouter des produits!!!');
            return redirect()->back();
        }
        if($this->agent->isPhone()){
            return view('mobile.checkout',compact('store','categories','cart','user'));
        }else{
            return view('frontEnd.checkout',compact('store','categories','cart','user'));
        }

    }


    public function checkout()
    {
        $cart       = Cart::where('user_id', Auth::user()->id)->orderBy('created_at','desc')->get();
        $user           = Auth::user();
        if($cart->count() == 0)
        {
            Session::flash('info','Votre panier est vide. Veuillez ajouter des produits!!!');
            return redirect()->back();
        }
        if($this->agent->isPhone()){
            return view('mobile.yanfomaSokoCheckout',compact('user'));
        }else{
            return view('yanfomasoko.checkout',compact('user'));
        }
    }


    public function storeOrder(Request $request)
    {
        $store          = Store::where('store_id',$request->store_id)->first();
        $store_id       = $store->id;
        $cart           = cart($store_id);
        if($cart->count() == 0){
            Session::flash('info','Votre panier est vide. Veuillez ajouter des produits!!!');
            return redirect()->route('cart.index');
        }
        $this->validate($request,[
            'name'      => 'required',
            'country'   => 'required',
            'city'      => 'required',
            'email'     => 'required|email',
            'phone'     => 'required',
            'payment'   => 'required',
            'address'   => 'required'
        ]);

        $purchasedId    = mt_rand(10000000, 99999999);
        $name           = $request->name;
        $country        = $request->country;
        $city           = $request->city;
        $email          = $request->email;
        $phone          = $request->phone;
        $address        = $request->address;
        $payment        = $request->payment;
        $to             = 'info@yanfoma.tech';
        //$to             = 'fabioued@yahoo.fr';
        $to2            = $store->email;

        $purchaser      = Purchased::create([
            'purchasedId'   => $purchasedId,
            'name'          => $name,
            'pays'          => $country,
            'ville'         => $city,
            'email'         => $email,
            'phone'         => $phone,
            'addresse'      => $address,
            'payment'       => $payment,
            'nbrProduits'   => $cart->count(),
            'total'         => getStoreTotal($store_id),
            'view'          => 'No',
            'user_id'       => Auth::user()->id,
            'store_id'      => $store_id,
        ]);
        foreach($cart as $product){

            Checkouts::create([
                'purchased_id'   =>$purchaser->id,
                'purchased_name' =>$purchaser->name,
                'name'           =>$product->product->name,
                'image'          =>$product->product->image_url,
                'qty'            =>$product->product_qty,
                'prix'           =>$product->product_price,
                'total'          =>$product->product_total,
                'payment'        => $payment,
                'store_id'       => $store_id
            ]);
        }

        $data1 = [
            'to'               =>$to,
            'name'             =>$name,
            'country'          =>$country,
            'city'             =>$city,
            'email'            =>$email,
            'phone'            =>$phone,
            'address'          =>$address,
            'payment'          =>$payment,
            'purchased_name'   =>$purchaser->name,
            'purchasedId'      =>$purchasedId,
            'cart'             =>$cart
        ];

        $data2 = [
            'to'        =>$to2,
            'name'      =>$name,
            'country'   =>$country,
            'city'      =>$city,
            'email'     =>$email,
            'phone'     =>$phone,
            'address'   =>$address,
            'payment'   =>$payment,
        ];

        $name           = $purchaser->name;
        $purchasedId    = $purchaser->purchasedId;
        $total          = $purchaser->total;

        Mail::send('emails.purchased',$data1,function($mail) use ($to,$email) {
            $mail->from($email);
            $mail->to($to);
            $mail->subject('Nouvelle Commande');
        },true);

//        Mail::send('emails.purchased',$data2,function($mail) use ($to2,$email) {
//            $mail->from($email);
//            $mail->to($to2);
//            $mail->subject('Nouvelle Commande');
//        },true);

        $data = [
            'purchasedId'  =>$purchasedId,
            'name'         =>$name,
            'total'        =>$total,
            'payment'      =>$payment,
        ];

        Mail::send('emails.thankYouPurchased',$data,function($mail) use ($to2,$email, $name) {
            $mail->from($to2);
            $mail->to($email,$name);
            $mail->subject('Votre Commande');
            $mail->replyTo($to2);
        },true);

        foreach($cart as $product){
            $product->delete();
        }

        Session::flash('success',trans('app.thanksPurchased'));

        return redirect()->route('thanks',$store->slug);
    }

    public function order(Request $request)
    {
        $cart       = Cart::where('user_id', Auth::user()->id)->orderBy('created_at','desc')->get();
        if($cart->count() == 0){
            Session::flash('info','Votre panier est vide. Veuillez ajouter des produits!!!');
            return redirect()->route('yanfomasoko.cart.index');
        }
        $this->validate($request,[
            'name'      => 'required',
            'country'   => 'required',
            'city'      => 'required',
            'email'     => 'required|email',
            'payment'   => 'required',
            'phone'     => 'required',
            'address'   => 'required'
        ]);

        $purchasedId    = mt_rand(10000000, 99999999);
        $name           = $request->name;
        $country        = $request->country;
        $city           = $request->city;
        $email          = $request->email;
        $phone          = $request->phone;
        $address        = $request->address;
        $payment        = $request->payment;
        $to             = 'info@yanfoma.tech';
        //$to             = 'fabioued@yahoo.fr';

        $purchaser          = Purchased::create([
            'purchasedId'   => $purchasedId,
            'name'          => $name,
            'pays'          => $country,
            'ville'         => $city,
            'email'         => $email,
            'phone'         => $phone,
            'addresse'      => $address,
            'payment'       => $payment,
            'nbrProduits'   => $cart->count(),
            'total'         => getTotal(),
            'view'          => 'No',
            'user_id'       => Auth::user()->id,
            'store_id'      => 1,
        ]);
        foreach($cart as $product){

            Checkouts::create([
                'purchased_id'   =>$purchaser->id,
                'purchased_name' =>$purchaser->name,
                'name'           =>$product->product->name,
                'image'          =>$product->product->image_url,
                'qty'            =>$product->product_qty,
                'prix'           =>$product->product_price,
                'total'          =>$product->product_total,
                'payment'        =>$payment,
                'store_id'       =>$product->store_id
            ]);
        }

        $data1 = [
            'to'               =>$to,
            'name'             =>$name,
            'country'          =>$country,
            'city'             =>$city,
            'email'            =>$email,
            'phone'            =>$phone,
            'address'          =>$address,
            'payment'          =>$payment,
            'purchased_name'   =>$purchaser->name,
            'purchasedId'      =>$purchasedId,
            'cart'             =>$cart
        ];

//        $data2 = [
//            'to'        =>$to2,
//            'name'      =>$name,
//            'country'   =>$country,
//            'city'      =>$city,
//            'email'     =>$email,
//            'phone'     =>$phone,
//            'address'   =>$address,
//        ];

        $name           = $purchaser->name;
        $purchasedId    = $purchaser->purchasedId;
        $total          = $purchaser->total;

        Mail::send('emails.purchased',$data1,function($mail) use ($to,$email) {
            $mail->from($email);
            $mail->to($to);
            $mail->subject('Nouvelle Commande');
        },true);

//        Mail::send('emails.purchased',$data2,function($mail) use ($to2,$email) {
//            $mail->from($email);
//            $mail->to($to2);
//            $mail->subject('Nouvelle Commande');
//        },true);

        $data = [
            'purchasedId'  =>$purchasedId,
            'name'         =>$name,
            'total'        =>$total,
            'payment'      =>$payment,
        ];

        Mail::send('emails.thankYouPurchased',$data,function($mail) use ($to,$email, $name) {
            $mail->from($to);
            $mail->to($email,$name);
            $mail->subject('Votre Commande');
            $mail->replyTo($to);
        },true);

        foreach($cart as $product){
            $product->delete();
        }

        Session::flash('success',trans('app.thanksPurchased'));

        return redirect()->route('yanfomasoko.thanks');
    }

    public function storeThanks($store_slug){
        $store         = Store::where('slug',$store_slug)->first();
        $store_id      = $store->id;
        $categories    = getCategories($store_id);
        $cart          = cart($store_id);
        if($this->agent->isPhone()){
            return view('mobile.thanks',compact('store','categories','cart'));
        }else{
            return view('frontEnd.thanks',compact('store','categories','cart'));
        }

    }

    public function thanks(){

        if($this->agent->isPhone()){
            return view('mobile.yanfomasokoThanks');
        }else{
            return view('yanfomasoko.thanks');
        }
    }

    public function storeCartDelete($store_slug,$id)
    {
        $store          = Store::where('slug',$store_slug)->first();
        $store_id       = $store->id;
        $cart           = Cart::where('product_id',$id)->where('store_id',$store_id)->where('user_id', Auth::user()->id)->first();
        $cart->delete();
        Session::flash('success', 'Produit Supprimé de Votre Panier !');
        return redirect()->back();
    }

    public function delete($id)
    {
        $product = Cart::where('user_id',Auth::user()->id)->where('product_id',$id)->first();
        $product->delete();
        Session::flash('success', 'Produit Supprimé de Votre Panier !');
        return redirect()->back();
    }

//    public function addToCart(){
//        $product = Product::find(request()->product_id);
//        $cartItem = Cart::add([
//            'id'    => $product->id,
//            'name'  => $product->name,
//            'qty'   => request()->quantity,
//            'price' => $product->price
//        ]);
//        Cart::associate($cartItem->rowId,'App\Product');
//        Session::flash('success', 'Ajouté dans Votre Panier !');
//        return redirect()->route('shop.single', $product->slug);
//    }
//
//    public function addDirect($id){
//        $product = Product::find($id);
//        $cartItem = Cart::add([
//            'id'    => $product->id,
//            'name'  => $product->name,
//            'qty'   => $product->minQty,
//            'price' => $product->price
//        ]);
//        Cart::associate($cartItem->rowId,'App\Product');
//        Session::flash('success', 'Ajouté dans Votre Panier !');
//        return redirect()->back();
//    }

}
