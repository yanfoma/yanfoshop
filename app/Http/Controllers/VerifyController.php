<?php

namespace App\Http\Controllers;

use App\User;
use Auth;
use Session;
use Illuminate\Http\Request;

class VerifyController extends Controller
{
    public function verify($token){

        $user = User::where('token',$token)->firstOrFail();

        $user->update(['verified' => 'yes', 'token' => null]);

        Auth::login($user);

        Session::flash('success', "Votre compte à été vérifié avec succès !!!");

        return redirect()->route('customer.welcome');

    }
}
