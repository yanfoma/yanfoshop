<?php

namespace App\Http\Controllers;

use App\Review;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    public function storeReview(Request $request)
    {
        request()->validate([
            'product_id'    => 'required',
            'user_id'       => 'required',
            'rating_desc'   => 'required',
            'rating'        => 'required',
        ]);
        //$data = $request->all();
        $check = Review::create([
           "product_id"     => $request->product_id,
           "user_id"        => $request->user_id,
           "rating_desc"    => $request->rating_desc,
           "rating"         => $request->rating,
           "valid"          => 0
        ]);
        $arr = array('msg' => 'Une Erreur est servenue Veuillez réessayer plus tard !!!', 'status' => false);
        if($check){
            $arr = array('msg' => 'Votre avis a bien été envoyé!!!', 'status' => true);
        }
        return Response()->json($arr);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function show(Review $review)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function edit(Review $review)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Review $review)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function destroy(Review $review)
    {
        //
    }
}
