<?php

namespace App\Http\Controllers;

use App\Category;
use App\Faq;
use App\Cart;
use App\ParentCategory;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Response;
use App\SendPurchaseEmail;
use App\Checkouts;
use App\Purchased;
use Jenssegers\Agent\Agent;
use App\Product;
use App\Settings;
use App\Store;
use Session;
use Mail;
use Auth;
use App\SlideShow;
use App\Mail\contactMessage;
use Illuminate\Http\Request;

use GeoIp2\Database\Reader;

use Newsletter;

class PagesController extends Controller
{

    public function home(Request $request){
        if(!Session::get('products.recently_viewed')){
            session()->put('products.recently_viewed', []);
        }
        $agent = new Agent();
        $slides     = SlideShow::orderBy('order','asc')->get()->take(4);
        $stores     = Store::all();
        $bonsDeals  = Product::where('price', '<=', 2000)->orderBy('price', 'asc')->take(12)->get();
        $promos     = Product::where('onSale', 1)->orderBy('price', 'asc')->take(12)->get();
        $nouveautes = Product::latest()->take(4)->get();
        $bestSelers = Checkouts::groupBy('name')->select('name', DB::raw('count(*) as total'))->orderBy('total','desc')->get();
        $categories = Category::has('products')->orderBy('created_at','desc')->take(7)->get();
        $parentCategories   = ParentCategory::orderBy('created_at','desc')->take(8)->get();
        $senseurs   = Category::with('products')->where('name','servomoteurs')->first();
        $vins       = Category::with('products')->where('name','Vin de Bissap')->first();
        $firstVin   = Category::where('store_id', 9)->with('products')->where('name', 'Vin de Bissap')->first()->products->last();
        $cloud      = Category::where('store_id', 1)->with('products')->where('name', 'Cloud')->first();
        $firstVps   = Category::where('store_id', 1)->with('products')->where('name', 'Cloud')->first()->products->last();
        $products   = Product::orderBy('created_at', 'desc')->paginate(8);

        if ($request->ajax()) {
            $view = view('layouts.frontEnd.data',compact('products'))->render();
            return response()->json(['html'=>$view]);
        }
        if($agent->isPhone()){
            return view('mobile.home',compact('products','nouveautes','bonsDeals','meilleurs','slides','categories','stores','cloud','firstVps','senseurs','vins','firstVin','promos','bestSelers','parentCategories','products'));
        }else{
            return view('frontEnd.home',compact('products','nouveautes','bonsDeals','meilleurs','slides','categories','stores','cloud','firstVps','senseurs','vins','firstVin','promos','bestSelers','parentCategories','products'));
        }
    }

    public function nosBoutiques(){
        $stores     = Store::all();
        $agent = new Agent();
        if($agent->isPhone()){
            return view('mobile.nosBoutiques',compact('stores'));
        }else{
            return view('frontEnd.nosBoutiques',compact('stores'));
        }
    }
    public function homeFilter(Request $request){
        switch ($request->getRequestUri()) {
            case "/nos-bons-deals":
                $products = Product::where('price', '<=', 2000)->orderBy('price', 'asc')->get();
                break;

            case "/nos-promos":
                $products = Product::where('onSale', 1)->orderBy('price', 'asc')->get();
                break;

            case "/nos-nouveautes":
                $products = Product::latest()->get();
                break;
            default:
                $products = Product::orderBy('created_at', 'desc')->get();
                break;
        }
        $agent = new Agent();
        $slides     = SlideShow::orderBy('order','asc')->get()->take(4);
        $stores     = Store::all();
        $categories = Category::has('products')->orderBy('created_at','desc')->take(7)->get();
        $parentCategories   = ParentCategory::orderBy('created_at','desc')->take(8)->get();
        if($agent->isPhone()){
            return $this->redirect()->back();
        }else{
            return view('frontEnd.homeFilters',compact('products','slides','categories','stores','parentCategories'));
        }
    }

    public function shop($store_slug)
    {
        //dd(Session::all());
        $store = Store::where('slug', $store_slug)->first();
       if(is_null($store)){
           return Response::view('errors.404', array(), 404);
       }
        $store_id = $store->id;
        if (request('query')) {

            switch (request('query')) {
                case "newest":
                    $products = Product::where('store_id', $store_id)->orderBy('created_at', 'desc')->get();
                    break;

                case "nameDesc":
                    $products = Product::where('store_id', $store_id)->orderBy('name', 'desc')->get();
                    break;

                case "nameAsc":
                    $products = Product::where('store_id', $store_id)->orderBy('name', 'asc')->get();
                    break;

                case "priceAsc":
                    $products = Product::where('store_id', $store_id)->orderBy('price', 'asc')->get();
                    break;

                case "priceDesc":
                    $products = Product::where('store_id', $store_id)->orderBy('price', 'desc')->get();
                    break;

                default:
                    $products = Product::where('store_id', $store_id)->where('name', 'like', '%' . request('query') . '%')->orderBy('name')->get();
                    break;

            }
        } else {
            $products = Product::where('store_id', $store_id)->OrderBy('id', 'desc')->get();
        }
        $nouveautes     = Product::where('store_id', $store_id)->orderBy('created_at', 'desc')->take(10)->get();
        $bonsDeals      = Product::where('store_id', $store_id)->orderBy('price', 'asc')->take(10)->get();
        $meilleurs      = Product::where('store_id', $store_id)->orderBy('price', 'desc')->take(10)->get();
        if ($store_id == 1){
            $cloud = Category::where('store_id', $store_id)->with('products')->where('name', 'Cloud')->first();
            $firstVps = Category::where('store_id', $store_id)->with('products')->where('name', 'Cloud')->first()->products->last();
        }else{

            $cloud = $firstVps = 0;
        }
        $categoriesAll = Category::where('store_id',$store_id)->get();
        $categories    = getCategories($store_id);
        $slides        = SlideShow::where('store_id',$store_id)->orderBy('order','desc')->get()->take(4);
        $cart          = cart($store_id);
        $reader = new Reader('GeoLite2-Country.mmdb');
        $location = $reader->country($_SERVER['REMOTE_ADDR'])->country->names['en'];
       // return view('frontEnd.shop',compact('products','nouveautes','bonsDeals','meilleurs','slides','categories','categoriesAll','cloud','firstVps','store','cart', 'location'));

        $agent = new Agent();

        if($agent->isPhone()){
            return view('mobile.index',compact('location','products','nouveautes','bonsDeals','meilleurs','slides','categories','categoriesAll','cloud','firstVps','store','cart'));
        }else{
            return view('frontEnd.shop',compact('location','products','nouveautes','bonsDeals','meilleurs','slides','categories','categoriesAll','cloud','firstVps','store','cart'));
        }
    }

    public function singleProduct($store_slug,$slug)
    {
        $store         = Store::where('slug',$store_slug)->first();
        $store_id      = $store->id;
        $product       = Product::where('store_id',$store_id)->where('slug', $slug)->firstOrFail();
        $bondeals      = Product::where('store_id',$store_id)->OrderBy('price','asc')->get();
        $latests       = Product::where('store_id',$store_id)->OrderBy('id','desc')->get();
        $categories    = getCategories($store_id);
        $cart          = cart($store_id);
        Session::push('products.recently_viewed', $product->id);
       // app('session')->set('products.recently_viewed', $product->id);
        //session()->put('products.recently_viewed', $product->id);
        //session()->push('recently_viewed', $product->id);
        if($this->agent->isPhone()){
            return view('mobile.shopSingle',compact('product','bondeals','latests','store','categories','cart'));
        }else{
            return view('frontEnd.shopSingle',compact('product','bondeals','latests','store','categories','cart'));
        }
    }

    public function categories($store_slug)
    {
        $store         = Store::where('slug',$store_slug)->first();
        $store_id      = $store->id;
        $categories    = getCategories($store_id);
        $cart          = cart($store_id);
        return view('mobile.categories',compact('store','categories','cart'));
    }
    public function viewCategory($store_slug,$name)
    {
        $store         = Store::where('slug',$store_slug)->first();
        $store_id      = $store->id;
        $category      = Category::where('store_id',$store_id)->where('name',$name)->first();
        $products      = $category->products()->get();
        $categories    = getCategories($store_id);
        $cart          = cart($store_id);

        if($this->agent->isPhone()){
            return view('mobile.viewCategory',compact('products','slides','category','store','categories','cart'));
        }else{
            return view('frontEnd.viewCategory',compact('products','slides','category','store','categories','cart'));
        }

    }


    public function redirect(){
        return redirect()->route('shop');
    }

    public function subscribe(){
        $email = request('email');
        $name  = request('name');
        Newsletter::subscribe($email,['LNAME'=>$name]);
        Session::flash('success',trans('app.succesRegistered'));
        return redirect()->back();
    }


    public function email(Request $request){

        $this->validate($request,[
            'name'      => 'required',
            'email'     => 'required|email',
            'subject'   => 'required',
            'message'   => 'required'
        ]);

        $email      = $request->email;
        $name       = $request->name;
        $message    = $request->message;
        $subject    = $request->subject;
        $phone      = $request->phone;
        $send       = 'fabioued@yahoo.fr';

        Mail::to($send)->send(new contactMessage($name,$message,$phone,$email,$subject));

        Session::flash('success',trans('app.thanksContact'));

        return redirect()->back();
    }

    public function search($store_slug,Request $request){

        $store    = Store::where('slug',$store_slug)->first();
        $store_id = $store->id;
        $title    ='Résultats de la recherche de : '.request('query');
        $keyword  = $request->input('query');
        if (!empty($keyword) && $keyword != "") {
            $search = "%{$keyword}%";
            $slug = $keyword;
            $product_query = Product::whereHas('category', function($query) use($request) {
                $query->where('id', $request->category);
            })->Where('name','LIKE','%'.$search.'%')->where('store_id',$store_id)->get();
        }else{
            $product_query= collect([]);
        }
        $categories    = getCategories($store_id);
        $cart          = cart($store_id);

        if($this->agent->isPhone()){
            return view('mobile.results', compact('product_query','title','store','categories','cart'));
        }else{
            return view('frontEnd/results', compact('product_query','title','store','categories','cart'));
        }
    }

    public function aboutUs($store_slug)
    {
        $store         = Store::where('slug',$store_slug)->first();
        $store_id      = $store->id;
        $categories    = getCategories($store_id);
        $cart          = cart($store_id);
        if($this->agent->isPhone()){
            return view('mobile.about',compact('store','categories','cart'));
        }else{
            return view('frontEnd.aboutUs',compact('store','categories','cart'));
        }

    }

    public function contact($store_slug)
    {
        $store      = Store::where('slug',$store_slug)->first();
        $store_id   = $store->id;
        $categories = getCategories($store_id);
        $cart       = cart($store_id);
        if($this->agent->isPhone()){
            return view('mobile.contact',compact('store','categories','cart'));
        }else{
            return view('frontEnd.contact',compact('store','categories','cart'));
        }

    }

    public function payments($store_slug)
    {
        $store      = Store::where('slug',$store_slug)->first();
        $store_id   = $store->id;
        $categories = getCategories($store_id);
        $cart       = cart($store_id);
        if($this->agent->isPhone()){
            return view('mobile.payments',compact('store','categories','cart'));
        }else{
            return view('frontEnd.payments',compact('store','categories','cart'));
        }
    }

    public function suivre($store_slug)
    {
        $store      = Store::where('slug',$store_slug)->first();
        $store_id   = $store->id;
        $categories = getCategories($store_id);
        $cart       = cart($store_id);
        return view('frontEnd.suivre',compact('store','categories','cart'));
    }

    public function devis($store_slug)
    {
        $store      = Store::where('slug',$store_slug)->first();
        $store_id   = $store->id;
        $categories = getCategories($store_id);
        $cart       = cart($store_id);
        return view('frontEnd.devis',compact('store','categories','cart'));
    }

    public function faq($store_slug)
    {
        $store          = Store::where('slug',$store_slug)->first();
        $store_id       = $store->id;
        $categories     = getCategories($store_id);
        $faqs           = Faq::orderBy('created_at','asc')->get();
        $cart           = cart($store_id);
        if($this->agent->isPhone()){
            return view('mobile.faq',compact('faqs','store','categories','cart'));
        }else{
            return view('frontEnd.faq',compact('faqs','store','categories','cart'));
        }
    }

    public function mobileHelp($store_slug){
        $store          = Store::where('slug',$store_slug)->first();
        $store_id       = $store->id;
        $cart           = cart($store_id);
        return view('mobile.help',compact('store','cart'));
    }



    /****************************************************************************/
    ////////////////            MOBILE VERSION             ///////////////////////
    /****************************************************************************/

    public function mobileHome(){
        return view('mobile.home');
    }

    public function mobileIndex(){
        return view('mobile.index');
    }



    public function mobileBoutiques(){
        $store = 2;
        return view('mobile.boutiques',compact('store'));
    }

    public function mobilePanier(){
        $stores = 2;
        return view('mobile.panier',compact('stores'));
    }

    public function mobileContact(){
        return view('mobile.contact');
    }

    public function mobileDevis()
    {
        return view('mobile.devis');
    }

    public function mobileVideo()
    {
        return view('mobile.video');
    }

    public function mobileSuivre()
    {
        return view('mobile.suivre');
    }
    public function mobilePayments()
    {
        return view('mobile.payments');
    }

}
