<?php

namespace App\Http\Controllers;

use App\Store;
use App\Wishlist;
use Auth;
use Session;
use Illuminate\Http\Request;

class WishlistController extends Controller
{
    public function wishlist($store_slug,$id){
        $store         = Store::where('slug',$store_slug)->first();
        $store_id      = $store->id;
        Wishlist::create([
            'product_id' => $id,
            'user_id'    => Auth::id(),
            'store_id'   => $store_id
        ]);
        Session::flash('success', 'Produit ajoute a votre liste de Souhait.');
        return redirect()->back();
    }

    public function unwishlist($store_slug,$id){
        $store         = Store::where('slug',$store_slug)->first();
        $store_id      = $store->id;
        $wishlist= Wishlist::where('product_id', $id)->where('user_id', Auth::id())->where('store_id',$store_id)->first();
        $wishlist->delete();
        Session::flash('success', 'Produit supprime de votre liste de Souhait.');
        return redirect()->back();
    }
}
