<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use App\PostTranslation;
use App\Product;
use App\SlideShow;
use App\Tag;
use App\User;
use App\Settings;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*$language       = app()->getLocale();
        $users_count    = User::all()->count();
//        $slides         = SlideShow::latest()->first();
        $products_count = Product::all()->count();
        return view('admin/dashboard',compact('users_count','products_count'));*/
        return redirect()->route("shop");
    }
}
