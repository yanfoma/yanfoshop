<?php

namespace App\Http\Controllers;

use App\SlideShow;
use Illuminate\Http\Request;
use Session;
use File;

class SlideShowController extends Controller
{
    //
    public function index()
    {
        $images = SlideShow::orderBy('id','asc')->get();
        return view('admin.slideshow.index')->with('slides',$images);
    }

    public function upload(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $image          = $request->image;

        $image_new_name = time().$image->getClientOriginalName();

        $image->move('uploads/slideshows/', $image_new_name);

        SlideShow::create([
            'title'         => $request->title,
            'image'         => 'uploads/slideshows/' .$image_new_name,
        ]);

        Session::flash('success',trans('app.imageAdded'));
        return redirect()->route('slideshow.index');
    }

    public function destroy($id)
    {
        //dd($id);
        $slide = SlideShow::find($id);
        $path  = parse_url($slide->image);
        File::delete(public_path($path['path']));
        $slide->delete();
        Session::flash('success',trans('app.imageDeleted'));
        return redirect()->route('slideshow.index');
    }

}
