<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Category;
use App\Product;
use App\Store;
use Auth;
use Illuminate\Http\Request;

class YanfomaSokoController extends Controller
{
    public function about(){
        if($this->agent->isPhone()){
            return view('mobile.aboutSoko');
        }else{
            return view('yanfomasoko.aboutUs');
        }
    }

    public function vendre(){
        if($this->agent->isPhone()){
            return view('mobile.vendre');
        }else{
            return view('yanfomasoko.vendre');
        }
    }

    public function contactUs(){
        if($this->agent->isPhone()){
            return view('mobile.contactSoko');
        }else{
            return view('yanfomasoko.contact');
        }
    }

    public function search(Request $request){
        $store    = Store::where('store_id',$request->store)->first();
        $store_id = $store->id;
        $title    ='Résultats de la recherche de : '.request('query');
        $keyword  = $request->input('query');
        if (!empty($keyword)) {
            $search = "%{$keyword}%";
        }
        $product_query = Product::where('store_id',$store_id)->Where('name','LIKE','%'.$search.'%')->get();

        $categories    = getCategories($store_id);
        $cart          = cart($store_id);
        return view('frontEnd/results', compact('product_query','title','store','categories','cart'));
    }
}
