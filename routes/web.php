<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/auth-center/login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::get('/',                 ['as' => 'welcome',   'uses' => 'PagesController@home']);
    Route::get('/nos-bons-deals',   ['as' => 'homeDeals', 'uses' => 'PagesController@homeFilter']);
    Route::get('/nos-promos',       ['as' => 'homePromo', 'uses' => 'PagesController@homeFilter']);
    Route::get('/nos-nouveautes',   ['as' => 'homeNewest','uses' => 'PagesController@homeFilter']);
    Route::get('/nos-boutiques',    ['as' => 'nosBoutiques','uses' => 'PagesController@nosBoutiques']);
    Route::post('/auth-center/login', 'Auth\LoginController@login')->name('postLogin');
    Route::get('/auth-center/se-deconnecter', 'Auth\LoginController@logout')->name('logout');

    // Registration Routes...
    Route::get('/auth-center/register', 'Auth\RegisterController@showRegistration')->name('register');
    Route::post('register', 'Auth\RegisterController@register')->name('registerPost');

    // Password Reset Routes...
    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.email');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');

    Route::get('a-propos-de-nous', [
        'uses' => 'YanfomaSokoController@about',
        'as' => 'yanfomaSoko.about'
    ]);

    Route::post('review', [
        'uses' => 'ReviewController@storeReview',
        'as' => 'review'
    ]);

    Route::get('devenir-vendeur', [
        'uses' => 'YanfomaSokoController@vendre',
        'as' => 'yanfomaSoko.vendre'
    ]);

    Route::get('nous-contacter', [
        'uses' => 'YanfomaSokoController@contactUs',
        'as' => 'yanfomaSoko.contactUs'
    ]);

    Route::post('/recherche', [

        'uses' => 'YanfomaSokoController@search',
        'as' => 'yanfomaSoko.search'
    ]);


    //Route::post('/resultats-de-la-recherche/{slug}',[
    //
    //    'uses' => 'PagesController@results',
    //    'as'   => 'results'
    //]);

    //Route::post('/resultats-de-la-recherche/{slug}',[
    //
    //    'uses' => 'YanfomaSokoController@results',
    //    'as'   => 'yanfomaSoko.results'
    //]);

    Route::get("/centre-d-aide/{store_slug}", [
        'uses' => 'PagesController@mobileHelp',
        'as' => 'mobile.help'
    ]);

    Route::get('/mobile/nos-boutiques', [
        'uses' => 'PagesController@mobileBoutiques',
        'as' => 'mobile.boutiques'
    ]);

    Route::get('/categories/{store_slug}', [
        'uses' => 'PagesController@categories',
        'as' => 'categories'
    ]);


    Route::get('/mobile/panier', [
        'uses' => 'PagesController@mobilePanier',
        'as' => 'mobile.panier'
    ]);
    Route::get('/mobile/login', [
        'uses' => 'Auth\LoginController@mobileLogin',
        'as' => 'mobile.login'
    ]);
    Route::get('/mobile/register', [
        'uses' => 'Auth\LoginController@mobileRegister',
        'as' => 'mobile.register'
    ]);

    Route::get('/mobile/demander-un-devis', [
        'uses' => 'PagesController@mobileDevis',
        'as' => 'mobile.devis'
    ]);
    Route::get('/mobile/tout-savoir-sur-ma-commande', [
        'uses' => 'PagesController@mobileSuivre',
        'as' => 'mobile.suivre'
    ]);

    Route::get('/mobile/nos-methodes-de-paiement', [
        'uses' => 'PagesController@mobilePayments',
        'as' => 'mobile.payments'
    ]);

    Route::get('/{store_slug}/faq', ['as' => 'faq', 'uses' => 'PagesController@faq']);
    Route::get('/{store_slug}/qui-sommes-nous', ['as' => 'aboutUs', 'uses' => 'PagesController@aboutUs']);
    Route::get('/{store_slug}/contact', ['as' => 'contact', 'uses' => 'PagesController@contact']);
    Route::get('/{store_slug}/partagez-nous-votre-experience', ['as' => 'testimonyIndex', 'uses' => 'TestimonyController@index']);
    Route::post('/{store_slug}/testimony/upload', ['as' => 'testimonyUpload', 'uses' => 'TestimonyController@upload']);
    Route::get('/{store_slug}/nos-methodes-de-paiement', ['as' => 'payments', 'uses' => 'PagesController@payments']);
    Route::get('/{store_slug}/tout-savoir-sur-ma-commande', ['as' => 'suivre', 'uses' => 'PagesController@suivre']);
    Route::get('/{store_slug}/demander-un-devis', ['as' => 'devis', 'uses' => 'PagesController@devis']);

    Route::get('/{store_slug}/ils-parlent-de-nous', ['as' => 'testimonyViewAll', 'uses' => 'TestimonyController@viewAll']);

    Route::resource('adminHome', 'AdminController', [['except' => 'show']]);

    Route::get('/verify/{token}', 'VerifyController@verify')->name('verify');

    Route::get('/{store_slug}/produits/{slug}', [
        'uses' => 'PagesController@singleProduct',
        'as' => 'shop.single'
    ]);

    Route::post('/{store_slug}/recherche', [

        'uses' => 'PagesController@search',
        'as' => 'results'
    ]);

    Route::get('{store_slug}/categories/{name}', [
        'uses' => 'PagesController@viewCategory',
        'as' => 'viewCategory'
    ]);

    Route::post('/subscribe', [

        'uses' => 'PagesController@subscribe',
        'as' => 'subscribe'
    ]);

    Route::post('/email', [

        'uses' => 'PagesController@email',
        'as' => 'email'
    ]);
});

Route::group(['middleware' => 'auth'], function () {

    Route::get('{store_slug}/mon-panier',[
        'uses' => 'CartController@storeCart',
        'as'   => 'store.cart.index'
    ]);

    Route::post('{store_slug}/ajouter-dans-le-panier',[
        'uses' => 'CartController@storeAddToCart',
        'as'   => 'store.cart.addToCart'
    ]);

    Route::get('{store_slug}/ajouter-dans-le-panier/{id}',[
        'uses' => 'CartController@storeAddDirect',
        'as'   => 'store.cart.addDirect'
    ]);

    Route::get('{store_slug}/votre-commande/',[
        'uses' => 'CartController@storeCheckout',
        'as'   => 'store.cart.checkout'
    ]);

    Route::post('{store_slug}/passer-votre-commande/',[
        'uses' => 'CartController@storeOrder',
        'as'   => 'store.cart.order'
    ]);

    Route::get('/{store_slug}/votre-panier/supprimer/{id}',[
        'uses' => 'CartController@storeCartDelete',
        'as'   => 'store.cart.delete'
    ]);

    Route::get('{store_slug}/transaction-finie',[
        'uses' => 'CartController@storeThanks',
        'as'   => 'thanks'
    ]);



    Route::get('panier',[
        'uses' => 'CartController@cart',
        'as'   => 'yanfomasoko.cart.index'
    ]);

    Route::post('yanfomasoko/ajouter-dans-le-panier',[
        'uses' => 'PagesController@addToCart',
        'as'   => 'yanfomasoko.cart.addToCart'
    ]);

    Route::get('ajouter-dans-le-panier/{id}',[
        'uses' => 'PagesController@addDirect',
        'as'   => 'yanfomasoko.cart.addDirect'
    ]);

    Route::get('passer-votre-commande/',[
        'uses' => 'CartController@checkout',
        'as'   => 'yanfomasoko.cart.checkout'
    ]);

    Route::post('passer-votre-commande/',[
        'uses' => 'CartController@order',
        'as'   => 'yanfomasoko.cart.order'
    ]);

    Route::get('votre-panier/supprimer/{id}',[
        'uses' => 'CartController@delete',
        'as'   => 'yanfomasoko.cart.delete'
    ]);

    Route::get('transaction-reussie',[
        'uses' => 'CartController@thanks',
        'as'   => 'yanfomasoko.thanks'
    ]);

    Route::get('{store_slug}/produit/like/{id}',                    ['as' => 'product.like',             'uses' => 'LikesController@like']);
    Route::get('{store_slug}/produit/unlike/{id}',                  ['as' => 'product.unlike',           'uses' => 'LikesController@unlike']);
    Route::get('{store_slug}/produit/wishlist/{id}',                ['as' => 'product.wishlist',         'uses' => 'WishlistController@wishlist']);
    Route::get('{store_slug}/produit/unwishlist/{id}',              ['as' => 'product.unwishlist',       'uses' => 'WishlistController@unwishlist']);

    Route::get('customer/mes-informations',[
        'uses' => 'CustomerController@welcome',
        'as'   => 'customer.welcome'
    ]);



    Route::get('customer/mon-adresse',[
        'uses' => 'CustomerController@adresse',
        'as'   => 'customer.adresse'
    ]);
    Route::get('customer/ma-liste-de-souhaits',[
        'uses' => 'CustomerController@wishlist',
        'as'   => 'customer.wishlist'
    ]);
    Route::get('customer/mon-mot-de-passe',[
        'uses' => 'CustomerController@password',
        'as'   => 'customer.password'
    ]);
    Route::get('/customer/faq', [
        'uses' => 'CustomerController@faq',
        'as'   => 'customer.faq'
    ]);
    Route::get('/customer/aide', [
        'uses' => 'CustomerController@aide',
        'as'   => 'customer.aide'
    ]);
    Route::get('/customer/edit/{id}', [
        'uses' => 'CustomerController@edit',
        'as'   => 'customer.edit'
    ]);
    Route::post('/customer/update/{id}', [
        'uses' => 'CustomerController@update',
        'as'   => 'customer.update'
    ]);
    Route::post('/customer/verify/{id}', [
        'uses' => 'CustomerController@verify',
        'as'   => 'customer.verify'
    ]);
    Route::post('/customer/update-addresse/{id}', [
        'uses' => 'CustomerController@updateAddresse',
        'as'   => 'customer.updateAddresse'
    ]);
    Route::post('/customer/update-password/{id}', [
        'uses' => 'CustomerController@updatePassword',
        'as'   => 'customer.updatePassword'
    ]);
    Route::get('/customer/adress/{id}', [
        'uses' => 'CustomerController@adress',
        'as'   => 'customer.adress'
    ]);
    Route::get('/customer/mes-commandes', [
        'uses' => 'CustomerController@commandeView',
        'as'   => 'customer.commandeView'
    ]);

    Route::get('/customer/suivre-mes-commandes/{purchasedId}', [
        'uses' => 'CustomerController@suivreCommande',
        'as'   => 'customer.suivreCommande'
    ]);

    Route::get('/customer/confirmer-mes-commandes', [
        'uses' => 'CustomerController@commandeConfirmation',
        'as'   => 'customer.commandeConfirmation'
    ]);

    Route::post('/customer/confirmer/store', [
        'uses' => 'CustomerController@confirmStore',
        'as'   => 'customer.confirmStore'
    ]);
    Route::get('/customer/commandeDetail/{id}', [
        'uses' => 'CustomerController@commandeDetail',
        'as'   => 'customer.commandeDetail'
    ]);
    Route::get('/customer/collect-data/', [
        'uses' => 'CustomerController@collectData',
        'as'   => 'customer.collectData'
    ]);
    Route::post('/customer/store/collect-data/', [
        'uses' => 'CustomerController@updateData',
        'as'   => 'customer.updateData'
    ]);
});

Route::get('boutiques/{store_slug}',[
    'uses' => 'PagesController@shop',
    'as'   => 'shop'
]);