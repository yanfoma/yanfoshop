@extends('layouts.frontEnd.appHome')

@section('title')
    Qui Sommes Nous? Yanfoma Soko
@endsection

@section('style')
    <style>
        hr {
            margin-top: 1rem;
            margin-bottom: 1rem;
            border: 0;
            border-top: 1px solid rgba(0,0,0,.1);
        }
    </style>
@endsection
@section('content')
    <section class="breadcumb_area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <div class="breadcumb_section">
                        <div class="page_title">
                            <h3>
                                Qui Sommes Nous?
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
        <section class="faq">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="widget3">
                        <div class=" blog-box">
                            <div class="panel-body">
                                <h5 class="heading">C’est quoi Yanfoma Soko?</h5>
                                <p class="par">yanfoShop est un shopping mall en ligne qui se veut être une plateforme d'accès, de partages et d'échanges de technologie numérique,
                                    la première destination en ligne des makers, développeurs, entrepreneurs, amoureux de la technologie (technophiles). Il a été entièrement développé par Yanfoma.
                                    yanfoShop s’est donné pour mission d’offrir une grande variété de technologies numériques, facile à apprendre, à utiliser, et accessible à tous. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="widget3">
                        <div class=" video-box">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-sm-5 panel-image">
                                        <figure>
                                            <img class="img-responsive" src="https://res.cloudinary.com/yanfomaweb/image/upload/v1553269565/Yanfoma/HorzWback.png" alt="logo Yanfoma Soko" width="400">
                                            <figcaption>Logo de Yanfoma Soko</figcaption>
                                        </figure>
                                    </div>
                                    <div class="col-sm-7 panel-video" id="video">
                                        <h5 class="heading center">Vidéo De Présentation de Yanfoma Soko</h5>
                                        <div class="embed-responsive embed-responsive-16by9">
                                            <iframe class="embed-responsive-item" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" src="https://www.youtube.com/embed/PmW0euwk0Ck" allowfullscreen></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="widget3">
                        <div class=" blog-box">
                            <div class="panel-body">
                                <h5 class="heading">Pourquoi acheter chez Yanfoma Soko?</h5>
                                <p class="par">
                                    yanfoShop propose uniquement technologies numériques. Cette spécialisation nous oblige à offrir de la qualité. Egalement, nous avons une équipe qui prend à coeur votre apprentissage des produits que nous proposons. Nous sommes disposés à particulierement suivre chacun de nos clients pour que l’utilisation de nos produits soit le plus simple et efficace possible.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection