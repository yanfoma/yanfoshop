@extends('layouts.frontEnd.appHome')

@section('title')
    Passer Votre Commande : Yanfoma Soko
@endsection

@section('style')
    <style>
        .inner-banner               {  margin-bottom: 30px !important;  }
        .inner-banner .box h3       {  font-size: 20px;  }
        .progressbar li:before {
            content: counter(step);
            counter-increment: step;
            width: 60px;
            height: 60px;
            border: 3px solid #ddd;
            display: block;
            font-size: 25px;
            font-weight: bold;
            text-align: center;
            margin: 0 auto 10px auto;
            border-radius: 100px;
            line-height: 60px;
            background-color: #ddd!important;
            color: white;
            position: relative;
            z-index: 1;
        }

        .progressbar li.active2 + li:after{
        background: #18b7bf!important;
        }

        .progressbar li:after {
            content: '';
            position: absolute;
            width: 100%;
            height: 3px;
            background-color: #ddd;
            top: 30px;
            left: -50%;
            z-index: 0;
        }

        .progressbar li:first-child:after {
            content: none;
        }

        .progressbar li:first-child:before {
            background: #18b7bf!important;
            border-color: #18b7bf!important;
        }
    </style>
@endsection
@section('content')
    <div class="text-center">
        <div class="container inner-banner">
            <div class="box">
                <h3>Confirmer Votre Commande</h3>
            </div><!-- /.box -->
        </div><!-- /.container -->
    </div>
    <ul class="progressbar">
        <li class="active2"><a href="{{route('yanfomasoko.cart.index')}}"> Panier</a> </li>
        <li class="active">Confirmer Commande</li>
        <li>Terminé</li>
    </ul>

    <section class="checkout-area bg-white ptb-30 widget4">
        <div class="container">
            <div class="cart-outer">
                <form method="post" action="{{route('yanfomasoko.cart.order')}}" enctype="multipart/form-data" class="widget4 billing-info">
                    {{csrf_field()}}
                    <div class="row">
                        <div class="col-lg-6">
                            <h3 class="small-title">Vos Informations</h3>
                            <div class="ho-form">
                                <div class="ho-form-inner">
                                    <div class="single-input ">
                                        <label for="customer-firstname">Nom & Prénoms *</label>
                                        <input type="text" name="name" placeholder="Nom et Prénoms" required="required" aria-required="true" value="{{$user->name}}">
                                        @if ($errors->has('name'))
                                            <div id="uname-error" class="error">{{ $errors->first('name') }}></div>
                                        @endif
                                    </div>
                                    <div class="single-input">
                                        <label for="customer-lastname">Pays *</label>
                                        <input type="text" name="country" placeholder="Ex: Burkina Faso " required="required" aria-required="true" value="{{$user->pays}}">
                                        @if ($errors->has('country'))
                                            <div id="uname-error" class="error">{{ $errors->first('country') }}</div>
                                        @endif
                                    </div>
                                    <div class="single-input">
                                        <label for="customer-lastname">Ville/Quartier *</label>
                                        <input type="text" name="city" placeholder="Ex: Ouagadougou / karpala " required="required" aria-required="true" value="{{$user->ville}}">
                                        @if ($errors->has('city'))
                                            <div id="uname-error" class="error">{{ $errors->first('city') }}</div>
                                        @endif
                                    </div>
                                    <div class="single-input">
                                        <label for="customer-email">Email *</label>
                                        <input type="text" name="email" placeholder="Addresse Email" required="required" aria-required="true" value="{{$user->email}}">
                                        @if ($errors->has('email'))
                                            <div id="uname-error" class="error">{{ $errors->first('email') }}</div>
                                        @endif
                                    </div>
                                    <div class="single-input">
                                        <label for="customer-phone">Portable *</label>
                                        <input type="text" name="phone" placeholder="Numéro de Téléphone" required="required" aria-required="true" value="{{$user->wa_number}}">
                                        @if ($errors->has('phone'))
                                            <div id="uname-error" class="error">{{ $errors->first('phone') }}</div>
                                        @endif
                                    </div>
                                    <div class="single-input">
                                        <label for="customer-address">Addresse *</label>
                                        <textarea name="address" rows="5" cols="50" placeholder="Veuillez decrire votre addresse" required="required" aria-required="true">{{$user->addresse}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="order-infobox">
                                <h3 class="small-title">Résumé de Vos Achats </h3>
                                <span>({{$cart->count()}}) Produit(s)</span>
                                <div class="checkout-table table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th class="text-left">PRODUITS</th>
                                            <th class="text-left">QUANTITE</th>
                                            <th class="text-right">TOTAL</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($cart as $product)
                                            <tr>
                                                <td class="text-left">{{$product->product->name}}</td>
                                                <td class="text-center"><span>× {{$product->product_qty}}</span></td>
                                                <td class="text-right">{{$product->product_total}}</td>
                                            </tr>
                                            {{--<tr>--}}
                                            {{--<th class="text-left">TEMPS DE LIVRAISON</th>--}}
                                            {{--@if($product->product->stock_location)--}}
                                            {{--<td class="text-center">7 </td>--}}
                                            {{--@else--}}
                                            {{--<td class="text-center">20 - 30 </td>--}}
                                            {{--@endif--}}
                                            {{--<td class="text-right">jours</td>--}}
                                            {{--</tr>--}}
                                        @endforeach
                                        </tbody>
                                        <tfoot>
                                        <tr class="total-price">
                                            <th class="text-left">Total A Payer</th>
                                            <td class="text-center"> {{getTotal()}}   </td>
                                            <td class="text-right">  CFA</td>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>

                                <div class="payment-method bg-grey">
                                    <h4 class="text-left">Veuillez sélectionner votre méthode de Paiement</h4>
                                    <div class="check-payment">
                                        <input type="radio" name="payment" id="checkout-payment-method-1" class="ho-radio" checked="checked" value="Orange Money" required>
                                        <label for="checkout-payment-method-1">Orange Money</label>
                                    </div>
                                    <div class="paypal-payment">
                                        <input type="radio" name="payment" id="checkout-payment-method-2" class="ho-radio" value="Ecobank Pay">
                                        <label for="checkout-payment-method-2">Ecobank Pay</label>
                                    </div>
                                    <div class="paypal-payment">
                                        <input type="radio" name="payment" id="checkout-payment-method-3" class="ho-radio" value="Cash Chez Kaanu (CCK)">
                                        <label for="checkout-payment-method-3">Cash Chez Kaanu (CCK)</label>
                                    </div>
                                    <div class="paypal-payment">
                                        <input type="radio" name="payment" id="checkout-payment-method-4" class="ho-radio" value="Dépôt Bancaire">
                                        <label for="checkout-payment-method-4">Dépôt Bancaire</label>
                                    </div>
                                    <div class="paypal-payment">
                                        <input type="radio" name="payment" id="checkout-payment-method-5" class="ho-radio" value="Bitcoin">
                                        <label for="checkout-payment-method-5">Bitcoin</label>
                                    </div>
                                </div>
                                <button class="ho-button btn-primary  ho-button-fullwidth mt-30" type="submit">
                                    <span>Commander Maintenant</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection