@extends('layouts.frontEnd.appHome')

@section('title')
    Devenir Vendeur sur Yanfoma Soko
@endsection

@section('style')
    <style>
        hr {
            margin-top: 1rem;
            margin-bottom: 1rem;
            border: 0;
            border-top: 1px solid rgba(0,0,0,.1);
        }
    </style>
@endsection
@section('content')
    <section class="faq">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="widget3">
                        <div class=" blog-box">
                            <div class="panel-body">
                                {{--<h5 class="heading">C’est quoi yanfoShop?</h5>--}}
                                <p class="par" style="height: auto!important;">
                                    Conditions pour la vente de produits via une boutique dediée Yanfoma Soko (yanfoma.com):
                                    <br>1. Frais: Gratuit
                                    <br>2. Chaque vendeur doit avoir un logo, une description de sa boutique en ligne et un identifiant pour identifier la boutique sur Soko
                                    <br>3. Yanfoma reserve le droit de motifier ces termes.
                                </p>
                                <p class="par">
                                    Pour être vendeur sur Yanfoma Soko, envoyez votre identifiant, la description de votre boutique, la liste de vos produits(nom, prix, description du produit, image), ainsi que le logo à l'adresse mail info@yanfoma.tech
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection