@extends('layouts.frontEnd.app')

@section('title')
    NOS MÉTHODES DE PAIEMENT
@endsection

@section('style')
    <style>
        .widget {
            background: #fff  none repeat scroll 0 0;
            box-shadow: 0 0 20px rgba(0,0,0,.2);
        }
    </style>
@endsection
@section('content')
    <section class="breadcumb_area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <div class="breadcumb_section">
                        <div class="page_title">
                            <h3>
                                NOS MÉTHODES DE PAIEMENT
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="faq">
        <div class="container">
            <div class="row" id="Bitcoin">
                <div class="col-sm-12">
                    <div class="widget5">
                        <div class=" blog-box">
                            <div class="panel-body">
                                <h5 class="heading">BITCOIN</h5>
                                <p class="par2">
                                    Pour payer en Bitcoin, vous devez repondre au mail que vous recevrez apres la commande en precisant que vous voulez utiliser cette methode de paiement.
                                    Vous recevrez la somme a envoyer en Bitcoin et le temps limite pour effectuer la transaction.
                                    Vous avez une reduction de 1000 Fr CFA lorsque vous faites votre paiement en Bitcoin.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" >
                <div class="col-sm-12">
                    <div class="widget5">
                        <div class=" blog-box">
                            <div class="panel-body" >
                                <h5 class="heading">Ecobank Pay</h5>
                                <p class="par2" id="EcobankPay" style="padding-bottom: 250px!important;">
                                    Vous pouvez payer via Ecobank Pay à travers le QR Code que suivant:<br>
                                    <img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1547479354/Screen_Shot_2019-01-14_at_10.42.42_PM.png" width="200">
                                    <br>
                                    Vous avez une réduction de 1000 Fr CFA lorsque vous faites votre paiement via Ecobank Pay.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" id="OrangeMoney">
                <div class="col-sm-12">
                    <div class="widget5">
                        <div class=" blog-box">
                            <div class="panel-body">
                                <h5 class="heading">Orange Money</h5>
                                <p class="par2">
                                    Vous devez envoyer la somme equivalente a votre commande au numero suivant: +22667402030.
                                    Notez que ce numero est au Burkina Faso. Apres le paiement, vous devez repondre au mail
                                    que vous avez recu apres la commande en nous envoyant le numero de la transaction.
                                    Vous avez une reduction de 500 Fr CFA lorsque vous faites votre paiement par Orange Money.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" id="CCK">
                <div class="col-sm-12">
                    <div class="widget5">
                        <div class=" blog-box">
                            <div class="panel-body">
                                <h5 class="heading">Cash Chez Kaanu (CCK)</h5>
                                <p class="par2">
                                    Vous pouvez payer au centre Kaanu lorsque votre colis arrivera.
                                    Il vous suffit d'emmener la somme equivalente a votre commande
                                    lorsque vous aller recuperer votre colis et payer avec l'agent Kaanu.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" id="DepotBancaire">
                <div class="col-sm-12">
                    <div class="widget5">
                        <div class="blog-box">
                            <div class="panel-body">
                                <h5 class="heading">Dépôt Bancaire</h5>
                                <p class="par2">
                                    Vous devez vous rendre à l'agence Ecobank la plus proche et déposer la somme équivalente à votre commande dans notre compte bancaire. Après la transaction, devez répondre au mail que vous avez reçu après la commande avec en fichier joint, une photo du reçu de dépôt à la banque. Voici le numéro du compte bancaire: (le SWIFT est important pour ceux qui sont hors du Burkina Faso)
                                    <br><strong>SWIFT: ECOCBFBF</strong>
                                    <br><strong>Account No: 170048532001</strong><br>
                                    Vous avez une réduction de 1000 Fr CFA lorsque vous faites votre paiement par dépôt bancaire Ecobank.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    {{--<script>--}}
        {{--$(window).load(function(){--}}
            {{--if (window.location.hash == "#Bitcoin") { $('body').scrollTo('#Bitcoin');}--}}
            {{--else if (window.location.hash == "#tyjh") {$('.js-accordion-open[data-id=32]').trigger('click');}--}}
            {{--else if (window.location.hash == "#ngfc") {$('.js-accordion-open[data-id=31]').trigger('click');}--}}
            {{--else {return false;}--}}
        {{--});--}}
    {{--</script>--}}
@endsection