@extends('layouts.frontEnd.app')

@section('title')
   Votre Experience {{$store->name}}
@endsection

@section('content')
    <section class="testimonials-block" id="testimonials">
        <div class="container">
            <div class="title text-center">
                <span class="up up-primary">Temoignages</span>
                <h2>Ils Parlent de Nous</h2>
                <span class="section-title-line"></span>
            </div>
            <div class="row">
                @if($testimonies->count())
                    @foreach($testimonies as $testimony)
                        <div class="col-md-4 col-sm-12 fadeInUp animated">
                            <div class="custom-card testimonials-item text-center">
                                <div class="image">
                                    <img class="img-fluid mb-4 rounded-circle" src="{{asset('https://res.cloudinary.com/yanfomaweb/image/upload/v1548546096/Yanfoma/yanfoShopTestimonyUser.png')}}" alt="">
                                </div>
                                <p class="mb-4 par2">{{$testimony->message}}</p>
                                <h6 class="mb-0 text-primary">- {{$testimony->name}}</h6>
                            </div>
                        </div>
                    @endforeach
                    @else
                    <div class="widget4 row col-sm-12 col-md-12 col-lg-12">
                        <div class="col-md-4 col-sm-12">
                            <div class="image">
                                <img class="img-fluid mb-4 rounded-circle" src="{{asset('https://res.cloudinary.com/yanfomaweb/image/upload/v1548547819/Yanfoma/testimony.png')}}" alt="" width="250">
                            </div>
                        </div>
                        <div class="col-md-7 col-sm-12 mt-50">
                            <h5 style="font-size: 20px; line-height: 50px!important;">Aucun Temoignage pour l'instant!!! <br>Soyez le premier a partager votre experience sur notre plateforme</h5>
                            <div class="col-sm-4 col-md-offset-2">
                                <a href="{{route('testimonyIndex',['store_slug' => $store->slug])}}" class="btn btn-primary btn-lg">Je partage mon avis</a>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </section>
@endsection