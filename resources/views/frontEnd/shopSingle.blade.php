@extends('layouts.frontEnd.app')

@section('title')
    {{$store->name}} : {{$product->name}}
@endsection

@section('style')
    <link rel="stylesheet" type="text/css" href="{{asset('css/star-rating-svg.css')}}">
    <style>
        .pdetails-singleimage img{
            border-radius: 20px!important;
        }
        .font-vert{
            color:green!important;
        }
        .socialsharing_product {
            background-color: transparent!important;
            color: dimgrey;
            border-radius: 5px!important;
            padding: 8px!important;
            margin-right: 3px!important;
            border: 2px solid #e5e5e5!important;
        }
        .socialsharing_product:hover {
            background-color: orangered!important;
            color: white!important;
        }
    </style>
@endsection
@section('content')
    <!-- Shop Page Area -->
    <div class="shop-page-area bg-white ptb-30">
        <div class="container2">
            <div class="row">
                <div class="col-lg-9 order-1 order-lg-2">
                    <div class="pdetails borderShadow">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="pdetails-images">
                                    <div class="pdetails-largeimages pdetails-imagezoom">
                                        <div class="pdetails-singleimage" data-src="{{$product->image_url}}">
                                            <img src="{{$product->image_url}}" alt="{{$product->name}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 ">
                                <div class="pdetails-content">
                                    <h3>{{$product->name}}</h3>
                                    <div class="pdetails-pricebox">
                                            <div class="pdetails-socialshare">
                                                @if($product->onSale)
                                                    <del class="oldprice">{{number_format($product->price, 0 , ',' , ' ')}} </del>
                                                    <span class="price">{{number_format($product->sale_amount, 0 , ',' , ' ')}} CFA </span>
                                                @else
                                                    <span class="price">{{number_format($product->price, 0 , ',' , ' ')}} CFA </span>
                                                @endif
                                                @if($product->onSale && $product->saleType == 2)
                                                    <li class="flag-discount">-{{ $product->salePercentage }} %</li>
                                                @endif
                                                <ul>
                                                    @if($product->is_liked_by_auth_user())
                                                        <li><a class="unlike" href="{{route('product.unlike', [ 'store_slug' => $store->slug,'id' => $product->id ])}}"><i class="lnr lnr-thumbs-up"></i></a></li>
                                                    @else
                                                        <li><a class="like" href="{{route('product.like', ['store_slug' => $store->slug, 'id' => $product->id ])}}"><i class="lnr lnr-thumbs-up"></i></a></li>
                                                    @endif
                                                    @if($product->is_wished_by_auth_user())
                                                        <li><a class="unfavoris" href="{{route('product.unwishlist', [ 'store_slug' => $store->slug,'id' => $product->id ])}}"><i class="lnr lnr-heart"></i></a></li>
                                                    @else
                                                        <li><a class="favoris" href="{{route('product.wishlist', [ 'store_slug' => $store->slug,'id' => $product->id ])}}"><i class="lnr lnr-heart"></i></a></li>
                                                    @endif
                                                </ul>
                                            </div>
                                    </div>
                                    <div class="pdetails-quantity">
                                        <form action="{{route('store.cart.addToCart',['store_slug' => $store->slug])}}" method="post">
                                            {{csrf_field()}}
                                            <div class="quantity-select">
                                                <input type="hidden" name="product_id" value="{{$product->id}}">
                                                <input type="text"   name="quantity"  value="{{$product->minQty}}">
                                                <div class="inc qtybutton">+<i class="ion ion-ios-arrow-up"></i></div>
                                                <div class="dec qtybutton">-<i class="ion ion-ios-arrow-down"></i></div>
                                            </div>
                                            <button type="submit" class="btn btn-primary"> <i class="lnr lnr-cart"></i> Ajouter au panier</button>
                                        </form>
                                    </div>
                                    <div class="pdetails-categories">
                                        <div class="compare-table table-responsive">
                                            <table class="table table-bordered table-hover mb-0">
                                                <tbody>
                                                <tr>
                                                    <th><span class="red-text"><i class="fa fa-exclamation-triangle red-text"></i> Quantité Minimale: </span></th>
                                                    <td>
                                                        <ul>
                                                            <li><a href="#">{{$product->minQty}}</a></li>
                                                        </ul>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th><span><i class="fa fa-clock-o theme-text"></i> Temps De Livraison:</span></th>
                                                    <td>
                                                        <ul>
                                                            @if($product->stock_location)
                                                                @switch($product->stock_location)
                                                                    @case("bf")
                                                                        <li><a href="#" class="font-vert">Disponible dans la semaine au Burkina Faso</a></li>
                                                                    @break
                                                                    @case("ci")
                                                                        <li><a href="#" class="font-vert" >Disponible dans la semaine en Cote D'Ivoire</a></li>
                                                                    @break
                                                                    @case("cm")
                                                                        <li><a href="#" class="font-vert" >Disponible dans la semaine au Cameroun</a></li>
                                                                    @break
                                                                @endswitch
                                                            @else
                                                                <li><a href="#" class="font-vert" >Disponible dans 20~30 jours</a></li>
                                                            @endif
                                                        </ul>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th><span><i class="fa fa-truck theme-text"></i>
                                                            Livraison
                                                        </span></th>
                                                    <td>
                                                        <ul>
                                                            <li><a href="#" class="font-vert" >
                                                               @if($product->livraison == "Gratuite")
                                                                   {{ $product->livraison}}
                                                               @else
                                                                   {{$product->livraison}} : {{$product->prixLivraison}} CFA
                                                                @endif</a>
                                                            </li>
                                                        </ul>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th> <span><i class="fa fa-list-alt theme-text"></i> Categorie:</span></th>
                                                    <td>
                                                        <ul>
                                                            <li><a href="#" class="font-vert">{{$product->category->name}}</a></li>

                                                        </ul>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    @include('layouts.frontEnd.share')
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pdetails-allinfo borderShadow">
                        <ul class="nav pdetails-allinfotab justify-content-center" id="product-details" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="product-details-area1-tab" data-toggle="tab" href="#product-details-area1"
                                   role="tab" aria-controls="product-details-area1" aria-selected="true">Description</a>
                            </li>
                            @if($product->more_info)
                                <li class="nav-item">
                                    <a class="nav-link" id="product-details-area2-tab" data-toggle="tab" href="#product-details-area2"
                                       role="tab" aria-controls="product-details-area2" aria-selected="false">Plus d'Informations</a>
                                </li>
                            @endif
                            <li class="nav-item">
                                <a class="nav-link" id="product-details-area3-tab" data-toggle="tab" href="#product-details-area3"
                                role="tab" aria-controls="product-details-area3" aria-selected="false">Avis ({{$product->reviews->count()}})</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="product-details-ontent">
                            <div class="tab-pane fade show active" id="product-details-area1" role="tabpanel"
                                 aria-labelledby="product-details-area1-tab">
                                <div class="pdetails-description">
                                    {!!html_entity_decode(strip_tags($product->description))!!}
                                </div>
                            </div>
                            <div class="tab-pane fade" id="product-details-area2" role="tabpanel" aria-labelledby="product-details-area2-tab">
                                <div class="pdetails-moreinfo">
                                    {!! $product->more_info !!}
                                </div>
                            </div>
                            <div class="tab-pane fade" id="product-details-area3" role="tabpanel" aria-labelledby="product-details-area3-tab">
                                <div class="pdetails-reviews">
                                    @if($product->reviews->count())
                                        <div class="product-review">
                                            <div class="row widget2">
                                                <div class="row col-sm-12">
                                                    <div class="col-sm-4 rating-left">
                                                        <h2 class="rate">{{floor($product->reviews->avg('rating'))}}.0</h2>
                                                        <div class="my-rating-2" data-rating="{{floor($product->reviews->avg('rating'))}}"></div>
                                                        <div class="total">
                                                            <i class="fa fa-user"></i> {{$product->reviews->count()}} Avis
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <canvas id="bar-chart-horizontal" width="1000" height="400"></canvas>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="">
                                            @foreach($product->reviews as $review)
                                                <div class="single-comment">
                                                    <div class="single-comment-thumb" style="border: none!important;width:40px;">
                                                        <img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1548546096/Yanfoma/yanfoShopTestimonyUser.png" alt="user logo" width="40">
                                                    </div>
                                                    <div class="single-comment-content col-md-12">
                                                        <div class="single-comment-content-top">
                                                            <h6>{{$review->user->name}} – {{Date::parse($review->created_at)->diffForHumans()}}</h6>
                                                            <div class="rattingbox">
                                                                <div class="my-rating-3" data-rating="{{$review->rating}}"></div>
                                                            </div>
                                                        </div>
                                                        <p>
                                                            {{$review->rating_desc}}
                                                        </p>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    @else
                                        <h5 class="text-center">Aucun Avis pour l'instant !!!</h5>
                                    @endif
                                    @if(Auth::check())
                                        @if(! $product->is_reviewed_by_auth_user())
                                            <div class="commentbox mt-5">
                                                <h5>Donner votre Avis</h5>
                                                <form method="post" action="javascript:void(0)" class="ho-form" id="myform">
                                                        {{csrf_field()}}
                                                        <div class="ho-form-inner">
                                                            <div class="single-input">
                                                                <div class="rattingbox hover-action">
                                                                    <div class="my-rating"></div>
                                                                    <input type="hidden" name="rating" id="rating">
                                                                    <input type="hidden" name="user_id" id="user_id" value="{{Auth::user()->id}}">
                                                                    <input type="hidden" name="productid" id="productid" value="{{$product->id}}">
                                                                </div>
                                                            </div>
                                                            <div class="single-input">
                                                                <label for="new-review-textbox">Décrivez votre expérience</label>
                                                                <textarea name="rating_desc" cols="30" rows="5"  id="rating_desc"  required></textarea>
                                                            </div>
                                                            <div class="alert alert-success col-md-12 d-none" id="msg_div" style="margin-top: 20px!important;">
                                                                <span id="res_message"></span>
                                                            </div>
                                                            <div class="single-input">
                                                                <button class="ho-button btn-submit" type="submit" id="send_form"><span>Envoyer</span></button>
                                                            </div>
                                                        </div>
                                                    </form>
                                            </div>
                                        @endif
                                    @else
                                        <h5 class="text-center">Veuillez vous <a href="{{route('login')}}" style="color: #ffaf20!important;">Connecter</a>  pour donner votre Avis</h5>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 order-2 order-lg-1 borderShadow">
                    <div class="shop-widgets">

                        <div class="single-widget widget-categories">
                            <h5 class="widget-title">Nos Categories</h5>
                            <ul>
                                @foreach($categories as $category)
                                <li><a href="{{route('viewCategory',['store_slug' => $category->store->slug,'name' => $category->name ])}}">{{$category->name}} <span>{{$category->products->count()}}</span></a></li>
                                @endforeach
                            </ul>
                        </div>

                        <!-- Recommended Product -->
                        <div class="single-widget widget-recommended-products mt-30">
                            <div class="section-title">
                                <h3>DÉJÀ VUS</h3>
                            </div>
                            <div class="recommended-products-slider-2 slider-navigation-2 slider-navigation-2-m0">
                                <div class="product-slider-col">
                                    @foreach($recently_viewedProducts as $product)
                                        <article class="hoproduct hoproduct-4">
                                        <div class="hoproduct-image">
                                            <a class="hoproduct-thumb" href="{{route('shop.single',['store_slug' => $store->slug, 'slug' => $product->slug])}}">
                                                <img class="hoproduct-frontimage" src="{{$product->image_url}}" alt="product image">
                                            </a>
                                        </div>
                                        <div class="hoproduct-content">
                                            <h5 class="hoproduct-title product-title"><a href="{{route('shop.single',['store_slug' => $store->slug, 'slug' => $product->slug])}}">{{$product->name}}</a></h5>
                                            <div class="pricebox">
                                                @if($product->onSale)
                                                    <del class="oldprice">{{number_format($product->price, 0 , ',' , ' ')}} </del>
                                                    <span class="price">{{number_format($product->sale_amount, 0 , ',' , ' ')}} CFA </span>
                                                @else
                                                    <span class="price">{{number_format($product->price, 0 , ',' , ' ')}} CFA </span>
                                                @endif
                                            </div>
                                        </div>
                                    </article>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--// Product Details Area -->
    @include('layouts.frontEnd.recommended')
@endsection

@section('scripts')
    <script type="text/javascript" src="{{asset('js/jquery.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/Chart.bundle.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/jquery.star-rating-svg.js')}}"></script>
    <script type="text/javascript" >
        $(".my-rating-2").starRating({
            totalStars: 5,
            starShape: 'rounded',
            starSize: 40,
            emptyColor: 'lightgray',
            hoverColor: '#ffad10',
            activeColor: '#ff9811',
            useGradient: true,
            readOnly: true,
            forceRoundUp:true,
            useFullStars: true
        });

        $(".my-rating-3").starRating({
            totalStars: 5,
            starShape: 'rounded',
            starSize: 20,
            emptyColor: 'lightgray',
            hoverColor: '#ffad10',
            activeColor: '#ff9811',
            useGradient: true,
            readOnly: true,
            forceRoundUp:true,
            useFullStars: true
        });
        new Chart(document.getElementById("bar-chart-horizontal"), {
            type: 'horizontalBar',
            data: {
                labels: ["5", "4", "3", "2", "1"],
                datasets: [
                    {
                        label: "",
                        data: [{{$product->getRating(5)}},{{$product->getRating(4)}},{{$product->getRating(3)}},{{$product->getRating(2)}},{{$product->getRating(1)}}],
                        backgroundColor: ["rgba(104, 194, 148, 0.8)", "rgba(164,213, 114, 0.8)", "rgba(254, 211, 48, 0.8)", "rgba(253, 162, 71, 0.8)", "rgba(252, 122, 64, 0.8"],
                    }
                ]
            },
            options: {
                legend: { display: false },
                title: {
                    display: false,
                    text: ''
                },
                scales: {
                    xAxes: [{
                        display: false,
                        gridLines: {
                            display:false
                        }
                    }],
                    yAxes: [{
                        gridLines: {
                            display:false
                        }
                    }]
                }
            }
        });

        var rating = document.getElementById("rating").value;
        $(".my-rating").starRating({
           totalStars: 5,
            starShape: 'rounded',
            starSize: 40,
            emptyColor: 'lightgray',
            hoverColor: '#ffad10',
            activeColor: '#ff9811',
            useGradient: true,
            forceRoundUp:true,
            minRating: 1,
            useFullStars: true,
            disableAfterRate: false,
            callback: function(currentRating, $el){
                rating = currentRating;
                //alert('rated ' + currentRating);
                //alert(rating);
                //console.log('DOM element ', $el);
            }
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(".btn-submit").click(function(e) {
            e.preventDefault();
            let rating_desc = document.getElementById("rating_desc").value;
            let product_id  = document.getElementById("productid").value;
            let user_id      = document.getElementById("user_id").value;
           //alert(user_id);
//            alert(rating);
            $('#send_form').html('Envoie en cours...');
            $.ajax({
                type:'POST',
                url:'{{route('review')}}',
                data:{user_id:user_id,product_id:product_id,rating_desc:rating_desc, rating:rating},
                    success: function( response ) {
                        $('#send_form').html('Envoyer');
                        $('#res_message').show();
                        $('#res_message').html(response.msg);
                        $('#msg_div').removeClass('d-none');
                        $('#send_form').hide();
                        document.getElementById("myform").reset();
                        setTimeout(function(){
                            $('#res_message').hide();
                            $('#msg_div').hide();
                        },1000);
                    }
//                }
//            });
            });
        });
    </script>
@endsection

@section('post_header')
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" 		   content="{{$product->name}}" />
    <meta property="og:site_name" 	   content="YanfoShop">
    <meta property="og:title"          content="{{$product->name}}" />
    <meta property="og:description"    content="{{$product->name}}" />
    <meta property="og:image"          content="{{$product->image_url}}" />
    <meta property="og:type" 	       content="article">
    <meta property="og:url"            content="https://yanfoma.com">
@endsection