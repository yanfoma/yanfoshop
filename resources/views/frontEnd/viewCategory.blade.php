@extends('layouts.frontEnd.app')

@section('title')
	{{$store->name}} : {{$category->name}}
@endsection
@section('style')
	<link rel="stylesheet" href="{{ asset('css/frontEnd/shop.css') }}"                 type="text/css" media="screen,projection">
	<style>
		.breadcrumb1{
			background: #18b7bf;
			color: white;!important;
		}
		.breadcrumb1 .breadcrumb-item a, .breadcrumb1 .breadcrumb-item .fa{
			color: white;!important;
		}
	</style>
@endsection
@section('content')
	<div class="shop-page-area bg-white ptb-30">
		<div class="container2">
			<div class="row">
				<div class="col-lg-9 order-1 order-lg-2">

{{--					<div class="banner-area">--}}
{{--						<div class="imgbanner imgbanner-2">--}}
{{--							<a href="#">--}}
{{--								<img src="{{$category->image_url}}" alt="banner">--}}
{{--							</a>--}}
{{--						</div>--}}
{{--					</div>--}}

					<div class="shop-filters mt-30">
						<div class="shop-filters-viewmode">
							<button class="is-active" data-view="grid"><i class="ion ion-ios-keypad"></i></button>
							<button data-view="list" class="is-active"><i class="ion ion-ios-list"></i></button>
						</div>
						<span class="shop-filters-viewitemcount">{{$category->name}} {{$products->count()}} Résultat(s)</span>
						<div class="shop-filters-sortby">
							{{--<b>Trier Par:</b>--}}
							{{--<div class="select-sortby">--}}
								{{--<button class="select-sortby-current">Relevance</button>--}}
								{{--<ul class="select-sortby-list dropdown-list">--}}
									{{--<li><a href="#">Relevance</a></li>--}}
									{{--<li><a href="#">Name, A-Z</a></li>--}}
									{{--<li><a href="#">Name, Z-A</a></li>--}}
									{{--<li><a href="#">Prix, Decroissant</a></li>--}}
									{{--<li><a href="#">Prix, Croissant</a></li>--}}
								{{--</ul>--}}
							{{--</div>--}}
						</div>
					</div>
					<div class="shop-page-products mt-30">
						@if($products->count())
							<div class="row no-gutters">
								@foreach($products as $product)
									<div class="col-lg-4 col-md-4 col-sm-6 col-12">
										<!-- Single Product -->
										<article class="hoproduct product">
											<div class="hoproduct-image">
												<a class="hoproduct-thumb" href="{{route('shop.single',['store_slug' => $store->slug, 'slug' => $product->slug])}}">
													<img class="hoproduct-frontimage" src="{{$product->image_url}}" alt="{{$product->name}}">
												</a>
												<ul class="hoproduct-actionbox">
													<li><a href="{{route('store.cart.addDirect',['store_slug' => $store->slug,'id' => $product->id])}}"><i class="lnr lnr-cart"></i></a></li>
													<li><a href="{{route('shop.single',['store_slug' => $store->slug, 'slug' => $product->slug])}}"><i class="lnr lnr-eye"></i></a></li>
													@if($product->is_wished_by_auth_user())
														<li>
															<a class="wished" href="{{route('product.unwishlist', ['store_slug' => $store->slug, 'id' => $product->id ])}}">
																<i class="lnr lnr-heart"></i>
															</a>
														</li>
													@else
														<li>
															<a class="#" href="{{route('product.wishlist', ['store_slug' => $store->slug, 'id' => $product->id ])}}">
																<i class="lnr lnr-heart"></i>
															</a>
														</li>
													@endif
												</ul>
												<ul class="hoproduct-flags">
													@if($product->onSale && $product->saleType == 2)
														<li class="flag-pack">Promo</li>
														<li class="flag-discount">-{{ $product->salePercentage }} %</li>
													@endif
												</ul>
											</div>
											<div class="hoproduct-content">
												<h5 class="hoproduct-title">
													<a href="{{route('shop.single',['store_slug' => $store->slug, 'slug' => $product->slug])}}">{{$product->name}}
													</a>
												</h5>
												<div class="hoproduct-pricebox">
													<div class="pricebox">
														@if($product->onSale)
															<del class="oldprice">{{number_format($product->price, 0 , ',' , ' ')}} </del>
															<span class="price">{{number_format($product->sale_amount, 0 , ',' , ' ')}} CFA </span>
														@else
															<span class="price">{{number_format($product->price, 0 , ',' , ' ')}} CFA </span>
														@endif
														<div class="float_right">
															<i class="fa fa-exclamation-triangle" style="color: yellow;"></i>
															Min: {{$product->minQty}}
														</div>
													</div>
												</div>
												<p class="hoproduct-content-description" style="text-align: justify!important;"> {!!html_entity_decode(str_limit(strip_tags($product->description),500))!!}</p>
											</div>
										</article>
									</div>
								@endforeach
							</div>
						@else
							<div class="box text-center">
								<h3>Aucun Produit pour l'instant !!!</h3>
							</div>
						@endif
					</div>
				</div>
				<div class="col-lg-3 order-2 order-lg-1">
					<div class="shop-widgets">
						@if($products->count())
							<div class="single-widget widget-categories">
								<h5 class="widget-title">Nos Categories</h5>
								<ul>
									@foreach($categories as $category)
										<li><a href="{{route('viewCategory',['store_slug' => $store->slug, 'name' => $category->name ])}}">{{$category->name}} <span>{{$category->products->count()}}</span></a></li>
									@endforeach
								</ul>
							</div>
						@endif

						{{--<div class="shop-widgetbox mt-30">--}}
							{{--<div class="single-widget widget-filters">--}}
								{{--<h5 class="widget-title">Filter By Price</h5>--}}
								{{--<div class="widget-filter-inner">--}}
									{{--<div class="range-slider" data-range_min="0" data-range_max="1000" data-cur_min="300" data-cur_max="800">--}}
										{{--<div class="bar" style="left: 57px; width: 117px;"></div>--}}
										{{--<span class="range-slider-leftgrip" tabindex="0" style="left: 57px;"></span>--}}
										{{--<span class="range-slider-rightgrip" tabindex="0" style="left: 154px;"></span>--}}
									{{--</div>--}}
									{{--<div class="single-widget-range-price">--}}
										{{--<b>Price: </b> $<span class="range-slider-leftlabel">300</span> - $<span class="range-slider-rightlabel">800</span>--}}
									{{--</div>--}}
								{{--</div>--}}
							{{--</div>--}}
						{{--</div>--}}
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('post_header')
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" 		   content="YanfomaShop" />
	<meta property="og:site_name" 	   content="Yanfoma">
	<meta property="fb:app_id" 		   content="400025927061215">
	<meta property="og:title"          content="YanfomaShop" />
	<meta property="og:description"    content="YanfomaShop" />
	<meta property="og:image"          content="{{asset('images/frontEnd/YanfomaShop.png')}}" />
	<meta property="og:type" 	       content="article">
	<meta property="og:url"            content="https://yanfoma.tech">
@endsection