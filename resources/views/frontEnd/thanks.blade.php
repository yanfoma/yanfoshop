@extends('layouts.frontEnd.app')

@section('title')
    Transaction Complète
@endsection

@section('style')
    <style>
        .inner-banner               {  margin-bottom: 30px !important;  }
        .inner-banner .box h3       {  font-size: 20px;  }
        .progressbar li:before {
            content: counter(step);
            counter-increment: step;
            width: 60px;
            height: 60px;
            border: 3px solid #ddd;
            display: block;
            font-size: 25px;
            font-weight: bold;
            text-align: center;
            margin: 0 auto 10px auto;
            border-radius: 100px;
            line-height: 60px;
            background-color: #18b7bf!important;
            color: white;
            position: relative;
            z-index: 1;
        }

        .progressbar li{
            color: #18b7bf!important;
        }

        .progressbar li.active2 + li:after{
            background: #18b7bf!important;
        }

        .progressbar li:after {
            content: '';
            position: absolute;
            width: 100%;
            height: 3px;
            background-color: #18b7bf;
            top: 30px;
            left: -50%;
            z-index: 0;
        }

        .progressbar li:first-child:after {
            content: none;
        }

        .progressbar li:first-child:before {
            background: #18b7bf!important;
            border-color: #18b7bf!important;
        }
    </style>
@endsection

@section('content')
    <div class="inner-banner text-center">
        <div class="container">
            <div class="box" style="margin-bottom: 40px;">
                <h2><strong>Votre commande a été bien placée  Mais elle n'est pas confirmée!!!</strong></h2>
            </div><!-- /.box -->
        </div><!-- /.container -->
    </div>
    <ul class="progressbar">
        <li >Panier</li>
        <li >Confirmer Commande</li>
        <li class="active">Terminé</li>
    </ul>

    <section class="checkout-area">
        <div class="container">
            <div class="row bottom">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="thanks">
                                <div class="textThank">
                                    <p>
                                        Votre commande a été recue.
                                        Veuillez effectuer le paiement dans les plus bref delais pour finaliser votre commande.
                                    </p>
                                    <p style="text-align: center;!important;">
                                        Merci de Votre consideration!!!
                                    </p>
                                </div>
                                {{--<img src="{{asset('images/frontEnd/thankYouPurshased.png')}}" class="img">--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection