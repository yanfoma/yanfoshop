@extends('layouts.frontEnd.appHome')

@section('title')
    Yanfoma Soko
@endsection

@section('content')
        <style type="text/css">
            .ajax-load{
                background: #e1e1e1;
                padding: 10px 0px;
                width: 100%;
            }
        </style>
        <div class="shop" style="margin-bottom: 10px!important;">
            @include('layouts.frontEnd.slideShowHome')
            <div class="container2">
                <div class="shop-page-products mt-30">
                    <div class="row no-gutters">
                        @foreach($products as $product)
                            <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                                <article class="hoproduct product2">
                                    <div class="hoproduct-image">
                                        <a class="hoproduct-thumb" href="{{route('shop.single',['store_slug' =>$product->store->slug,'slug' => $product->slug])}}">
                                            <img class="hoproduct-frontimage" src="{{$product->image_url}}" alt="{{$product->name}}">
                                        </a>
                                        <ul class="hoproduct-actionbox">
                                            <li><a href="{{route('store.cart.addDirect',['store_slug' => $product->store->slug,'id' => $product->id])}}"><i class="lnr lnr-cart"></i></a></li>
                                            <li><a href="{{route('shop.single',['store_slug' => $product->store->slug, 'id' => $product->slug])}}"><i class="lnr lnr-eye"></i></a></li>
                                            @if($product->is_wished_by_auth_user())
                                                <li>
                                                    <a class="wished" href="{{route('product.unwishlist', ['store_slug' => $product->store->slug,  'id' => $product->id ])}}">
                                                        <i class="lnr lnr-heart"></i>
                                                    </a>
                                                </li>
                                            @else
                                                <li>
                                                    <a class="" href="{{route('product.wishlist', ['store_slug' => $product->store->slug, 'id' => $product->id ])}}">
                                                        <i class="lnr lnr-heart"></i>
                                                    </a>
                                                </li>
                                            @endif
                                            @if($product->is_liked_by_auth_user())
                                                <li><a class="unlike" href="{{route('product.unlike', [ 'store_slug' => $product->store->slug, 'id' => $product->id ])}}"><i class="lnr lnr-thumbs-up"></i></a></li>
                                            @else
                                                <li><a class="" href="{{route('product.like', [ 'store_slug' => $product->store->slug, 'id' => $product->id ])}}"><i class="lnr lnr-thumbs-up"></i></a></li>
                                            @endif
                                        </ul>
                                        <ul class="hoproduct-flags">
                                            <li class="flag-pack">{{$product->store->name}}</li>
                                            @if($product->onSale && $product->saleType == 2)
                                                <li class="flag-discount">-{{ $product->salePercentage }} %</li>
                                            @endif
                                        </ul>
                                    </div>
                                    <div class="hoproduct-content">
                                        <h5 class="hoproduct-title product-title"><a href="{{route('shop.single',['store_slug' =>$product->store->slug,'slug' => $product->slug])}}">{{$product->name}}</a></h5>
                                        <div class="hoproduct-pricebox product-pricebox bg-blue-orange4">
                                            <div class="pricebox">
                                                <div class="float_left">
                                                    @if($product->onSale)
                                                        <del class="oldprice">{{number_format($product->price, 0 , ',' , ' ')}} </del>
                                                        <span class="price">{{number_format($product->sale_amount, 0 , ',' , ' ')}} CFA </span>
                                                    @else
                                                        <span class="price">{{number_format($product->price, 0 , ',' , ' ')}} CFA </span>
                                                    @endif
                                                </div>
                                                <div class="float_right">
                                                    <i class="fa fa-exclamation-triangle" style="color: yellow;"></i>
                                                    Min: {{$product->minQty}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
@endsection

@section('post_header')
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" 		   content="Yanfoma Soko" />
    <meta property="og:site_name" 	   content="Yanfoma">
    <meta property="fb:app_id" 		   content="400025927061215">
    <meta property="og:title"          content="Yanfoma Soko" />
    <meta property="og:description"    content="Yanfoma Soko" />
    <meta property="og:image"          content="https://res.cloudinary.com/yanfomaweb/image/upload/v1553782826/Yanfoma/metaImage2.png" />
    <meta property="og:type" 	       content="article">
    <meta property="og:url"            content="https://yanfoma.com">
@endsection

@section('scripts')
    <script>
        $('.owl-carousel').owlCarousel({
            loop:true,
            margin: 0,
            nav:false,
            autoplay:true,
            responsiveClass:true,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        });
    </script>

@endsection