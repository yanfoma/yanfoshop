@extends('layouts.frontEnd.app')

@section('title')
    Nous Contacter YanfoShop
@endsection
@section('content')
    <div class="contact-us-area ptb-30">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 widget3" style="margin-right: 10px!important;">
                    <div class="commentbox">
                        <h2>Envoyez Nous Un Email</h2>
                        <form action="{{route('email')}}" method="post" novalidate="novalidate" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="control-label">Nom <span class="required">*</span></label>
                                        <input type="text" name="name" class="form-control" value="{{ old('name') }}" placeholder="Votre Nom *" required>
                                        @if ($errors->has('name'))
                                            <div id="uname-error" class="error">{{ $errors->first('name') }}</div>
                                        @endif
                                    </div>
                                    <div class="col-md-6">
                                        <label class="control-label">Portable <span class="required">*</span></label>
                                        <input type="text" name="phone" class="form-control" value="{{ old('phone') }}" placeholder="Votre numero de portable (veuillez preciser l'indicatif) *" required>
                                        @if ($errors->has('phone'))
                                            <div id="uname-error" class="error">{{ $errors->first('phone') }}</div>
                                        @endif
                                    </div>
                                    <div class="col-md-12">
                                        <label class="control-label">Email<span class="required">*</span></label>
                                        <input type="email" name="email" class="form-control required email" value="{{ old('email') }}" placeholder="Votre e-mail *" required aria-required="true">
                                        @if ($errors->has('email'))
                                            <div id="uname-error" class="error">{{ $errors->first('email') }}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Sujet</label>
                                <input type="text" name="subject" class="form-control" value="{{ old('subject') }}" placeholder="Sujet">
                                @if ($errors->has('subject'))
                                    <div id="uname-error" class="error">{{ $errors->first('subject') }}</div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label class="control-label">Message <span class="required">*</span></label>
                                <textarea name="message" class="form-control" placeholder="Votre Message...." aria-required="true" rows="3">{{ old('message') }}</textarea>
                                @if ($errors->has('message'))
                                    <div id="uname-error" class="error">{{ $errors->first('message') }}</div>
                                @endif
                            </div>
                            <div class="text-right">
                                <input type="submit" class="btn btn-primary" value="Envoyer">
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-4 pt-50 pt-lg-20 widget3 coordonees">
                    <h2>Nos Coordonnées</h2>
                    <div class="contact-content">
                        @if($store->id == 1)
                            <div class="single-content">
                                    <span class="single-content-icon">
                                        <i class="lnr lnr-map-marker"></i>
                                    </span>
                                Ouagadougou - Burkina Faso <br> Abidjan, Cote d'Ivoire<br>
                                Yaounde, Cameroun Kigali, Rwanda
                            </div>
                        @else
                            @if($store->addresse)
                                <div class="single-content">
                                    <span class="single-content-icon">
                                        <i class="lnr lnr-map-marker"></i>
                                    </span>
                                    {{$store->addresse}}
                                </div>
                            @endif
                        @endif
                        @if($store->id == 1)
                            <div class="single-content">
                                <span class="single-content-icon">
                                    <i class="lnr lnr-phone-handset"></i>
                                </span>
                                <a href="#">+(226) 71 33 15 23</a><br>
                                <a href="#">+(226) 74 33 42 77</a><br>
                                <a href="#"> +(886) 989 59 72 35</a>
                            </div>
                        @else
                            @if($store->phone)
                                <div class="single-content">
                                    <span class="single-content-icon">
                                        <i class="lnr lnr-phone-handset"></i>
                                    </span>
                                    <a href="#">{{$store->phone}}</a>
                                </div>
                            @endif
                        @endif
                        @if($store->email)
                            <div class="single-content">
                                <span class="single-content-icon">
                                    <i class="lnr lnr-envelope"></i>
                                </span>
                                <a href="#">{{$store->email}}</a>
                            </div>
                        @endif
                        <div class="singleSocial">
                            <ul class="list list-icons list-icons-style-3 mt-xlg">
                                @if($store->facebook)
                                    <li><a href="{{$store->facebook}}"><i class="fa fa-fw fa-facebook"></i> <strong>Facebook</strong></a></li>
                                @endif
                                @if($store->messenger)
                                    <li><a href="{{$store->messenger}}" target="_blank"><i class="fa fa-fw fa-comments"></i> <strong>Messenger</strong></a></li>
                                @endif
                                @if($store->whatsapp)
                                    <li><a href="{{$store->whatsapp}}"><i class="fa fa-fw fa-whatsapp"></i> <strong>Whatsapp</strong></a></li>
                                @endif
                                @if($store->linkedIn)
                                    <li><a href="{{$store->linkedIn}}"><i class="fa fa-fw fa-linkedin"></i> <strong>LinkedIn</strong></a></li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection