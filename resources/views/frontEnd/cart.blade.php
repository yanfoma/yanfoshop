@extends('layouts.frontEnd.app')

@section('title')
    {{$store->name}}: Mon Panier
@endsection

@section('style')
    <style>
        .inner-banner               {  margin-bottom: 30px !important;  }
        .inner-banner .box h3       {  font-size: 20px;  }
        .cart-table thead tr        {  border: 1px solid #f6f6f6 !important;  }
        .cart-section .table-outer  {  box-shadow: none !important;  }
        .pull-left                  {  padding-top: 20px;  padding-bottom: 30px;  }
        .btn-primary{color: #fff!important;}
        .progressbar li:before {
            content: counter(step);
            counter-increment: step;
            width: 60px;
            height: 60px;
            border: 3px solid #ddd;
            display: block;
            font-size: 25px;
            font-weight: bold;
            text-align: center;
            margin: 0 auto 10px auto;
            border-radius: 100px;
            line-height: 60px;
            background-color: #ddd!important;
            color: white;
            position: relative;
            z-index: 1;
        }

        /*.progressbar li.active + li:after{*/
             /*background: #18b7bf!important;*/
        /*}*/
        .progressbar li:after {
            content: '';
            position: absolute;
            width: 100%;
            height: 3px;
            background-color: #ddd;
            top: 30px;
            left: -50%;
            z-index: 0;
        }
        .progressbar li:first-child:after {
            content: none;
        }
    </style>
@endsection

@section('content')
    @if($cart->count())
        <div class="inner-banner text-center">
            <div class="container2">
                <div class="box">
                    <h3>Dans Votre Panier: {{$cart->count()}} Produits</h3>
                </div><!-- /.box -->
                <ul class="progressbar">
                    <li class="active">Panier</li>
                    <li>Confirmer Commande</li>
                    <li>Terminé</li>
                </ul>
            </div><!-- /.container -->
        </div>
        <section class="cart-section">
            <div class="container2">
                <!--Cart Outer-->
                <div class="cart-outer">
                    <div class="table-outer">
                        <table class="cart-table hoverable">
                            <thead class="cart-header">
                            <tr>
                                <th></th>
                                <th class="prod-column"></th>
                                <th>Nom</th>
                                <th>Quantité</th>
                                <th class="price">Prix</th>
                                <th>Total</th>
                                <th>Supprimer</th>
                            </tr>
                            </thead>

                            <tbody>
                            @php $index = 1; @endphp
                            @foreach($cart as $product)
                                <tr>
                                    <td class="unit-price"></td>
                                    <td colspan="2" class="prod-column">
                                        <div class="available-info">
                                            <span class="icon fa fa-check" style="margin-left: -80px"></span>
                                        </div>
                                        <div class="column-box">
                                            <figure class="prod-thumb">
                                                <a href="#">
                                                    <img src="{{asset($product->product->image_url)}}"
                                                         alt="{{$product->product->name}}" width="70" height="70">
                                                </a>
                                            </figure>
                                            <h3 class="prod-title padd-top-20">{{$product->product->name}}</h3>
                                        </div>
                                    </td>
                                    <td class="qty">
                                        <input class="quantity" type="text" value="{{$product->product_qty}}" name="quantity"
                                               readonly>
                                    </td>
                                    <td class="price">{{$product->product_price}} CFA</td>
                                    <td class="sub-total">{{$product->product_total}} CFA</td>
                                    <td class="remove">
                                        <a href="{{route('store.cart.delete',['store_slug' => $store->slug,'id'=>$product->product_id])}}" class="remove-btn">
                                            <span class="icon fa fa-close" style="color: red"></span>
                                        </a>
                                    </td>
                                </tr>
                                @php $index++; @endphp
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="update-cart-box clearfix">
                        <div class="pull-left">
                            <h3>Total:</h3>
                        </div>
                        <div class="pull-right">&emsp;
                            <h3>{{getStoreTotal($store->id)}} CFA</h3>
                        </div>
                    </div>
                    <div class="pull-right">
                        <a href="{{route('store.cart.checkout',['store_slug' => $store->slug])}}" class="btn btn-primary commande">Passer la commande</a>
                    </div>
                </div>
            </div>
        </section>
    @else
        <div class="inner-banner text-center padd-60">
            <div class="container">
                <img src="{{asset('images/empty-cart.png')}}">
                <div class="box">
                    <h2 style="font-size: 45px;">Votre Panier est Vide !!!</h2>
                    <span>Ajouter des produits pour commencer</span>
                </div><!-- /.box -->
                <div class="col-md-12 but">
                    {{--<a href="{{route('shop',['store_slug' => $store->slug])}}" class="btn btn-primary">Retourner à la boutique</a> &emsp;--}}
                </div>
            </div>
        </div>
    @endif
@endsection

@section('scripts')
@endsection