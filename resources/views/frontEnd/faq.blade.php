@extends('layouts.frontEnd.app')

@section('title')
    Foire Aux Questions YanfoShop
@endsection
@section('content')
    <section class="breadcumb_area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <div class="breadcumb_section">
                        <div class="page_title">
                            <h3>
                                Nous répondons à toutes vos questions
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="faq">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="widget3 margin-bottom-none">
                        <div class="section-header">
                            <h5 class="heading-design-h5">
                                Foire Aux Questions
                            </h5>
                        </div>
                        <div class="panel-group margin-bottom-none" id="faqAccordion">
                            @if($faqs->count())
                                @foreach($faqs as $faq)
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" class="js-accordion-open"  data-id="{{$faq->id}}" data-parent="#faqAccordion" href="#collapse{{$faq->id}}">{{$faq->question}}</a>
                                            </h4>
                                        </div>
                                        <div id="collapse{{$faq->id}}" class="panel-collapse collapse out">
                                            <div class="panel-body faqReponse">
                                                <h5><span class="label label-primary">Reponse</span></h5>
                                                <br>
                                                <p>{!! $faq->response !!}</p>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="aide">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="widget3 margin-bottom-none">
                        <div class="section-header">
                            <h5 class="heading-design-h5 center">
                                Nous contacter
                            </h5>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 margin-top">
                                <div class="panel panel-default text-white bg-primary">
                                    <div class="panel-heading bg-primary">Facebook</div>
                                    <div class="panel-body">
                                        <p class="card-text text-white">Rejoignez notre groupe Facebook yanfomashop!!!</p><br>
                                    </div>
                                    <div class="panel-footer bg-primary">
                                        <a class="btn btn-primary center-block" href="https://www.facebook.com/groups/2171625413121414/">Rejoindre</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 margin-top">
                                <div class="panel panel-default text-white bg-primary">
                                    <div class="panel-heading bg-primary">Messanger</div>
                                    <div class="panel-body">
                                        <p class="card-text text-white">Contactez nous sur messenger pour toutes vos questions. </p><br>
                                    </div>
                                    <div class="panel-footer bg-primary">
                                        <a class="btn btn-primary center-block" href="https://www.messenger.com/t/yanfoma">Envoyer</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clear-fix"></div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 margin-top">
                                <div class="panel panel-default text-white bg-primary">
                                    <div class="panel-heading bg-primary text-white">Whatsapp</div>
                                    <div class="panel-body">
                                        <p class="card-text text-white">Contacter nous sur whatsapp </p>
                                    </div>
                                    <div class="panel-footer card-text text-white bg-primary">
                                        <a class="btn btn-primary center-block" href="https://chat.whatsapp.com/EsCjgPWHuPf39ujQKANX5C">Yanfoshop</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 margin-top">
                                <div class="panel panel-default bg-primary">
                                    <div class="panel-heading bg-primary">Email</div>
                                    <div class="panel-body">
                                        <p class="card-text text-white">info@yanfoma.tech</p>
                                    </div>

                                    <div class="panel-footer card-text text-white bg-primary">
                                        <a class="btn btn-primary center-block" href="mailto:info@yanfoma.tech">Contacter Nous !!!</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script>
        $(window).load(function(){
               if (window.location.hash == "#Bitcoin") { $('body').scrollTo('#Bitcoin'); $('.js-accordion-open[data-id=8]').trigger('click');}
               else if (window.location.hash == "#tyjh") {$('.js-accordion-open[data-id=32]').trigger('click');}
               else if (window.location.hash == "#ngfc") {$('.js-accordion-open[data-id=31]').trigger('click');}
               else {return false;}
           });
    </script>
@endsection