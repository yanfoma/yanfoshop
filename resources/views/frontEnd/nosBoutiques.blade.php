@extends('layouts.frontEnd.appHome')

@section('title')
    Yanfoma Soko
@endsection

@section('content')
        <style type="text/css">
            .ajax-load{
                background: #e1e1e1;
                padding: 10px 0px;
                width: 100%;
            }
        </style>
        <div class="mt-30 nosBoutiques pb-4" >
            <div class="container">
                <div class="section-title">
                    <h3>Nos Boutiques</h3>
                </div>
                <div class="row">
                    @foreach($stores as $store)
                        <div class="col-lg-3 store">
                            <a href="{{route('shop',$store->slug)}}">
                                <div class="cover">
                                    <img src="{{$store->image}}" alt="store image">
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
@endsection

@section('post_header')
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" 		   content="Yanfoma Soko" />
    <meta property="og:site_name" 	   content="Yanfoma">
    <meta property="fb:app_id" 		   content="400025927061215">
    <meta property="og:title"          content="Yanfoma Soko" />
    <meta property="og:description"    content="Yanfoma Soko" />
    <meta property="og:image"          content="https://res.cloudinary.com/yanfomaweb/image/upload/v1553782826/Yanfoma/metaImage2.png" />
    <meta property="og:type" 	       content="article">
    <meta property="og:url"            content="https://yanfoma.com">
@endsection

@section('scripts')
    <script>
        $('.owl-carousel').owlCarousel({
            loop:true,
            margin: 0,
            nav:false,
            autoplay:true,
            responsiveClass:true,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        });
    </script>

@endsection