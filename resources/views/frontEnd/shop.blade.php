@extends('layouts.frontEnd.app')

@section('title')
    {{$store->name}}
@endsection

@section('style')

@endsection
@section('content')
        <div class="shop" style="margin-bottom: 10px!important;">
            @include('layouts.frontEnd.slideShow')
            <div class="container2">
                 @include('layouts.frontEnd.bonDeals')
                 @include('layouts.frontEnd.categoriesListing')
{{--                 @if($store->id == 1)--}}
{{--                   @include('layouts.frontEnd.vps')--}}
{{--                 @endif--}}

               @include('layouts.frontEnd.productsListing')

               @if($store->id == 1)
                   @include('layouts.frontEnd.newsletter')
                   @include('layouts.frontEnd.help')
               @endif
            </div>
@endsection

@section('post_header')
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" 		   content="{{$store->name}}" />
    <meta property="og:site_name" 	   content="{{$store->name}}">
    <meta property="fb:app_id" 		   content="400025927061215">
    <meta property="og:title"          content="{{$store->name}}" />
    <meta property="og:description"    content="{{$store->name}}" />
    <meta property="og:image"          content="{{asset('images/frontEnd/YanfomaShop.png')}}" />
    <meta property="og:type" 	       content="article">
    <meta property="og:url"            content="https://yanfoma.com/"{{$store->slug}}>
    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://metatags.io/">
    <meta property="og:title" content="Meta Tags — Preview, Edit and Generate">
    <meta property="og:description" content="With Meta Tags you can edit and experiment with your content then preview how your webpage will look on Google, Facebook, Twitter and more!">
    <meta property="og:image" content="{{$store->image}}">
    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:url" content="https://metatags.io/">
    <meta property="twitter:title" content="Meta Tags — Preview, Edit and Generate">
    <meta property="twitter:description" content="With Meta Tags you can edit and experiment with your content then preview how your webpage will look on Google, Facebook, Twitter and more!">
    <meta property="twitter:image" content="{{$store->image}}">
@endsection

@section('scripts')
     <script>
         $('.owl-carousel').owlCarousel({
             loop:true,
             margin: 0,
             nav:false,
             autoplay:true,
             responsiveClass:true,
             responsive:{
                 0:{
                     items:1
                 },
                 600:{
                     items:1
                 },
                 1000:{
                     items:1
                 }
             }
         });

     </script>
@endsection