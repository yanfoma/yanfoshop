@extends('layouts.frontEnd.appHome')

@section('title')
    Yanfoma Soko
@endsection

@section('content')
        <style type="text/css">
            .ajax-load{
                background: #e1e1e1;
                padding: 10px 0px;
                width: 100%;
            }
        </style>
        <div class="shop" style="margin-bottom: 10px!important;">
            @include('layouts.frontEnd.slideShowHome')
            <div class="container2">
                @include('layouts.frontEnd.nosBoutiques')
                @include('layouts.frontEnd.storesProducts')
                @include('layouts.frontEnd.promoAndDeals')
                @include('layouts.frontEnd.vins')
{{--             @include('layouts.frontEnd.senseurHome')--}}
                @include('layouts.frontEnd.homeProductsListing')

{{--                @include('layouts.frontEnd.social')--}}
{{--                <div class="banners-area bg-white">--}}
{{--                    <div class="row">--}}
{{--                        <!-- Single Banner -->--}}
{{--                        <div class="col-md-6">--}}
{{--                            <div class="imgbanner mt-30">--}}
{{--                                <a href="product-details.html">--}}
{{--                                    <img src="http://demo.devitems.com/haltico-v3/haltico/images/banner/banner-image-13.jpg" alt="bannner image">--}}
{{--                                </a>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <!--// Single Banner -->--}}

{{--                        <!-- Single Banner -->--}}
{{--                        <div class="col-md-6">--}}
{{--                            <div class="imgbanner mt-30">--}}
{{--                                <a href="product-details.html">--}}
{{--                                    <img src="http://demo.devitems.com/haltico-v3/haltico/images/banner/banner-image-13.jpg" alt="bannner image">--}}
{{--                                </a>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <!--// Single Banner -->--}}

{{--                        <div class="banner-area">--}}
{{--                            <div class="container">--}}
{{--                                <div class="imgbanner imgbanner-2 mt-30">--}}
{{--                                    <a href="{{route('shop','yanfoShop')}}">--}}
{{--                                        <img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1554454631/Yanfoma/banner.png" alt="banner">--}}
{{--                                    </a>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="banner-area">--}}
{{--                    <div class="container2">--}}
{{--                        <div class="imgbanner imgbanner-2 mt-30">--}}
{{--                            <a href="product-details.html">--}}
{{--                                <img src="http://demo.devitems.com/haltico-v3/haltico/images/banner/banner-image-10.jpg" alt="banner">--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>
        </div>
@endsection

@section('post_header')
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" 		   content="Yanfoma Soko" />
    <meta property="og:site_name" 	   content="Yanfoma">
    <meta property="fb:app_id" 		   content="400025927061215">
    <meta property="og:title"          content="Yanfoma Soko" />
    <meta property="og:description"    content="Yanfoma Soko" />
    <meta property="og:image"          content="https://res.cloudinary.com/yanfomaweb/image/upload/v1553782826/Yanfoma/metaImage2.png" />
    <meta property="og:type" 	       content="article">
    <meta property="og:url"            content="https://yanfoma.com">
@endsection

@section('scripts')
    <script>
        $('.owl-carousel').owlCarousel({
            loop:true,
            margin: 0,
            nav:false,
            autoplay:true,
            responsiveClass:true,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        });
    </script>

@endsection
