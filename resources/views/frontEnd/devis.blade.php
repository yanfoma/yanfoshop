@extends('layouts.frontEnd.app')

@section('title')
    Demander un Devis YanfoShop
@endsection
@section('content')
    <section class="breadcumb_area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <div class="breadcumb_section">
                        <div class="page_title">
                            <h3>
                                Demander un Devis
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="faq">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="widget3">
                        <div class=" blog-box">
                            <div class="panel-body">
                                <h5 class="heading">Comment demander un devis</h5>
                                <p class="par">
                                    C'est très simple. Envoyez nous un email à l'adresse suivante:  info@yanfoma.tech
                                    avec la liste complete de vos produits
                                    et leur spécifications.
                                    Votre requête sera traitée en 24h Chrono.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection