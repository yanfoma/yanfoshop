@extends('layouts.frontEnd.app')

@section('title')
    Tout Savoir Sur Ma Commande
@endsection
@section('style')
    <style>
        .Bitcoin {
            height: 400px!important;
        }
    </style>
@endsection
@section('content')
    <section class="breadcumb_area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <div class="breadcumb_section">
                        <div class="page_title">
                            <h3>
                                Tout Savoir Sur Ma Commande
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="faq">
        <div class="container">
            <div class="row" id="Bitcoin">
                <div class="col-sm-12">
                    <div class="widget5 Bitcoin">
                        <div class=" blog-box">
                            <div class="panel-body">
                                <h5 class="heading">Modes d'Expédition et Délais de Livraison</h5>
                                <p class="par2">
                                    Vous recevrez un e-mail de confirmation après avoir passé une commande et un autre e-mail après l'expédition de votre commande.
                                    Le second contient les informations de suivi et des instructions sur la manière de suivre votre colis.
                                    Le temps total nécessaire pour recevoir votre commande se décrit comme suit:<br>
                                    <strong>Temps de traitement: </strong> est la somme du <strong>Temps De Traitement</strong> et du <strong>Temps d'Expédition</strong>.<br>
                                    <strong>Délai de traitement</strong> = Le temps nécessaire pour préparer vos articles à expédier. Cela comprend la préparation de vos articles, l'exécution de contrôles de qualité et l'emballage pour l'expédition.
                                    <br><strong>Délai d'expédition</strong> = Le temps nécessaire pour que vos articles arrivent à votre destination.<br><br>
                                    <span style="font-style: italic; color: #25b6c4; font-weight: bold;">Le délai de livraison total est calculé à partir du moment où votre commande est passée jusqu'au moment où elle vous est livrée.</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" >
                <div class="col-sm-12">
                    <div class="widget5">
                        <div class=" blog-box">
                            <div class="panel-body">
                                <h5 class="heading">Comment Suivre Ma Commande?</h5>
                                <p class="par2" id="EcobankPay">
                                    Une fois votre commande expédiée, Vous pouvez tracker votre commande à travers votre espace personnel.<br>
                                    Pour cela connectez-vous puis clicquez sur: <br>
                                    1- <strong>"Mon Profile"</strong> <br>
                                    2- Sur le menu latéral clicquez sur <strong>"Mes Commandes"</strong>.
                                    Vous verrez la liste de vos commandes et celles qui ont été expédiées.<br>
                                    3- Sur chacune de ces commandes cliquez sur le boutton <strong>"Suivre"</strong> et vous verrez où se trouve votre commande.
                                    <br>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" id="OrangeMoney">
                <div class="col-sm-12">
                    <div class="widget5">
                        <div class=" blog-box">
                            <div class="panel-body">
                                <h5 class="heading">Où et Comment Récupérer Ma Commande ? </h5>
                                <p class="par2">
                                    <strong>Dans les pays où Yanfoma est représenté</strong>, notre équipe sur place se chargera de collecter le colis
                                    et vous contactera pour que vous définissiez le meilleur moyen pour vous d’entrer en possession de votre bien.<br><br>
                                    {{--2- <strong>Dans les pays non-couverts par Yanfoma</strong>, le colis sera mis à votre nom et adresse et vous le collecterez vous-même.--}}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" id="CCK">
                <div class="col-sm-12">
                    <div class="widget5">
                        <div class=" blog-box">
                            <div class="panel-body">
                                <h5 class="heading">Zones de Couverture de YanfoShop </h5>
                                <div class="row">
                                    <div class="col-sm-12 panel-image">
                                        <figure>
                                            <img  src="https://res.cloudinary.com/yanfomaweb/image/upload/v1553174144/Yanfoma/mapYanfoma.png" alt="logo yanfoSho" >
                                            <figcaption class="text-center">Yanfoma est présent dans trois (03) pays pour l'instant</figcaption>
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" id="DepotBancaire">
                <div class="col-sm-12">
                    <div class="widget5">
                        <div class=" blog-box">
                            <div class="panel-body">
                                <h5 class="heading">Je ne trouve pas mon produit. Comment Faire?</h5>
                                <p class="par2">
                                    Cher Client, pas de panique,ne vous inquiétez pas nous avons une bonne nouvelle.
                                    Veuillez juste nous demander un devis en envoyant un e-mail à info@yanfoma.tech
                                    avec votre liste de produits recherchés.
                                    Nous traiterons votre requête en 24h.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    {{--<script>--}}
        {{--$(window).load(function(){--}}
            {{--if (window.location.hash == "#Bitcoin") { $('body').scrollTo('#Bitcoin');}--}}
            {{--else if (window.location.hash == "#tyjh") {$('.js-accordion-open[data-id=32]').trigger('click');}--}}
            {{--else if (window.location.hash == "#ngfc") {$('.js-accordion-open[data-id=31]').trigger('click');}--}}
            {{--else {return false;}--}}
        {{--});--}}
    {{--</script>--}}
@endsection