@extends('layouts.frontEnd.app')

@section('title')
	YanfomaShop
@endsection

@section('style')
	<link rel="stylesheet" href="{{ asset('css/frontEnd/shop.css') }}"                 type="text/css" media="screen,projection">
	<style>
		.breadcrumb1{
			background: #18b7bf;
			color: white;!important;
		}
		.breadcrumb1 .breadcrumb-item a, .breadcrumb1 .breadcrumb-item .fa{
			color: white;!important;
		}
		.categories-list{
			margin-bottom: 100px;
		}
	</style>
@endsection
@section('content')
	<div class="shop" style="margin-bottom: 10px!important;">
		@include('layouts.frontEnd.slideShow')
		<!-- Categories List Page -->
			<section class="categories-list main-categories-list">
				<div class="container2">
					<div id="Restaurant" class="row categories">
						<div class="col-lg-2 col-md-2 col-sm-2">
							<ol class="breadcrumb breadcrumb1">
								<li class="breadcrumb-item active">
									<i class="fa fa-arrow-circle-o-left"></i>
									<a href="{{ URL::previous() }}">Retour</a>
								</li>
							</ol>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12">
							<ol class="breadcrumb text-center">
								<li class="breadcrumb-item text-left"><a href="{{route('home')}}">{{trans('app.home')}}</a></li>
								<li class="breadcrumb-item text-right"><a href="#">Catégories</a></li>
							</ol>
						</div>
						@foreach($categories as $categorie)
							<div class="col-lg-3 col-md-3 col-sm-3">
								<div class="widget blue-widget">
									<div class="widget-header">
										<h4>{{$categorie->name}}</h4>
										<h5>{{$categorie->products->count() ? $categorie->products->count()." Produits" : "Aucun Produit" }} </h5>
									</div>
									@if($categorie->products->count())
										<div class="widget-body">
											<a  href="{{route('viewCategory',[ 'name' => $categorie->name ])}}" class="btn btn-md btn-primary">Produits
												<i class="fa fa-arrow-circle-o-right"></i>
											</a>
										</div>
									@endif
								</div>
							</div>
						@endforeach
					</div>
				</div>
			</section>
			<!-- End Categories List Page -->
	</div>
@endsection

@section('post_header')
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" 		   content="YanfomaShop" />
	<meta property="og:site_name" 	   content="Yanfoma">
	<meta property="fb:app_id" 		   content="400025927061215">
	<meta property="og:title"          content="YanfomaShop" />
	<meta property="og:description"    content="YanfomaShop" />
	<meta property="og:image"          content="{{asset('images/frontEnd/YanfomaShop.png')}}" />
	<meta property="og:type" 	       content="article">
	<meta property="og:url"            content="https://yanfoma.tech">
@endsection

@section('scripts')
	<script>
        $('.owl-carousel').owlCarousel({
            loop:true,
            margin: 0,
            nav:false,
            autoplay:true,
            responsiveClass:true,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        });
	</script>
@endsection