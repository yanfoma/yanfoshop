@extends('layouts.frontEnd.app')

@section('title')
	{{$title}}
@endsection

@section('style')
@endsection
@section('content')
	<!-- Shop Page Area -->
	<div class="shop-page-area bg-white ptb-30">
		<div class="container">
			@if($product_query->count() >0)
				<div class="row">
					<div class="col-lg-9 order-1 order-lg-2">
						<div class="title">
							<h2 style="text-align: center!important;">{{$title}}</h2>
							<h4 style="text-align: center!important;">{{$product_query->count()}} Résultats</h4>
						</div>
						<div class="shop-page-products mt-30">
							<div class="row no-gutters">
								@foreach($product_query as $product)
									<div class="col-lg-4 col-md-4 col-sm-6 col-12">
										<!-- Single Product -->
										<article class="hoproduct product">
											<div class="hoproduct-image product-image">
												<a class="hoproduct-thumb" href="{{route('shop.single',['store_slug' => $store->slug, 'slug' => $product->slug])}}">
													<img class="hoproduct-frontimage" src="{{$product->image_url}}" alt="product image">
												</a>
												<ul class="hoproduct-actionbox">
													<li><a href="{{route('store.cart.addDirect',['store_slug' => $store->slug,'id' => $product->id])}}"><i class="lnr lnr-cart"></i></a></li>
													<li><a href="{{route('shop.single',['store_slug' => $store->slug, 'slug' => $product->slug])}}"><i class="lnr lnr-eye"></i></a></li>
													{{--@if($product->is_wished_by_auth_user())--}}
														{{--<li>--}}
															{{--<a class="unwished" href="{{route('product.unwishlist', ['store_slug' => $store->slug, 'id' => $product->id ])}}">--}}
																{{--<i class="lnr lnr-heart"></i>--}}
															{{--</a>--}}
														{{--</li>--}}
													{{--@else--}}
														{{--<li>--}}
															{{--<a class="wished" href="{{route('product.wishlist', ['store_slug' => $store->slug, 'id' => $product->id ])}}">--}}
															{{--<i class="lnr lnr-heart"></i>--}}
															{{--</a>--}}
														{{--</li>--}}
													{{--@endif--}}
													{{--@if($product->is_liked_by_auth_user())--}}
														{{--<li><a class="unlike" href="{{route('product.unlike', ['store_slug' => $store->slug, 'id' => $product->id ])}}"><i class="lnr lnr-thumbs-up"></i></a></li>--}}
													{{--@else--}}
														{{--<li><a class="like" href="{{route('product.like', ['store_slug' => $store->slug, 'id' => $product->id ])}}"><i class="lnr lnr-thumbs-up"></i></a></li>--}}
													{{--@endif--}}
												</ul>
												<ul class="hoproduct-flags">
													@if($product->onSale && $product->saleType == 2)
														<li class="flag-discount">-{{ $product->salePercentage }} %</li>
													@endif
												</ul>
											</div>
											<div class="hoproduct-content">
												<h5 class="hoproduct-title product-title"><a href="{{route('shop.single',['store_slug' => $store->slug, 'slug' => $product->slug])}}">{{$product->name}}</a></h5>
												<div class="hoproduct-pricebox product-pricebox">
													<div class="pricebox">
														<div class="float_left">
															@if($product->onSale)
																<del class="oldprice">{{number_format($product->price, 0 , ',' , ' ')}} </del>
																<span class="price">{{number_format($product->sale_amount, 0 , ',' , ' ')}} CFA </span>
															@else
																<span class="price">{{number_format($product->price, 0 , ',' , ' ')}} CFA </span>
															@endif
														</div>
														<div class="float_right">
															<i class="fa fa-exclamation-triangle" style="color: yellow;"></i>
															Min: {{$product->minQty}}
														</div>
													</div>
												</div>
											</div>
										</article>
										<!--// Single Product -->
									</div>
								@endforeach
							</div>
						</div>
					</div>
					<div class="col-lg-3 order-2 order-lg-1">
						<div class="shop-widgets">
							<div class="single-widget widget-categories">
								<h5 class="widget-title">NOS CATÉGORIES</h5>
								<ul>
									@foreach($categories as $category)
									<li><a href="{{route('viewCategory',['store_slug' => $store->slug, 'name' => $category->name ])}}">{{$category->name}} <span>{{$category->products->count()}}</span></a></li>
									@endforeach
								</ul>
							</div>

							{{--<div class="shop-widgetbox mt-30">--}}
								{{--<div class="single-widget widget-filters">--}}
									{{--<h5 class="widget-title">FILTRE PAR PRIX</h5>--}}
									{{--<div class="widget-filter-inner">--}}
										{{--<div class="range-slider" data-range_min="0" data-range_max="1000"--}}
											 {{--data-cur_min="300" data-cur_max="800">--}}
											{{--<div class="bar"></div>--}}
											{{--<span class="range-slider-leftgrip"></span>--}}
											{{--<span class="range-slider-rightgrip"></span>--}}
										{{--</div>--}}
										{{--<div class="single-widget-range-price">--}}
											{{--<b>Prix: </b> <span class="range-slider-leftlabel"></span> CFA - <span class="range-slider-rightlabel"></span> CFA--}}
										{{--</div>--}}
									{{--</div>--}}
								{{--</div>--}}
							{{--</div>--}}

							{{--<!-- Recommended Product -->--}}
							{{--<div class="single-widget widget-recommended-products mt-30">--}}
								{{--<div class="section-title">--}}
									{{--<h3>Nos Recommandations</h3>--}}
								{{--</div>--}}
								{{--<div class="recommended-products-slider-2 slider-navigation-2 slider-navigation-2-m0">--}}
									{{--<div class="product-slider-col">--}}
										{{--<!-- Single Product -->--}}
										{{--<article class="hoproduct hoproduct-4">--}}
											{{--<div class="hoproduct-image">--}}
												{{--<a class="hoproduct-thumb" href="product-details.html">--}}
													{{--<img class="hoproduct-frontimage" src="images/product/product-image-1.jpg"--}}
														 {{--alt="product image">--}}
													{{--<img class="hoproduct-backimage" src="images/product/product-image-22.jpg"--}}
														 {{--alt="product image">--}}
												{{--</a>--}}
											{{--</div>--}}
											{{--<div class="hoproduct-content">--}}
												{{--<h5 class="hoproduct-title"><a href="product-details.html">SonicFuel--}}
														{{--Wireless Over-Ear Headphones</a></h5>--}}
												{{--<div class="hoproduct-rattingbox">--}}
													{{--<div class="rattingbox">--}}
														{{--<span class="active"><i class="ion ion-ios-star"></i></span>--}}
														{{--<span class="active"><i class="ion ion-ios-star"></i></span>--}}
														{{--<span class="active"><i class="ion ion-ios-star"></i></span>--}}
														{{--<span class="active"><i class="ion ion-ios-star"></i></span>--}}
														{{--<span class="active"><i class="ion ion-ios-star"></i></span>--}}
													{{--</div>--}}
												{{--</div>--}}
												{{--<div class="hoproduct-pricebox">--}}
													{{--<div class="pricebox">--}}
														{{--<del class="oldprice">$35.11</del>--}}
														{{--<span class="price">$34.11</span>--}}
													{{--</div>--}}
												{{--</div>--}}
											{{--</div>--}}
										{{--</article>--}}
										{{--<!--// Single Product -->--}}

										{{--<!-- Single Product -->--}}
										{{--<article class="hoproduct hoproduct-4">--}}
											{{--<div class="hoproduct-image">--}}
												{{--<a class="hoproduct-thumb" href="product-details.html">--}}
													{{--<img class="hoproduct-frontimage" src="images/product/product-image-7.jpg"--}}
														 {{--alt="product image">--}}
													{{--<img class="hoproduct-backimage" src="images/product/product-image-8.jpg"--}}
														 {{--alt="product image">--}}
												{{--</a>--}}
											{{--</div>--}}
											{{--<div class="hoproduct-content">--}}
												{{--<h5 class="hoproduct-title"><a href="product-details.html">SonicFuel--}}
														{{--Wireless Over-Ear Headphones</a></h5>--}}
												{{--<div class="hoproduct-rattingbox">--}}
													{{--<div class="rattingbox">--}}
														{{--<span class="active"><i class="ion ion-ios-star"></i></span>--}}
														{{--<span class="active"><i class="ion ion-ios-star"></i></span>--}}
														{{--<span class="active"><i class="ion ion-ios-star"></i></span>--}}
														{{--<span class="active"><i class="ion ion-ios-star"></i></span>--}}
														{{--<span class="active"><i class="ion ion-ios-star"></i></span>--}}
													{{--</div>--}}
												{{--</div>--}}
												{{--<div class="hoproduct-pricebox">--}}
													{{--<div class="pricebox">--}}
														{{--<del class="oldprice">$35.11</del>--}}
														{{--<span class="price">$34.11</span>--}}
													{{--</div>--}}
												{{--</div>--}}
											{{--</div>--}}
										{{--</article>--}}
										{{--<!--// Single Product -->--}}

										{{--<!-- Single Product -->--}}
										{{--<article class="hoproduct hoproduct-4">--}}
											{{--<div class="hoproduct-image">--}}
												{{--<a class="hoproduct-thumb" href="product-details.html">--}}
													{{--<img class="hoproduct-frontimage" src="images/product/product-image-18.jpg"--}}
														 {{--alt="product image">--}}
													{{--<img class="hoproduct-backimage" src="images/product/product-image-19.jpg"--}}
														 {{--alt="product image">--}}
												{{--</a>--}}
											{{--</div>--}}
											{{--<div class="hoproduct-content">--}}
												{{--<h5 class="hoproduct-title"><a href="product-details.html">SonicFuel--}}
														{{--Wireless Over-Ear Headphones</a></h5>--}}
												{{--<div class="hoproduct-rattingbox">--}}
													{{--<div class="rattingbox">--}}
														{{--<span class="active"><i class="ion ion-ios-star"></i></span>--}}
														{{--<span class="active"><i class="ion ion-ios-star"></i></span>--}}
														{{--<span class="active"><i class="ion ion-ios-star"></i></span>--}}
														{{--<span class="active"><i class="ion ion-ios-star"></i></span>--}}
														{{--<span class="active"><i class="ion ion-ios-star"></i></span>--}}
													{{--</div>--}}
												{{--</div>--}}
												{{--<div class="hoproduct-pricebox">--}}
													{{--<div class="pricebox">--}}
														{{--<del class="oldprice">$35.11</del>--}}
														{{--<span class="price">$34.11</span>--}}
													{{--</div>--}}
												{{--</div>--}}
											{{--</div>--}}
										{{--</article>--}}
										{{--<!--// Single Product -->--}}
									{{--</div>--}}

									{{--<div class="product-slider-col">--}}
										{{--<!-- Single Product -->--}}
										{{--<article class="hoproduct hoproduct-4">--}}
											{{--<div class="hoproduct-image">--}}
												{{--<a class="hoproduct-thumb" href="product-details.html">--}}
													{{--<img class="hoproduct-frontimage" src="images/product/product-image-15.jpg"--}}
														 {{--alt="product image">--}}
												{{--</a>--}}
											{{--</div>--}}
											{{--<div class="hoproduct-content">--}}
												{{--<h5 class="hoproduct-title"><a href="product-details.html">SonicFuel--}}
														{{--Wireless Over-Ear Headphones</a></h5>--}}
												{{--<div class="hoproduct-rattingbox">--}}
													{{--<div class="rattingbox">--}}
														{{--<span class="active"><i class="ion ion-ios-star"></i></span>--}}
														{{--<span class="active"><i class="ion ion-ios-star"></i></span>--}}
														{{--<span class="active"><i class="ion ion-ios-star"></i></span>--}}
														{{--<span class="active"><i class="ion ion-ios-star"></i></span>--}}
														{{--<span class="active"><i class="ion ion-ios-star"></i></span>--}}
													{{--</div>--}}
												{{--</div>--}}
												{{--<div class="hoproduct-pricebox">--}}
													{{--<div class="pricebox">--}}
														{{--<del class="oldprice">$35.11</del>--}}
														{{--<span class="price">$34.11</span>--}}
													{{--</div>--}}
												{{--</div>--}}
											{{--</div>--}}
										{{--</article>--}}
										{{--<!--// Single Product -->--}}

										{{--<!-- Single Product -->--}}
										{{--<article class="hoproduct hoproduct-4">--}}
											{{--<div class="hoproduct-image">--}}
												{{--<a class="hoproduct-thumb" href="product-details.html">--}}
													{{--<img class="hoproduct-frontimage" src="images/product/product-image-12.jpg"--}}
														 {{--alt="product image">--}}
													{{--<img class="hoproduct-backimage" src="images/product/product-image-13.jpg"--}}
														 {{--alt="product image">--}}
												{{--</a>--}}
											{{--</div>--}}
											{{--<div class="hoproduct-content">--}}
												{{--<h5 class="hoproduct-title"><a href="product-details.html">SonicFuel--}}
														{{--Wireless Over-Ear Headphones</a></h5>--}}
												{{--<div class="hoproduct-rattingbox">--}}
													{{--<div class="rattingbox">--}}
														{{--<span class="active"><i class="ion ion-ios-star"></i></span>--}}
														{{--<span class="active"><i class="ion ion-ios-star"></i></span>--}}
														{{--<span class="active"><i class="ion ion-ios-star"></i></span>--}}
														{{--<span class="active"><i class="ion ion-ios-star"></i></span>--}}
														{{--<span class="active"><i class="ion ion-ios-star"></i></span>--}}
													{{--</div>--}}
												{{--</div>--}}
												{{--<div class="hoproduct-pricebox">--}}
													{{--<div class="pricebox">--}}
														{{--<del class="oldprice">$35.11</del>--}}
														{{--<span class="price">$34.11</span>--}}
													{{--</div>--}}
												{{--</div>--}}
											{{--</div>--}}
										{{--</article>--}}
										{{--<!--// Single Product -->--}}

										{{--<!-- Single Product -->--}}
										{{--<article class="hoproduct hoproduct-4">--}}
											{{--<div class="hoproduct-image">--}}
												{{--<a class="hoproduct-thumb" href="product-details.html">--}}
													{{--<img class="hoproduct-frontimage" src="images/product/product-image-10.jpg"--}}
														 {{--alt="product image">--}}
													{{--<img class="hoproduct-backimage" src="images/product/product-image-11.jpg"--}}
														 {{--alt="product image">--}}
												{{--</a>--}}
											{{--</div>--}}
											{{--<div class="hoproduct-content">--}}
												{{--<h5 class="hoproduct-title"><a href="product-details.html">SonicFuel--}}
														{{--Wireless Over-Ear Headphones</a></h5>--}}
												{{--<div class="hoproduct-rattingbox">--}}
													{{--<div class="rattingbox">--}}
														{{--<span class="active"><i class="ion ion-ios-star"></i></span>--}}
														{{--<span class="active"><i class="ion ion-ios-star"></i></span>--}}
														{{--<span class="active"><i class="ion ion-ios-star"></i></span>--}}
														{{--<span class="active"><i class="ion ion-ios-star"></i></span>--}}
														{{--<span class="active"><i class="ion ion-ios-star"></i></span>--}}
													{{--</div>--}}
												{{--</div>--}}
												{{--<div class="hoproduct-pricebox">--}}
													{{--<div class="pricebox">--}}
														{{--<del class="oldprice">$35.11</del>--}}
														{{--<span class="price">$34.11</span>--}}
													{{--</div>--}}
												{{--</div>--}}
											{{--</div>--}}
										{{--</article>--}}
										{{--<!--// Single Product -->--}}
									{{--</div>--}}

									{{--<div class="product-slider-col">--}}
										{{--<!-- Single Product -->--}}
										{{--<article class="hoproduct hoproduct-4">--}}
											{{--<div class="hoproduct-image">--}}
												{{--<a class="hoproduct-thumb" href="product-details.html">--}}
													{{--<img class="hoproduct-frontimage" src="images/product/product-image-7.jpg"--}}
														 {{--alt="product image">--}}
													{{--<img class="hoproduct-backimage" src="images/product/product-image-8.jpg"--}}
														 {{--alt="product image">--}}
												{{--</a>--}}
											{{--</div>--}}
											{{--<div class="hoproduct-content">--}}
												{{--<h5 class="hoproduct-title"><a href="product-details.html">SonicFuel--}}
														{{--Wireless Over-Ear Headphones</a></h5>--}}
												{{--<div class="hoproduct-rattingbox">--}}
													{{--<div class="rattingbox">--}}
														{{--<span class="active"><i class="ion ion-ios-star"></i></span>--}}
														{{--<span class="active"><i class="ion ion-ios-star"></i></span>--}}
														{{--<span class="active"><i class="ion ion-ios-star"></i></span>--}}
														{{--<span class="active"><i class="ion ion-ios-star"></i></span>--}}
														{{--<span class="active"><i class="ion ion-ios-star"></i></span>--}}
													{{--</div>--}}
												{{--</div>--}}
												{{--<div class="hoproduct-pricebox">--}}
													{{--<div class="pricebox">--}}
														{{--<del class="oldprice">$35.11</del>--}}
														{{--<span class="price">$34.11</span>--}}
													{{--</div>--}}
												{{--</div>--}}
											{{--</div>--}}
										{{--</article>--}}
										{{--<!--// Single Product -->--}}

										{{--<!-- Single Product -->--}}
										{{--<article class="hoproduct hoproduct-4">--}}
											{{--<div class="hoproduct-image">--}}
												{{--<a class="hoproduct-thumb" href="product-details.html">--}}
													{{--<img class="hoproduct-frontimage" src="images/product/product-image-4.jpg"--}}
														 {{--alt="product image">--}}
												{{--</a>--}}
											{{--</div>--}}
											{{--<div class="hoproduct-content">--}}
												{{--<h5 class="hoproduct-title"><a href="product-details.html">SonicFuel--}}
														{{--Wireless Over-Ear Headphones</a></h5>--}}
												{{--<div class="hoproduct-rattingbox">--}}
													{{--<div class="rattingbox">--}}
														{{--<span class="active"><i class="ion ion-ios-star"></i></span>--}}
														{{--<span class="active"><i class="ion ion-ios-star"></i></span>--}}
														{{--<span class="active"><i class="ion ion-ios-star"></i></span>--}}
														{{--<span class="active"><i class="ion ion-ios-star"></i></span>--}}
														{{--<span class="active"><i class="ion ion-ios-star"></i></span>--}}
													{{--</div>--}}
												{{--</div>--}}
												{{--<div class="hoproduct-pricebox">--}}
													{{--<div class="pricebox">--}}
														{{--<del class="oldprice">$35.11</del>--}}
														{{--<span class="price">$34.11</span>--}}
													{{--</div>--}}
												{{--</div>--}}
											{{--</div>--}}
										{{--</article>--}}
										{{--<!--// Single Product -->--}}

										{{--<!-- Single Product -->--}}
										{{--<article class="hoproduct hoproduct-4">--}}
											{{--<div class="hoproduct-image">--}}
												{{--<a class="hoproduct-thumb" href="product-details.html">--}}
													{{--<img class="hoproduct-frontimage" src="images/product/product-image-5.jpg"--}}
														 {{--alt="product image">--}}
												{{--</a>--}}
											{{--</div>--}}
											{{--<div class="hoproduct-content">--}}
												{{--<h5 class="hoproduct-title"><a href="product-details.html">SonicFuel--}}
														{{--Wireless Over-Ear Headphones</a></h5>--}}
												{{--<div class="hoproduct-rattingbox">--}}
													{{--<div class="rattingbox">--}}
														{{--<span class="active"><i class="ion ion-ios-star"></i></span>--}}
														{{--<span class="active"><i class="ion ion-ios-star"></i></span>--}}
														{{--<span class="active"><i class="ion ion-ios-star"></i></span>--}}
														{{--<span class="active"><i class="ion ion-ios-star"></i></span>--}}
														{{--<span class="active"><i class="ion ion-ios-star"></i></span>--}}
													{{--</div>--}}
												{{--</div>--}}
												{{--<div class="hoproduct-pricebox">--}}
													{{--<div class="pricebox">--}}
														{{--<del class="oldprice">$35.11</del>--}}
														{{--<span class="price">$34.11</span>--}}
													{{--</div>--}}
												{{--</div>--}}
											{{--</div>--}}
										{{--</article>--}}
										{{--<!--// Single Product -->--}}
									{{--</div>--}}
								{{--</div>--}}
							{{--</div>--}}
							{{--<!--// Recommended Product -->--}}

							{{--<div class="imgbanner mt-30">--}}
								{{--<a href="product-details.html">--}}
									{{--<img src="images/banner/banner-image-9.jpg" alt="banner">--}}
								{{--</a>--}}
							{{--</div>--}}
						</div>
					</div>
				</div>
			@else
				<blockquote>Aucun Produit Trouvé</blockquote>
			@endif
		</div>
	</div>
@endsection
