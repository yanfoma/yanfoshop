@extends('layouts.frontEnd.app')

@section('title')
   Votre Experience {{$store->name}}
@endsection

@section('content')
    <section class="breadcumb_area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <div class="breadcumb_section">
                        <div class="page_title">
                            <h3>
                                Votre Avis nous interresse
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="contact">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="widget2 top-space margin-bottom-none">
                        <div class="widget-header">
                            {{--<h1>Envoyer un review</h1>--}}
                        </div>
                        <form action="{{route('testimonyUpload',['store_slug' => $store->slug])}}" method="post"  enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="control-label">Nom <span class="required">*</span></label>
                                        <input type="text" name="name" class="form-control" value="{{ old('name') }}" placeholder="Votre Nom *" required>
                                        @if ($errors->has('name'))
                                            <div id="uname-error" class="error">{{ $errors->first('name') }}</div>
                                        @endif
                                    </div>
                                    <div class="col-md-6">
                                        <label class="control-label">Portable <span class="required">*</span></label>
                                        <input type="text" name="phone" class="form-control" value="{{ old('phone') }}" placeholder="Votre numero de portable (veuillez preciser l'indicatif) *" required>
                                        @if ($errors->has('phone'))
                                            <div id="uname-error" class="error">{{ $errors->first('phone') }}</div>
                                        @endif
                                    </div>
                                    <div class="col-md-6">
                                        <label class="control-label">Email<span class="required">*</span></label>
                                        <input type="email" name="email" class="form-control required email" value="{{ old('email') }}" placeholder="Votre e-mail *" required aria-required="true">
                                        @if ($errors->has('email'))
                                            <div id="uname-error" class="error">{{ $errors->first('email') }}</div>
                                        @endif
                                    </div>
                                    <div class="col-md-6">
                                        <label class="control-label">Pays <span class="required">*</span></label>
                                        <input type="text" name="country" class="form-control" value="{{ old('country') }}" placeholder="Votre Pays*" required>
                                        @if ($errors->has('country'))
                                            <div id="uname-error" class="error">{{ $errors->first('country') }}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Message <span class="required">*</span></label>
                                <textarea name="message" class="form-control" placeholder="Votre Message (150 characters max)...." aria-required="true" rows="3" maxlength="150">{{ old('message') }}</textarea>
                                @if ($errors->has('message'))
                                    <div id="uname-error" class="error">{{ $errors->first('message') }}</div>
                                @endif
                            </div>
                            <div class="text-right">
                                <input type="submit" class="btn btn-primary" value="Envoyer">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="faq">
    </section>
@endsection