@extends('layouts.frontEnd.error')
@section('title')
    404 Yanfoma Soko
@endsection

@section('content')
    <a href="{{route('welcome')}}" class="fa fa-arrow-left"></a>
    <div class="error">
        <img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1553270364/Yanfoma/VertDback.png" alt="logo">
        <h1>404</h1>
        <p>Nous sommes désolés, mais il semblerait que cette page n'existe plus.</p>
        <a class="btn btn-outline-primary btn-block btn-lg " href="{{route('welcome')}}">Retourner à l'accueil</a>
    </div>
@endsection

