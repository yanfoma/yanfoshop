@extends('layouts.frontEnd.error')

@section('title')
    500 Yanfoma Soko
@endsection

@section('content')
    <div class="content">
        <div class="title">Something went wrong.</div>

        @if(app()->bound('sentry') && !empty(Sentry::getLastEventID()))
            <div class="subtitle">Error ID: {{ Sentry::getLastEventID() }}</div>

            <!-- Sentry JS SDK 2.1.+ required -->
            <script src="https://cdn.ravenjs.com/3.3.0/raven.min.js"></script>

            <script>
                Raven.showReportDialog({
                    eventId: '{{ Sentry::getLastEventID() }}',
                    // use the public DSN (dont include your secret!)
                    dsn: 'https://3b7668ae41584df3a6afa9d0ff1cbef0@sentry.io/1297185',
                    user: {
                        'name': 'Jane Doe',
                        'email': 'jane.doe@example.com',
                    }
                });
            </script>
        @endif
    </div>
@endsection
