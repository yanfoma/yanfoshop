@extends('layouts.frontEnd.customer')

@section('title')
    Mon Addresse Yanfoma Soko
@endsection

@section('style')
    <style></style>
@endsection

@section('content')
    <section class="user_account">
        <div class="container">
            <div class="row">
               @include('customer.sidebar')
                <div class="col-lg-8 col-md-8 col-sm-7">
                    <div class="widget2">
                        <div class="section-header">
                            <h5 class="heading-design-h5">
                                Mon Addresse
                            </h5>
                        </div>
                        <form method="POST" action="{{ route('customer.updateAddresse',['id' => Auth::user()->id] ) }}">
                            {!! csrf_field() !!}
                            <div class="row">
                                <div class="col-sm-6 {!! $errors->has('pays') ? 'has-error' : '' !!}">
                                    <div class="form-group">
                                        <label for="pays">Pays</label>
                                        <select name="pays" class="selectpicker form-control show-tick" data-live-search="true" data-header="Pays De Naissance" title="Pays De Naissance" required>
                                            @if($user->pays)
                                                <option value="{{$user->pays}}" selected> {{ $user->pays }} </option>
                                            @endif
                                            <option value="Burkina Faso">Burkina Faso</option>
                                            <option value="Cote d'Ivoire">Cote d'Ivoire</option>
                                            <option value="Cameroun">Cameroun</option>
                                            <option value="Mail">Mail</option>
                                            <option value="Sénégal">Sénégal</option>
                                            <option value="Rwanda">Rwanda</option>
                                        </select>
                                        {!! $errors->first('pays', '<small class="help-block">:message</small>') !!}
                                    </div>
                                </div>
                                <div class="col-sm-6 {!! $errors->has('ville') ? 'has-error' : '' !!}">
                                    <div class="form-group">
                                        <label for="ville">Ville</label>
                                        <select name="ville" class="selectpicker form-control show-tick" data-live-search="true" data-header="Ville" title="Ville" required>
                                            @if($user->ville)
                                                <option value="{{$user->ville}}" selected> {{ $user->ville }} </option>
                                            @endif
                                            <option value="Bobo-Dioulasso">Bobo-Dioulasso</option>
                                            <option value="Koudougou">Koudougou</option>
                                            <option value="Ouagadougou">Ouagadougou</option>
                                        </select>
                                        {!! $errors->first('ville', '<small class="help-block">:message</small>') !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 {!! $errors->has('addresse') ? 'has-error' : '' !!}">
                                    <div class="form-group">
                                        <label class="control-label">Addresse <span class="required">*</span></label>
                                        <textarea name="addresse" class="form-control border-form-control" rows="10" cols="50" required> {{$user->addresse}}</textarea>
                                        {!! $errors->first('addresse', '<small class="help-block">:message</small>') !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 text-right">
                                    <button type="reset" class="btn btn-danger btn-lg"> Annuler </button>
                                    <button type="submit" class="btn btn-primary btn-lg"> Sauvegarger </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection