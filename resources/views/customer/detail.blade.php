@extends('layouts.frontEnd.customer')

@section('title')
    Detail commande
@endsection

@section('style')
    <style>
    </style>
@endsection

@section('content')
    <section class="user_account">
        <div class="container">
            <div class="row">
                @include('customer.sidebar')
                <div class="col-lg-8 col-md-8">
                    <div class="widget">
                        <h5 class="heading-design-h5">
                            Détails De La Commande
                        </h5>
                        <div>
                            @if($detail->count())
                                <table class="table table-striped table-hover table-responsive">
                                    <thead class="thead-light">
                                        <tr>
                                            <th scope="col">Nom</th>
                                            <th scope="col">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($detail as $dt)
                                            <tr>
                                                <td><img src="{{ $dt->image_url }}" class="thumb-img img-responsive" alt="" ></td>
                                                <td>({{ $dt->qty}}) {{ $dt->coName}}</td>
                                                <td>{{ $dt->coTotal}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {{ $detail->links() }}
                                <div class="item-footer">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12">
                                            <span class="item-views" style="text-decoration: underline"><b>Date de la Commande: {{\Jenssegers\Date\Date::parse($dt->created_at)->format('l j F Y H:i')}}</b></span>
                                        </div>
                                    </div>
                                </div>
                            @else
                                <div class="alert alert-info" role="alert" id="centre">
                                    Aucun Produit
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection