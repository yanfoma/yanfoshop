@extends('layouts.frontEnd.customer')

@section('title')
    Suivre Mes commandes
@endsection

@section('style')
    <style>
        .wizard-v3-content {
            background: #fff;
            width: 780px;
            box-shadow: 0px 8px 20px 0px rgba(0, 0, 0, 0.15);
            -o-box-shadow: 0px 8px 20px 0px rgba(0, 0, 0, 0.15);
            -ms-box-shadow: 0px 8px 20px 0px rgba(0, 0, 0, 0.15);
            -moz-box-shadow: 0px 8px 20px 0px rgba(0, 0, 0, 0.15);
            -webkit-box-shadow: 0px 8px 20px 0px rgba(0, 0, 0, 0.15);
            border-radius: 10px;
            -o-border-radius: 10px;
            -ms-border-radius: 10px;
            -moz-border-radius: 10px;
            -webkit-border-radius: 10px;
            margin: 110px 0;
            font-family: 'Roboto', sans-serif;
            position: relative;
            display: flex;
            display: -webkit-flex;
        }
    </style>
@endsection

@section('content')
    <section class="user_account">
        <div class="container">
            <div class="row">
                @include('customer.sidebar')
                <div class="col-lg-8 col-md-8">
                    <div class="widget">
                        <h5 class="heading-design-h5">
                            Suivi de la commande {{$purchased->purchasedId}}
                        </h5>
                        @foreach($purchased->trackings as $tracking)
                            <div class="panel panel-default">
                                <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question{{$tracking->id}}" aria-expanded="false" style="background-color:rgba(23,193,201,0.6); !important; border-radius: 5px">
                                    <h4 class="panel-title">
                                        <a href="#" class="ing">{{$tracking->step}} : {{$tracking->date}}</a>
                                    </h4>
                                </div>
                                <div id="question{{$tracking->id}}" class="panel-collapse collapse" style="height: 0px;" aria-expanded="false">
                                    <div class="panel-body">
                                        @if($tracking->tackingCode)
                                            <h5><span class="label label-primary">Code</span> {{$tracking->tackingCode}}</h5>
                                        @endif
                                        <br>
                                        {!! $tracking->note !!}
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection