@extends('layouts.frontEnd.customer')

@section('title')
    Changer De Mot De Passe Yanfoma Soko
@endsection

@section('style')
    <style></style>
@endsection

@section('content')
    <section class="user_account">
        <div class="container">
            <div class="row">
               @include('customer.sidebar')
                <div class="col-lg-8 col-md-8 col-sm-7">
                    <div class="widget2">
                        <div class="section-header">
                            <h5 class="heading-design-h5">
                                Nouveau Mot De Passe
                            </h5>
                            <p>Pour plus de Sécurité veuillez choisir un mot de passe de 8 lettres minimum composé de lettres en Majuscule, minuscule, chiffres, et de caracteres spéciaux</p>
                        </div>
                        <form method="POST" action="{{ route('customer.updatePassword',['id' => Auth::user()->id] ) }}">
                            {!! csrf_field() !!}
                            <div class="row">
                                <div class="col-sm-6 {!! $errors->has('password') ? 'has-error' : '' !!}">
                                    <div class="form-group">
                                        <label for="newpassword1">Nouveau mot de passe</label>
                                        <input name="password" type="password" class="form-control" id="newspassword1"
                                               placeholder="Nouveau mot de passe" value="">
                                        {!! $errors->first('password', '<small class="help-block">:message</small>') !!}
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="newpassword">Confirmation nouveau mot de passe</label>
                                        <input name="password_confirmation" type="password" class="form-control" id="newpassword" placeholder="Confirmation nouveau mot de passe" value="{{old('password_confirmation')}}">
                                        {!! $errors->first('password', '<small class="help-block">:message</small>') !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 text-right">
                                    <button type="reset" class="btn btn-danger btn-lg"> Annuler </button>
                                    <button type="submit" class="btn btn-primary btn-lg"> Sauvegarger </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection