@extends('layouts.frontEnd.customer')

@section('title')
    Foire Aux Questions YanfomaShop
@endsection

@section('content')
    <section class="user_account">
        <div class="container">
            <div class="row">
                @include('customer.sidebar')
                <div class="col-lg-8 col-md-8 col-sm-7">
                    <div class="widget margin-bottom-none">
                        <div class="panel-group margin-bottom-none" id="faqAccordion">
                            @if($faqs->count())
                                @foreach($faqs as $faq)
                                    <div class="panel panel-default ">
                                        <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question{{$faq->id}}" aria-expanded="false">
                                            <h4 class="panel-title">
                                                <a href="#" class="ing">{{$faq->question}} </a>
                                            </h4>
                                        </div>
                                        <div id="question{{$faq->id}}" class="panel-collapse collapse" style="height: 0px;" aria-expanded="false">
                                            <div class="panel-body">
                                                <h5><span class="label label-primary">Reponse</span></h5>
                                                <br>
                                                {{--<p>{{strip_tags($faq->response)}}</p>--}}
                                                <p>{!! $faq->response !!}</p>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection