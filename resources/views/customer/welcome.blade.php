@extends('layouts.frontEnd.customer')

@section('title')
    MES INFORMATIONS Yanfoma Soko
@endsection

@section('style')
    <style></style>
@endsection

@section('content')
    <section class="user_account">
        <div class="container">
            <div class="row">
                @include('customer.sidebar')
                <div class="col-lg-8 col-md-8 col-sm-7">
                    <div class="widget2">
                        <div class="section-header">
                            <h5 class="heading-design-h5">
                                Mes Informations
                            </h5>
                        </div>
                        <form method="POST" action="{{ route('customer.update',['id' => Auth::user()->id] ) }}">
                            {!! csrf_field() !!}
                            <div class="row">
                                <div class="col-sm-6 {!! $errors->has('name') ? 'has-error' : '' !!}">
                                    <div class="form-group">
                                        <label class="control-label">Nom</label>
                                        <input name="name" type="text" class="form-control border-form-control" id="name" value="{{ $user->name}}">
                                        {!! $errors->first('name', '<small class="help-block">:message</small>') !!}
                                    </div>
                                </div>
                                <div class="col-sm-6 {!! $errors->has('email') ? 'has-error' : '' !!}">
                                    <div class="form-group">
                                        <label class="control-label">Email</label>
                                        <input name="email" type="email" class="form-control border-form-control" id="email" value="{{ $user->email}}">
                                        {!! $errors->first('email', '<small class="help-block">:message</small>') !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 {!! $errors->has('sexe') ? 'has-error' : '' !!}">
                                    <div class="form-group">
                                        <label class="control-label">Sexe</label>
                                        <select class="selectpicker form-control show-tick"  name="sexe" data-header="sexe" title="sexe">
                                            @if($user->sexe) <option value="{{$user->sexe}}" selected > {{ $user->sexe }} </option> @endif
                                            <option value="Masculin">Masculin</option>
                                            <option value="Feminin">Feminin</option>
                                        </select>
                                        {!! $errors->first('sexe', '<small class="help-block">:message</small>') !!}
                                    </div>
                                </div>
                                <div class="col-sm-6 {!! $errors->has('dob') ? 'has-error' : '' !!}" >
                                    <div class="form-group">
                                        <label for="dob">Date de Naissance</label>
                                        <input name="dob" type="date" class="form-control" id="dob" placeholder="Date de Naissance" value="{{$user->dob}}">
                                        {!! $errors->first('dob', '<small class="help-block">:message</small>') !!}
                                        @if(session()->has('ok'))
                                            <div class="alert alert-danger alert-dismissible ">{!! session('ok') !!}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 {!! $errors->has('education') ? 'has-error' : '' !!}" >
                                    <div class="form-group">
                                        <label for="education">Education</label>
                                        <input name="education" type="text" class="form-control" id="education" placeholder="Education" value="{{$user->education}}">
                                        {!! $errors->first('education', '<small class="help-block">:message</small>') !!}
                                        @if(session()->has('ok'))
                                            <div class="alert alert-danger alert-dismissible ">{!! session('ok') !!}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-6 {!! $errors->has('profession') ? 'has-error' : '' !!}" >
                                    <div class="form-group">
                                        <label for="profession">Profession</label>
                                        <input name="profession" type="text" class="form-control" id="profession" placeholder="Profession" value="{{$user->profession}}">
                                        {!! $errors->first('profession', '<small class="help-block">:message</small>') !!}
                                        @if(session()->has('ok'))
                                            <div class="alert alert-danger alert-dismissible ">{!! session('ok') !!}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 {!! $errors->has('wa_number') ? 'has-error' : '' !!}">
                                    <div class="form-group">
                                        <label class="control-label">Numéro Whatsapp</label><br>
                                        <input name="wa_number" type="tel" id="phone" class="form-control border-form-control" value="{{ $user->wa_number}}" title="Numéro Whatsapp">
                                        {!! $errors->first('wa_number', '<small class="help-block">:message</small>') !!}
                                    </div>
                                </div>
                                <div class="col-sm-6 {!! $errors->has('interests') ? 'has-error' : '' !!}">
                                    <div class="form-group">
                                        <label for="interests">Mes Interets</label>
                                        <select class="selectpicker form-control show-tick"  name="interests[]" multiple data-live-search="true" data-header="Mes Interets" title="Mes Interets">
                                            @foreach($interests as $interet)
                                                <option value="{{$interet->id}}" {{ (in_array($interet->id, $user->interests->pluck('id')->toArray())) ? 'selected' : '' }}>{{$interet->name}}</option>
                                            @endforeach
                                        </select>
                                        {!! $errors->first('interests', '<small class="help-block">:message</small>') !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 text-right">
                                    <button type="reset" class="btn btn-danger btn-lg"> Annuler </button>
                                    <button type="submit" class="btn btn-primary btn-lg"> Sauvegarger </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <br><br>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        @if($user->verified !="yes")
                            <div class="widget2">
                                <div class="section-header">
                                    <h5 class="heading-design-h5">
                                        Status
                                    </h5>
                                </div>
                                <div class="alert alert-warning" role="alert">
                                    Email Pas encore valider!!!
                                    Veuillez valider votre Email.
                                </div>
                            </div>
                            <div class="widget2">
                                <div class="section-header">
                                    <h5 class="heading-design-h5">
                                        Verifier Mon Mail
                                    </h5>
                                </div>
                                <form method="POST" action="{{ route('customer.verify',['id' => Auth::user()->id] ) }}">
                                    {!! csrf_field() !!}
                                    <div class="row">
                                        <div class="col-sm-12 {!! $errors->has('email') ? 'has-error' : '' !!}">
                                            <div class="form-group">
                                                <label class="control-label">Email</label>
                                                <input name="verifyemail" type="email" class="form-control border-form-control" id="verifyemail" value="{{ $user->email}}">
                                                {!! $errors->first('verifyemail', '<small class="help-block">:message</small>') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 text-right">
                                            <button type="reset" class="btn btn-danger btn-lg"> Annuler </button>
                                            <button type="submit" class="btn btn-primary btn-lg"> Verifier </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        @else
                            <div class="widget2">
                                <div class="section-header">
                                    <h5 class="heading-design-h5">
                                        Status
                                    </h5>
                                </div>
                                <div class="alert alert-success" role="alert">
                                    Votre Email Est Valide !!!
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
                {{--<div class="col-lg-8 col-md-8 col-sm-7">--}}
                    {{--<div class="widget">--}}
                        {{--<div class="section-header">--}}
                            {{--<h5 class="heading-design-h5">--}}
                                {{--Verifier Mon Whatsapp--}}
                            {{--</h5>--}}
                        {{--</div>--}}
                        {{--<form method="POST" action="{{ route('customer.verify',['id' => Auth::user()->id] ) }}">--}}
                            {{--{!! csrf_field() !!}--}}
                            {{--<div class="row">--}}
                                {{--<div class="col-sm-12 {!! $errors->has('wa_number') ? 'has-error' : '' !!}">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label class="control-label">Numéro Whatsapp</label>--}}
                                        {{--<input name="wa_number" type="tel" id="phone" class="form-control border-form-control" value="{{ $user->wa_number}}" title="Numéro Whatsapp">--}}
                                        {{--{!! $errors->first('wa_number', '<small class="help-block">:message</small>') !!}--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="row">--}}
                                {{--<div class="col-sm-12 text-right">--}}
                                    {{--<button type="reset" class="btn btn-danger btn-lg"> Annuler </button>--}}
                                    {{--<button type="submit" class="btn btn-primary btn-lg"> Verifier </button>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</form>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script src="{{asset('js/intlTelInput.js')}}"></script>
    <script>
        var input = document.querySelector("#phone");
        window.intlTelInput(input, {
            onlyCountries: ["bf", "ci", "ml","rw","sn"],
            nationalMode: true,
            hiddenInput: "full_phone",
            utilsScript: "../../js/utils.js" // just for formatting/placeholders etc
        });
        $("dob").flatpickr(optional_config);
    </script>
@endsection