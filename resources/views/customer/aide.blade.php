@extends('layouts.frontEnd.customer')

@section('title')
    Aide Yanfoma Soko
@endsection

@section('content')
    <section class="user_account">
        <div class="container">
            <div class="row">
                @include('customer.sidebar')
                <div class="col-lg-8 col-md-8 col-sm-7">
                    <div class="widget2">
                        <div class="section-header">
                            <h5 class="heading-design-h5">
                                Aide
                            </h5>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 margin-top">
                                <div class="card text-white bg-primary">
                                    <div class="card-header">Facebook</div>
                                    <div class="card-body">
                                        <p class="card-text text-white">Rejoignez notre groupe Facebook yanfomashop!!!</p><br>
                                        <a class="btn btn-info center-block" href="https://www.facebook.com/groups/2171625413121414/">Rejoindre</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 margin-top">
                                <div class="card text-white bg-primary">
                                    <div class="card-header">Messanger</div>
                                    <div class="card-body">
                                        <p class="card-text text-white">Contactez nous sur messenger pour toutes vos questions. </p><br>
                                        <a class="btn btn-info center-block" href="https://www.messenger.com/t/yanfoma">Envoyer</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clear-fix"></div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 margin-top">
                                <div class="card text-white bg-primary">
                                    <div class="card-header">Whatsapp</div>
                                    <div class="card-body">
                                        <p class="card-text text-white">Contacter nous sur whatsapp </p><br>
                                        <a class="btn btn-info center-block" href="https://chat.whatsapp.com/EsCjgPWHuPf39ujQKANX5C">Yanfoma Soko</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 margin-top">
                                <div class="card text-white bg-primary">
                                    <div class="card-header">Email</div>
                                    <div class="card-body">
                                        <p class="card-text text-white">info@yanfoma.tech</p><br>
                                        <a class="btn btn-info center-block" href="mailto:info@yanfoma.tech">Contacter Nous !!!</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection