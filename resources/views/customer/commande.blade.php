@extends('layouts.frontEnd.customer')

@section('title')
    Mes Commandes Yanfoma Soko
@endsection

@section('style')
    <style>
        .thumb-img {
            border-radius: 59px;
            height: 80px;
            width: 80px;
        }

        .action {
            text-align: center;
        }

        .table> thead> tr> th, .table> tbody> tr> th, .table> tfoot> tr> th, .table> thead> tr> td, .table> tbody> tr> td, .table> tfoot> tr> td {
            border-top: 1px solid #dddddd;
            line-height: 1.846;
            padding: 8px;
            vertical-align: middle;
        }

        .table {
            margin-bottom: 0;
        }

        .table-design td:nth-child(1) {
            width: 96px;
        }

        .table-design ul li {
            display: inline-table;
        }

        .action .label {
            border-radius: 50px;
            display: inline-block;
            height: 22px;
            line-height: 17px;
            width: 22px;
        }
    </style>
@endsection

@section('content')
    <section class="user_account">
        <div class="container">
            <div class="row">
                @include('customer.sidebar')
                <div class="col-lg-8 col-md-8">
                    <div class="widget2">
                        <div class="section-header">
                            <h5 class="heading-design-h5">
                                Historique et statut de vos commandes
                            </h5>
                            <p class="block-title-border"> Vous trouverez ici vos commandes passées depuis la création de votre compte </p><br>
                            <div class="widget-body">
                                @if($mesCommandes->count())
                                    <table class="table table-design table-hover table-striped table-responsive">
                                        <thead class="thead-light">
                                        <tr>
                                            <th scope="col">Code</th>
                                            <th scope="col">Nom</th>
                                            <th scope="col">Total</th>
                                            <th scope="col">Detail</th>
                                            <th scope="col">Status</th>
                                            <th scope="col">Suivre</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($mesCommandes as $index => $commande)
                                            <tr>
                                                <td>
                                                    {{$commande->purchasedId}}
                                                </td>
                                                <td>
                                                    <div class="item-meta">
                                                        <ul>
                                                            <li class="item-date"><i class="fa fa-clock-o"></i> {{\Jenssegers\Date\Date::parse($commande->created_at)->format('d-m-Y')}}</li>
                                                        </ul>
                                                    </div>
                                                </td>
                                                <td><strong>{{ $commande->total}} CFA</strong></td>
                                                <td>
                                                    <div class="action">
                                                        <a class="btn btn-primary" href="{{route('customer.commandeDetail',['id' => $commande->id])}}">Detail</a>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="action">
                                                       @switch($commande->payed)
                                                            @case(0)
                                                                <a class="btn btn-danger"><i class="fa fa-exclamation-circle"></i> Non Payé</a>
                                                            @break

                                                            @case(1)
                                                                <a class="btn btn-info"><i class="fa fa-clock-o"></i> En attente</a>
                                                            @break

                                                            @case(2)
                                                                <a class="btn btn-success"><i class="fa fa-check-circle"></i> Payé</a>
                                                            @break
                                                        @endswitch
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="action">
                                                        @switch($commande->payed)

                                                        @case(0)
                                                            <div class="alert alert-danger" role="alert">
                                                                Veuillez  <a href="{{ route('customer.commandeConfirmation')}}" class="alert-link">Confirmer</a>
                                                            </div>
                                                        @break

                                                        @case(1)
                                                        <a class="btn btn-info"><i class="fa fa-clock-o"></i> En attente</a>
                                                        @break

                                                        @case(2)
                                                            <a class="btn btn-success" href="{{route('customer.suivreCommande',['purchasedId' => $commande->purchasedId])}}"><i class="fa fa-truck"></i>Suivre</a>
                                                        @break

                                                        @endswitch
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    <div class="text-center">
                                        {{$mesCommandes->links() }}
                                    </div>
                                @else
                                @endif
                            </div>
                        </div>
                        <div class="item-footer">
                            <div class="row">
                                <div class="col-xs-12 col-md-12">
                                    <span class="item-views"><b>{{$mesCommandes->count()}} Commandes</b></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
@endsection