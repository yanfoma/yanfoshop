<div class="col-lg-4 col-md-4 col-sm-5">
    <div class="user-account-sidebar">
        <aside class="user-info-wrapper">
            <div class="user-cover" style="background-image: url(https://res.cloudinary.com/yanfomaweb/image/upload/v1539202053/Yanfoma/user-cover.png);"></div>
            <div class="user-info">
                <div class="user-avatar">
                    @if($user->verified =="yes")
                        <div class="info-label" data-toggle="tooltip" title="Verified Account" data-original-title="Verified Account">
                            <i class="fa fa-check-circle"></i>
                        </div>
                    @endif
                    {{--<a class="edit-avatar" href="#"><i class="fa fa-check-circle"></i></a>--}}
                    <img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1539208782/Yanfoma/avatar-1.png" alt="User"></div>
                <div class="user-data">
                    <h4>{{Auth::user()->name}}</h4>
                    <span><i class="icofont icofont-ui-calendar"></i> {{Auth::user()->email}}</span>
                </div>
            </div>
        </aside>
        <nav class="list-group">
            <a class="list-group-item {{\Request::is('customer/mes-informations') ?'active':''}}" href="{{route('customer.welcome')}}"><i class="fa fa-user"></i> MES INFORMATIONS</a>
            <a class="list-group-item {{\Request::is('customer/mon-adresse') ?'active':''}}"           href="{{route('customer.adresse')}}"><i class="fa fa-map-marker"></i> ADDRESSES </a>
            <a class="list-group-item {{\Request::is('customer/mon-mot-de-passe') ?'active':''}}" href="{{route('customer.password')}}"><i class="fa fa-lock"></i> Changer De Mot De Passe</a>
            {{--<a class="list-group-item" href="{{route('cart.index')}}"><i class="fa fa-shopping-bag"></i>--}}
                {{--<span>Mon Panier</span>--}}
                {{--@if(Cart::content()->count())--}}
                    {{--<span class="badge1 badge1-danger" style="margin-left: 10px;margin-top: 15px">{{Cart::content()->count()}}</span>--}}
                {{--@endif--}}
            {{--</a>--}}
            <a class="list-group-item {{\Request::is('customer/mes-commandes') ?'active':''}}"               href="{{ route('customer.commandeView')}}"><i class="fa fa-history"></i>
                Mes Commandes
                @if(Auth::user()->purchased()->count())
                    <span class="badge1 badge1-danger" style="margin-left: 10px;margin-top: 15px">{{Auth::user()->purchased()->count()}}</span>
                @endif
            </a>

            <a class="list-group-item {{\Request::is('customer/confirmer-mes-commandes') ?'active':''}}"    href="{{ route('customer.commandeConfirmation')}}"><i class="fa fa-check-circle"></i>
                Confimer Mes Commandes
            </a>

            <a class="list-group-item {{\Request::is('customer/ma-liste-de-souhaits') ?'active':''}}" href="{{route('customer.wishlist')}}"><i class="fa fa-heart"></i>
                Liste de Souhaits
                @if(Auth::user()->wishlist()->count())
                    <span class="badge1 badge1-danger"  style="margin-left: 10px;margin-top: 15px">{{Auth::user()->wishlist()->count()}}</span>
                @endif
            </a>
            {{--<a class="list-group-item {{\Request::is('en/customer/faq','fr/customer/faq') ?'active':''}}"                                   href="{{route('customer.faq')}}"><i class="fa fa-exclamation-circle"></i> Faq</a>--}}
            <a class="list-group-item {{\Request::is('en/customer/aide','fr/customer/aide') ?'active':''}}"                                 href="{{route('customer.aide')}}"><i class="fa fa-question-circle"></i> Aide</a>
            <a class="list-group-item" href="{{route('logout')}}"><i class="fa fa-sign-out"></i>Deconnection</a>
        </nav>
    </div>
</div>