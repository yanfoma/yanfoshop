@extends('layouts.frontEnd.customer')

@section('title')
    Liste de Souhaits  Yanfoma Soko
@endsection

@section('style')
    <style>
    </style>
@endsection

@section('content')
    <section class="user_account">
        <div class="container">
            <div class="row">
                @include('customer.sidebar')
                <div class="col-lg-8 col-md-8">
                    <div class="widget2">
                        <h5 class="heading-design-h5">
                            Liste de Souhaits
                        </h5>
                        <div>
                            @if($wishlists->count())
                                <table class="table table-striped table-hover table-responsive">
                                    <thead class="thead-light">
                                        <tr>
                                            <th scope="col">Image</th>
                                            <th scope="col">Nom</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($wishlists as $wishlist)
                                            <tr>
                                                <td><img src="{{ $wishlist->product->image_url }}" class="thumb-img img-responsive" alt="" ></td>
                                                <td>{{ $wishlist->product->name}}</td>
                                                <td>{{ $wishlist->product->price}}</td>
                                                <td>
                                                    <div class="action">
                                                        <a class="btn btn-danger" href="">X</a>
                                                        <a class="btn btn-info" href=""><i class="fa fa-eye"></i></a>
                                                    </div>
                                                </td>
                                                {{--<td>--}}
                                                    {{--<div class="action">--}}
                                                        {{--<a class="btn btn-danger" href="{{route('product.unwishlist', [ 'id' => $wishlist->product->id ])}}">X</a>--}}
                                                        {{--<a class="btn btn-info" href="{{route('shop.single',['slug' => $wishlist->product->slug])}}"><i class="fa fa-eye"></i></a>--}}
                                                    {{--</div>--}}
                                                {{--</td>--}}
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {{ $wishlists->links() }}
                                <div class="item-footer">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12">
                                            <span class="item-views"><b>{{$wishlists->count()}} Produit(s)</b></span>
                                        </div>
                                    </div>
                                </div>
                            @else
                                <div class="alert alert-info" role="alert" id="centre">
                                    Aucun Produit dans votre liste de souhaits
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection