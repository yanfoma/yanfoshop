@extends('layouts.frontEnd.customer')

@section('title')
    Confirmer Mes Commandes YanfoShop
@endsection

@section('style')
    <style>
        .thumb-img {
            border-radius: 59px;
            height: 80px;
            width: 80px;
        }

        .action {
            text-align: center;
        }

        .table> thead> tr> th, .table> tbody> tr> th, .table> tfoot> tr> th, .table> thead> tr> td, .table> tbody> tr> td, .table> tfoot> tr> td {
            border-top: 1px solid #dddddd;
            line-height: 1.846;
            padding: 8px;
            vertical-align: middle;
        }

        .table {
            margin-bottom: 0;
        }

        .table-design td:nth-child(1) {
            width: 96px;
        }

        .table-design ul li {
            display: inline-table;
        }

        .action .label {
            border-radius: 50px;
            display: inline-block;
            height: 22px;
            line-height: 17px;
            width: 22px;
        }
    </style>
@endsection

@section('content')
    <section class="user_account">
        <div class="container">
            <div class="row">
                @include('customer.sidebar')
                <div class="col-lg-8 col-md-8">
                    <div class="widget2">
                        <div class="section-header">
                            <h5 class="heading-design-h5">
                                Confirmer Mon Virement EcoBank
                            </h5>
                            <p class="block-title-border"> Veuillez mettre en ligne votre recu de virement Ecobank afin de confirmer votre commande.</p><br>
                            <div class="widget-body">
                                <form method="POST" action="{{ route('customer.confirmStore',['id' => Auth::user()->id] ) }}" enctype='multipart/form-data'>
                                    {!! csrf_field() !!}
                                    <div class="row">
                                        <div class="col-sm-12 {!! $errors->has('commandeCode') ? 'has-error' : '' !!}">
                                            <div class="form-group">
                                                <label for="commandeCode">Mes Commandes</label>
                                                <select class="selectpicker form-control show-tick"  name="commandeCode"  data-live-search="true" data-header="Mes Commandes" title="Mes Commandes">
                                                    @foreach($mesCommandes as $commande)
                                                        <option value="{{$commande->purchasedId}}" {{ (in_array($commande->purchasedId, $user->interests->pluck('purchasedId')->toArray())) ? 'selected' : '' }}>{{$commande->purchasedId}}</option>
                                                    @endforeach
                                                </select>
                                                {!! $errors->first('commandeCode', '<small class="help-block">:message</small>') !!}
                                            </div>
                                        </div>
                                        <div class="col-sm-12 {!! $errors->has('education') ? 'has-error' : '' !!}" >
                                            <div class="form-group">
                                                <label for="dateVersement">Date De versement</label>
                                                <div class='input-group date' id='datetimepicker1'>
                                                    <input name="dateVersement" type="text" class="form-control" id="dateVersement" placeholder="Date De Versement" value="{{old('dateVersement')}}">
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                    {!! $errors->first('dateVersement', '<small class="help-block">:message</small>') !!}
                                                    @if(session()->has('ok'))
                                                        <div class="alert alert-danger alert-dismissible ">{!! session('ok') !!}</div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 {!! $errors->has('image') ? 'has-error' : '' !!}" >
                                            <div class="form-group">
                                                <label for="image" class="">Recu Du Versement</label><br>
                                                <input name="image" type="file" id="input-file-events" class="dropify-event" value="{{ old('image') }}"/>
                                                {!! $errors->first('image', '<small class="help-block">:message</small>') !!}
                                                @if(session()->has('ok'))
                                                    <div class="alert alert-danger alert-dismissible ">{!! session('ok') !!}</div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 {!! $errors->has('addresse') ? 'has-error' : '' !!}">
                                            <div class="form-group">
                                                <label for="note" class="">Observations <span class="#"></span></label>
                                                <textarea name="note" class="form-control border-form-control" rows="10" cols="50">{{ old('note') }}</textarea>
                                                {!! $errors->first('note', '<small class="help-block">:message</small>') !!}
                                                @if(session()->has('ok'))
                                                    <div class="alert alert-danger alert-dismissible ">{!! session('ok') !!}</div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 text-center">
                                                <button type="reset" class="btn btn-danger btn-lg"> Annuler </button>
                                                <button type="submit" class="btn btn-primary btn-lg"> Sauvegarger </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js" charset="UTF-8"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.es.min.js"></script>
    <script >
        $(function () {
            $('#datetimepicker1').datepicker({
                format: "dd/mm/yyyy",
                language: "fr",
                orientation: "bottom",
                autoclose: true,
                clearBtn: true,
                todayHighlight: true
            });
        });
    </script>
@endsection