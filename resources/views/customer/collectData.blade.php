@extends('layouts.frontEnd.collectData')

@section('title')
    MES INFORMATIONS Yanfoma Soko
@endsection
@section('style')
    <style>
        .widget2 {
            background: #fff none repeat scroll 0 0;
            box-shadow: 0 0 20px rgba(0,0,0,.1);
            border-radius: 12px;
            display: inline-block;
            border: 1px solid #dddddd;
            margin-bottom: 50px!important;
            padding: 30px;
            position: relative;
            width: 100%;
        }
    </style>
@endsection
@section('content')
    <section class="user_account">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="alert alert-warning text-center" role="alert">
                       Avant de Continuer Veuillez prendre quelques minutes pour renseigner vos Informations. Merci!!!
                    </div>
                    <div class="widget2">
                        <div class="section-header">
                            <h5 class="heading-design-h5">
                                Mes Informations
                            </h5>
                        </div>
                        <form method="POST" action="{{ route('customer.updateData')}}">
                            {!! csrf_field() !!}
                            <div class="row">
                                <div class="col-sm-6 {!! $errors->has('sexe') ? 'has-error' : '' !!}">
                                    <div class="form-group">
                                        <label class="control-label">Sexe</label>
                                        <select class="selectpicker form-control show-tick"  name="sexe" data-header="sexe" title="veuillez choisir votre sexe">
                                            @if($user->sexe) <option value="{{$user->sexe}}" selected > {{ $user->sexe }} </option> @endif
                                            <option value="Masculin">Masculin</option>
                                            <option value="Feminin">Feminin</option>
                                        </select>
                                        {!! $errors->first('sexe', '<small class="help-block">:message</small>') !!}
                                    </div>
                                </div>
                                <div class="col-sm-6 {!! $errors->has('dob') ? 'has-error' : '' !!}" >
                                    <div class="form-group">
                                        <label for="dob">Date de Naissance</label>
                                        <input name="dob" type="date" class="form-control" id="dob" placeholder="Date de Naissance" value="{{$user->dob}}">
                                        {!! $errors->first('dob', '<small class="help-block">:message</small>') !!}
                                        @if(session()->has('ok'))
                                            <div class="alert alert-danger alert-dismissible ">{!! session('ok') !!}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 {!! $errors->has('pays') ? 'has-error' : '' !!}">
                                    <div class="form-group">
                                        <label for="pays">Pays</label>
                                        <select name="pays" class="selectpicker form-control show-tick" data-live-search="true" data-header="Pays De Naissance" title="Pays De Naissance" required>
                                            @if($user->pays)
                                                <option value="{{$user->pays}}" selected> {{ $user->pays }} </option>
                                            @endif
                                            <option value="Burkina Faso">Burkina Faso</option>
                                            <option value="Cote d'Ivoire">Cote d'Ivoire</option>
                                            <option value="Cameroun">Cameroun</option>
                                            <option value="Mail">Mail</option>
                                            <option value="Sénégal">Sénégal</option>
                                            <option value="Rwanda">Rwanda</option>
                                        </select>
                                        {!! $errors->first('pays', '<small class="help-block">:message</small>') !!}
                                    </div>
                                </div>
                                <div class="col-sm-6 {!! $errors->has('interests') ? 'has-error' : '' !!}">
                                    <div class="form-group">
                                        <label for="interests">Mes Interets</label>
                                        <select class="selectpicker form-control show-tick"  name="interests[]" multiple data-live-search="true" data-header="Mes Interets" title="Mes Interets">
                                            @foreach($interests as $interet)
                                                <option value="{{$interet->id}}" {{ (in_array($interet->id, $user->interests->pluck('id')->toArray())) ? 'selected' : '' }}>{{$interet->name}}</option>
                                            @endforeach
                                        </select>
                                        {!! $errors->first('interests', '<small class="help-block">:message</small>') !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 {!! $errors->has('education') ? 'has-error' : '' !!}" >
                                    <div class="form-group">
                                        <label for="education">Education</label>
                                        <input name="education" type="text" class="form-control" id="education" placeholder="Education" value="{{$user->education}}">
                                        {!! $errors->first('education', '<small class="help-block">:message</small>') !!}
                                        @if(session()->has('ok'))
                                            <div class="alert alert-danger alert-dismissible ">{!! session('ok') !!}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-6 {!! $errors->has('profession') ? 'has-error' : '' !!}" >
                                    <div class="form-group">
                                        <label for="profession">Profession</label>
                                        <input name="profession" type="text" class="form-control" id="profession" placeholder="Profession" value="{{$user->profession}}">
                                        {!! $errors->first('profession', '<small class="help-block">:message</small>') !!}
                                        @if(session()->has('ok'))
                                            <div class="alert alert-danger alert-dismissible ">{!! session('ok') !!}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 {!! $errors->has('addresse') ? 'has-error' : '' !!}">
                                    <div class="form-group">
                                        <label class="control-label">Addresse <span class="required">*</span></label>
                                        <textarea name="addresse" class="form-control border-form-control" rows="10" cols="30" required> {{$user->addresse}}</textarea>
                                        {!! $errors->first('addresse', '<small class="help-block">:message</small>') !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 text-right">
                                    <button type="submit" class="btn btn-primary btn-lg"> Sauvegarger </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <br><br>
                </div>
            </div>
            <br><br>
        </div>
    </section>
@endsection
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/js/bootstrap-select.min.js"></script>
    <script type="application/javascript" src="{{asset('js/bootstrap-select.min.js')}}"></script>
    <script type="application/javascript" src="{{asset('js/intlTelInput.js')}}"></script>
    <script type="application/javascript">
        let input = document.querySelector("#phone");
        window.intlTelInput(input, {
            onlyCountries: ["bf", "ci", "ml","rw","sn"],
            nationalMode: true,
            hiddenInput: "full_phone",
            utilsScript: "../../js/utils.js" // just for formatting/placeholders etc
        });
        $("dob").flatpickr(optional_config);
    </script>
@endsection