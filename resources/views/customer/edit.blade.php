@extends('layouts.frontEnd.app')

@section('title')
    Vos informations
@endsection


@section('content')

    <div class="py-5 text-center">
        <img class="d-block mx-auto mb-4 img-circle" src="https://res.cloudinary.com/yanfomaweb/image/upload/v1537299818/afrikeveil/avatar.png" alt="" width="72" height="72">
        <h2>Vos informations personnelles</h2>
        <br>
        <p class="lead">
        </p>
    </div>
    <hr class="mb-4">
    <div class="row">

        <div class="col-md-8 order-md-1 col-md-offset-3">

            <form class="needs-validation" method="POST"
                  action="{{ route('customer.update',['id' => Auth::user()->id] ) }}">

                {!! csrf_field() !!}

                <div class="row">

                    <div class="col-md-8 mb-3 {!! $errors->has('name') ? 'has-error' : '' !!} ">
                        <label for="name">Nom</label>
                        <input name="name" type="text" class="form-control" id="name" placeholder=""value="{{ $user->name}}">
                        {!! $errors->first('name', '<small class="help-block">:message</small>') !!}
                    </div>

                    <div class="col-md-8 mb-3" {!! $errors->has('wa_number') ? 'has-error' : '' !!}>
                        <label for="wa_number">Numéro WhatSapp</label>
                        <input name="wa_number" type="text" class="form-control" id="wa_number" placeholder=""
                               value="{{ $user->wa_number}}">
                        {!! $errors->first('wa_number', '<small class="help-block">:message</small>') !!}
                    </div>

                    <div class="col-md-8 mb-3" {!! $errors->has('email') ? 'has-error' : '' !!}>

                        <label for="email">Email</label>
                        <input name="email" type="email" class="form-control" id="email" placeholder=""
                               value="{{ $user->email}}">
                        {!! $errors->first('email', '<small class="help-block">:message</small>') !!}
                    </div>

                    <div class="col-md-8 mb-3" {!! $errors->has('password') ? 'has-error' : '' !!}>
                        <label for="oldpassword">Mot de passe</label>
                        <input name="password_init" type="password" class="form-control" id="oldpassword"
                               placeholder="Ancien mot de passe" value="">
                        {!! $errors->first('password_init', '<small class="help-block">:message</small>') !!}
                        @if(session()->has('ok'))
                            <div class="alert alert-danger alert-dismissible ">{!! session('ok') !!}</div>
                        @endif
                    </div>

                    <div class="col-md-8 mb-3" {!! $errors->has('password') ? 'has-error' : '' !!}>
                        <label for="newpassword1">Nouveau mot de passe</label>
                        <input name="password" type="password" class="form-control" id="newspassword1"
                               placeholder="Nouveau mot de passe" value="">
                        {!! $errors->first('password', '<small class="help-block">:message</small>') !!}
                    </div>

                    <div class="col-md-8 mb-3" {!! $errors->has('password') ? 'has-error' : '' !!}>
                        <label for="newpassword">Confirmation nouveau mot de passe</label>
                        <input name="password_confirmation" type="password" class="form-control" id="newpassword"
                               placeholder="Confirmation nouveau mot de passe" value="">
                        {!! $errors->first('password', '<small class="help-block">:message</small>') !!}
                    </div>

                </div>
            </form>
        </div>
    </div>

    <div class="row">

        <div class="col-md-3 order-md-1 col-md-offset-2">
            <hr class="mb-4">
            <a href="javascript:history.back()" class="btn btn-primary btn-lg btn-block">
                Retour à votre compte
            </a>
            <hr class="mb-4">
        </div>

        <div class="col-md-3 order-md-1 col-md-offset-1">
            <hr class="mb-4">
            <button class="btn btn-primary btn-lg btn-block" type="submit">Valider</button>
            <hr class="mb-4">
            </form>

        </div>
    </div>
@endsection