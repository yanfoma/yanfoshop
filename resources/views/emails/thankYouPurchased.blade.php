<html>
    <body>
        <p>Cher {{$name}},
            <br><br>Nous avons reçu votre commande  <strong>No:{{$purchasedId}} Montant: {{$total}}</strong>
        </p>
        <p>
            Pour confirmer votre commande veuillez effectuer votre payment <strong> {{$payment}} </strong>
            <br><br>En rappel vous povez consulter les modes de paiement sur la Faq au niveau de votre profil ou clicquez sur
            <a href="{{route('customer.faq')}}" class="theme-btn btn-style-one center">Accéder à la FAQ</a>
            <br>pour de plus amples informations.
            <br><br>  Merci de Votre consideration!!!
        </p>
    </body>
</html>