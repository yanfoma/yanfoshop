@component('mail::message')

<span style="color:black">
    {{trans('app.contactMessage')}} {{ $name }}

    Message:{{ $message }}
</span>


@component('mail::panel', ['url' => ''])
#Contacts:

Name: {{ $name }}


Services: {{ $services }}

Phone: {{ $phone }}

Email: {{ $email }}


@endcomponent

@endcomponent
