<html>
<head></head>
    <body>
        <div class="container">
            <h1 class="text-center">You have a new Purchase</h1>
            <p>Résumer</p>
        </div>
        <table>
            @php $index = 1; @endphp
            @foreach($cart as $product)
                <tr>
                    <td>
                        <span style="color:#3b3b3b;font-weight: bold;">{{$index}}</span><br />
                        <span style="color:#3b3b3b;font-weight: bold;">Nom :</span>  {{$product->product->name}} <br />
                        <span style="color:#3b3b3b;font-weight: bold;">Quantité :</span>  {{$product->product_qty}} <br />
                        <span style="color:#3b3b3b;font-weight: bold;">Prix :</span>  {{$product->product_price}} CFA <br />
                        <span style="color:#3b3b3b;font-weight: bold;">Total :</span>  {{$product->product_total}} CFA
                        <hr>
                    </td>
                </tr>
                @php $index++; @endphp
            @endforeach
        </table>
        <p><strong>No de la Commande:</strong> {{$purchasedId}}</p>
        <p><strong>Client:</strong> {{$purchased_name}}</p>
        <p><strong>Methode de paiement:</strong> {{$payment}}</p>
        <p>Veuillez vous connecter sur <a href="https://webadmin.yanfoma.com"> Webadmin </a> pour voir les details de la commande</p>
    </body>
</html>