@extends('layouts.login')

@section('title')
    YanfoShop Login
@endsection

@section('style')
    <style>
        .hide {
            display: none;
        }
        .intl-tel-input {
            display: table-cell;
        }
        .intl-tel-input .selected-flag {
            z-index: 4;
        }
        .intl-tel-input .country-list {
            z-index: 5;
        }
        .input-group .intl-tel-input .form-control {
            border-top-left-radius: 4px;
            border-top-right-radius: 0;
            border-bottom-left-radius: 4px;
            border-bottom-right-radius: 0;
        }
    </style>
@endsection

@section('content')
    <section class="login-main-wrapper">
        <div class="container-fluid pl-0 pr-0">
            <div class="row no-gutters">
                <div class="col-md-5 p-5 bg-white full-height">
                    <div class="login-main-left">
                        <div class="text-center mb-2 login-main-left-header pt-4">
                            <a href="{{route('welcome')}}"><img src="{{env('LogoLogin')}}" class="img-fluid" alt="login logo" width="150"></a>
                        </div>
                        <form class="widget5" method="POST" action="{{ route('registerPost') }}" >
                            {{ csrf_field() }}
                            <div>
                                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name" class="control-label">{{ trans('app.name') }} </label>
                                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
                                    @if ($errors->has('name'))
                                        <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
                                    @endif
                                </div>
                            </div>
                            <div>
                                <div class="form-group {{ $errors->has('wa_number') ? ' has-error' : '' }}">
                                    <label for="wa_number" class="control-label">{{ trans('app.wa_number') }} (Veuillez inclure le code)  </label>
                                    <input id="wa_number" type="tel"  class="form-control" name="wa_number" value="{{ old('wa_number') }}" autofocus required placeholder="+22670236010">
                                    @if ($errors->has('wa_number'))
                                        <span class="help-block"><strong>{{ $errors->first('wa_number') }}</strong></span>
                                    @endif
                                </div>
                            </div>
                            <div>
                                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class="control-label">{{ trans('app.email') }} </label>
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                                    @if ($errors->has('email'))
                                        <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                                    @endif
                                </div>
                            </div>
                            <div>
                                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password" class="control-label">{{ trans('app.password') }} </label>
                                        <input id="password" type="password" class="form-control" name="password" required>
                                        @if ($errors->has('password'))
                                            <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
                                        @endif
                                </div>
                            </div>
                            <div>
                                <div class="form-group">
                                    <label for="password-confirm" class="control-label">{{ trans('app.passwordConfirm') }} </label>
                                    <input id="id" type="hidden" name="id" value="{{app('request')->get('id')}}">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                </div>
                            </div>

                            <div class="mt-4">
                                <div class="row">
                                    <div class="col-12">
                                        <button type="submit" class="btn btn-outline-primary btn-block btn-lg">{{ trans('app.send') }}</button>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center mt-3">
                                <p class="light-gray">Vous avez deja un compte ? <a class="primary-text" href="{{route('login')}}">{{ trans('app.login') }}</a></p>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="login-main-right bg-white p-5 mt-5 mb-5">
                        <div class="owl-carousel owl-carousel-login owl-theme owl-loaded">
                            <div class="item">
                                <div class="carousel-login-card text-center">
                                    <img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1547462821/Yanfoma/coupon.png" class="img-fluid" alt="LOGO">
                                </div>
                            </div>
                            <div class="item">
                                <div class="carousel-login-card text-center">
                                    <img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1538330209/Yanfoma/42742077_931203717065312_2103532500335722496_n.png" class="img-fluid" alt="LOGO">
                                    <h5 class="mt-5 mb-3">YanfoShop</h5>
                                </div>
                            </div>
                            <div class="item">
                                <div class="carousel-login-card text-center">
                                    <img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1547147206/Yanfoma/bienvenue384x256.jpg" class="img-fluid" alt="LOGO">
                                    <h5 class="mt-5 mb-3">YanfoShop: La Boutique En Ligne Africaine100% Industrie 4.0</h5>
                                </div>
                            </div>
                            <div class="item">
                                <div class="carousel-login-card text-center">
                                    <img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1547147890/Yanfoma/commande.png" class="img-fluid" alt="LOGO">
                                    <h5 class="mt-5 mb-3">Commandes Personnalisées</h5>
                                </div>
                            </div>
                            <div class="item">
                                <div class="carousel-login-card text-center">
                                    <img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1547147737/Yanfoma/livraisonFin.png" class="img-fluid" alt="LOGO">
                                    <h5 class="mt-5 mb-3">Récupérer votre commande gratuitement avec Kaanu</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script src="https://askbootstrap.com/preview/vidoe-v1-2/theme-two/vendor/owl-carousel/owl.carousel.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){

            $('.bfh-countries');

            $(".owl-carousel").owlCarousel({
                items: 1,
                lazyLoad: true,
                loop: true,
                autoplay: true,
                autoplaySpeed: 1000,
                autoplayTimeout: 2000,
                autoplayHoverPause: true,
            });
        });
    </script>
@endsection