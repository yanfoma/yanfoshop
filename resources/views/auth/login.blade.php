@extends('layouts.login')

@section('title')
    Yanfoma Soko: Se Connecter
@endsection
@section('style')
@endsection

@section('content')
    <section class="login-main-wrapper">
        <div class="container-fluid pl-0 pr-0">
            <div class="row no-gutters" >
                <div class="col-md-5 p-5 bg-white full-height">
                    <div class="login-main-left">
                        <div class="text-center mb-3 login-main-left-header pt-4">
                            <a href="{{route('welcome')}}"><img src="{{env('LogoLogin')}}" class="img-fluid" alt="login logo" width="150"></a>
                        </div>
                        <div class="">
                            <form class="widget5" method="POST" action="{{route('login')}}">
                                {{ csrf_field() }}
                                <div>
                                    <div class="form-group {{ $errors->has('wa_confirmed') ? ' has-error' : '' }}">
                                        <input id="wa_confirmed" type="checkbox" name="wa_confirmed" class="form-control" value="{{ old('wa_confirmed') }}">
                                        @if ($errors->has('wa_confirmed'))
                                            <span class="help-block"><strong>{{ $errors->first('wa_confirmed') }}</strong></span>
                                        @endif
                                        <label for="wa_confirmed" class="center-align">{{ trans('app.wa_confirmed') }} </label>
                                    </div>
                                </div>
                                <div id="whatsapp">
                                    <div class="form-group {{ $errors->has('wa_number') ? ' has-error' : '' }}">
                                        <i class="mdi-communication-call prefix"></i>
                                        <input id="wa_number" type="text" name="wa_number" class="form-control" value="{{ old('wa_number') }}">
                                        @if ($errors->has('wa_number'))
                                            <span class="help-block"><strong>{{ $errors->first('wa_number') }}</strong></span>
                                        @endif
                                        <label for="wa_number" class="center-align">{{ trans('app.wa_number') }} </label>
                                    </div>
                                </div>
                                <div id="mail">
                                    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                        <i class="mdi-communication-email prefix"></i>
                                        <input id="email" type="email" name="email"  class="form-control" value="{{ old('email') }}">
                                        @if ($errors->has('email'))
                                            <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                                        @endif
                                        <label for="email" class="center-align">{{ trans('app.email') }} </label>
                                    </div>
                                </div>
                                <div >
                                    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                        <i class="mdi-action-lock-outline prefix"></i>
                                        <input id="password" type="password" class="form-control" name="password" required>
                                        @if ($errors->has('password'))
                                            <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
                                        @endif
                                        <label for="password">{{ trans('app.password') }} </label>
                                    </div>
                                </div>
                                <div>
                                    <div class="custom-control custom-checkbox">
                                        <input id="remember" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                        <label for="remember">{{ trans('app.rememberMe') }} /</label>
                                        <a class="center primary-text" href="{{route('password.email')}}">{{ trans('app.ForgotPassword') }} ?</a>
                                    </div>
                                </div>
                                <div class="mt-4">
                                    <div class="row">
                                        <div class="col-12">
                                            <button type="submit" class="btn btn-outline-primary btn-block btn-lg">{{ trans('app.login') }}</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center mt-5">
                                    <p class="light-gray">Vous n'avez pas de compte ? <a class="primary-text" href="{{route('register')}}">{{ trans('app.register') }}</a></p>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-7 ">
                    <div class="login-main-right p-5 mt-5 mb-5 bg-white">
                        <div class="owl-carousel owl-carousel-login owl-theme owl-loaded">
                            <div class="item">
                                <div class="carousel-login-card text-center">
                                    <img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1547462821/Yanfoma/coupon.png" class="img-fluid" alt="LOGO">
                                </div>
                            </div>
                            <div class="item">
                                <div class="carousel-login-card text-center">
                                    <img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1547318237/Yanfoma/Untitled-2.jpg" class="img-fluid" alt="LOGO">
                                </div>
                            </div>
                            <div class="item">
                                <div class="carousel-login-card text-center">
                                    <img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1547147206/Yanfoma/bienvenue384x256.jpg" class="img-fluid" alt="LOGO">
                                    <h5 class="mt-5 mb-3">YanfoShop: La Boutique En Ligne Africaine100% Industrie 4.0</h5>
                                </div>
                            </div>
                            <div class="item">
                                <div class="carousel-login-card text-center">
                                    <img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1547147890/Yanfoma/commande.png" class="img-fluid" alt="LOGO">
                                    <h5 class="mt-5 mb-3">Commandes Personnalisées</h5>
                                </div>
                            </div>
                            <div class="item">
                                <div class="carousel-login-card text-center">
                                    <img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1547147737/Yanfoma/livraisonFin.png" class="img-fluid" alt="LOGO">
                                    <h5 class="mt-5 mb-3">Récupérer votre commande gratuitement avec Kaanu</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{asset('js/owlLogin.carousel.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $(".owl-carousel").owlCarousel({
                items: 1,
                lazyLoad: true,
                loop: true,
                autoplay: true,
                autoplaySpeed: 1000,
                autoplayTimeout: 2000,
                autoplayHoverPause: true,
            });
        });
    </script>
    <script  type="text/javascript">
        $('#whatsapp').hide();
        $('#wa_confirmed').on('click' , function (event) {
            if ($(this).prop('checked')) {
                $('#mail').hide();
                $('#whatsapp').show();
            } else {
                $('#mail').show();
                $('#whatsapp').hide();
            }
        });
        $('.mailSelect').selectpicker();
    </script>
@endsection

