@extends('layouts.login')
@section('title')
    Yanfoma Soko: Recuperation de Mot de Passe
@endsection

@section('content')
    <section class="login-main-wrapper">
        <div class="container-fluid pl-0 pr-0">
            <div class="row no-gutters" >
                <div class="col-md-5 p-5 bg-white full-height">
                    <div class="login-main-left">
                        <div class="text-center mb-5 login-main-left-header pt-4">
                            <a href="{{route('welcome')}}"><img src="{{env('LogoLogin')}}" class="img-fluid" alt="login logo" width="200"></a>
                        </div>
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="widget4">
                            <div class="input-field col pb-5 s12 center">
                                <h4 class="text-center">Mot de Passe Oublié</h4>
                                <p class="text-center pt-2">Réinitialiser Votre Mot de Passe</p>
                            </div>
                            <form class="form-inliner pt-5" method="POST" action="{{ route('password.email') }}">
                            {{ csrf_field() }}
                            <div>
                                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                    <i class="mdi-communication-email prefix"></i>
                                    <input id="email" type="email" name="email"  class="form-control" value="{{ old('email') }}">
                                    @if ($errors->has('email'))
                                        <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                                    @endif
                                    <label for="email" class="center-align">Votre Email</label>
                                </div>
                            </div>
                            <div class="mt-4">
                                <div class="row">
                                    <div class="col-12">
                                        <button type="submit" class="btn btn-outline-primary btn-block btn-lg">Envoyer</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-7 ">
                    <div class="login-main-right p-5 mt-5 mb-5 bg-white">
                        <div class="owl-carousel owl-carousel-login owl-theme owl-loaded">
                            <div class="item">
                                <div class="carousel-login-card text-center">
                                    <img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1547462821/Yanfoma/coupon.png" class="img-fluid" alt="LOGO">
                                </div>
                            </div>
                            <div class="item">
                                <div class="carousel-login-card text-center">
                                    <img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1547318237/Yanfoma/Untitled-2.jpg" class="img-fluid" alt="LOGO">
                                </div>
                            </div>
                            <div class="item">
                                <div class="carousel-login-card text-center">
                                    <img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1547147206/Yanfoma/bienvenue384x256.jpg" class="img-fluid" alt="LOGO">
                                    <h5 class="mt-5 mb-3">YanfoShop: La Boutique En Ligne Africaine100% Industrie 4.0</h5>
                                </div>
                            </div>
                            <div class="item">
                                <div class="carousel-login-card text-center">
                                    <img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1547147890/Yanfoma/commande.png" class="img-fluid" alt="LOGO">
                                    <h5 class="mt-5 mb-3">Commandes Personnalisées</h5>
                                </div>
                            </div>
                            <div class="item">
                                <div class="carousel-login-card text-center">
                                    <img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1547147737/Yanfoma/livraisonFin.png" class="img-fluid" alt="LOGO">
                                    <h5 class="mt-5 mb-3">Récupérer votre commande gratuitement avec Kaanu</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="row">
        <div class="col s11 m3 l5 card-panel z-depth-3 forgotPassword" style="margin-left:15px;">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <form class=" offset-s3" method="POST" action="{{ route('password.email') }}">
                {{ csrf_field() }}
                <div class="row">
                    <div class="input-field col s12 center">
                        <h4>{{ trans('app.ForgotPassword') }}</h4>
                        <p class="center">{{ trans('app.YouCanResetYourPassword') }}</p>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s12{{ $errors->has('email') ? ' has-error' : '' }}">
                        <i class="mdi-communication-email prefix"></i>
                        <input id="email" type="email" name="email" value="{{ old('email') }}" required >
                        @if ($errors->has('email'))
                            <span class="help-block" >
                                <strong style="margin-bottom: 20px;!important;">{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                        <label for="email" class="center-align">{{ trans('app.email') }}</label>
                    </div>
                </div>
                <div class="row" style="margin-top: 20px;!important;">
                    <button type="submit" class="btn btn-primary center">
                        {{ trans('app.SendPasswordResetLink') }}
                    </button>
                    <a href="{{route('login')}}" class="btn btn-primary green">{{ trans('app.login') }}</a>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript" src="{{asset('js/owlLogin.carousel.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $(".owl-carousel").owlCarousel({
                items: 1,
                lazyLoad: true,
                loop: true,
                autoplay: true,
                autoplaySpeed: 1000,
                autoplayTimeout: 2000,
                autoplayHoverPause: true,
            });
        });
    </script>
    <script  type="text/javascript">
        $('#whatsapp').hide();
        $('#wa_confirmed').on('click' , function (event) {
            if ($(this).prop('checked')) {
                $('#mail').hide();
                $('#whatsapp').show();
            } else {
                $('#mail').show();
                $('#whatsapp').hide();
            }
        });
        $('.mailSelect').selectpicker();
    </script>
@endsection
