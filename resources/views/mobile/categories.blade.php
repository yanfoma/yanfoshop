@extends('layouts.frontEnd.mobile.app')

@section('title')
    Toutes Nos Categories
@endsection

@section('content')
    <div class="page-content header-clear-large">
        <div class="widgetSimple2 top-20">
            <div data-height="130" class="caption caption-margins round-medium shadow-huge" style="height: 130px;">
                <div class="caption-center">
                    <h1 class="primary-heading bolder text-center">Toutes Nos Categories</h1>
                </div>
                <div class="caption-overlay bg-black opacity-70"></div>
                <div class="caption-bg bg-18"></div>
            </div>
        </div>
        <div class="widgetSimple2">
            <div class="archive-box-item">
                <div class="archive-box-content archive-box-current">
                    @if($categories->count())
                        @foreach($categories as $category)
                            <a href="{{route('viewCategory',['store_slug' => $store->slug,'name' => $category->name ])}}" class="primary-heading">{{$category->name}}<em>({{$category->products->count()}})</em></a>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
            <script type="text/javascript">
                //Preload Image
                $(function() {$(".preload-search-image").lazyload({threshold : 0});});

                //Search Menu Functions
                function search_menu(){
                    $('[data-search]').on('keyup', function() {
                        var searchVal = $(this).val();
                        if (searchVal != '') {
                            $('.menu-search-trending').addClass('disabled-search-item');
                            $('#menu-search-list .search-results').removeClass('disabled-search-list');
                            $('#menu-search-list [data-filter-item]').addClass('disabled-search-item');
                            $('#menu-search-list [data-filter-item][data-filter-name*="' + searchVal.toLowerCase() + '"]').removeClass('disabled-search-item');
                        } else {
                            $('.menu-search-trending').removeClass('disabled-search-item');
                            $('#menu-search-list .search-results').addClass('disabled-search-list');
                            $('#menu-search-list [data-filter-item]').removeClass('disabled-search-item');
                        }
                    });
                    return false;
                }
                search_menu();

                //Menu Search Values//
                $('.menu-search-trending a').on('click',function(){
                    var e = jQuery.Event("keydown");
                    e.which = 32;
                    var search_value = $(this).text();
                    $('.search-header input').val(search_value);
                    $('.search-results').removeClass('disabled-search-list');
                    $('[data-filter-item]').addClass('disabled-search-item');
                    $('[data-filter-item][data-filter-name*="' + search_value.toLowerCase() + '"]').removeClass('disabled-search-item');
                    $('#search-page').addClass('move-search-list-up');
                    $('.search-header a').addClass('search-close-active');
                    $('.menu-search-trending').addClass('disabled-search-item');
                    return false;
                });

                $('#menu-hider, .close-menu, .menu-hide').on('click',function(){
                    $('.menu-box').removeClass('menu-box-active');
                    $('#menu-hider').removeClass('menu-hider-active');
                    setTimeout(function(){$('#search-page').removeClass('move-search-list-up');},100);
                    $('[data-filter-item]').addClass('disabled-search-item');
                    $('.search-header input').val('');
                    $('.menu-search-trending').removeClass('disabled-search-item');
                    $('.search-header a').removeClass('search-close-active');
                    $('#search-page').removeClass('move-search-list-up');
                    return false;
                });
                $('#menu-search-list input').on('focus',function(){
                    $('#search-page').addClass('move-search-list-up');
                    $('.search-header a').addClass('search-close-active');
                    return false;
                });
                $('.search-header a').on('click',function(){
                    $('.menu-search-trending').removeClass('disabled-search-item');
                    $('#menu-search-list .search-results').addClass('disabled-search-list');
                    $('.search-header input').val('');
                    $('#search-page').removeClass('move-search-list-up');
                    $('.search-header a').removeClass('search-close-active');
                    return false;
                });
            </script>
@endsection

@section('post_header')
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" 		   content="Yanfoma Soko" />
    <meta property="og:site_name" 	   content="Yanfoma">
    <meta property="fb:app_id" 		   content="400025927061215">
    <meta property="og:title"          content="Yanfoma Soko" />
    <meta property="og:description"    content="Yanfoma Soko" />
    <meta property="og:image"          content="https://res.cloudinary.com/yanfomaweb/image/upload/v1553782826/Yanfoma/metaImage2.png')}}" />
    <meta property="og:type" 	       content="article">
    <meta property="og:url"            content="https://yanfoma.com">
@endsection

