@extends('layouts.frontEnd.mobile.appHome')

@section('title')
    Yanfoma Soko Devenir Vendeur
@endsection
@section('content')
    <div class="page-content header-clear-large">
        <div class="widget">
            <div class="content">
                <img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1554927676/Yanfoma/VertWback.png" alt="logo yanfoShop" class="preload-image center-item" width="120">
            </div>
            <div class="content-title bottom-30 top-30">
                <h1 class="bottom-10 color-primary-soko text-center font-20">Comment Devenir Vendeur ?</h1>
                <p class="justify-text">
                    Conditions pour la vente de produits via une boutique dediée Yanfoma Soko (yanfoma.com):
                    <br><br><strong>1.</strong> Frais: Gratuit
                    <br><strong>2.</strong> Chaque vendeur doit avoir un logo, une description de sa boutique en ligne et un identifiant pour identifier la boutique sur Soko
                    <br><strong>3.</strong> Yanfoma reserve le droit de motifier ces termes.
                    <br><br>Pour être vendeur sur Yanfoma Soko, envoyez votre identifiant, la description de votre boutique, la liste de vos produits(nom, prix, description du produit, image), ainsi que le logo à l'adresse mail info@yanfoma.tech
                </p>
            </div>
        </div>
        <div class="decoration decoration-margins"></div>
    </div>
    <a href="#" class="back-to-top-badge back-to-top-small bg-highlight"><i class="fa fa-angle-up"></i>Back to Top</a>
@endsection

@section('post_header')
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" 		   content="Yanfoma Soko" />
    <meta property="og:site_name" 	   content="Yanfoma">
    <meta property="fb:app_id" 		   content="400025927061215">
    <meta property="og:title"          content="Yanfoma Soko" />
    <meta property="og:description"    content="Yanfoma Soko" />
    <meta property="og:image"          content="https://res.cloudinary.com/yanfomaweb/image/upload/v1553782826/Yanfoma/metaImage2.png')}}" />
    <meta property="og:type" 	       content="article">
    <meta property="og:url"            content="https://yanfoma.com">
@endsection