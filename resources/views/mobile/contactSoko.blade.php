@extends('layouts.frontEnd.mobile.appHome')

@section('title')
    Yanfoma Soko Contact
@endsection
@section('content')
    <div class="page-content header-clear-large">
        <div id="page-transitions" class="page-build light-skin highlight-blue">
            <div class="page-content header-clear-small">
                <div class="widgetSimple2">
                    <div class="content-title bottom-20 top-20">
                        <h4 class="primary-heading-soko text-center">Envoyez Nous Un Email</h4>
                    </div>
                    <div class="container bottom-0">
                        <div class="contact-form">
                            <form action="{{route('email')}}" class="contactForm" method="post" novalidate="novalidate" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <fieldset>
                                    <div class="formFieldWrap">
                                        <label class="field-title contactNameField" for="contactNameField">Nom*</label>
                                        <input type="text" name="name" value="{{ old('name') }}" placeholder="Votre Nom *" required class="contactField requiredField" id="contactNameField" />
                                        @if ($errors->has('name'))
                                            <div id="uname-error" class="error">{{ $errors->first('name') }}</div>
                                        @endif
                                    </div>
                                    <div class="formFieldWrap">
                                        <label class="field-title contactPhoneField" for="contactPhoneField">Portable*</label>
                                        <input type="text" name="phone" value="{{ old('phone') }}" placeholder="Votre numero de portable (veuillez preciser l'indicatif) *" required class="contactField requiredField" id="#" />
                                        @if ($errors->has('phone'))
                                            <div id="uname-error" class="error">{{ $errors->first('phone') }}</div>
                                        @endif
                                    </div>
                                    <div class="formFieldWrap">
                                        <label class="field-title contactEmailField" for="contactEmailField">Email*</label>
                                        <input type="email" name="email" value="{{ old('email') }}" placeholder="Votre e-mail *" required aria-required="true" class="contactField requiredField requiredEmailField" id="#" />
                                        @if ($errors->has('email'))
                                            <div id="uname-error" class="error">{{ $errors->first('email') }}</div>
                                        @endif
                                    </div>

                                    <div class="formFieldWrap">
                                        <label class="field-title contactEmailField" for="contactSubjectField">Sujet</label>
                                        <input type="text" name="subject" value="{{ old('subject') }}" placeholder="Sujet *" required aria-required="true" class="contactField requiredField" id="#" />
                                        @if ($errors->has('subject'))
                                            <div id="uname-error" class="error">{{ $errors->first('subject') }}</div>
                                        @endif
                                    </div>
                                    <div class="formTextareaWrap">
                                        <label class="field-title contactMessageTextarea" for="contactMessageTextarea">Message*</label>
                                        <textarea name="message" placeholder="Votre Message...." aria-required="true" rows="3" class="contactTextarea requiredField" id="#">{{ old('message') }}</textarea>
                                        @if ($errors->has('message'))
                                            <div id="uname-error" class="error">{{ $errors->first('message') }}</div>
                                        @endif
                                    </div>
                                    <div class="formSubmitButtonErrorsWrap contactFormButton">
                                        <input type="submit" class="buttonWrap button bg-yellow-dark button-sm button-rounded uppercase ultrabold contactSubmitButton shadow-small top-30" id="#" value="Send Message" data-formId="contactForm" />
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                    <div class="decoration"></div>
                </div>
                <div class="content widget">
                    <div class="content-title bottom-20 top-30 ">
                        <h1 class="primary-heading-soko text-center">Nos Coordonnées</h1>
                    </div>
                    <div class="decoration"></div>
                    <div class="contact-information last-column">
                        <div class="container no-bottom">
                            <p class="contact-information">
                                <strong class="primary-heading-soko">Addresse</strong>
                                <br> Ouagadougou - Burkina Faso
                                <br> Abidjan, Cote d'Ivoire
                                <br> Yaounde, Cameroun
                                <br> Kigali, Rwanda
                            </p>
                            <p class="contact-information">
                                <strong class="primary-heading-soko">Infoline</strong>
                                <br>
                                <a href="tel: +226 71 33 15 23"><i class="fa fa-phone-square color-green-dark"></i>+(226) 71 33 15 23</a>
                                <a href="tel: +226 74 33 42 77"><i class="fa fa-phone-square color-green-dark"></i>+(226) 74 33 42 77</a>
                                <a href="tel: +886 989 59 72 35"><i class="fa fa-phone-square color-green-dark"></i>+(886) 989 59 72 35</a>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="widget">
                    <h1 class="primary-heading-soko text-center bottom-20">Nos Reseaux Sociaux</h1>
                    <div class="content">
                        <a href="https://www.facebook.com/yanfoma/" class="button-social button-rounded shadow-small bg-facebook button button-sm button-icon regularbold"><i class="fab fa-facebook-f"></i>Facebook</a>
                        <a href="https://m.me/yanfoma" class="button-social button-rounded shadow-small bg-twitter button button-sm button-icon regularbold"><i class="fa fa-comments-o"></i>Mesenger</a>
                        <a href="https://chat.whatsapp.com/invite/EsCjgPWHuPf39ujQKANX5C" class="button-social button-rounded shadow-small bg-whatsapp button button-sm button-icon regularbold"><i class="fab fa-whatsapp"></i>Whatsapp</a>
                        <a href="https://www.linkedin.com/company-beta/22333242" class="button-social button-rounded shadow-small bg-linkedin button button-sm button-icon regularbold"><i class="fab fa-linkedin"></i>Linkedin</a>
                    </div>
                </div>
            </div>
            <a href="#" class="back-to-top-badge back-to-top-small bg-highlight"><i class="fa fa-angle-up"></i>Back to Top</a>
        </div>
    </div>
@endsection

@section('post_header')
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" 		   content="Yanfoma Soko" />
    <meta property="og:site_name" 	   content="Yanfoma">
    <meta property="fb:app_id" 		   content="400025927061215">
    <meta property="og:title"          content="Yanfoma Soko" />
    <meta property="og:description"    content="Yanfoma Soko" />
    <meta property="og:image"          content="https://res.cloudinary.com/yanfomaweb/image/upload/v1553782826/Yanfoma/metaImage2.png')}}" />
    <meta property="og:type" 	       content="article">
    <meta property="og:url"            content="https://yanfoma.com">
@endsection