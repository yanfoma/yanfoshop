@extends('layouts.frontEnd.mobile.appHome')

@section('title')
    Yanfoma Soko Créer un Compte
@endsection
@section('content')
    <div class="page-content header-clear-large">
        <div class="page-login page-login-full">
            <div class="widget">
                <h3 class="ultrabold top-30 bottom-0 center-text primary-heading-soko">Créer un Compte</h3>
                <form class="widget5" method="POST" action="{{ route('registerPost') }}">
                    {{ csrf_field() }}
                    <div class="page-login-field top-30">
                        @if ($errors->has('name'))
                            <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
                        @endif
                        <i class="fa fa-user color-yellow-dark"></i>
                        <input id="name" type="text" name="name" value="{{ old('name') }}" placeholder="Votre Nom" required>
                        <em>(requis)</em>
                    </div>
                    <div id="whatsapp" class="page-login-field top-30" >
                        @if ($errors->has('wa_number'))
                            <span class="help-block"><strong>{{ $errors->first('wa_number') }}</strong></span>
                        @endif
                        <i class="fa fa-phone color-yellow-dark"></i>
                        <input id="wa_number" type="text" name="wa_number" value="{{ old('wa_number') }}" placeholder="Votre numéro Whatsapp veillez l'indicatif">
                        <em>(requis)</em>
                    </div>
                    <div class="page-login-field top-30">
                        @if ($errors->has('email'))
                            <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                        @endif
                        <i class="fa fa-user color-yellow-dark"></i>
                        <input id="email" type="email" name="email" value="{{ old('email') }}" placeholder="Votre Email" required>
                        <em>(requis)</em>
                    </div>
                    <div class="page-login-field bottom-30">
                        @if ($errors->has('password'))
                            <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
                        @endif
                        <i class="fa fa-lock color-yellow-dark"></i>
                        <input id="password" type="password" class="form-control" name="password" required placeholder="Votre Mot de Passe">
                        <em>(requis)</em>
                    </div>
                    <div class="page-login-field bottom-30">
                        @if ($errors->has('password'))
                            <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
                        @endif
                        <i class="fa fa-lock color-yellow-dark"></i>
                        <input id="id" type="hidden" name="id" value="{{app('request')->get('id')}}">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Confirmer votre mot de passe">
                        <em>(requis)</em>
                    </div>
                    <div class="page-login-links bottom-10">
                        Vous avez deja un compte ?<a class="forgot float-right primary-heading-soko" href="{{route('mobile.login')}}"><i class="fa fa-user float-right"></i>Se Connecter</a>
                        <div class="clear"></div>
                    </div>
                    <button class="button bg-yellow-dark button-full button-rounded button-sm uppercase ultrabold shadow-small">Créer</button>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('post_header')
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" 		   content="Yanfoma Soko" />
    <meta property="og:site_name" 	   content="Yanfoma">
    <meta property="fb:app_id" 		   content="400025927061215">
    <meta property="og:title"          content="Yanfoma Soko" />
    <meta property="og:description"    content="Yanfoma Soko" />
    <meta property="og:image"          content="https://res.cloudinary.com/yanfomaweb/image/upload/v1553782826/Yanfoma/metaImage2.png')}}" />
    <meta property="og:type" 	       content="article">
    <meta property="og:url"            content="https://yanfoma.com">
@endsection