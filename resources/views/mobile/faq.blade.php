@extends('layouts.frontEnd.mobile.app')

@section('title')
    Foire Aux Questions YanfoShop
@endsection
@section('content')
    <div class="page-content header-clear-large">
        <div class="widgetSimple2 bottom-30 top-20 shadow-small" style="padding:10px;">
            <div class="faq-wrapper">
                <h3 class="primary-heading center-text">FOIRE AUX QUESTIONS</h3>
                @if($faqs->count())
                    @foreach($faqs as $faq)
                        <div class="faq">
                            <h6 class="faq-question pointer color-primary">{{$faq->question}}<i class="fa fa-plus"></i></h6>
                            <div class="faq-answer">
                                <p class="justify-text">{!! $faq->response !!}</p>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
@endsection

@section('post_header')
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" 		   content="Yanfoma Soko" />
    <meta property="og:site_name" 	   content="Yanfoma">
    <meta property="fb:app_id" 		   content="400025927061215">
    <meta property="og:title"          content="Yanfoma Soko" />
    <meta property="og:description"    content="Yanfoma Soko" />
    <meta property="og:image"          content="https://res.cloudinary.com/yanfomaweb/image/upload/v1553782826/Yanfoma/metaImage2.png')}}" />
    <meta property="og:type" 	       content="article">
    <meta property="og:url"            content="https://yanfoma.com">
@endsection