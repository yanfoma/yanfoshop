@extends('layouts.frontEnd.mobile.appHome')

@section('title')
    Mes Commandes Yanfoma Soko
@endsection

@section('content')
    <div class="page-content header-clear-large">
        <div id="page-vcard">
            <div class="widgetSimple2">
                <div class="vcard-header">
                    <img data-src="https://res.cloudinary.com/yanfomaweb/image/upload/v1539208782/Yanfoma/avatar-1.png" src="https://res.cloudinary.com/yanfomaweb/image/upload/v1539208782/Yanfoma/avatar-1.png" class="preload-image shadow-medium"  alt="User">
                    <h4 class="">{{Auth::user()->name}}</h4>
                    <em class="small-text color-gray-dark bottom-10">{{Auth::user()->email}}</em>
                    @if($user->verified =="yes")
                        <a href="#" target="_blank" class="default-link button button-xs button-green button-rounded uppercase ultrabold bottom-30 shadow-small"><i class="fa fa-check-circle"></i> Email Verifer</a>
                    @endif
                </div>
            </div>
            <div class="decoration opacity-90 bottom-0"></div>

            <div class="widgetSimple2 bottom-50">
                @if($mesCommandes->count())
                    <table class="table-borders-dark shadow-small">
                        <tbody><tr>
                            <th>Code</th>
                            <th>Date</th>
                            <th>Total</th>
                            <th>Detail</th>
                            <th>Suivre</th>
                        </tr>
                        @foreach($mesCommandes as $index => $commande)
                            <tr>
                                <td> {{$commande->purchasedId}}</td>
                                <td>{{\Jenssegers\Date\Date::parse($commande->created_at)->format('d-m-Y')}}</td>
                                <td>{{ $commande->total}} CFA</td>
                                <td><a class="color-primary" href="{{route('customer.commandeDetail',['id' => $commande->id])}}">Detail</a></td>
                                <td><a class="color-primary-soko" href="{{route('customer.suivreCommande',['purchasedId' => $commande->purchasedId])}}"><i class="fa fa-truck"></i> Suivre</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="pagination">
                        {{$mesCommandes->links() }}
                    </div>
                @endif
                    <span class="item-views"><b>{{$mesCommandes->count()}} Commandes</b></span>
            </div>
        </div>
    </div>
@endsection

@section('post_header')
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" 		   content="Yanfoma Soko" />
    <meta property="og:site_name" 	   content="Yanfoma">
    <meta property="fb:app_id" 		   content="400025927061215">
    <meta property="og:title"          content="Yanfoma Soko" />
    <meta property="og:description"    content="Yanfoma Soko" />
    <meta property="og:image"          content="https://res.cloudinary.com/yanfomaweb/image/upload/v1553782826/Yanfoma/metaImage2.png')}}" />
    <meta property="og:type" 	       content="article">
    <meta property="og:url"            content="https://yanfoma.com">
@endsection