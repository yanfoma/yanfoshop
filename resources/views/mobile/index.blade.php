@extends('layouts.frontEnd.mobile.app')

@section('title')
    Yanfoma Soko
@endsection

@section('content')
    <div class="page-content header-clear-large">
        @if($slides->count())
            <div class="widgetSlider">
                <div class="#">
                    @if($store->id == 1)
                        <div class="single-slider owl-carousel owl-no-dots">
                        <div class="item shadow-small">
                            <img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1555428457/Yanfoma/welcomeMob.png" alt="img">
                        </div>
                        <div class="item shadow-small">
                            <img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1555435722/Yanfoma/Makers.png" alt="img">
                        </div>
                        <div class="item shadow-small">
                            <img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1555427437/Yanfoma/CloudMob.png" alt="img">
                        </div>
                        <div class="item shadow-small">
                            <img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1555426897/Yanfoma/PaymentsMobile.png" alt="img">
                        </div>
                        <div class="item shadow-small">
                            <img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1555427833/Yanfoma/livraisonMob.png" alt="img">
                        </div>
                    </div>
                    @else
                        @foreach($slides as $slide)
                            <div class="item shadow-small">
                                <img src="{{$slide->image}}" alt="img">
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        @endif
        <div class="content-boxed">
            <div class="content">
                <div class="divider"></div>
                <div class="grid-columns bottom-30">
                    <div>
                        <i class="fa fa-truck color-blue-dark fa-3x"></i>
                        <h3 class=" color-primary font-15">Livraison Gratuite</h3>
                        {{--<p>--}}
                            {{--Livraison Gratuite pour tous nos produits--}}
                        {{--</p>--}}
                    </div>
                    <div>
                        <i class="fa fa-bolt color-blue-dark fa-3x"></i>
                        <h3 class="color-primary font-15">Commande Rapide</h3>
                        {{--<p>--}}
                            {{--Produits de Produits de Qualité et d'Origine Unique--}}
                        {{--</p>--}}
                    </div>
                    <div>
                        <i class="fa fa-thumbs-o-up color-blue-dark fa-2x"></i>
                        <h3 class="color-primary font-15">Qualité & Originalité</h3>
                        {{--<p>--}}
                            {{--Commande Rapide et Personnalisée--}}
                        {{--</p>--}}
                    </div>
                    <div>
                        <i class="far fa-life-ring color-blue-dark fa-3x"></i>
                        <h3 class="color-primary font-15">Support 24h & 7j/7</h3>
                        {{--<p>--}}
                            {{--Une Équipe Performante À Votre écoute 24H/24 7j/7--}}
                        {{--</p>--}}
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <div class="widget1">
            <div class="news-home">
                <div class="news-tabs bottom-30">
                    <a href="#" data-tab="news-tab-1" class="one-half center-text active-tab-button">Nouveautés</a>
                    <a href="#" data-tab="news-tab-2" class="one-half last-column center-text bg-white">Bons Deals</a>
                    <div class="clear"></div>
                </div>
                <div class="news-tabs-content">
                    <div class="tab-item active-tab bg-white" id="news-tab-1">
                        <div class="single-slider owl-carousel owl-no-dots bottom-30">
                            @foreach($nouveautes as $product)
                                <div class="store-slide-1">
                                    <a href="#" class="store-slide-image"><img class="responsive-image" src="{{$product->image_url}}" alt="{{$product->name}}"></a>
                                    <div class="store-slide-title bottom-15">
                                        <a href="{{route('shop.single',['store_slug' => $store->slug, 'slug' => $product->slug])}}">
                                            <h3 class="bottom-0 center-text">{{$product->name}}</h3>
                                        </a>
                                        {{--<em class="color-gray-dark small-text center-text">Sport Band, Water Proof, 132 GB, Black</em>--}}
                                    </div>
                                    <div class="store-slide-price">
                                        @if($product->onSale)
                                            <b>{{number_format($product->sale_amount, 0 , ',' , ' ')}} CFA<br> <del>{{number_format($product->price, 0 , ',' , ' ')}}</del></b>
                                        @else
                                            <b>{{number_format($product->price, 0 , ',' , ' ')}} CFA</b>
                                        @endif
                                        <a  href="{{route('shop.single',['store_slug' => $store->slug, 'slug' => $product->slug])}}"> <span class="button bg-primary button-rounded button-s uppercase ultrabold shadow-small" ><i class="fa fa-eye"></i>Voir</span></a>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="store-slide-button">
                                        <a href="{{route('store.cart.addDirect',['store_slug' => $store->slug, 'id' => $product->id])}}" class="button bg-primary button-center button-rounded button-full button-s uppercase ultrabold shadow-small">Ajouter au Panier</a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="tab-item" id="news-tab-2">
                        <div class="single-slider owl-carousel owl-no-dots bottom-30">
                            @foreach($bonsDeals as $product)
                                <div class="store-slide-1">
                                    <a href="#" class="store-slide-image"><img class="responsive-image" src="{{$product->image_url}}" alt="{{$product->name}}"></a>
                                    <div class="store-slide-title bottom-15">
                                        <a href="{{route('shop.single',['store_slug' => $store->slug, 'slug' => $product->slug])}}">
                                            <h3 class="bottom-0 center-text">{{$product->name}}</h3>
                                        </a>
                                        {{--<em class="color-gray-dark small-text center-text">Sport Band, Water Proof, 132 GB, Black</em>--}}
                                    </div>
                                    <div class="store-slide-price">
                                        @if($product->onSale)
                                            <b>{{number_format($product->sale_amount, 0 , ',' , ' ')}} CFA<br> <del>{{number_format($product->price, 0 , ',' , ' ')}}</del></b>
                                        @else
                                            <b>{{number_format($product->price, 0 , ',' , ' ')}} CFA</b>
                                        @endif
                                        <a  href="{{route('shop.single',['store_slug' => $store->slug, 'slug' => $product->slug])}}"> <span class="button bg-primary button-rounded button-s uppercase ultrabold shadow-small" ><i class="fa fa-eye"></i>Voir</span></a>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="store-slide-button">
                                        <a href="{{route('store.cart.addDirect',['store_slug' => $store->slug, 'id' => $product->id])}}" class="button bg-primary button-center button-rounded button-full button-s uppercase ultrabold shadow-small">Ajouter au Panier</a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-boxed widget">
            <div class="content-title bottom-20">
                <h2 class="uppercase ultrabold center-text full-top small-bottom primary-heading">Nos Catégories</h2>
                {{--<a href="#" class="color-primary uppercase ultrabold">Tout Voir <i class="fa fa-arrow-circle-o-right"></i></a>--}}
            </div>

            <div class="content">
                <ul class="link-list">
                    @foreach($categories as $categorie)
                        <li><a href="{{route('viewCategory',['store_slug' => $store->slug,'name' => $categorie->name ])}}">
                                <i class="fa fa-star color-yellow-dark"></i>
                                <span>{{$categorie->name}}</span>
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        @if($store->id == 1)
        <div class="content-strip">
            <div class="container">
                <h2 class="bold uppercase color-white center-text">NOS SERVEURS</h2>
                <p class="small-text primary-heading opacity-50 center-text half-bottom top-30">Avec Yanfoma Cloud Services</p>
                <p class="boxed-text color-white opacity-80 bottom-50">
                    Solution VPS a partir de 6000 Francs CFA seulement par Mois.
                </p>
                <a href="https://yacs.yanfoma.tech" target="_blank" class="button button-ghost border-gray-light button-rounded button-center full-top button-s uppercase bold">Acceder</a>
            </div>
            <div class="overlay bg-black opacity-70"></div>
            <div class="content-strip-bg preload-image" data-src="https://res.cloudinary.com/yanfomaweb/image/upload/v1557411254/Yanfoma/back233.png" style="background-image: url(https://www.enableds.com/products/appeca20/images/pictures/1.jpg);"></div>
        </div>
        <div class="content content-boxed widget">
            <div class="decoration"></div>
            <div class="content">
                <h4 class="uppercase ultrabold full-top small-bottom center-text primary-heading">Nos Serveurs</h4>
            </div>
                <div class="content">
                    @foreach($cloud->products as $key=>$product)
                         @if(++$key % 2 ==1)
                             <div class="one-half">
                                 <img data-src="{{$product->image_url}}" src="{{$product->image_url}}" alt="{{$product->name}}" class="preload-image rounded-image responsive-image">
                                 <a class="center-text" href="{{route('shop.single',['store_slug' => $store->slug, 'slug' => $product->slug])}}">{{$product->name}}</a>
                             </div>
                         @elseif($key % 2 ==0)
                             <div class="one-half last-column">
                                 <img data-src="{{$product->image_url}}" src="{{$product->image_url}}" alt="{{$product->name}}" class="preload-image rounded-image responsive-image">
                                 <a class="center-text" href="{{route('shop.single',['store_slug' => $store->slug, 'slug' => $product->slug])}}">{{$product->name}}</a>
                             </div>
                         <div class="clear"></div>
                         @endif
                    @endforeach
                </div>
            </div>
        @endif
        <div class="">
            <div class="store-items widgetSimpl1">
                @foreach($products as $product)
                <div class="store-item widgetSimple3">
                    <a href="{{route('shop.single',['store_slug' =>$store->slug,'slug' => $product->slug])}}"><img src="{{$product->image_url}}" alt="{{$product->name}}"></a>
                    <br>
                    <div class="buttons-row content-center">
                        <a href="{{route('store.cart.addDirect',['store_slug' => $store->slug,'id' => $product->id])}}" class="scale-hover store-item-button-1"><i class="fa fa-shopping-cart"></i></a>
                        <a href="{{route('shop.single',['store_slug' =>$store->slug,'slug' => $product->slug])}}" class="scale-hover store-item-button-3"><i class="fa fa-eye"></i></a>
                    </div>
                    @if($product->onSale)
                    <em class="primary-heading">
                        <del class="color-red-light">{{number_format($product->price, 0 , ',' , ' ')}}</del>
                        {{number_format($product->sale_amount, 0 , ',' , ' ')}} CFA
                    </em>
                    <em class="offer">Promo</em>
                    @else
                        <em class="primary-heading center-text">
                            {{number_format($product->price, 0 , ',' , ' ')}} CFA
                        </em>
                    @endif
                    <a href="{{route('shop.single',['store_slug' =>$store->slug,'slug' => $product->slug])}}"><strong class="bottom-20">{{$product->name}}</strong></a>
                </div>
                @endforeach
                <div class="clear"></div>
            </div>
        </div>
        @if($store->id == 1)
            <div class="widget content top-30">
                <h3 class="uppercase ultrabold half-top no-bottom center-text color-black"><i class="fa fa-link color-blue-dark icon-clear-right"></i>Besoin d'Aide ?</h3>
                <p class="center-text half-bottom">Veuillez consulter notre centre d'aide. Nous y repondons a toutes vos questions!!!</p>
                <div class="store-links bottom-15">
                    <div class="store-slide-button">
                        <a href="{{route('mobile.help',['store_slug' => $store->slug])}}" class="button bg-primary button-center button-rounded button-full button-s color-white uppercase ultrabold shadow-small">Centre D'Aide</a>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection

@section('post_header')
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" 		   content="Yanfoma Soko" />
    <meta property="og:site_name" 	   content="Yanfoma">
    <meta property="fb:app_id" 		   content="400025927061215">
    <meta property="og:title"          content="Yanfoma Soko" />
    <meta property="og:description"    content="Yanfoma Soko" />
    <meta property="og:image"          content="https://res.cloudinary.com/yanfomaweb/image/upload/v1553782826/Yanfoma/metaImage2.png')}}" />
    <meta property="og:type" 	       content="article">
    <meta property="og:url"            content="https://yanfoma.com">
@endsection