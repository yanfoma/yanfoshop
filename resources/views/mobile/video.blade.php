@extends('layouts.frontEnd.mobile.app')

@section('title')
    YanfoShop En Video
@endsection
@section('content')
    <div class="page-content header-clear-large">
        <div class="widget1">
            <h4 class="bolder bottom-20 primary-heading text-center">Vidéo De Présentation de YanfoShop</h4>
            <div class="responsive-video">
                <iframe class="image-rounded" src="https://www.youtube.com/embed/PmW0euwk0Ck" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
        <div class="decoration decoration-margins"></div>
    </div>
    <a href="#" class="back-to-top-badge back-to-top-small bg-highlight"><i class="fa fa-angle-up"></i>Back to Top</a>
@endsection

@section('post_header')
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" 		   content="Yanfoma Soko" />
    <meta property="og:site_name" 	   content="Yanfoma">
    <meta property="fb:app_id" 		   content="400025927061215">
    <meta property="og:title"          content="Yanfoma Soko" />
    <meta property="og:description"    content="Yanfoma Soko" />
    <meta property="og:image"          content="https://res.cloudinary.com/yanfomaweb/image/upload/v1553782826/Yanfoma/metaImage2.png')}}" />
    <meta property="og:type" 	       content="article">
    <meta property="og:url"            content="https://yanfoma.com">
@endsection