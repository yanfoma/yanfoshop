@extends('layouts.frontEnd.mobile.appHome')

@section('title')
    Changer De Mot De Passe Yanfoma Soko
@endsection

@section('content')
    <div class="page-content header-clear-large">
        <div id="page-vcard">
            <div class="widgetSimple2">
                <div class="vcard-header">
                    <img data-src="https://res.cloudinary.com/yanfomaweb/image/upload/v1539208782/Yanfoma/avatar-1.png" src="https://res.cloudinary.com/yanfomaweb/image/upload/v1539208782/Yanfoma/avatar-1.png" class="preload-image shadow-medium"  alt="User">
                    <h4 class="">{{Auth::user()->name}}</h4>
                    <em class="small-text color-gray-dark bottom-10">{{Auth::user()->email}}</em>
                    @if($user->verified =="yes")
                        <a href="#" target="_blank" class="default-link button button-xs button-green button-rounded uppercase ultrabold bottom-30 shadow-small"><i class="fa fa-check-circle"></i> Email Verifer</a>
                    @endif
                </div>
            </div>
            <div class="decoration opacity-90 bottom-0"></div>

            <div class="bottom-50">
                <form class="bottom-50" method="POST" action="{{ route('customer.updatePassword',['id' => Auth::user()->id] ) }}">
                    {!! csrf_field() !!}
                    <div class="content">
                        <div class="notification-large notification-has-icon notification-yellow bottom-0">
                            <div class="notification-icon"><i class="fa fa-exclamation notification-icon"></i></div>
                            <h1 class="uppercase ultrabold">Attention</h1>
                            <p>Pour plus de Sécurité veuillez choisir un mot de passe de 8 lettres minimum composé de lettres en Majuscule, minuscule, chiffres, et de caracteres spéciaux</p>
                            <a href="#" class="close-notification"><i class="fa fa-times"></i></a>
                        </div>
                    </div>
                    <div class="widgetSimple2">
                        <label class="control-label">Nouveau mot de passe</label>
                        <div class="input-simple-2 has-icon input-red bottom-15">
                            <i class="fa fa-lock"></i>
                            <input name="password" type="password" class="form-control" id="newspassword1"
                                   placeholder="Nouveau mot de passe" value="">
                            {!! $errors->first('password', '<small class="help-block">:message</small>') !!}
                        </div>
                        <label class="control-label">Confirmation nouveau mot de passe</label>
                        <div class="input-simple-2 has-icon input-red bottom-15">
                            <i class="fa fa-lock"></i>
                            <input name="password_confirmation" type="password" class="form-control" id="newpassword" placeholder="Confirmation nouveau mot de passe" value="{{old('password_confirmation')}}">
                            {!! $errors->first('password', '<small class="help-block">:message</small>') !!}
                        </div>
                        <div class="clear"></div>
                        <div class="one-half ">
                            <button type="submit" class="top-30 default-link button button-s bg-highlight button-rounded uppercase ultrabold shadow-medium button-full">Sauvegarger</button>
                        </div>
                        <div class="one-half last-column">
                            <button type="reset" class="top-30 default-link button button-s bg-red-dark button-rounded uppercase ultrabold shadow-medium button-full">Annuler</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('post_header')
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" 		   content="Yanfoma Soko" />
    <meta property="og:site_name" 	   content="Yanfoma">
    <meta property="fb:app_id" 		   content="400025927061215">
    <meta property="og:title"          content="Yanfoma Soko" />
    <meta property="og:description"    content="Yanfoma Soko" />
    <meta property="og:image"          content="https://res.cloudinary.com/yanfomaweb/image/upload/v1553782826/Yanfoma/metaImage2.png')}}" />
    <meta property="og:type" 	       content="article">
    <meta property="og:url"            content="https://yanfoma.com">
@endsection

@section('scripts')
    <script src="{{asset('js/intlTelInput.js')}}"></script>
    <script>
        var input = document.querySelector("#phone");
        window.intlTelInput(input, {
            onlyCountries: ["bf", "ci", "ml","rw","sn"],
            nationalMode: true,
            hiddenInput: "full_phone",
            utilsScript: "../../js/utils.js" // just for formatting/placeholders etc
        });
        $("dob").flatpickr(optional_config);
    </script>
@endsection