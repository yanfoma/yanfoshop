@extends('layouts.frontEnd.mobile.appHome')

@section('title')
    Mon Panier : YanfomaSoko
@endsection
@section('content')
    <div class="page-content header-clear-large">
        @if($cart->count())
            <div class="content content-boxed content-boxed-padding shadow-small">
                <h3 class="primary-heading-soko top-10 bottom-20 text-center">Dans Votre Panier: {{$cart->count()}} Produit(s)</h3>
                @foreach($cart as $product)
                    <div class="store-cart-2">
                        <img class="preload-image" src="{{asset($product->product->image_url)}}" data-src="{{asset($product->product->image_url)}}" alt="{{$product->product->name}}">
                        <strong>{{$product->product->name}}</strong>
                        <span>Boutique: {{getStoreName($product->store_id)}}</span>
                        <em>{{$product->product_total}}<del>{{$product->product_price}}</del></em>
                        <!-- <a href="{{route('yanfomasoko.cart.delete',['id'=>$product->product_id])}}">Supprimer</a> -->
                        <input class="quantity" type="text" value="{{$product->product_qty}}" name="quantity" readonly>
                    </div>
                @endforeach
                <div class="decoration"></div>
                    <div class="store-cart-total top-20 bottom-30">
                        <strong class="font-16 uppercase ultrabold">Total</strong>
                        <span class="font-16 uppercase ultrabold">{{getTotal()}} CFA</span>
                        <div class="clear"></div>
                    </div>

                    <div class="decoration"></div>

                    <a href="{{route('yanfomasoko.cart.checkout')}}" class="button bg-yellow-dark button-rounded button-full button-sm ultrabold uppercase shadow-small">Passer la commande</a>
            </div>
            @else
            <div class="content top-40 content-boxed content-boxed-padding content-margins content-round shadow-medium">
                <div class="above-overlay">
                    <img class="" src="{{asset('images/empty-cart.png')}}" width="100" style="left: 30%;margin: 20px;">
                    <h1 class="color-primary-soko center-text bolder bottom-30">Votre Panier est Vide !!!</h1>
                    <p class="color-primary-soko opacity-80 center-text bottom-30">
                        Ajouter des produits pour commencer
                    </p>
                    <a href="{{route('mobile.boutiques')}}" class="button button-s button-center bg-yellow-dark button-rounded top-30 bottom-40 uppercase ultrabold ">Nos Boutiques</a>
                </div>
                <div class="overlay bg-white opacity-90"></div>
            </div>
        @endif
    </div>
@endsection
@section('post_header')
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" 		   content="Yanfoma Soko" />
<meta property="og:site_name" 	   content="Yanfoma">
<meta property="fb:app_id" 		   content="400025927061215">
<meta property="og:title"          content="Yanfoma Soko" />
<meta property="og:description"    content="Yanfoma Soko" />
<meta property="og:image"          content="https://res.cloudinary.com/yanfomaweb/image/upload/v1553782826/Yanfoma/metaImage2.png')}}" />
<meta property="og:type" 	       content="article">
<meta property="og:url"            content="https://yanfoma.com">
@endsection