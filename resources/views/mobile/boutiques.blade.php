@extends('layouts.frontEnd.mobile.appHome')

@section('title')
    Nos Boutiques
@endsection
@section('content')
    <div class="page-content header-clear-large light-skin highlight-blue" >
        <div class="">
            @foreach($stores as $store)
                <div class="cover-item">
                    <div class="cover-content cover-content-center">
                        <img class="cover-icon circleWhite" src="{{$store->logo}}">
                        <h2 class="color-white regular center-text bottom-20 ultrabold font-30">{{$store->name}}</h2>
                        @if($store->id == 1)
                            <p class="color-white center-text opacity-80 bottom-30 font-14">
                                La Boutique Electronique Africaine 100% Industrie 4.0.
                            </p>
                        @endif
                        <a href="{{route('shop',$store->slug)}}" class="button button-blue button-s border-white button-rounded button-center uppercase ultrabold">Visiter</a>
                    </div>
                    <div class="cover-overlay overlay bg-highlight"></div>
                </div>
            @endforeach
        </div>
    </div>
    <a href="#" class="back-to-top-badge back-to-top-small bg-highlight"><i class="fa fa-angle-up"></i>Back to Top</a>
@endsection

@section('post_header')
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" 		   content="Yanfoma Soko" />
    <meta property="og:site_name" 	   content="Yanfoma">
    <meta property="fb:app_id" 		   content="400025927061215">
    <meta property="og:title"          content="Yanfoma Soko" />
    <meta property="og:description"    content="Yanfoma Soko" />
    <meta property="og:image"          content="https://res.cloudinary.com/yanfomaweb/image/upload/v1553782826/Yan{{asset('foma/m/frontEnd/mobileetaImage2.png')}}')}}" />
    <meta property="og:type" 	       content="article">
    <meta property="og:url"            content="https://yanfoma.com">
@endsection

