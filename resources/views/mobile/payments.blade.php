@extends('layouts.frontEnd.mobile.app')

@section('title')
    NOS MÉTHODES DE PAIEMENT
@endsection
@section('content')
    <div class="page-content header-clear-large">
        <div class="widget1">
            <div class="content-title bottom-30 top-30">
                <h1 class="bottom-10 primary-heading text-center font-20">BITCOIN</h1>
                <p class="justify-text">
                    Pour payer en Bitcoin, vous devez repondre au mail que vous recevrez apres la commande en precisant que vous voulez utiliser cette methode de paiement. Vous recevrez la somme a envoyer en Bitcoin et le temps limite pour effectuer la transaction. Vous avez une reduction de 1000 Fr CFA lorsque vous faites votre paiement en Bitcoin.
                </p>
            </div>
        </div>
        <div class="widget1">
            <div class="content-title bottom-30 top-30">
                <h1 class="bottom-10 primary-heading text-center font-20">Ecobank Pay</h1>
                <p class="justify-text">
                    Vous pouvez payer via Ecobank Pay à travers le QR Code que suivant:
                    <img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1547479354/Screen_Shot_2019-01-14_at_10.42.42_PM.png" width="200">
                    <br>
                    Vous avez une réduction de 1000 Fr CFA lorsque vous faites votre paiement via Ecobank Pay.
                </p>
            </div>
        </div>
        <div class="widget1">
            <div class="content-title bottom-30 top-30">
                <h1 class="bottom-10 primary-heading text-center font-20">Orange Money</h1>
                <p class="justify-text">
                    Vous devez envoyer la somme equivalente a votre commande au numero suivant: +22667402030. Notez que ce numero est au Burkina Faso. Apres le paiement, vous devez repondre au mail que vous avez recu apres la commande en nous envoyant le numero de la transaction. Vous avez une reduction de 500 Fr CFA lorsque vous faites votre paiement par Orange Money.
            </div>
        </div>
        <div class="widget1 bottom-30">
            <h4 class="bolder bottom-20 primary-heading text-center">Cash Chez Kaanu (CCK)</h4>
            <p class="justify-text">
                Vous pouvez payer au centre Kaanu lorsque votre colis arrivera. Il vous suffit d'emmener la somme equivalente a votre commande lorsque vous aller recuperer votre colis et payer avec l'agent Kaanu.
            </p>
        </div>
        <div class="widget1 bottom-30">
            <h4 class="bolder bottom-20 primary-heading text-center">Dépôt Bancaire</h4>
            <p class="justify-text">
                Vous devez vous rendre à l'agence Ecobank la plus proche et déposer la somme équivalente à votre commande dans notre compte bancaire. Après la transaction, devez répondre au mail que vous avez reçu après la commande avec en fichier joint, une photo du reçu de dépôt à la banque. Voici le numéro du compte bancaire: (le SWIFT est important pour ceux qui sont hors du Burkina Faso)
                SWIFT: ECOCBFBF
                Account No: 170048532001
                Vous avez une réduction de 1000 Fr CFA lorsque vous faites votre paiement par dépôt bancaire Ecobank.
            </p>
        </div>
        <div class="decoration decoration-margins"></div>
    </div>
    <a href="#" class="back-to-top-badge back-to-top-small bg-highlight"><i class="fa fa-angle-up"></i>Back to Top</a>
@endsection

@section('post_header')
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" 		   content="Yanfoma Soko" />
    <meta property="og:site_name" 	   content="Yanfoma">
    <meta property="fb:app_id" 		   content="400025927061215">
    <meta property="og:title"          content="Yanfoma Soko" />
    <meta property="og:description"    content="Yanfoma Soko" />
    <meta property="og:image"          content="https://res.cloudinary.com/yanfomaweb/image/upload/v1553782826/Yanfoma/metaImage2.png')}}" />
    <meta property="og:type" 	       content="article">
    <meta property="og:url"            content="https://yanfoma.com">
@endsection