@extends('layouts.frontEnd.mobile.appHome')

@section('title')
   Aide Yanfoma Soko
@endsection

@section('content')
    <div class="page-content header-clear-large">
        <div id="page-vcard">
            <div class="widgetSimple2">
                <div class="vcard-header">
                    <img data-src="https://res.cloudinary.com/yanfomaweb/image/upload/v1539208782/Yanfoma/avatar-1.png" src="https://res.cloudinary.com/yanfomaweb/image/upload/v1539208782/Yanfoma/avatar-1.png" class="preload-image shadow-medium"  alt="User">
                    <h4 class="">{{Auth::user()->name}}</h4>
                    <em class="small-text color-gray-dark bottom-10">{{Auth::user()->email}}</em>
                    @if($user->verified =="yes")
                        <a href="#" target="_blank" class="default-link button button-xs button-green button-rounded uppercase ultrabold bottom-30 shadow-small"><i class="fa fa-check-circle"></i> Email Verifer</a>
                    @endif
                </div>
            </div>
            <div class="decoration opacity-90 bottom-0"></div>

            <div class="bottom-50">
                <div class="widgetSimple2">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 margin-top">
                            <div class="card text-white bg-primary">
                                <div class="card-header">Facebook</div>
                                <div class="card-body content-center">
                                    <p class="card-text text-white font-16">Rejoignez notre groupe Facebook yanfomashop!!!</p><br>
                                    <a class="default-link button button-large button-full button-blue button-rounded  ultrabold shadow-small" href="https://www.facebook.com/groups/2171625413121414/">Rejoindre</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 margin-top">
                            <div class="card text-white bg-primary">
                                <div class="card-header">Messanger</div>
                                <div class="card-body content-center ">
                                    <p class="card-text text-white font-16">Contactez nous sur messenger pour toutes vos questions. </p><br>
                                    <a class="default-link button button-large button-full button-blue button-rounded ultrabold shadow-small" href="https://www.messenger.com/t/yanfoma">Envoyer</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clear-fix"></div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 margin-top">
                            <div class="card text-white bg-primary">
                                <div class="card-header">Whatsapp</div>
                                <div class="card-body content-center ">
                                    <p class="card-text text-white font-16">Contacter nous sur whatsapp </p><br>
                                    <a class="default-link button button-large button-full button-blue button-rounded ultrabold shadow-small" href="https://chat.whatsapp.com/EsCjgPWHuPf39ujQKANX5C">Yanfoma Soko</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 margin-top">
                            <div class="card text-white bg-primary">
                                <div class="card-header">Email</div>
                                <div class="card-body content-center ">
                                    <p class="card-text text-white font-16">info@yanfoma.tech</p><br>
                                    <a class="default-link button button-large button-full button-blue button-rounded ultrabold shadow-small" href="mailto:info@yanfoma.tech">Contacter Nous</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('post_header')
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" 		   content="Yanfoma Soko" />
    <meta property="og:site_name" 	   content="Yanfoma">
    <meta property="fb:app_id" 		   content="400025927061215">
    <meta property="og:title"          content="Yanfoma Soko" />
    <meta property="og:description"    content="Yanfoma Soko" />
    <meta property="og:image"          content="https://res.cloudinary.com/yanfomaweb/image/upload/v1553782826/Yanfoma/metaImage2.png')}}" />
    <meta property="og:type" 	       content="article">
    <meta property="og:url"            content="https://yanfoma.com">
@endsection

@section('scripts')
    <script src="{{asset('js/intlTelInput.js')}}"></script>
    <script>
        var input = document.querySelector("#phone");
        window.intlTelInput(input, {
            onlyCountries: ["bf", "ci", "ml","rw","sn"],
            nationalMode: true,
            hiddenInput: "full_phone",
            utilsScript: "../../js/utils.js" // just for formatting/placeholders etc
        });
        $("dob").flatpickr(optional_config);
    </script>
@endsection