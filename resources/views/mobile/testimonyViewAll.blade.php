@extends('layouts.frontEnd.mobile.app')

@section('title')
    Yanfoma Soko
@endsection

@section('content')
    <div class="page-content header-clear-large">
        <div class="widgetSimple2 top-20">
            <div data-height="130" class="caption caption-margins round-medium shadow-huge" style="height: 130px;">
                <div class="caption-center">
                    <h1 class="primary-heading bolder text-center">Temoignages</h1>
                    <p class="under-heading color-white opacity-90 bottom-0 text-center">
                        Ils Parlent de Nous
                    </p>
                </div>
                <div class="caption-overlay bg-black opacity-70"></div>
                <div class="caption-bg bg-18"></div>
            </div>
        </div>
        @if($testimonies->count())
            @foreach($testimonies as $testimony)
                <div class="widgetSimple2">
                    <div class="content bottom-0">
                        <div class="review-2 container">
                            <img src="{{asset('https://res.cloudinary.com/yanfomaweb/image/upload/v1548546096/Yanfoma/yanfoShopTestimonyUser.png')}}">
                            <h1>{{$testimony->name}}</h1>
                            <em></em>
                            <p>
                                {{$testimony->message}}
                            </p>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
    </div>
@endsection

@section('post_header')
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" 		   content="Yanfoma Soko" />
    <meta property="og:site_name" 	   content="Yanfoma">
    <meta property="fb:app_id" 		   content="400025927061215">
    <meta property="og:title"          content="Yanfoma Soko" />
    <meta property="og:description"    content="Yanfoma Soko" />
    <meta property="og:image"          content="https://res.cloudinary.com/yanfomaweb/image/upload/v1553782826/Yanfoma/metaImage2.png')}}" />
    <meta property="og:type" 	       content="article">
    <meta property="og:url"            content="https://yanfoma.com">
@endsection