@extends('layouts.frontEnd.mobile.app')

@section('title')
    VOTRE AVIS NOUS INTERRESSE
@endsection
@section('content')
    <div class="page-content header-clear-large">
        <div class="page-login page-login-full">
            <div class="#">
                <h4 class="ultrabold top-30 bottom-0 center-text primary-heading">VOTRE AVIS NOUS INTERRESSE</h4>
                <form class="widget5" method="POST" action="#">
                    {{ csrf_field() }}
                    <div class="page-login-field top-30">
                        @if ($errors->has('name'))
                            <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
                        @endif
                        <i class="fa fa-user color-primary"></i>
                        <input id="name" type="text" name="name" value="{{ old('name') }}" placeholder="Votre Nom" required>
                        <em>(requis)</em>
                    </div>
                    <div id="portable" class="page-login-field top-30" >
                        @if ($errors->has('phone'))
                            <span class="help-block"><strong>{{ $errors->first('phone') }}</strong></span>
                        @endif
                        <i class="fa fa-phone color-primary"></i>
                        <input id="phone" type="text" name="phone" value="{{ old('phone') }}" placeholder="Votre numéro portable veillez l'indicatif">
                        <em>(requis)</em>
                    </div>
                    <div id="mail">
                        <div class="page-login-field top-30">
                            @if ($errors->has('email'))
                                <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                            @endif
                            <i class="fa fa-user color-primary"></i>
                            <input id="email" type="email" name="email" value="{{ old('email') }}" placeholder="Votre Email" required>
                            <em>(requis)</em>
                        </div>
                    </div>
                    <div class="page-login-field bottom-30">
                        @if ($errors->has('country'))
                            <span class="help-block"><strong>{{ $errors->first('country') }}</strong></span>
                        @endif
                        <i class="fa fa-lock color-primary"></i>
                        <input id="country" type="text" class="form-control" name="country" placeholder="Votre Pays*" required>
                        <em>(requis)</em>
                    </div>
                    <div class="page-login-field bottom-30">
                        @if ($errors->has('message'))
                            <span class="help-block"><strong>{{ $errors->first('message') }}</strong></span>
                        @endif
                        <div class="input-simple-2 textarea has-icon">
                            <i class="fa fa-edit color-primary"></i>
                            <textarea name="message" class="textarea-simple-2" placeholder="Votre Message (150 characters max)...." rows="3" maxlength="150" required>{{ old('message') }}</textarea>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <button style="margin-top: 100px!important;" class="button bg-primary button-full button-rounded button-sm uppercase ultrabold shadow-small">Envoyer</button>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('post_header')
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" 		   content="Yanfoma Soko" />
    <meta property="og:site_name" 	   content="Yanfoma">
    <meta property="fb:app_id" 		   content="400025927061215">
    <meta property="og:title"          content="Yanfoma Soko" />
    <meta property="og:description"    content="Yanfoma Soko" />
    <meta property="og:image"          content="https://res.cloudinary.com/yanfomaweb/image/upload/v1553782826/Yanfoma/metaImage2.png')}}" />
    <meta property="og:type" 	       content="article">
    <meta property="og:url"            content="https://yanfoma.com">
@endsection