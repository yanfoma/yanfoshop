@extends('layouts.frontEnd.mobile.app')

@section('title')
    Demander un Devis
@endsection
@section('content')
    <div class="page-content header-clear-large">
        <div class="widget1">
            <div class="content-title bottom-30 top-30">
                <h1 class="bottom-10 primary-heading text-center font-20">Comment demander un devis?</h1>
                <p class="justify-text">
                    C'est très simple. Envoyez nous un email à l'adresse suivante: info@yanfoma.tech avec la liste complete de vos produits et leur spécifications. Votre requête sera traitée en 24h Chrono.
            </div>
        </div>
    <a href="#" class="back-to-top-badge back-to-top-small bg-highlight"><i class="fa fa-angle-up"></i>Back to Top</a>
@endsection

@section('post_header')
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" 		   content="Yanfoma Soko" />
    <meta property="og:site_name" 	   content="Yanfoma">
    <meta property="fb:app_id" 		   content="400025927061215">
    <meta property="og:title"          content="Yanfoma Soko" />
    <meta property="og:description"    content="Yanfoma Soko" />
    <meta property="og:image"          content="https://res.cloudinary.com/yanfomaweb/image/upload/v1553782826/Yanfoma/metaImage2.png')}}" />
    <meta property="og:type" 	       content="article">
    <meta property="og:url"            content="https://yanfoma.com">
@endsection