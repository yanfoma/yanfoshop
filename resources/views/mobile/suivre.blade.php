@extends('layouts.frontEnd.mobile.app')

@section('title')
    Tout Savoir Sur Ma Commande
@endsection
@section('content')
    <div class="page-content header-clear-large">
        <div class="widget1">
            <div class="content-title bottom-30 top-30">
                <h1 class="bottom-10 primary-heading text-center font-20">Modes d'Expédition et Délais de Livraison</h1>
                <p class="justify-text">
                    Vous recevrez un e-mail de confirmation après avoir passé une commande et un autre e-mail après l'expédition de votre commande. Le second contient les informations de suivi et des instructions sur la manière de suivre votre colis. Le temps total nécessaire pour recevoir votre commande se décrit comme suit:
                    Temps de traitement: est la somme du Temps De Traitement et du Temps d'Expédition.
                    Délai de traitement = Le temps nécessaire pour préparer vos articles à expédier. Cela comprend la préparation de vos articles, l'exécution de contrôles de qualité et l'emballage pour l'expédition.
                    Délai d'expédition = Le temps nécessaire pour que vos articles arrivent à votre destination.
                </p>
                <h5 class="primary-heading">Le délai de livraison total est calculé à partir du moment où votre commande est passée jusqu'au moment où elle vous est livrée.</h5>
            </div>
        </div>
        <div class="widget1">
            <div class="content-title bottom-30 top-30">
                <h1 class="bottom-10 primary-heading text-center font-20">Comment Suivre Ma Commande?</h1>
                <p class="justify-text">
                    Une fois votre commande expédiée, Vous pouvez tracker votre commande à travers votre espace personnel.
                    Pour cela connectez-vous puis clicquez sur:
                    1- "Mon Profile"
                    2- Sur le menu latéral clicquez sur "Mes Commandes". Vous verrez la liste de vos commandes et celles qui ont été expédiées.
                    3- Sur chacune de ces commandes cliquez sur le boutton "Suivre" et vous verrez où se trouve votre commande.
                </p>
            </div>
        </div>
        <div class="widget1">
            <div class="content-title bottom-30 top-30">
                <h1 class="bottom-10 primary-heading text-center font-20">Où et Comment Récupérer Ma Commande ?</h1>
                <p class="justify-text">
                    Dans les pays où Yanfoma est représenté, notre équipe sur place se chargera de collecter le colis et vous contactera pour que vous définissiez le meilleur moyen pour vous d’entrer en possession de votre bien.
            </div>
        </div>
        <div class="widget1">
            <h1 class="bottom-10 primary-heading text-center font-20">Zones de Couverture de YanfoShop</h1>
            <img class="preload-image horizontal-center scale-box2" width="520" src="https://res.cloudinary.com/yanfomaweb/image/upload/v1553174144/Yanfoma/mapYanfoma.png" data-src="https://res.cloudinary.com/yanfomaweb/image/upload/v1553174144/Yanfoma/mapYanfoma.png" alt="img">
            <h1 class="bottom-10 primary-heading text-center font-20">Yanfoma est présent dans trois (03) pays pour l'instant</h1>
        </div>
        <div class="widget1 bottom-30">
            <h4 class="bolder bottom-20 primary-heading text-center">Je ne trouve pas mon produit. Comment Faire?</h4>
            <p class="justify-text">
                Cher Client, pas de panique,ne vous inquiétez pas nous avons une bonne nouvelle. Veuillez juste nous demander un devis en envoyant un e-mail à info@yanfoma.tech avec votre liste de produits recherchés. Nous traiterons votre requête en 24h.
            </p>
        </div>

        <div class="decoration decoration-margins"></div>
    </div>
    <a href="#" class="back-to-top-badge back-to-top-small bg-highlight"><i class="fa fa-angle-up"></i>Back to Top</a>
@endsection

@section('post_header')
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" 		   content="Yanfoma Soko" />
    <meta property="og:site_name" 	   content="Yanfoma">
    <meta property="fb:app_id" 		   content="400025927061215">
    <meta property="og:title"          content="Yanfoma Soko" />
    <meta property="og:description"    content="Yanfoma Soko" />
    <meta property="og:image"          content="https://res.cloudinary.com/yanfomaweb/image/upload/v1553782826/Yanfoma/metaImage2.png')}}" />
    <meta property="og:type" 	       content="article">
    <meta property="og:url"            content="https://yanfoma.com">
@endsection