@extends('layouts.frontEnd.mobile.app')

@section('title')
	Toutes Nos Categories
@endsection

@section('content')
	<div class="page-content header-clear-large">
		<div class="single-slider owl-carousel owl-has-dots gallery bottom-30">
			<a class="show-gallery" href="{{$product->image_url}}"><img src="{{$product->image_url}}" alt="{{$product->name}}"></a>
		</div>
		<div class="store-product content">
			<h2 class="store-product-title">{{$product->name}}</h2>
			<span class="store-product-price">
				@if($product->onSale)
					<em><del>{{number_format($product->price, 0 , ',' , ' ')}} CFA</del></em>
					<strong class="primary-heading">{{number_format($product->sale_amount, 0 , ',' , ' ')}}</strong>
				@else
					<strong class="primary-heading">{{number_format($product->price, 0 , ',' , ' ')}} <span style="font-size: 12px;">CFA</span> </strong>
				@endif
			</span>
			@if($product->onSale && $product->saleType == 2)
				<span class="store-product-discount bg-green-dark top-20 bottom-10">Promo {{ $product->salePercentage }} %</span>
				<br>
			@endif
		</div>
		<div class="widgetSimple2">
			<ul class="font-icon-list">
				<li class="color-red-dark"><i class="fa fa-exclamation-triangle color-red-dark"></i>Quantité Minimale: {{$product->minQty}}</li>
				@if($product->stock_location)
					<li><i class="fa fa-clock-o color-blue-dark"></i> Disponible dans la semaine</li>
				@else
					<li><i class="fa fa-clock-o color-blue-dark"></i>Disponible dans 20~30 jours</li>
				@endif
				<li><i class="fa fa-truck color-blue-dark"></i>Livraison Gratuite</li>
				<li><i class="fa fa-list-alt color-blue-dark"></i>Categorie: {{$product->category->name}}</li>
			</ul>
		</div>
		<div class="content">
			<form action="{{route('store.cart.addToCart',['store_slug' => $store->slug])}}" method="post">
				{{csrf_field()}}
				<div class="quantity-select bottom-30">
					<input type="hidden" name="product_id" value="{{$product->id}}">
					<input type="text"   name="quantity"  value="{{$product->minQty}}">
					<div class="inc qtybutton">+<i class="fa fa-arrow-up"></i></div>
					<div class="dec qtybutton">-<i class="fa fa-arrow-down"></i></div>
				</div>
				<br>
				<button type="submit" class="button button-blue button-icon button-full button-sm shadow-small top-15 button-rounded uppercase ultrabold"> <i class="fas fa-shopping-bag"></i> Ajouter au panier</button>
			</form>
			<div class="one-half">
				@if($product->is_wished_by_auth_user())
					<a class="button button-red button-icon button-full button-sm shadow-small top-15 bottom-30 button-rounded uppercase ultrabold" href="{{route('product.unwishlist', [ 'store_slug' => $store->slug,'id' => $product->id ])}}"><i class="fa fa-heart"></i> Favoris</a>
				@else
					<a class="button bg-gray-light button-icon button-full button-sm shadow-small top-15 bottom-30 button-rounded uppercase ultrabold" href="{{route('product.wishlist', [ 'store_slug' => $store->slug,'id' => $product->id ])}}"><i class="fa fa-heart-o"></i> Favoris</a>
				@endif
			</div>
			<div class="one-half last-column">
				@if($product->is_liked_by_auth_user())
					<a class="button bg-facebook button-icon button-full button-sm shadow-small top-15 bottom-30 button-rounded uppercase ultrabold" href="{{route('product.unlike', [ 'store_slug' => $store->slug,'id' => $product->id ])}}"><i class="fa fa-thumbs-up"></i>Vous Aimez</a>
				@else
					<a class="button bg-gray-light button-icon button-full button-sm shadow-small top-15 bottom-30 button-rounded uppercase ultrabold" href="{{route('product.like', ['store_slug' => $store->slug, 'id' => $product->id ])}}"><i class="fa fa-thumbs-o-up"></i>Aimer</a>
				@endif
			</div>
		</div>

		<div class="content widgetSimple shadow-small">
			<div class="content-title bottom-20">
				<h1 class="primary-heading center-text top-5">Description</h1>
			</div>
			<div class="container ">
				<p class="bottom-0">
					{!!html_entity_decode(strip_tags($product->description))!!}
				</p>
			</div>
			<div class="social-icons bottom-10 content-center">
				<h5 class="text-center"><i class="fa fa-share-alt color-blue-dark bottom-5"></i> Partager</h5>
				<a href="https://www.facebook.com/sharer/sharer.php?u=&t=" title="Yanfoshop" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(document.URL) + '&t=' + encodeURIComponent(document.URL)); return false;"  class="icon icon-xs icon-round shadow-small bg-facebook"><i class="fab fa-facebook-f"></i></a>
				<a href="https://www.facebook.com/sharer/sharer.php?u=&t=" title="Yanfoshop" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(document.URL) + '&t=' + encodeURIComponent(document.URL)); return false;"  class="icon icon-xs icon-round shadow-small bg-twitter"><i class="fa fa-commenting"></i></a>
				<a href="https://www.linkedin.com/shareArticle?mini=true&url={{url()->current()}}&title={{$product->title}}&summary={!! str_limit(strip_tags($product->content), $limiit=65,$end='...')  !!}&source=yanfoma"   title="Yanfoshop" target="_blank"  class="icon icon-xs icon-round shadow-small bg-linkedin"><i class="fab fa-linkedin"></i></a>
				<a href="whatsapp://send?text={{ urlencode(Request::fullUrl()) }}"                 						               	    															     				   title="Yanfoshop" target="_blank"  class="icon icon-xs icon-round shadow-small bg-whatsapp"><i class="fab fa-whatsapp"></i></a>
			</div>
		</div>
		@if($product->more_info)
			<div class="content widgetSimple">
				<div class="content-title bottom-20">
					<h1 class="primary-heading center-text top-5">Plus d'Informations</h1>
				</div>
				<div class="container ">
					<p class="bottom-0">
						{!! $product->more_info !!}
					</p>
				</div>
				<div class="decoration"></div>
			</div>
		@endif
		<div class=" widgetSimple2 bottom-40">
			<div class="content-title bottom-20">
				<h3 class="primary-heading center-text top-5">Récommendations</h3>
			</div>
			<div class="single-slider owl-carousel owl-no-dots bottom-15">
				@foreach($latests as $product)
					<div class="store-slide-2">
						<a href="{{route('shop.single',['store_slug' => $store->slug, 'slug' => $product->slug])}}" class="store-slide-image">
							<img src="{{$product->image_url}}" alt="{{$product->name}}">
						</a>
						<div class="store-slide-title">
							<a href="{{route('shop.single',['store_slug' => $store->slug, 'slug' => $product->slug])}}"><em class="color-primary primary-heading">{{$product->name}}</em></a>
						</div>
						<div class="store-slide-button">
							@if($product->onSale)
								<del class="oldprice">{{number_format($product->price, 0 , ',' , ' ')}} </del>
								<strong>{{number_format($product->sale_amount, 0 , ',' , ' ')}} CFA </strong>
							@else
								<strong>{{number_format($product->price, 0 , ',' , ' ')}}   </strong>
							@endif
							<a href="{{route('store.cart.addDirect',['store_slug' => $store->slug, 'id' => $product->id])}}"><i class="fa fa-shopping-cart color-blue-dark"></i>Ajouter au panier</a>
						</div>
					</div>
				@endforeach
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script type="text/javascript">
		/* Product Quantity */
        function productQuantity() {
            $('.quantity-select').append('<div class="dec qtybutton">-</i></div><div class="inc qtybutton">+</div>');
            $('.qtybutton').on('click', function () {
                var $button = $(this);
                var oldValue = $button.parent().find('input[name=quantity]').val();
                var newVal;
                if ($button.text() == "+") {
                    newVal = parseFloat(oldValue) + 1;
                } else {
                    if (oldValue > 1) {
                        newVal = parseFloat(oldValue) - 1;
                    } else {
                        newVal = 1;
                    }
                }
                $button.parent().find('input[name=quantity]').val(newVal);
            });
        }
        productQuantity();

        //Preload Image
        $(function() {$(".preload-search-image").lazyload({threshold : 0});});

        //Search Menu Functions
        function search_menu(){
            $('[data-search]').on('keyup', function() {
                var searchVal = $(this).val();
                if (searchVal != '') {
                    $('.menu-search-trending').addClass('disabled-search-item');
                    $('#menu-search-list .search-results').removeClass('disabled-search-list');
                    $('#menu-search-list [data-filter-item]').addClass('disabled-search-item');
                    $('#menu-search-list [data-filter-item][data-filter-name*="' + searchVal.toLowerCase() + '"]').removeClass('disabled-search-item');
                } else {
                    $('.menu-search-trending').removeClass('disabled-search-item');
                    $('#menu-search-list .search-results').addClass('disabled-search-list');
                    $('#menu-search-list [data-filter-item]').removeClass('disabled-search-item');
                }
            });
            return false;
        }
        search_menu();

        //Menu Search Values//
        $('.menu-search-trending a').on('click',function(){
            var e = jQuery.Event("keydown");
            e.which = 32;
            var search_value = $(this).text();
            $('.search-header input').val(search_value);
            $('.search-results').removeClass('disabled-search-list');
            $('[data-filter-item]').addClass('disabled-search-item');
            $('[data-filter-item][data-filter-name*="' + search_value.toLowerCase() + '"]').removeClass('disabled-search-item');
            $('#search-page').addClass('move-search-list-up');
            $('.search-header a').addClass('search-close-active');
            $('.menu-search-trending').addClass('disabled-search-item');
            return false;
        });

        $('#menu-hider, .close-menu, .menu-hide').on('click',function(){
            $('.menu-box').removeClass('menu-box-active');
            $('#menu-hider').removeClass('menu-hider-active');
            setTimeout(function(){$('#search-page').removeClass('move-search-list-up');},100);
            $('[data-filter-item]').addClass('disabled-search-item');
            $('.search-header input').val('');
            $('.menu-search-trending').removeClass('disabled-search-item');
            $('.search-header a').removeClass('search-close-active');
            $('#search-page').removeClass('move-search-list-up');
            return false;
        });
        $('#menu-search-list input').on('focus',function(){
            $('#search-page').addClass('move-search-list-up');
            $('.search-header a').addClass('search-close-active');
            return false;
        });
        $('.search-header a').on('click',function(){
            $('.menu-search-trending').removeClass('disabled-search-item');
            $('#menu-search-list .search-results').addClass('disabled-search-list');
            $('.search-header input').val('');
            $('#search-page').removeClass('move-search-list-up');
            $('.search-header a').removeClass('search-close-active');
            return false;
        });
	</script>
@endsection

@section('post_header')
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" 		   content="Yanfoma Soko" />
	<meta property="og:site_name" 	   content="Yanfoma">
	<meta property="fb:app_id" 		   content="400025927061215">
	<meta property="og:title"          content="Yanfoma Soko" />
	<meta property="og:description"    content="Yanfoma Soko" />
	<meta property="og:image"          content="https://res.cloudinary.com/yanfomaweb/image/upload/v1553782826/Yanfoma/metaImage2.png')}}" />
	<meta property="og:type" 	       content="article">
	<meta property="og:url"            content="https://yanfoma.com">
@endsection