@extends('layouts.frontEnd.mobile.appLogin')

@section('title')
    Yanfoma Soko Contact
@endsection
@section('content')
    <div class="page-content header-clear-large">
        <div class="page-login page-login-full">
            <div class="widget">
                <h3 class="ultrabold top-30 bottom-0 center-text primary-heading-soko">Se Connecter</h3>
                <form class="widget5" method="POST" action="{{route('login')}}">
                    {{ csrf_field() }}
                    <div>
                        <div class="form-group {{ $errors->has('wa_confirmed') ? ' has-error' : '' }}">
                            <input id="wa_confirmed" type="checkbox" name="wa_confirmed" class="form-control" value="{{ old('wa_confirmed') }}">
                            @if ($errors->has('wa_confirmed'))
                                <span class="help-block"><strong>{{ $errors->first('wa_confirmed') }}</strong></span>
                            @endif
                            <label for="wa_confirmed" class="center-align">Utiliser votre numéro Whatsapp?</label>
                        </div>
                    </div>
                    <div id="whatsapp" class="page-login-field top-30" >
                        @if ($errors->has('wa_number'))
                            <span class="help-block"><strong>{{ $errors->first('wa_number') }}</strong></span>
                        @endif
                        <i class="fa fa-phone color-yellow-dark"></i>
                        <input id="wa_number" type="text" name="wa_number" value="{{ old('wa_number') }}" placeholder="Votre numéro Whatsapp">
                        <em>(requis)</em>
                    </div>
                    <div id="mail">
                        <div class="page-login-field top-30">
                            @if ($errors->has('email'))
                                <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                            @endif
                            <i class="fa fa-user color-yellow-dark"></i>
                            <input id="email" type="email" name="email" value="{{ old('email') }}" placeholder="Votre Email" required>
                            <em>(requis)</em>
                        </div>
                    </div>
                    <div class="page-login-field bottom-30">
                        @if ($errors->has('password'))
                            <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
                        @endif
                        <i class="fa fa-lock color-yellow-dark"></i>
                        <input id="password" type="password" class="form-control" name="password" required placeholder="Votre Mot de Passe">
                        <em>(requis)</em>
                    </div>
                    <div class="switch-box bottom-30">
                        <h5>Se Souvenir de moi</h5>
                        <div class="ios-switch">
                            <input type="checkbox" name="remember" class="ios-switch-checkbox" id="myonoffswitch" {{ old('remember') ? 'checked' : '' }}>
                            <label class="ios-switch-label" for="myonoffswitch"></label>
                        </div>
                    </div>
                    <div class="page-login-links bottom-10">
                        <a class="forgot float-right primary-heading-soko" href="{{route('mobile.register')}}"><i class="fa fa-user float-right"></i>Créer un Compte</a>
                        <a class="create float-left primary-heading-soko" href="#"><i class="fa fa-eye"></i>Mot de passe oublié</a>
                        <div class="clear"></div>
                    </div>
                    <button class="button bg-yellow-dark button-full button-rounded button-sm uppercase ultrabold shadow-small">Se Connecter</button>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('post_header')
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" 		   content="Yanfoma Soko" />
    <meta property="og:site_name" 	   content="Yanfoma">
    <meta property="fb:app_id" 		   content="400025927061215">
    <meta property="og:title"          content="Yanfoma Soko" />
    <meta property="og:description"    content="Yanfoma Soko" />
    <meta property="og:image"          content="https://res.cloudinary.com/yanfomaweb/image/upload/v1553782826/Yanfoma/metaImage2.png')}}" />
    <meta property="og:type" 	       content="article">
    <meta property="og:url"            content="https://yanfoma.com">
@endsection
@section('scripts')
    <script  type="text/javascript">
        $('#whatsapp').hide();
        $('#wa_confirmed').on('click' , function (event) {
            if ($(this).prop('checked')) {
                $('#mail').hide();
                $('#whatsapp').show();
            } else {
                $('#mail').show();
                $('#whatsapp').hide();
            }
        });
        $('.mailSelect').selectpicker();
    </script>
@endsection