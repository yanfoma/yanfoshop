@extends('layouts.frontEnd.mobile.app')

@section('title')
    Centre D'aide
@endsection
@section('content')
    <div class="page-content header-clear-large">
        <div class="content content-boxed content-boxed-full" style="box-shadow: 0 4px 24px 0 rgba(0,0,0,.2);border-radius: 13px!important;">
            <ul class="link-list bottom-0">
                <li><a href="#" data-menu="sheet-devis"><i class="fa fa-angle-right"></i><i class="font-16 fa fa-server color-blue-dark"></i><span>Demander Un Devis</span></a></li>
                <li><a href="#" data-menu="sheet-video"><i class="fa fa-angle-right"></i><i class="font-14 fab fa-youtube color-red-dark"></i><span>YanfoShop En Video</span></a></li>
                <em class="menu-divider primary-heading center-text">METHODES DE PAIEMENT<i class="font-14 fa fa-dollar-sign color-blue-dark"></i></em>
                <li><a href="#" data-menu="sheet-bitcoin"><i class="fa fa-angle-right"></i><i class="font-14 fa fa-check-square color-blue-dark"></i><span>Bitcoin</span></a></li>
                <li><a href="#" data-menu="sheet-Ecobank-Pay"><i class="fa fa-angle-right"></i><i class="font-14 fa fa-check-circle color-blue-dark"></i><span>Ecobank Pay</span></a></li>
                <li><a href="#" data-menu="sheet-Orange-Money"><i class="fa fa-angle-right"></i><i class="font-14 fa fa-thumbs-up color-blue-dark"></i><span>Orange Money</span></a></li>
                <li><a href="#" data-menu="sheet-Cash-Chez-Kaanu"><i class="fa fa-angle-right"></i><i class="font-14 fa fa-phone color-blue-dark"></i><span>Cash Chez Kaanu (CCK)</span></a></li>
                <li><a href="#" data-menu="sheet-Depot-Bancaire"><i class="fa fa-angle-right"></i><i class="font-14 fa fa-flag color-blue-dark"></i><span>Dépôt Bancaire</span></a></li>
                <em class="menu-divider primary-heading center-text">Tout Savoir Sur Ma Commande<i class="font-14 fa fa-dollar-sign color-blue-dark"></i></em>
                <li><a href="#" data-menu="sheet-expedition"><i class="fa fa-angle-right"></i><i class="font-14 fa fa-list color-blue-dark"></i><span>Modes d'Expédition et Délais de Livraison</span></a></li>
                <li><a href="#" data-menu="sheet-suivre"><i class="fa fa-angle-right"></i><i class="font-14 fa fa-list color-blue-dark"></i><span>Comment Suivre Ma Commande?</span></a></li>
                <li><a href="#" data-menu="sheet-recuperer"><i class="fa fa-angle-right"></i><i class="font-14 fa fa-list color-blue-dark"></i><span>Où et Comment Récupérer Ma Commande ?</span></a></li>
                <li><a href="#" data-menu="sheet-couverture"><i class="fa fa-angle-right"></i><i class="font-14 fa fa-list color-blue-dark"></i><span>Zones de Couverture de YanfoShop</span></a></li>
                <li><a href="#" data-menu="sheet-produit-introuve"><i class="fa fa-angle-right"></i><i class="font-14 fa fa-list color-blue-dark"></i><span>Je ne trouve pas mon produit. Comment Faire?</span></a></li>
                <li><a href="#" data-menu="sheet-resaux-sociaux"><i class="font-14 fa fa-share-alt color-blue-dark"></i><span>NOS RESEAUX SOCIAUX</span></a></li>
            </ul>
            <div id="sheet-resaux-sociaux" class="menu-box menu-bottom">
                <div class="menu-title">
                    <h4 class="primary-heading center-text">Suivez-nous</h4>
                    <a href="#" class="menu-hide"><i class="fa fa-times"></i></a>
                </div>
                <div class="sheet-share-thumbnails">
                    <a href="https://www.facebook.com/yanfoma/"><i class="fab fa-facebook-f bg-facebook"></i><span>Facebook</span></a>
                    <a href="https://m.me/yanfoma"><i class="fa fa-comments-o bg-twitter"></i><span>Messenger</span></a>
                    <a href="https://chat.whatsapp.com/invite/EsCjgPWHuPf39ujQKANX5C"><i class="fab fa-whatsapp bg-whatsapp"></i><span>WhatsApp</span></a>
                    <a href="https://www.linkedin.com/company-beta/22333242"><i class="fab fa-linkedin bg-linkedin"></i><span>LinkedIn</span></a>
                    <a href="mailto:info@yanfoma.com"><i class="fas fa-envelope bg-mail"></i><span>Email</span></a>
                    <a href="tel:22671331523"><i class="fa fa-phone-square bg-phone"></i><span>Portable</span></a>
                    <div class="clear"></div>
                </div>
            </div>
            <div id="sheet-devis" class="menu-box menu-bottom bg-transparent">
                <div class="menu-wrapper">
                    <div class="menu-title">
                        <h4 class="primary-heading center-text">Comment demander un devis?</h4>
                        <a href="#" class="menu-hide"><i class="fa fa-times"></i></a>
                    </div>
                    <div class="content">
                        <p class="content-justify">
                            C'est très simple. Envoyez nous un email à l'adresse suivante: info@yanfoma.tech avec la liste complete de vos produits et leur spécifications.
                            Votre requête sera traitée en 24h Chrono.
                        </p>
                    </div>
                </div>
            </div>
            <div id="sheet-devis" class="menu-box menu-bottom bg-transparent">
                <div class="menu-wrapper">
                    <div class="menu-title">
                        <h4 class="primary-heading center-text">Comment demander un devis?</h4>
                        <a href="#" class="menu-hide"><i class="fa fa-times"></i></a>
                    </div>
                    <div class="content">
                        <p class="content-justify">
                            C'est très simple. Envoyez nous un email à l'adresse suivante: info@yanfoma.tech avec la liste complete de vos produits et leur spécifications.
                            Votre requête sera traitée en 24h Chrono.
                        </p>
                    </div>
                </div>
            </div>
            {{--<div id="sheet-faq" class="menu-box menu-bottom bg-transparent">--}}
                {{--<div class="menu-wrapper">--}}
                    {{--<div class="menu-title">--}}
                        {{--<h1 class="primary-heading center-text">FOIRE AUX QUESTIONS</h1>--}}
                        {{--<a href="#" class="menu-hide"><i class="fa fa-times"></i></a>--}}
                    {{--</div>--}}
                    {{--<div class="content content-boxed">--}}
                        {{--<ul class="nested-link-list link-list link-list-small bottom-0">--}}
                            {{--@if($faqs->count())--}}
                                {{--@foreach($faqs as $faq)--}}
                                    {{--<li>--}}
                                        {{--<a class="inner-link-list" href="#"><i class="font-13 fa fa-question-circle color-primary"></i><span>{{$faq->question}}</span><i class="fa fa-angle-down"></i></a>--}}
                                        {{--<ul class="link-list">--}}
                                            {{--<li><p class="justify-text">{!! $faq->response !!}</p></li>--}}
                                        {{--</ul>--}}
                                    {{--</li>--}}
                                {{--@endforeach--}}
                            {{--@endif--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            <div id="sheet-video" class="menu-box menu-bottom">
                <div class="menu-title">
                    <h4 class="primary-heading center-text">YanfoShop en Vidéo</h4>
                    <a href="#" class="menu-hide"><i class="fa fa-times"></i></a>
                </div>
                <iframe class="responsive-image" src="https://www.youtube.com/embed/7e90gBu4pas" frameborder="0" allowfullscreen=""></iframe>
                <div class="content bottom-0">
                    <h4 class="center-text top-30 bottom-30">Decouvrez en videos qui nous sommes et nos services que nous proposons.</h4>
                </div>
            </div>
            <div id="sheet-expedition" class="menu-box menu-bottom bg-transparent">
                <div class="menu-wrapper">
                    <div class="menu-title">
                        <h4 class="primary-heading center-text">Modes d'Expédition et Délais de Livraison</h4>
                        <a href="#" class="menu-hide"><i class="fa fa-times"></i></a>
                    </div>
                    <div class="content">
                        <p class="content-justify">
                            Vous recevrez un e-mail de confirmation après avoir passé une commande et un autre e-mail après l'expédition de votre commande.
                            Le second contient les informations de suivi.<br>
                            <span style="font-style: italic; color: #25b6c4; font-weight: bold;">Le délai de livraison total est calculé à partir du moment où votre commande est passée jusqu'au moment où elle vous est livrée.</span>
                        </p>
                    </div>
                </div>
            </div>
            <div id="sheet-suivre" class="menu-box menu-bottom bg-transparent">
                <div class="menu-wrapper">
                    <div class="menu-title">
                        <h4 class="primary-heading center-text">Comment Suivre Ma Commande?</h4>
                        <a href="#" class="menu-hide"><i class="fa fa-times"></i></a>
                    </div>
                    <div class="content">
                        <p class="content-justify">
                            Une fois votre commande expédiée, Vous pouvez tracker votre commande à travers votre espace personnel.<br>
                            Pour cela connectez-vous puis clicquez sur: <br>
                            1- <strong>"Mon Profile"</strong> <br>
                            2- Sur le menu latéral clicquez sur <strong>"Mes Commandes"</strong>.
                            Vous verrez la liste de vos commandes et celles qui ont été expédiées.<br>
                            3- Sur chacune de ces commandes cliquez sur le boutton <strong>"Suivre"</strong> et vous verrez où se trouve votre commande.
                            <br>
                        </p>
                    </div>
                </div>
            </div>

            <div id="sheet-recuperer" class="menu-box menu-bottom bg-transparent">
                <div class="menu-wrapper">
                    <div class="menu-title">
                        <h4 class="primary-heading center-text">Où et Comment Récupérer Ma Commande ?</h4>
                        <a href="#" class="menu-hide"><i class="fa fa-times"></i></a>
                    </div>
                    <div class="content">
                        <p class="content-justify">
                            <strong>Dans les pays où Yanfoma est représenté</strong>, notre équipe sur place se chargera de collecter le colis
                            et vous contactera pour que vous définissiez le meilleur moyen pour vous d’entrer en possession de votre bien.<br><br>
                        </p>
                    </div>
                </div>
            </div>

            <div id="sheet-couverture" class="menu-box menu-bottom bg-transparent">
                <div class="menu-wrapper">
                    <div class="menu-title">
                        <h4 class="primary-heading center-text">Zones de Couverture de YanfoShop</h4>
                        <a href="#" class="menu-hide"><i class="fa fa-times"></i></a>
                    </div>
                    <div class="content">
                        <figure>
                            <img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1553174144/Yanfoma/mapYanfoma.png"  width="300" alt="logo yanfoSho">
                            <figcaption class="text-center">Yanfoma est présent dans trois (03) pays pour l'instant</figcaption>
                        </figure>
                    </div>
                </div>
            </div>

            <div id="sheet-produit-introuve" class="menu-box menu-bottom bg-transparent">
                <div class="menu-wrapper">
                    <div class="menu-title">
                        <h4 class="primary-heading center-text">Je ne trouve pas mon produit. Comment Faire?</h4>
                        <a href="#" class="menu-hide"><i class="fa fa-times"></i></a>
                    </div>
                    <div class="content">
                        <p class="content-justify">
                            Cher Client, pas de panique,ne vous inquiétez pas nous avons une bonne nouvelle. Veuillez juste nous demander un devis en envoyant un e-mail à info@yanfoma.tech avec votre liste de produits recherchés.
                            Nous traiterons votre requête en 24h.
                        </p>
                    </div>
                </div>
            </div>

            <div id="sheet-bitcoin" class="menu-box menu-bottom bg-transparent">
                <div class="menu-wrapper">
                    <div class="menu-title">
                        <h4 class="primary-heading center-text">Payer Par Bitcoin</h4>
                        <a href="#" class="menu-hide"><i class="fa fa-times"></i></a>
                    </div>
                    <div class="content">
                        <p class="content-justify">
                            Pour payer en Bitcoin, vous devez repondre au mail que vous recevrez apres la commande en precisant que vous voulez utiliser cette methode de paiement. Vous recevrez la somme a envoyer en Bitcoin et le temps limite pour effectuer la transaction.
                            Vous avez une reduction de 1000 Fr CFA lorsque vous faites votre paiement en Bitcoin.
                        </p>
                    </div>
                </div>
            </div>
            <div id="sheet-Ecobank-Pay" class="menu-box menu-bottom bg-transparent">
                <div class="menu-wrapper">
                    <div class="menu-title">
                        <h4 class="primary-heading center-text">Payer Par Ecobank Pay</h4>
                        <a href="#" class="menu-hide"><i class="fa fa-times"></i></a>
                    </div>
                    <div class="content">
                        <p class="content-justify">
                            Vous pouvez payer via Ecobank Pay à travers le QR Code que suivant:
                            <br>
                            <img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1547479354/Screen_Shot_2019-01-14_at_10.42.42_PM.png" width="200">
                            <br>
                            Vous avez une réduction de 1000 Fr CFA lorsque vous faites votre paiement via Ecobank Pay.
                        </p>
                    </div>
                </div>
            </div>
            <div id="sheet-Orange-Money" class="menu-box menu-bottom bg-transparent">
                <div class="menu-wrapper">
                    <div class="menu-title">
                        <h4 class="primary-heading center-text">Payer Par Orange Money</h4>
                        <a href="#" class="menu-hide"><i class="fa fa-times"></i></a>
                    </div>
                    <div class="content">
                        <p class="content-justify">
                            Vous devez envoyer la somme équivalente a votre commande au <strong> numéro suivant: +22667402030</strong>. Notez que ce numero est au Burkina Faso. Apres le paiement, vous devez repondre au mail que vous avez recu apres la commande en nous envoyant le numero de la transaction.
                            Vous avez une réduction de 500 Fr CFA lorsque vous faites votre paiement par Orange Money.
                        </p>
                    </div>
                </div>
            </div>

            <div id="sheet-Cash-Chez-Kaanu" class="menu-box menu-bottom bg-transparent">
                <div class="menu-wrapper">
                    <div class="menu-title">
                        <h4 class="primary-heading center-text">Payer Par Cash Chez Kaanu</h4>
                        <a href="#" class="menu-hide"><i class="fa fa-times"></i></a>
                    </div>
                    <div class="content">
                        <p class="content-justify">
                            Vous pouvez payer au centre Kaanu lorsque votre colis arrivera. Il vous suffit d'emmener
                            la somme équivalente a votre commande lorsque vous aller récuperer votre colis et payer avec l'agent Kaanu.
                        </p>
                    </div>
                </div>
            </div>

            <div id="sheet-Depot-Bancaire" class="menu-box menu-bottom bg-transparent">
                <div class="menu-wrapper">
                    <div class="menu-title">
                        <h4 class="primary-heading center-text">Payer Par Dépôt Bancaire</h4>
                        <a href="#" class="menu-hide"><i class="fa fa-times"></i></a>
                    </div>
                    <div class="content">
                        <p class="content-justify">
                            Vous devez vous rendre à l'agence Ecobank la plus proche et déposer la somme équivalente à votre commande dans notre compte bancaire. Après la transaction, devez répondre au mail que vous avez reçu après la commande avec en fichier joint, une photo du reçu de dépôt à la banque. Voici le numéro du compte bancaire: (le SWIFT est important pour ceux qui sont hors du Burkina Faso)
                            <br><strong> SWIFT: ECOCBFBF</strong>
                            <br><strong>Account No: 170048532001</strong>
                            <br>Vous avez une réduction de 1000 Fr CFA lorsque vous faites votre paiement par dépôt bancaire Ecobank.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('post_header')
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" 		   content="Yanfoma Soko" />
    <meta property="og:site_name" 	   content="Yanfoma">
    <meta property="fb:app_id" 		   content="400025927061215">
    <meta property="og:title"          content="Yanfoma Soko" />
    <meta property="og:description"    content="Yanfoma Soko" />
    <meta property="og:image"          content="https://res.cloudinary.com/yanfomaweb/image/upload/v1553782826/Yanfoma/metaImage2.png')}}" />
    <meta property="og:type" 	       content="article">
    <meta property="og:url"            content="https://yanfoma.com">
@endsection