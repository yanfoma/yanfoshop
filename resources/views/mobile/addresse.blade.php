@extends('layouts.frontEnd.mobile.appHome')

@section('title')
    Mon Addresse Yanfoma Soko
@endsection

@section('content')
    <div class="page-content header-clear-large">
        <div id="page-vcard">
            <div class="widgetSimple2">
                <div class="vcard-header">
                    <img data-src="https://res.cloudinary.com/yanfomaweb/image/upload/v1539208782/Yanfoma/avatar-1.png" src="https://res.cloudinary.com/yanfomaweb/image/upload/v1539208782/Yanfoma/avatar-1.png" class="preload-image shadow-medium"  alt="User">
                    <h4 class="">{{Auth::user()->name}}</h4>
                    <em class="small-text color-gray-dark bottom-10">{{Auth::user()->email}}</em>
                    @if($user->verified =="yes")
                        <a href="#" target="_blank" class="default-link button button-xs button-green button-rounded uppercase ultrabold bottom-30 shadow-small"><i class="fa fa-check-circle"></i> Email Verifer</a>
                    @endif
                </div>
            </div>
            <div class="decoration opacity-90 bottom-0"></div>

            <div class="bottom-50">
                <form class="bottom-50" method="POST" action="{{ route('customer.updateAddresse',['id' => Auth::user()->id] ) }}">
                    {!! csrf_field() !!}
                    <div class="widgetSimple2">
                        <label class="control-label">Pays</label>
                        <div class="select-box select-box-2 bottom-15">
                            <select name="pays" class="selectpicker form-control show-tick" data-live-search="true" data-header="Pays De Naissance" title="Pays De Naissance" required>
                                @if($user->pays)
                                    <option value="{{$user->pays}}" selected> {{ $user->pays }} </option>
                                @endif
                                <option value="Burkina Faso">Burkina Faso</option>
                                <option value="Cote d'Ivoire">Cote d'Ivoire</option>
                                <option value="Cameroun">Cameroun</option>
                                <option value="Mail">Mail</option>
                                <option value="Sénégal">Sénégal</option>
                                <option value="Rwanda">Rwanda</option>
                            </select>
                            {!! $errors->first('pays', '<small class="help-block">:message</small>') !!}
                        </div>
                        <label class="control-label">Ville</label>
                        <div class="select-box select-box-2 bottom-15">
                            <select name="ville" class="selectpicker form-control show-tick" data-live-search="true" data-header="Ville" title="Ville" required>
                                @if($user->ville)
                                    <option value="{{$user->ville}}" selected> {{ $user->ville }} </option>
                                @endif
                                <option value="Bobo-Dioulasso">Bobo-Dioulasso</option>
                                <option value="Koudougou">Koudougou</option>
                                <option value="Ouagadougou">Ouagadougou</option>
                            </select>
                            {!! $errors->first('ville', '<small class="help-block">:message</small>') !!}
                        </div>
                        <div class="input-simple-2 textarea has-icon">
                            <i class="fa fa-edit"></i>
                            <textarea name="addresse" class="textarea-simple-2 form-control border-form-control" rows="10" cols="50" required> {{$user->addresse}}</textarea>
                            {!! $errors->first('addresse', '<small class="help-block">:message</small>') !!}
                        </div>
                        <div class="clear"></div>
                        <div class="one-half ">
                            <button type="submit" class="top-30 default-link button button-s bg-highlight button-rounded uppercase ultrabold shadow-medium button-full">Sauvegarger</button>
                        </div>
                        <div class="one-half last-column">
                            <button type="reset" class="top-30 default-link button button-s bg-red-dark button-rounded uppercase ultrabold shadow-medium button-full">Annuler</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('post_header')
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" 		   content="Yanfoma Soko" />
    <meta property="og:site_name" 	   content="Yanfoma">
    <meta property="fb:app_id" 		   content="400025927061215">
    <meta property="og:title"          content="Yanfoma Soko" />
    <meta property="og:description"    content="Yanfoma Soko" />
    <meta property="og:image"          content="https://res.cloudinary.com/yanfomaweb/image/upload/v1553782826/Yanfoma/metaImage2.png')}}" />
    <meta property="og:type" 	       content="article">
    <meta property="og:url"            content="https://yanfoma.com">
@endsection

@section('scripts')
    <script src="{{asset('js/intlTelInput.js')}}"></script>
    <script>
        var input = document.querySelector("#phone");
        window.intlTelInput(input, {
            onlyCountries: ["bf", "ci", "ml","rw","sn"],
            nationalMode: true,
            hiddenInput: "full_phone",
            utilsScript: "../../js/utils.js" // just for formatting/placeholders etc
        });
        $("dob").flatpickr(optional_config);
    </script>
@endsection