@extends('layouts.frontEnd.mobile.app')

@section('title')
    Transaction Complète
@endsection
@section('content')
    <div class="page-content header-clear-large">
            <div class="content top-40 content-boxed content-boxed-padding content-margins content-round shadow-medium">
                <div class="above-overlay">
                    <h4 class="color-white center-text bolder top-20">Commande Effectuée!!!</h4>
                    <p class="color-white opacity-100 bottom-30" style="padding: 10px; text-align: justify;">
                        Votre commande a été recue. Veuillez effectuer le paiement dans les plus bref delais pour finaliser votre commande.
                        <br><span class="center-text"> Merci de Votre consideration!!!</span>
                    </p>
                </div>
                <div class="overlay bg-blue-dark opacity-90"></div>
            </div>
    </div>
@endsection
@section('post_header')
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" 		   content="Yanfoma Soko" />
<meta property="og:site_name" 	   content="Yanfoma">
<meta property="fb:app_id" 		   content="400025927061215">
<meta property="og:title"          content="Yanfoma Soko" />
<meta property="og:description"    content="Yanfoma Soko" />
<meta property="og:image"          content="https://res.cloudinary.com/yanfomaweb/image/upload/v1553782826/Yanfoma/metaImage2.png')}}" />
<meta property="og:type" 	       content="article">
<meta property="og:url"            content="https://yanfoma.com">
@endsection