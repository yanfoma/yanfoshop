@extends('layouts.frontEnd.mobile.app')

@section('title')
    {{$title}}
@endsection

@section('content')
    <div class="page-content header-clear-large">
        <div class="page-content header-clear-small">
            @if($product_query->count() >0)
                <div data-height="150" class="caption caption-margins round-medium" style="height: 150px;">
                    <div class="widget1 left-15 text-left">
                        <h1 class="primary-heading bolder text-center">{{$title}}</h1>
                        <p class="under-heading color-gray-dark opacity-90 bottom-0 center-text" style="font-style: italic!important;">
                           ( {{$product_query->count()}} ) Résultats.
                        </p>
                    </div>
                </div>
                <div class="search-page">
                    <div class="search-results content-boxed shadow-large">
                        {{--<div class="widgetSimple search search-header" style="margin-left: 4px!important;margin-right: 4px!important;">--}}
                            {{--<form action="{{route('results',['store_slug' => $store->slug])}}" class="header-searchbox" method="POST">--}}
                                {{--{{csrf_field()}}--}}
                                {{--<i class="fa fa-search"></i>--}}
                                {{--<input type="text"  name="query" required placeholder="chercher votre produit ici ...">--}}
                                {{--<input type="hidden" name="category" value="{{$category->id}}" >--}}
                                {{--<a href="#" class=""><i class="fa fa-times-circle color-red2-dark"></i></a>--}}
                            {{--</form>--}}
                        {{--</div>--}}
                        <div class="content">
                            @if($product_query->count())
                                @foreach($product_query as $product)
                                    <div data-filter-item="{{$product->name}}" data-filter-name="{{$product->name}}" class="search-result-list">
                                        <img class="shadow-small" src="{{$product->image_url}}" alt="{{$product->name}}">
                                        <h1 style="font-size: 13px!important;">{{$product->name}}</h1>
                                        <p>
                                            @if($product->onSale)
                                                <del class="oldprice">{{number_format($product->price, 0 , ',' , ' ')}} </del>
                                                <strong class="price">{{number_format($product->sale_amount, 0 , ',' , ' ')}} CFA </strong>
                                            @else
                                                <strong class="price">{{number_format($product->price, 0 , ',' , ' ')}} CFA </strong>
                                            @endif
                                            <br>
                                            <span class="color-red-dark">Min: {{$product->minQty}}</span>
                                        </p>
                                        <a href="{{route('shop.single',['store_slug' => $store->slug, 'slug' => $product->slug])}}" class="bg-primary color-white">Voir</a>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            @else
                <blockquote>Aucun Produit Trouvé</blockquote>
            @endif
        </div>
    </div>
@endsection

@section('scriptss')
            <script type="text/javascript">
                //Preload Image
                $(function() {$(".preload-search-image").lazyload({threshold : 0});});

                //Search Menu Functions
                function search_menu(){
                    $('[data-search]').on('keyup', function() {
                        var searchVal = $(this).val();
                        if (searchVal != '') {
                            $('.menu-search-trending').addClass('disabled-search-item');
                            $('#menu-search-list .search-results').removeClass('disabled-search-list');
                            $('#menu-search-list [data-filter-item]').addClass('disabled-search-item');
                            $('#menu-search-list [data-filter-item][data-filter-name*="' + searchVal.toLowerCase() + '"]').removeClass('disabled-search-item');
                        } else {
                            $('.menu-search-trending').removeClass('disabled-search-item');
                            $('#menu-search-list .search-results').addClass('disabled-search-list');
                            $('#menu-search-list [data-filter-item]').removeClass('disabled-search-item');
                        }
                    });
                    return false;
                }
                search_menu();

                //Menu Search Values//
                $('.menu-search-trending a').on('click',function(){
                    var e = jQuery.Event("keydown");
                    e.which = 32;
                    var search_value = $(this).text();
                    $('.search-header input').val(search_value);
                    $('.search-results').removeClass('disabled-search-list');
                    $('[data-filter-item]').addClass('disabled-search-item');
                    $('[data-filter-item][data-filter-name*="' + search_value.toLowerCase() + '"]').removeClass('disabled-search-item');
                    $('#search-page').addClass('move-search-list-up');
                    $('.search-header a').addClass('search-close-active');
                    $('.menu-search-trending').addClass('disabled-search-item');
                    return false;
                });

                $('#menu-hider, .close-menu, .menu-hide').on('click',function(){
                    $('.menu-box').removeClass('menu-box-active');
                    $('#menu-hider').removeClass('menu-hider-active');
                    setTimeout(function(){$('#search-page').removeClass('move-search-list-up');},100);
                    $('[data-filter-item]').addClass('disabled-search-item');
                    $('.search-header input').val('');
                    $('.menu-search-trending').removeClass('disabled-search-item');
                    $('.search-header a').removeClass('search-close-active');
                    $('#search-page').removeClass('move-search-list-up');
                    return false;
                });
                $('#menu-search-list input').on('focus',function(){
                    $('#search-page').addClass('move-search-list-up');
                    $('.search-header a').addClass('search-close-active');
                    return false;
                });
                $('.search-header a').on('click',function(){
                    $('.menu-search-trending').removeClass('disabled-search-item');
                    $('#menu-search-list .search-results').addClass('disabled-search-list');
                    $('.search-header input').val('');
                    $('#search-page').removeClass('move-search-list-up');
                    $('.search-header a').removeClass('search-close-active');
                    return false;
                });
            </script>
@endsection

@section('post_header')
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" 		   content="Yanfoma Soko" />
    <meta property="og:site_name" 	   content="Yanfoma">
    <meta property="fb:app_id" 		   content="400025927061215">
    <meta property="og:title"          content="Yanfoma Soko" />
    <meta property="og:description"    content="Yanfoma Soko" />
    <meta property="og:image"          content="https://res.cloudinary.com/yanfomaweb/image/upload/v1553782826/Yanfoma/metaImage2.png')}}" />
    <meta property="og:type" 	       content="article">
    <meta property="og:url"            content="https://yanfoma.com">
@endsection

