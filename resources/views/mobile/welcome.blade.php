@extends('layouts.frontEnd.mobile.appHome')

@section('title')
    MES INFORMATIONS Yanfoma Soko
@endsection

@section('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/css/bootstrap-select.min.css"                 type="text/css" media="screen,projection">
    <link rel="stylesheet" href="{{ asset('css/intlTelInput.css') }}"         type="text/css" media="screen,projection">
@endsection

@section('content')
    <div class="page-content header-clear-large">
        <div id="page-vcard">
            <div class="widgetSimple2">
                <div class="vcard-header">
                    <img data-src="https://res.cloudinary.com/yanfomaweb/image/upload/v1539208782/Yanfoma/avatar-1.png" src="https://res.cloudinary.com/yanfomaweb/image/upload/v1539208782/Yanfoma/avatar-1.png" class="preload-image shadow-medium"  alt="User">
                    <h4 class="">{{Auth::user()->name}}</h4>
                    <em class="small-text color-gray-dark bottom-10">{{Auth::user()->email}}</em>
                    @if($user->verified =="yes")
                        <a href="#" target="_blank" class="default-link button button-xs button-green button-rounded uppercase ultrabold bottom-30 shadow-small"><i class="fa fa-check-circle"></i> Email Verifer</a>
                    @endif
                </div>
            </div>
            <div class="decoration opacity-90 bottom-0"></div>

            <div class="bottom-50">
                <form class="bottom-50" method="POST" action="{{ route('customer.update',['id' => Auth::user()->id] ) }}">
                    {!! csrf_field() !!}
                    <div class="widgetSimple2">
                        <div class="#">
                        <label class="control-label">Nom</label>
                        <div class="input-simple-2 has-icon input-green bottom-15">
                            <i class="fa fa-user"></i>
                            <input name="name" type="text" class="form-control border-form-control" id="name" value="{{ $user->name}}">
                            {!! $errors->first('name', '<small class="help-block">:message</small>') !!}
                        </div>
                        <label class="control-label">Numéro Whatsapp</label>
                        <div class="input-simple-2 has-icon input-green bottom-15">
                            <i class="fa fa-phone"></i>
                            <input name="wa_number" type="tel" id="phone" class="form-control border-form-control" value="{{ $user->wa_number}}" title="Numéro Whatsapp">
                            {!! $errors->first('wa_number', '<small class="help-block">:message</small>') !!}
                        </div>
                        <label class="control-label">Email</label>
                        <div class="input-simple-2 has-icon input-blue bottom-15">
                            <i class="fa fa-envelope"></i>
                            <input name="email" type="email" class="form-control border-form-control" id="email" value="{{ $user->email}}">
                            {!! $errors->first('email', '<small class="help-block">:message</small>') !!}
                        </div>
                        <label class="control-label">Sexe</label>
                        <div class="select-box select-box-2 bottom-15">
                            <select class="selectpicker form-control show-tick"  name="sexe" data-header="sexe" title="sexe">
                                @if($user->sexe) <option value="{{$user->sexe}}" selected > {{ $user->sexe }} </option> @endif
                                <option value="Masculin">Masculin</option>
                                <option value="Feminin">Feminin</option>
                            </select>
                            {!! $errors->first('sexe', '<small class="help-block">:message</small>') !!}
                        </div>
                        <label for="dob">Date de Naissance</label>
                        <div class="input-simple-2 has-icon input-blue bottom-15">
                            <i class="fa fa-calendar"></i>
                            <input name="dob" type="date" class="form-control" id="dob" placeholder="Date de Naissance" value="{{$user->dob}}">
                            {!! $errors->first('dob', '<small class="help-block">:message</small>') !!}
                            @if(session()->has('dob'))
                                <div class="alert alert-danger alert-dismissible ">{!! session('dob') !!}</div>
                            @endif
                        </div>
                        <label for="education">Education</label>
                        <div class="input-simple-2 has-icon input-blue bottom-15">
                            <i class="fa fa-building"></i>
                            <input name="education" type="text" class="form-control" id="education" placeholder="Education" value="{{$user->education}}">
                            {!! $errors->first('education', '<small class="help-block">:message</small>') !!}
                            @if(session()->has('education'))
                                <div class="alert alert-danger alert-dismissible ">{!! session('education') !!}</div>
                            @endif
                        </div>
                        <label for="profession">Profession</label>
                        <div class="input-simple-2 has-icon input-blue bottom-15">
                            <i class="fa fa-suitcase"></i>
                            <input name="profession" type="text" class="form-control" id="profession" placeholder="Profession" value="{{$user->profession}}">
                            {!! $errors->first('profession', '<small class="help-block">:message</small>') !!}
                            @if(session()->has('profession'))
                                <div class="alert alert-danger alert-dismissible ">{!! session('profession') !!}</div>
                            @endif
                        </div>
                        <label for="interests">Mes Interets</label>
                        <div class="input-simple-2 has-icon input-blue bottom-15">
                            <select class="selectpicker form-control show-tick"  name="interests[]" multiple data-live-search="true" data-header="Mes Interets" title="Mes Interets">
                                @foreach($interests as $interet)
                                    <option value="{{$interet->id}}" {{ (in_array($interet->id, $user->interests->pluck('id')->toArray())) ? 'selected' : '' }}>{{$interet->name}}</option>
                                @endforeach
                            </select>
                            {!! $errors->first('interests', '<small class="help-block">:message</small>') !!}
                            @if(session()->has('interests'))
                                <div class="alert alert-danger alert-dismissible ">{!! session('interests') !!}</div>
                            @endif
                        </div>
                        <div class="clear"></div>
                        <div class="one-half ">
                            <button type="submit" class="top-30 default-link button button-s bg-highlight button-rounded shadow-medium button-full">Sauvegarger</button>
                        </div>
                        <div class="one-half last-column">
                            <button type="reset" class="top-30 default-link button button-s bg-red-dark button-rounded   shadow-medium button-full">Annuler</button>
                        </div>
                    </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('post_header')
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" 		   content="Yanfoma Soko" />
    <meta property="og:site_name" 	   content="Yanfoma">
    <meta property="fb:app_id" 		   content="400025927061215">
    <meta property="og:title"          content="Yanfoma Soko" />
    <meta property="og:description"    content="Yanfoma Soko" />
    <meta property="og:image"          content="https://res.cloudinary.com/yanfomaweb/image/upload/v1553782826/Yanfoma/metaImage2.png')}}" />
    <meta property="og:type" 	       content="article">
    <meta property="og:url"            content="https://yanfoma.com">
@endsection

@section('scripts')
    <script src="{{asset('js/intlTelInput.js')}}"></script>
    <script>
        var input = document.querySelector("#phone");
        window.intlTelInput(input, {
            onlyCountries: ["bf", "ci", "ml","rw","sn"],
            nationalMode: true,
            hiddenInput: "full_phone",
            utilsScript: "../../js/utils.js" // just for formatting/placeholders etc
        });
        $("dob").flatpickr(optional_config);
    </script>
@endsection