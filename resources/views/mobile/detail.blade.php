@extends('layouts.frontEnd.mobile.appHome')

@section('title')
    Detail commande
@endsection

@section('content')
    <div class="page-content header-clear-large">
        <div id="page-vcard">
            <div class="widgetSimple2">
                <div class="vcard-header">
                    <img data-src="https://res.cloudinary.com/yanfomaweb/image/upload/v1539208782/Yanfoma/avatar-1.png" src="https://res.cloudinary.com/yanfomaweb/image/upload/v1539208782/Yanfoma/avatar-1.png" class="preload-image shadow-medium"  alt="User">
                    <h4 class="">{{Auth::user()->name}}</h4>
                    <em class="small-text color-gray-dark bottom-10">{{Auth::user()->email}}</em>
                    @if($user->verified =="yes")
                        <a href="#" target="_blank" class="default-link button button-xs button-green button-rounded uppercase ultrabold bottom-30 shadow-small"><i class="fa fa-check-circle"></i> Email Verifer</a>
                    @endif
                </div>
            </div>
            <div class="decoration opacity-90 bottom-0"></div>

            <div class="widgetSimple2 bottom-50">
                @if($detail->count())
                    @foreach($detail as $dt)
                        <div class="store-slide-2">
                            <a href="#" class="store-slide-image">
                                <img class="preload-image" src="{{ $dt->image_url }}" data-src="{{ $dt->image_url }}" alt="{{ $dt->coName}}">
                            </a>
                            <div class="store-slide-title">
                                <strong>{{ $dt->coName}}</strong>
                            </div>
                            <div class="store-slide-button">
                                <strong>({{ $dt->qty}}) Total: {{ $dt->coTotal}} CFA</strong>
                            </div>
                        </div>
                    @endforeach
                        <span class="item-views" style="text-decoration: underline"><b>Date de la Commande: {{\Jenssegers\Date\Date::parse($dt->created_at)->format('l j F Y H:i')}}</b></span>
                        {{ $detail->links() }}
                @endif
            </div>
        </div>
    </div>
@endsection

@section('post_header')
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" 		   content="Yanfoma Soko" />
    <meta property="og:site_name" 	   content="Yanfoma">
    <meta property="fb:app_id" 		   content="400025927061215">
    <meta property="og:title"          content="Yanfoma Soko" />
    <meta property="og:description"    content="Yanfoma Soko" />
    <meta property="og:image"          content="https://res.cloudinary.com/yanfomaweb/image/upload/v1553782826/Yanfoma/metaImage2.png')}}" />
    <meta property="og:type" 	       content="article">
    <meta property="og:url"            content="https://yanfoma.com">
@endsection