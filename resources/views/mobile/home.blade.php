@extends('layouts.frontEnd.mobile.appHome')

@section('title')
    Yanfoma Soko
@endsection


@section('style')
    <style>
        .blog-categories-1 em{
            text-transform: none!important;
            font-size: 19px!important;
        }
        .color-red-dark h5{
            color: red!important;
        }
        .content-boxed-padding-2 {
            padding: 5px 5px!important;
        }
        .content-margins-2{
            margin-left: -15px;
            margin-right: 25px;
        }
        .item img{
            border-top-left-radius: 8px!important;
            border-top-right-radius: 8px!important;
            border-bottom-right-radius: 8px!important;
            border-bottom-left-radius: 8px!important;
        }
        .blog-categories-3{

        }

        .blog-categories em{
            top: 35%!important;
            font-size: 10px!important;
            text-transform: none!important;
            padding: 5px!important;
        }

        .blog-categories a img{
            width: 100%!important;
            height: 70px!important;
        }
    </style>
@endsection

@section('content')
    <div class="page-content header-clear-large">
        <div class="content-boxed-padding-2 content-margins-2" style="margin-top: 10px!important;">
            <div class="slider">
                <div class="single-slider owl-carousel owl-with-dots content-boxed content-round">
                    <div class="item">
                        <img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1562152595/Yanfoma/mobi1.png" class="responsive-image bottom-0" alt="img">
                    </div>
                    <div class="item shadow-small">
                        <img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1562152783/Yanfoma/mobi2.png" class="responsive-image bottom-0" alt="img">
                    </div>
                    <div class="item shadow-small">
                        <img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1562153017/Yanfoma/mobi3.png" class="responsive-image bottom-0" alt="img">
                    </div>
                    <div class="item shadow-small">
                        <img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1562153231/Yanfoma/mobi4.png" class="responsive-image bottom-0" alt="img">
                    </div>
                    <div class="item shadow-small">
                        <img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1562153447/Yanfoma/mobi5.png" class="responsive-image bottom-0" alt="img">
                    </div>
                </div>
            </div>
        </div>
        <div class=" bottom-0">
            <div class="single-slider owl-carousel owl-no-dots">
                @foreach($stores as $store)
                    @foreach($store->products->sortByDesc('created_at')->take(12) as $product)
                        <div class="store-slide-2">
                            <a href="{{route('shop.single',['store_slug' => $product->store->slug, 'slug' => $product->slug])}}" class="store-slide-image">
                                <img src="{{$product->image_url}}">
                            </a>
                            <div class="store-slide-title">
                                <em>{{$product->name}}</em>
                            </div>
                            <div class="store-slide-button">
                                @if($product->onSale)
                                    <strong>{{number_format($product->sale_amount, 0 , ',' , ' ')}} CFA</strong>
                                @else
                                    <strong>{{number_format($product->price, 0 , ',' , ' ')}} CFA</strong>
                                @endif
                                <a href="{{route('shop.single',['store_slug' => $product->store->slug, 'slug' => $product->slug])}}"><i class="fa fa-shopping-cart color-blue-dark"></i> Voir</a>
                            </div>
                        </div>
                    @endforeach
                @endforeach
            </div>
        </div>
        {{--<div class="widgetSlider">--}}
            {{--<div class="#">--}}
                {{--<div class="single-slider owl-carousel owl-no-dots">--}}
                    {{--<div class="item shadow-small">--}}
                        {{--<img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1555434091/Yanfoma/welcomeSoko.png" alt="img">--}}
                    {{--</div>--}}
                    {{--<div class="item shadow-small">--}}
                        {{--<a href="{{route('shop','yanfoShop')}}" ><img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1555429633/Yanfoma/yanfoShopMobile.png" alt="img"></a>--}}
                    {{--</div>--}}
                    {{--<div class="item shadow-small">--}}
                        {{--<img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1555426897/Yanfoma/PaymentsMobile.png" alt="img">--}}
                    {{--</div>--}}
                    {{--<div class="item shadow-small">--}}
                        {{--<img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1555427833/Yanfoma/livraisonMob.png" alt="img">--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

        <div class="widget">
            <div class="content-title bottom-20">
                <h2 class="font-18 bolder text-center">NOS CATÉGORIES</h2>
            </div>
            <div class="blog-categories blog-categories-3 bottom-10">
                @foreach($categories as $category)
                    <a href="{{route('viewCategory',['store_slug' => $category->store->slug,'name' => $category->name ])}}"><strong></strong>
                        <em>{{$category->name}}</em>
                        <span class="bg-orange-dark opacity-50"></span>
                        <img src="images/empty.png" data-src="https://res.cloudinary.com/yanfomaweb/image/upload/v1562109766/Yanfoma/catmobile.png" class="preload-image responsive-image" alt="img">
                    </a>
                @endforeach
                <div class="clear"></div>
            </div>
        </div>

        <div class="content widget content-boxed-padding margin-top-15 bg-blue-gradient" style="margin-top: 20px!important;">
            <div class="accordion accordion-style-0">
                <div class="accordion-border">
                    <a href="#" class="font-14" data-accordion="accordion-11"><i class="fa fa fa-truck color-yellow-dark"></i><span>Livraison Gratuite</span><i class="fa fa-plus"></i></a>
                    <div class="accordion-content" id="accordion-11">
                        <p class="bottom-10">Livraison Gratuite pour tous nos produits</p>
                    </div>
                </div>
                <div class="accordion-border">
                    <a href="#" class="font-14" data-accordion="accordion-12"><i class="fa fa fa-bolt color-yellow-dark"></i><span>COMMANDE RAPIDE</span><i class="fa fa-plus"></i></a>
                    <div class="accordion-content" id="accordion-12">
                        <p class="bottom-10">Produits de Produits de Qualité et d'Origine Unique</p>
                    </div>
                </div>
                <div class="accordion-border">
                    <a href="#" class="font-14" data-accordion="accordion-13"><i class="fa fa fa-thumbs-o-up color-yellow-dark"></i><span>Qualité et Originalité</span><i class="fa fa-plus"></i></a>
                    <div class="accordion-content" id="accordion-13">
                        <p class="bottom-10">Commande Rapide et Personnalisée</p>
                    </div>
                </div>
                <div class="accordion-border">
                    <a href="#" class="font-14" data-accordion="accordion-14"><i class="fa fa fa-support color-yellow-dark"></i><span>Support 24/7</span><i class="fa fa-plus"></i></a>
                    <div class="accordion-content" id="accordion-14">
                        <p class="bottom-10">Une Équipe Performante À Votre écoute 24H/24 7j/7</p>
                    </div>
                </div>

            </div>
        </div>

        <div class="widget">
            <div class="news-home">
                <div class="news-tabs bg-white">
                    <a href="#" data-tab="news-tab-2" class="one-third center-text">Nouveautes</a>
                    <a href="#" data-tab="news-tab-1" class="one-third center-text active-tab-button">Nos Deals</a>
                    <a href="#" data-tab="news-tab-3" class="one-third last-column center-text">En Promo</a>
                    <div class="clear"></div>
                </div>
                <div class="news-tabs-content">
                <div class="tab-item active-tab" id="news-tab-1">
                    <div class="content">
                        @foreach($bonsDeals->take(6) as $product)
                            <div class="news-list-item">
                                <a href="{{route('shop.single',['store_slug' => $product->store->slug, 'slug' => $product->slug])}}" class="store-slide-image">
                                    <img src="{{$product->image_url}}" class="rounded-image shadow-medium" alt="{{$product->name}}">
                                    <strong>{{$product->name}}</strong>
                                </a>
                                <span>
                                    <i class="fas fa-exclamation-triangle"></i> Min: {{$product->minQty}}
                                    @if($product->onSale)
                                        <a href="{{route('shop.single',['store_slug' => $product->store->slug, 'slug' => $product->slug])}}" class="color-highlight font-14 bolder">{{number_format($product->sale_amount, 0 , ',' , ' ')}} CFA</a>
                                    @else
                                        <a href="{{route('shop.single',['store_slug' => $product->store->slug, 'slug' => $product->slug])}}" class="color-highlight font-14 bolder">{{number_format($product->price, 0 , ',' , ' ')}} CFA</a>
                                    @endif
                                    <a href="{{route('shop.single',['store_slug' => $product->store->slug, 'slug' => $product->slug])}}"><i class="fa fa-eye color-orange-dark"></i></a>
                                </span>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="tab-item" id="news-tab-2">
                    <div class="content">
                            @foreach($bonsDeals->take(6) as $key=>$product)
                                @if($loop->iteration  % 2 == 1)
                                <div class="one-half">
                                        <div class="news-col-item">
                                            <a href="{{route('shop.single',['store_slug' => $product->store->slug, 'slug' => $product->slug])}}">
                                                <img src="{{$product->image_url}}" class="responsive-image rounded-image shadow-medium">
                                                @if($product->onSale)
                                                    <em class="bg-red-dark">{{number_format($product->sale_amount, 0 , ',' , ' ')}} CFA</em>
                                                @else
                                                    <em class="bg-green-dark">{{number_format($product->price, 0 , ',' , ' ')}} CFA</em>
                                                @endif
                                                <strong>{{$product->name}}</strong>
                                            </a>
                                            <span>
                                                <i class="fas fa-exclamation-triangle"></i> Min: {{$product->minQty}}
                                                <a href="{{route('shop.single',['store_slug' => $product->store->slug, 'slug' => $product->slug])}}"><i class="fa fa-eye color-orange-dark"></i></a>
                                            </span>
                                        </div>
                                    </div>
                                @endif
                                @if($loop->iteration  % 2 == 0)
                                    <div class="one-half last-column">
                                        <div class="news-col-item">
                                            <a href="{{route('shop.single',['store_slug' => $product->store->slug, 'slug' => $product->slug])}}">
                                                <img src="{{$product->image_url}}" class="responsive-image rounded-image shadow-medium">
                                                @if($product->onSale)
                                                    <em class="bg-red-dark">{{number_format($product->sale_amount, 0 , ',' , ' ')}} CFA</em>
                                                @else
                                                    <em class="bg-green-dark">{{number_format($product->price, 0 , ',' , ' ')}} CFA</em>
                                                @endif
                                                <strong>{{$product->name}}</strong>
                                            </a>
                                            <span>
                                                <i class="fas fa-exclamation-triangle"></i> Min: {{$product->minQty}}
                                                <a href="{{route('shop.single',['store_slug' => $product->store->slug, 'slug' => $product->slug])}}"><i class="fa fa-eye color-orange-dark"></i></a>
                                            </span>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        <div class="clear"></div>
                    </div>

                </div>
                <div class="tab-item" id="news-tab-3">
                    @foreach($promos->take(6) as $key=>$product)
                        @if($loop->iteration  % 2 == 1)
                            <div class="one-half">
                                <div class="news-col-item">
                                    <a href="{{route('shop.single',['store_slug' => $product->store->slug, 'slug' => $product->slug])}}">
                                        <img src="{{$product->image_url}}" class="responsive-image rounded-image shadow-medium">
                                        @if($product->onSale)
                                            <em class="bg-red-dark">{{number_format($product->sale_amount, 0 , ',' , ' ')}} CFA</em>
                                        @else
                                            <em class="bg-green-dark">{{number_format($product->price, 0 , ',' , ' ')}} CFA</em>
                                        @endif
                                        <strong>{{$product->name}}</strong>
                                    </a>
                                    <span>
                                        <i class="fas fa-exclamation-triangle"></i> Min: {{$product->minQty}}
                                        <a href="{{route('shop.single',['store_slug' => $product->store->slug, 'slug' => $product->slug])}}"><i class="fa fa-eye color-orange-dark"></i></a>
                                    </span>
                                </div>
                            </div>
                        @endif
                        @if($loop->iteration  % 2 == 0)
                            <div class="one-half last-column">
                                <div class="news-col-item">
                                    <a href="{{route('shop.single',['store_slug' => $product->store->slug, 'slug' => $product->slug])}}">
                                        <img src="{{$product->image_url}}" class="responsive-image rounded-image shadow-medium">
                                        @if($product->onSale)
                                            <em class="bg-red-dark">{{number_format($product->sale_amount, 0 , ',' , ' ')}} CFA</em>
                                        @else
                                            <em class="bg-green-dark">{{number_format($product->price, 0 , ',' , ' ')}} CFA</em>
                                        @endif
                                        <strong>{{$product->name}}</strong>
                                    </a>
                                    <span>
                                        <i class="fas fa-exclamation-triangle"></i> Min: {{$product->minQty}}
                                        <a href="{{route('shop.single',['store_slug' => $product->store->slug, 'slug' => $product->slug])}}"><i class="fa fa-eye color-orange-dark"></i></a>
                                    </span>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
            </div>
        </div>

        <div class="widget">
            <div class="content-title bottom-20">
                <h2 class="font-18 bolder text-center">NOS VINS</h2>
            </div>
            <div class="single-slider owl-carousel owl-no-dots bottom-30">
            @foreach($vins->products as $product)
                <div class="store-slide-1">
                    <a href="#" class="store-slide-image"><img src="{{$product->image_url}}" alt=""></a>
                    <div class="store-slide-title bottom-15">
                        <h3 class="bottom-0 center-text">{{$product->name}}</h3>
                    </div>
                    <div class="store-slide-price">
                        @if($product->onSale)
                            <strong>{{number_format($product->sale_amount, 0 , ',' , ' ')}} CFA<br> <del>{{number_format($product->price, 0 , ',' , ' ')}} CFA</del></strong>
                        @else
                            <strong>{{number_format($product->price, 0 , ',' , ' ')}} CFA</strong>
                        @endif
                        <div class="clear"></div>
                    </div>
                    <div class="store-slide-button">
                        <a href="{{route('shop.single',['store_slug' => $product->store->slug, 'slug' => $product->slug])}}" class="button button-green button-center button-rounded button-full button-s uppercase ultrabold shadow-small">Voir</a>
                    </div>
                </div>
            @endforeach
        </div>
        </div>
        <div class="widget">
            <div class="content-title bottom-20">
                <h2 class="font-18 bolder">NOS SENSEURS</h2>
                <a href="{{route('viewCategory',['store_slug' => "yanfoshop",'name' => "Senseurs & Modules" ])}}" class="color-highlight">Tout Voir</a>
            </div>
            @foreach($senseurs->products->sortByDesc('created_at')->take(3) as $product)
                <div class="store-slide-2">
                    <a href="{{route('shop.single',['store_slug' => $product->store->slug, 'slug' => $product->slug])}}" class="store-slide-image">
                        <img class="preload-image" src="{{$product->image_url}}" data-src="{{$product->image_url}}" alt="{{$product->name}}">
                    </a>
                    <div class="store-slide-title">
                        <strong>{{$product->name}}</strong>
                    </div>
                    <div class="store-slide-button">
                        <span class="store-product-price">
                            @if($product->onSale)
                                <em><del>{{number_format($product->price, 0 , ',' , ' ')}} CFA</del></em>
                                <strong class="color-orange-dark">{{number_format($product->sale_amount, 0 , ',' , ' ')}}</strong>
                            @else
                                <strong class="primary-heading">{{number_format($product->price, 0 , ',' , ' ')}} <span style="font-size: 12px;">CFA</span> </strong>
                            @endif
                        </span>
                        <a href="{{route('shop.single',['store_slug' => $product->store->slug, 'slug' => $product->slug])}}"><i class="fa fa-eye color-orange-dark"></i></a>
                    </div>
                </div>
                <div class="decoration bottom-0"></div>
            @endforeach
        </div>

        <div class="content widget bg-blue-gradient">
            <div class="content-title bottom-20">
                <h2 class="font-18 bolder text-center">NOS BOUTIQUES</h2>
            </div>
            <div  class="article-card  article-card-round bg-white">
                <a data-deploy-card="article-4" href="{{route('shop',"yanfoshop")}}" class="article-header">
                    <span class="article-image preload-image" data-src-retina="https://res.cloudinary.com/yanfomaweb/image/upload/v1557963871/Yanfoma/discover.png" data-src="https://res.cloudinary.com/yanfomaweb/image/upload/v1557963871/Yanfoma/discover.png"></span>
                    <span class="article-category color-white bg-green-dark uppercase">YanfoShop</span>
                    <span class="article-author color-gray-light"><i class="fa fa-bars"></i>technologie</span>
                </a>
            </div>
            <div  class="article-card  article-card-round bg-white shadow-medium bottom-50">
                <a data-deploy-card="article-4" href="{{route('shop',"emprise")}}" class="article-header">
                    <span class="article-image preload-image" data-src-retina="https://res.cloudinary.com/yanfomaweb/image/upload/v1561756696/Yanfoma/nosvins.png" data-src="https://res.cloudinary.com/yanfomaweb/image/upload/v1561756696/Yanfoma/nosvins.png"></span>
                    <span class="article-category color-white bg-red-dark uppercase">Emprise</span>
                    <span class="article-author color-gray-light"><i class="fa fa-bars"></i>vin</span>
                </a>
            </div>
        </div>


        <div class="content-title bottom-30">
            <h1>NOS SERVEURS</h1>
        </div>
        <div class="slider-margins bottom-30">
            <div class="double-slider owl-carousel owl-no-dots">
                @foreach($cloud->products->sortByDesc('price') as $product)
                    <div class="item bottom-10 shadow-small">
                        <div class="above-overlay above-overlay-bottom bottom-10">
                            @if($product->onSale)
                                <h3 class="color-orange-dark">{{number_format($product->sale_amount, 0 , ',' , ' ')}} CFA</h3>
                                <h5 class="color-white">{{$product->name}}</h5>
                            @else
                                <h3 class="color-orange-dark">{{number_format($product->price, 0 , ',' , ' ')}} CFA</h3>
                                <h5 class="color-white">{{$product->name}}</h5>
                            @endif
                        </div>
                        <div class="overlay overlay-gradient"></div>
                        <a href="{{route('shop.single',['store_slug' => $product->store->slug, 'slug' => $product->slug])}}">
                            <img src="{{$product->image_url}}" alt="{{$product->name}}" class="responsive-image">
                        </a>
                    </div>
                @endforeach
            </div>
        </div>

        <div class="decoration decoration-margins"></div>
        <div class="card card-violet">
            <h1 class="color-white bolder center-text top-20">Vendre Sur Yanfoma Soko</h1>
            <p class="small-text center-text color-white bottom-15 opacity-100">Jusqu'a 50000 Fr CFA -> Gratuit </p>
            <p class="color-white center-text opacity-80 top-10 bottom-30">
                Pour être vendeur sur Yanfoma Soko, envoyez votre identifiant, la description de votre boutique, la liste de vos produits(nom, prix, description du produit, image), ainsi que le logo à l'adresse mail info@yanfoma.tech
            </p>
            <a href="{{route('yanfomaSoko.vendre')}}" class="button button-s button-rounded button-white uppercase ultrabold button-center">En savoir Plus</a>
        </div>

        <div class=" content content-boxed" style="transition: all 300ms ease;">
            <p class="footer-text text-center">Suivez-Nous Sur nos Réseaux Sociaux</p>
            <div class="">
                <a href="https://www.facebook.com/yanfoma/" class="shareToFacebook button button-rounded bg-facebook button button-sm button-icon regularbold"><i class="fab fa-facebook-f"></i>Facebook</a>
                <a href="https://chat.whatsapp.com/invite/EsCjgPWHuPf39ujQKANX5C" class="shareToWhatsApp button button-rounded bg-whatsapp button button-sm button-icon regularbold"><i class="fab fa-whatsapp"></i>WhatsApp</a>
                <a href="https://m.me/yanfoma" class="shareToMail button button-rounded bg-mail button button-sm button-icon regularbold"><i class="fa fa-comments-o"></i>Messenger </a>
                <a href="https://www.linkedin.com/company-beta/22333242" class="shareToLinkedIn button button-rounded bg-linkedin button button-sm button-icon regularbold"><i class="fa fa-linkedin"></i> Linkedin</a>
            </div>
            <button class="add-button">Installer sur votre Telephone !!!</button>
            <p class="footer-copyright"><br></p>
        </div>

    </div>
@endsection

@section('post_header')
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" 		   content="Yanfoma Soko" />
    <meta property="og:site_name" 	   content="Yanfoma">
    <meta property="fb:app_id" 		   content="400025927061215">
    <meta property="og:title"          content="Yanfoma Soko" />
    <meta property="og:description"    content="Yanfoma Soko" />
    <meta property="og:image"          content="https://res.cloudinary.com/yanfomaweb/image/upload/v1553782826/Yanfoma/metaImage2.png')}}" />
    <meta property="og:type" 	       content="article">
    <meta property="og:url"            content="https://yanfoma.com">
@endsection