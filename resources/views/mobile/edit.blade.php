@extends('layouts.frontEnd.mobile.appHome')

@section('title')
    Vos informations
@endsection

@section('content')
    <div class="page-content header-clear-large">
        <div id="page-vcard" class="content">
            <div class="vcard-header">
                <img data-src="images/pictures/0s.png" src="images/empty.png" class="preload-image shadow-medium">
                <h1 class="large-text">Karla Black</h1>
                <em class="small-text color-gray-dark bottom-10">Photographer at Enabled</em>
                <a href="scripts/vcard.vcf" target="_blank" class="default-link button button-xs button-blue button-rounded uppercase ultrabold bottom-30 shadow-small">Save to contacts</a>
            </div>
            <div class="decoration opacity-90 bottom-0"></div>

            <h1 class="vcard-title color-blue-dark bold">Phone</h1>
            <div class="vcard-field"><strong>Mobile</strong><a href="tel:+1 234 567 890">+1 234 567 890</a><i class="fa fa-phone"></i></div>
            <div class="vcard-field"><strong>Office</strong><a href="tel:+2 234 567 890">+2 234 567 890</a><i class="fa fa-suitcase"></i></div>
            <div class="vcard-field"><strong>Personal</strong><a href="tel:+3 234 567 890">+3 234 567 890</a><i class="fa fa-user"></i></div>

            <h1 class="vcard-title color-blue-dark bold">Address</h1>
            <div class="vcard-field"><strong>Work</strong><a href="tel:+1 234 567 890">Milky Way, Planet Earth, <br> 2134 UltraMobile Street</a><i class="fa fa-map-marker"></i></div>
            <div class="vcard-field"><strong>Home</strong><a href="tel:+1 234 567 890">Milky Way, Planet Earth, <br> 1234 Enabled Avenue</a><i class="fa fa-map-marker"></i></div>

            <h1 class="vcard-title color-blue-dark bold">Email</h1>
            <div class="vcard-field"><strong>Home</strong><a href="mailto:home@domain.com">home@domain.com</a><i class="fa fa-home"></i></div>
            <div class="vcard-field"><strong>Office</strong><a href="mailto:office@domain.com">office@domain.com</a><i class="fa fa-suitcase"></i></div>
            <div class="vcard-field"><strong>Personal</strong><a href="mailto:personal@domain.com">personal@domain.com</a><i class="fa fa-user"></i></div>

            <h1 class="vcard-title color-blue-dark bold">Website</h1>
            <div class="vcard-field"><strong>URL</strong><a href="www.enableds.com">www.domain.com</a><i class="fa fa-globe"></i></div>

            <h1 class="vcard-title color-blue-dark bold">Social</h1>
            <div class="vcard-field"><strong>Facebook</strong><a href="https://www.enableds.com">karla.black</a><i class="fa fa-facebook"></i></div>
            <div class="vcard-field"><strong>Twitter</strong><a href="https://www.enableds.com">@karla.black</a><i class="fa fa-twitter"></i></div>
            <div class="vcard-field"><strong>Google Plus</strong><a href="https://www.enableds.com">@karla.black</a><i class="fa fa-google-plus"></i></div>

            <a href="scripts/vcard.vcf" target="_blank" class="top-30 default-link button button-s bg-highlight button-rounded uppercase ultrabold shadow-medium button-full">Download V-Card</a>
        </div>
    </div>
@endsection

@section('post_header')
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" 		   content="Yanfoma Soko" />
    <meta property="og:site_name" 	   content="Yanfoma">
    <meta property="fb:app_id" 		   content="400025927061215">
    <meta property="og:title"          content="Yanfoma Soko" />
    <meta property="og:description"    content="Yanfoma Soko" />
    <meta property="og:image"          content="https://res.cloudinary.com/yanfomaweb/image/upload/v1553782826/Yanfoma/metaImage2.png')}}" />
    <meta property="og:type" 	       content="article">
    <meta property="og:url"            content="https://yanfoma.com">
@endsection