@extends('layouts.frontEnd.mobile.appHome')

@section('title')
    Yanfoma Soko A Propos
@endsection
@section('content')
    <div class="page-content header-clear-large">
        <div class="widget">
            <div class="content">
                <img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1554927676/Yanfoma/VertWback.png" alt="logo yanfoShop" class="preload-image center-item" width="120">
            </div>
            <div class="content-title bottom-30 top-30">
                <h1 class="bottom-10 color-primary-soko text-center font-20">C’est quoi Yanfoma Soko?</h1>
                <p class="justify-text">
                    yanfoShop est un shopping mall en ligne qui se veut être une plateforme d'accès, de partages et d'échanges de technologie numérique,
                    la première destination en ligne des makers, développeurs, entrepreneurs, amoureux de la technologie (technophiles). Il a été entièrement développé par Yanfoma.
                    yanfoShop s’est donné pour mission d’offrir une grande variété de technologies numériques, facile à apprendre, à utiliser, et accessible à tous.
                </p>
            </div>
        </div>

        <div class="widget">
            <div data-height="350" class="caption caption-margins top-10 round-medium shadow-large" style="height: 350px;">
                <div class="caption-center">
                    <img class="preload-image horizontal-center scale-box" width="220" src="https://res.cloudinary.com/yanfomaweb/image/upload/v1563718961/Yanfoma/yanfomaSokoCirlce.png" alt="img">
                    <br> <h1 class="bottom-10 color-primary-soko text-center font-20">Logo de Yanfoma Soko</h1>
                </div>
                <div class="caption-overlay bg-black opacity-75"></div>
                <div class="caption-bg bg-18"></div>
            </div>
        </div>
        <div class="content widget">
            <h4 class="bolder bottom-20 color-primary-soko text-center">Vidéo De Présentation de Yanfoma Soko</h4>
            <div class="responsive-video">
                <iframe class="image-rounded" src="https://www.youtube.com/embed/PmW0euwk0Ck" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>

        <div class="content widget bottom-30">
            <h4 class="bolder bottom-20 color-primary-soko text-center">Pourquoi acheter chez Yanfoma Soko?</h4>
            <p class="justify-text">
                yanfoShop propose uniquement technologies numériques. Cette spécialisation nous oblige à offrir de la qualité. Egalement, nous avons une équipe qui prend à coeur votre apprentissage des produits que nous proposons. Nous sommes disposés à particulierement suivre chacun de nos clients pour que l’utilisation de nos produits soit le plus simple et efficace possible.
            </p>
        </div>

        <div class="decoration decoration-margins"></div>
    </div>
    <a href="#" class="back-to-top-badge back-to-top-small bg-highlight"><i class="fa fa-angle-up"></i>Back to Top</a>
@endsection

@section('post_header')
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" 		   content="Yanfoma Soko" />
    <meta property="og:site_name" 	   content="Yanfoma">
    <meta property="fb:app_id" 		   content="400025927061215">
    <meta property="og:title"          content="Yanfoma Soko" />
    <meta property="og:description"    content="Yanfoma Soko" />
    <meta property="og:image"          content="https://res.cloudinary.com/yanfomaweb/image/upload/v1553782826/Yanfoma/metaImage2.png')}}" />
    <meta property="og:type" 	       content="article">
    <meta property="og:url"            content="https://yanfoma.com">
@endsection