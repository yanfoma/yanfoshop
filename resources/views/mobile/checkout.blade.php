@extends('layouts.frontEnd.mobile.app')

@section('title')
    {{$store->name}}
@endsection

@section('content')
    <div class="page-content header-clear-large">
        <div class="content content-boxed content-boxed-padding shadow-small">
            <div class="content-title bottom-0 top-30">
                <h3 class="center-text color-primary bottom-20">Résumé de Vos Achats</h3>
            </div>
            <div class="checkout-total">
                <strong class="font-14 regularbold">Nombre de Produits</strong>
                <span class="font-14">{{$cart->count()}}</span>
                <div class="clear"></div>
                <strong class="font-16 half-top">Total A Payer </strong>
                <span class="font-16 ultrabold half-top">{{getTotal()}} CFA</span>
                <div class="clear"></div>
            </div>
        </div>

        <div class="decoration decoration-margins"></div>

        <div class="content content-boxed content-boxed-padding shadow-small">
            <div class="content-title bottom-0 top-30">
                <h3 class="center-text color-primary">Vos Informations</h3>
            </div>
            <div class="container">
                <form method="post" action="{{route('store.cart.order',[ 'slug' => $store->slug ])}}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="">
                        <div class="input-simple-2 input-green bottom-30">
                            <strong class="color-primary">Nom*</strong>
                            <input type="hidden" name="store_id"  value="{{ $store->id}}">
                            <input type="text" name="name" placeholder="Nom et Prénoms" required="required" aria-required="true" value="{{ Auth::user()->name }}">
                            @if ($errors->has('name'))
                                <div id="uname-error" class="error">{{ $errors->first('name') }}></div>
                            @endif
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="">
                        <div class="input-simple-2 input-green bottom-30">
                            <strong class="color-primary">Pays*</strong>
                            <input type="text" name="country" placeholder="Pays de destination" required="required" aria-required="true" value="{{ Auth::user()->pays }}">
                            @if ($errors->has('country'))
                                <div id="uname-error" class="error">{{ $errors->first('country') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="">
                        <div class="input-simple-2 input-green bottom-30">
                            <strong class="color-primary">Ville/Quartier*</strong>
                            <input type="text" name="city" placeholder="Ex: Ouagadougou / karpala " required="required" aria-required="true" value="{{ Auth::user()->ville }}">
                            @if ($errors->has('city'))
                                <div id="uname-error" class="error">{{ $errors->first('city') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="input-simple-2 input-green bottom-30">
                        <strong class="color-primary">Email *</strong>
                        <input type="text" name="email" placeholder="Addresse Email" required="required" aria-required="true" value="{{ Auth::user()->email }}">
                        @if ($errors->has('email'))
                            <div id="uname-error" class="error">{{ $errors->first('email') }}</div>
                        @endif
                    </div>
                    <div class="input-simple-2 input-green bottom-15">
                        <strong class="color-primary">Portable *</strong>
                        <input type="text" name="phone" placeholder="Numéro de Téléphone" required="required" aria-required="true" value="{{ Auth::user()->wa_number }}">
                        @if ($errors->has('phone'))
                            <div id="uname-error" class="error">{{ $errors->first('phone') }}</div>
                        @endif
                    </div>
                    <div class="input-simple-2 input-green bottom-30">
                        <strong class="color-primary">Addresse</strong>
                        <textarea name="address" placeholder="Veuillez decrire votre addresse" required="required" aria-required="true">{{ Auth::user()->addresse}}</textarea>
                    </div>
                    <div class="content-title bottom-30 top-10">
                        <h5>Sélectionner votre méthode de Paiement</h5>
                    </div>
                    <div class="checkboxes-demo bottom-30">
                        <div class="fac fac-radio-round fac-blue"><span></span>
                            <input id="box2-fac-radio-full" type="radio" checked="checked" name="payment"  value="Orange Money" required>
                            <label for="box2-fac-radio-full">Orange Money</label>
                        </div>
                        <div class="fac fac-radio-round fac-blue"><span></span>
                            <input id="box2-fac-radio-full1" type="radio" name="payment" value="Ecobank Pay">
                            <label for="box2-fac-radio-full1">Ecobank Pay</label>
                        </div>
                        <div class="fac fac-radio-round fac-blue"><span></span>
                            <input id="box2-fac-radio-full2" type="radio" name="payment" value="Cash Chez Kaanu (CCK)" >
                            <label for="box2-fac-radio-full2">Cash Chez Kaanu (CCK)</label>
                        </div>
                        <div class="fac fac-radio-round fac-blue"><span></span>
                            <input id="box2-fac-radio-full3" type="radio" name="payment" value="Dépôt Bancaire" >
                            <label for="box2-fac-radio-full3">Dépôt Bancaire</label>
                        </div>
                        <div class="fac fac-radio-round fac-blue"><span></span>
                            <input id="box2-fac-radio-full4" type="radio" name="payment" value="Bitcoin">
                            <label for="box2-fac-radio-full4">Bitcoin</label>
                        </div>
                    </div>
                <button type="submit" class="button bg-primary button-full button-rounded button-sm uppercase ultrabold shadow-small">Commander Maintenant</button>
            </form>
        </div>
        </div>
    </div>
@endsection

@section('post_header')
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" 		   content="Yanfoma Soko" />
    <meta property="og:site_name" 	   content="Yanfoma">
    <meta property="fb:app_id" 		   content="400025927061215">
    <meta property="og:title"          content="Yanfoma Soko" />
    <meta property="og:description"    content="Yanfoma Soko" />
    <meta property="og:image"          content="https://res.cloudinary.com/yanfomaweb/image/upload/v1553782826/Yanfoma/metaImage2.png')}}" />
    <meta property="og:type" 	       content="article">
    <meta property="og:url"            content="https://yanfoma.com">
@endsection