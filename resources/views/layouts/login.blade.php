<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    <!-- Bootstrap -->
    <link href="{{asset('css/bootstrapLogin.min.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
    <!-- Owl-Carousel -->
    <link href="{{asset('css/owlLogin.theme.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="{{asset('css/owlLogin.carousel.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
    <style>
        .widget4 {
            background: #fff none repeat scroll 0 0;
            box-shadow: 0 0 20px rgba(0,0,0,.1);
            border-radius: 12px;
            border: 1px solid #dddddd;
            margin-bottom: 50px!important;
            padding: 30px;
        }
        .widget5 {
            background: #fff none repeat scroll 0 0;
            box-shadow: 0 0 20px rgba(0,0,0,.2);
            border-radius: 12px;
            border: 1px solid #dddddd;
            padding: 20px!important;
            padding-bottom: 5px!important;
            width: 100%;
        }

        .widget{
            background: #fff none repeat scroll 0 0;
            box-shadow: 0 0 20px rgba(0,0,0,.1);
            border-radius: 12px;
            display: inline-block;
            border: 1px solid #dddddd;
            margin-bottom: 50px!important;
            padding: 30px;
            position: relative;
            width: 100%;
        }
        .primary-text{
            color: #ffaf20!important;
            font-weight: bold!important;
        }
        .btn-outline-primary{
            background: #ffaf20!important;
            border: 2px solid white!important;
            color: #fff!important;
            font-weight: bold;
        }
        .login-main-wrapper {
            background: #ffaf20;
            /* Old browsers */
            background: -moz-linear-gradient(45deg, #ffaf20 50%, #ff8106 100%);
            /* FF3.6-15 */
            background: -webkit-linear-gradient(45deg, #ffaf20 50%, #ff8106 100%);
            /* Chrome10-25,Safari5.1-6 */
            background: linear-gradient(45deg, #ffaf20 50%, #ff8106 100%);
            /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#6908ac', endColorstr='#1755de',GradientType=1 );
            /* IE6-9 fallback on horizontal gradient */
            height: calc(100vh - 0rem);
        }

        .login-main-right .owl-carousel .owl-dots .owl-dot {
            background: #ffaf20;
            border-radius: 50px;
            height: 16px;
            margin: 0 2px;
            opacity: unset;
            width: 16px;
        }
        .login-main-right.bg-white {
            margin: auto;
            max-width: 561px;
            margin-top: 100px!important;
            border-radius: 8px!important;
        }
        .mb-5, .my-5 {
            margin-bottom: 3rem!important;
        }
        .mt-5, .my-5 {
            margin-top: 3rem!important;
        }

        .full-height {
            height: calc(100vh - 0rem);
        }
        .p-5 {
            padding: 3rem!important;
        }
        .bg-white {
            background-color: #fff!important;
        }
        .login-main-left-header img{
            width: 200px;!important;
            height:auto;
            margin-top: -60px;
            /*border: 2px solid red;*/
        }

        .login-main-right .owl-carousel .owl-dots .owl-dot.active, .login-main-right .owl-carousel .owl-dots .owl-dot:hover {
            background: #1755DE;
        }

        .login-main-left {
            margin: auto;
            max-width: 335px;
        }
        .login-main-right.bg-white {
            border-radius: 2px;
            margin: auto;
            max-width: 561px;
        }
        .carousel-login-card h5 {
            font-size: 18px;
        }
        .login-main-wrapper .form-control {
            background: #f9f8f6 none repeat scroll 0 0;
        }

        .login-main-wrapper label{
            color: #000;
            font-size: 14px;
        }
        .login-main-right .owl-theme .owl-controls .owl-page span {
            background: red;
            border-radius: 50px;
            height: 16px;
            margin: 0 2px;
            opacity: unset;
            width: 16px;
        }
        .btn-lg {
            font-size: 15px;
            padding: 12px 16px;
        }
        .form-inliner{
            /*max-height: 500px;*/
            border-radius: 10px;
            /*background-image: linear-gradient(to bottom, #f2f2f2, #f2f1ef);*/
            /*padding: 20px;*/
            /*padding-top: 50px;*/
            margin-top: -60px;!important;
            /*margin-bottom:40px;!important;*/
        }
        </style>
</head>

<body>
    @yield('style')
    @yield('content')
    @include('layouts.scripts')
    @yield('scripts')
</body>
</html>
