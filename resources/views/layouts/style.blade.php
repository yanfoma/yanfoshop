<!-- Styles -->
<link href="{{ asset('css/materialize.css') }}"       type="text/css" rel="stylesheet" media="screen,projection">
<link href="{{ asset('css/style.css') }}"             type="text/css" rel="stylesheet" media="screen,projection">
<!-- Custome CSS-->
<link href="{{ asset('css/custom.css') }}"            type="text/css" rel="stylesheet" media="screen,projection">
<link href="{{ asset('css/perfect-scrollbar.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
<link href="{{ asset('css/dropify.min.css') }}"       type="text/css" rel="stylesheet" media="screen,projection">
<link href="{{ asset('css/toastr.min.css') }}"        type="text/css" rel="stylesheet" media="screen,projection">

<link href="{{asset('css/materialnote.css')}}"        type="text/css" rel="stylesheet" >

<!-- Include Editor style. -->
<link href='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.7.0/css/froala_editor.min.css' rel='stylesheet' type='text/css' />
<link href='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.7.0/css/froala_style.min.css'  rel='stylesheet' type='text/css' />

<link href="{{ asset('css/animate.css') }}" type="text/css" rel="stylesheet" media="screen,projection">

<!--Sweet Alert-->
<link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css'>
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.0/sweetalert.css'>

<link href="{{ asset('css/frontEnd/bootstrap.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
<link href="{{ asset('css/frontEnd/bootstrap.min.css') }}" type="text/css" rel="stylesheet" media="screen,projection">