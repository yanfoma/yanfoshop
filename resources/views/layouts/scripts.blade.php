<script type="text/javascript" src="{{ asset('js/jquery.js') }}"></script>
<script type="text/javascript" src="{{asset('js/toastr.min.js')}}"></script>
<script type="text/javascript">
    @if(Session::has('success'))
        toastr.success("{{ Session::get('success') }}");
    @endif
    @if(Session::has('info'))
        toastr.info("{{ Session::get('info') }}");
    @endif
    @if (Session::has('sweet_alert.alert'))
        swal({
        text: "{!! Session::get('sweet_alert.text') !!}",
        title: "{!! Session::get('sweet_alert.title') !!}",
        timer: "{!! Session::get('sweet_alert.timer') !!}",
        type: "{!! Session::get('sweet_alert.type') !!}",
        showConfirmButton: "{!! Session::get('sweet_alert.showConfirmButton') !!}",
        confirmButtonText: "{!! Session::get('sweet_alert.confirmButtonText') !!}",
        confirmButtonColor: "#AEDEF4"});
    @endif
</script>

