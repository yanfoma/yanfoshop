<div class="banner-area">
	<div class="container2">
		<div class="imgbanner imgbanner-2 mt-30">
			<a href="{{route('yanfomaSoko.vendre')}}">
				<img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1563293161/Yanfoma/vendeur.png" alt="banner">
			</a>
		</div>
	</div>
</div>


<div class="ho-section newarrival-bestseller-featured-product mtb-30 bg-grey pt-4 pb-4 pl-4 pr-4">
	<div class="#">
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<div class="section-title-2">
					<ul class="nav" id="bstab3" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" id="bstab3-area1-tab" data-toggle="tab" href="#bstab3-area1" role="tab"
							   aria-controls="bstab3-area1" aria-selected="true">NOS BONS DEALS (A moins de 2000 FCFA)</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="bstab3-area2-tab" data-toggle="tab" href="#bstab3-area2" role="tab" aria-controls="bstab3-area2"
							   aria-selected="false">En Promo</a>
						</li>
					</ul>
				</div>
				<div class="tab-content" id="bstab3-ontent">
					<div class="tab-pane fade show active" id="bstab3-area1" role="tabpanel" aria-labelledby="bstab3-area1-tab">
						<div class="product-slider new-best-featured-slider2 slider-navigation-2">
							@foreach($bonsDeals as $product)
								<div class="product-slider-col">
									<article class="hoproduct product">
										<div class="hoproduct-image">
											<a class="hoproduct-thumb" href="{{route('shop.single',['store_slug' =>$product->store->slug,'slug' => $product->slug])}}">
												<img class="hoproduct-frontimage" src="{{$product->image_url}}" alt="{{$product->name}}">
											</a>
											<ul class="hoproduct-actionbox">
												<li><a href="{{route('store.cart.addDirect',['store_slug' => $product->store->slug,'id' => $product->id])}}"><i class="lnr lnr-cart"></i></a></li>
												<li><a href="{{route('shop.single',['store_slug' => $product->store->slug, 'id' => $product->slug])}}"><i class="lnr lnr-eye"></i></a></li>
											</ul>
											<ul class="hoproduct-flags">
												<li class="flag-pack">{{$product->store->name}}</li>
												@if($product->onSale && $product->saleType == 1)
													<li class="flag-discount">- {{ $product->price - $product->sale_amount }} FCFA</li>
												@endif
											</ul>
										</div>
										<div class="hoproduct-content">
											<h5 class="hoproduct-title product-title"><a href="{{route('shop.single',['store_slug' =>$product->store->slug,'slug' => $product->slug])}}">{{$product->name}}</a></h5>
											<div class="hoproduct-pricebox product-pricebox bg-blue-orange4">
												<div class="pricebox">
													<div class="float_left">
														@if($product->onSale)
															<del class="oldprice">{{number_format($product->price, 0 , ',' , ' ')}} </del>
															<span class="price">{{number_format($product->sale_amount, 0 , ',' , ' ')}} CFA </span>
														@else
															<span class="price">{{number_format($product->price, 0 , ',' , ' ')}} CFA </span>
														@endif
													</div>
													<div class="float_right">
														<i class="fa fa-exclamation-triangle" style="color: yellow;"></i>
														Min: {{$product->minQty}}
													</div>
												</div>
											</div>
										</div>
									</article>
								</div>
							@endforeach
						</div>
					</div>
					<div class="tab-pane fade" id="bstab3-area2" role="tabpanel" aria-labelledby="bstab3-area2-tab">
						<div class="product-slider new-best-featured-slider2 slider-navigation-2">
							@foreach($promos as $product)
								<div class="product-slider-col">
									<article class="hoproduct product">
										<div class="hoproduct-image">
											<a class="hoproduct-thumb" href="{{route('shop.single',['store_slug' =>$product->store->slug,'slug' => $product->slug])}}">
												<img class="hoproduct-frontimage" src="{{$product->image_url}}" alt="{{$product->name}}">
											</a>
											<ul class="hoproduct-actionbox">
												<li><a href="{{route('store.cart.addDirect',['store_slug' => $product->store->slug,'id' => $product->id])}}"><i class="lnr lnr-cart"></i></a></li>
												<li><a href="{{route('shop.single',['store_slug' => $product->store->slug, 'id' => $product->slug])}}"><i class="lnr lnr-eye"></i></a></li>
											</ul>
											<ul class="hoproduct-flags">
												<li class="flag-pack">{{$product->store->name}}</li>
												@if($product->onSale && $product->saleType == 1)
													<li class="flag-discount">- {{ $product->price - $product->sale_amount }} FCFA</li>
												@endif
											</ul>
										</div>
										<div class="hoproduct-content">
											<h5 class="hoproduct-title product-title"><a href="{{route('shop.single',['store_slug' =>$product->store->slug,'slug' => $product->slug])}}">{{$product->name}}</a></h5>
											<div class="hoproduct-pricebox product-pricebox bg-blue-orange4">
												<div class="pricebox">
													<div class="float_left">
														@if($product->onSale)
															<span class="price">{{number_format($product->sale_amount, 0 , ',' , ' ')}} CFA </span>
														@else
															<span class="price">{{number_format($product->price, 0 , ',' , ' ')}} CFA </span>
														@endif
													</div>
													<div class="float_right">
														<i class="fa fa-exclamation-triangle" style="color: yellow;"></i>
														Min: {{$product->minQty}}
													</div>
												</div>
											</div>
										</div>
									</article>
								</div>
							@endforeach
						</div>
					</div>
				</div>
				<div class="imgbanner imgbanner-2 mt-30">
					<a href="{{route('shop.single',['store_slug' => 'yanfoshop','slug' => "kit-voiture-intelligente-basee-sur-arduino"])}}">
						<img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1561768600/Yanfoma/promo.png" alt="banner">
					</a>
				</div>
			</div>
{{--			<div class="col-lg-3 col-md-4">--}}
{{--				<div class="imgbanner imgbanner-2 mt-30">--}}
{{--					<a href="{{route('yanfomaSoko.vendre')}}">--}}
{{--						<img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1561765707/Yanfoma/vendre.png" alt="image banner">--}}
{{--					</a>--}}
{{--				</div>--}}
{{--			</div>--}}
		</div>
	</div>
</div>
