<style>
    .header-middle {
         padding:0!important;
    }
    .bg-theme{background:#18b7bf!important}
    .bg-blue{background:#03a9f4!important}
    .bg-violet{background:#19143b!important}
    .header_top {
        padding: 0px 0;
        padding-top: 20px;
        border-bottom: 1px solid rgba(255,255,255,0.2);
        width: 100%;
    }
    .contact_link span {
        color: #ffffff;
        line-height: 22px;
    }
    .top_left_sidebar {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
    }
    .switcher > ul > li,.header_right_info > ul > li {
        display: inline-block;
        padding-right: 15px;
        margin-right: 15px;
        position: relative;
    }
    .header_right_info ul li::before,.switcher ul li::before {
        position: absolute;
        content: "";
        width: 1px;
        height: 13px;
        background: #fff;
        top: 50%;
        -webkit-transform: translatey(-50%);
        transform: translatey(-50%);
        right: 0;
    }
    .header_right_info ul li:last-child::before,.switcher ul li:last-child::before{
        display: none;
    }
    .switcher {
        margin-left: 42px;
    }
    .switcher > ul > li > a,.header_right_info > ul > li > a {
        display: block;
        text-transform: capitalize;
        color: #ffffff;
        line-height: 22px;
    }
    .switcher ul li:last-child,.header_right_info ul li:last-child{
        padding: 0;
        margin: 0;
    }
    .header_right_info ul li a i,.switcher ul li a img {
        margin-right: 5px;
    }
</style>

<!-- Header -->
<header class="header bg-violet">
    <!-- Header Top Area -->
        <div class="header_top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="top_left_sidebar">
                            <div class="contact_link">
                                <span>Accueil : <strong><a href="{{route('welcome')}}"> Yanfoma Soko</a> </strong></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-9">
                        <div class="header_right_info text-right">
                            <ul>
                                <li><a href="{{route('testimonyViewAll',['store_slug' => $store->slug])}}">Ils parlent de Nous</a></li>
                                <li><a href="{{route('testimonyIndex',['store_slug' => $store->slug])}}">Partagez votre avis</a></li>
                                @if($store->id == 1)
                                    <li><a href="{{route('devis',['store_slug' => $store->slug])}}">Demander un Devis</a></li>
                                @endif
                                @if(Auth::check())
                                    <li class="my_account">
                                        <a href="{{route('customer.welcome')}}"><i class="fa fa-user" aria-hidden="true"></i> Mon Profile </a>
                                    </li>
                                    <li class="#"><a href="{{route('logout')}}" class="big"><i class="fa fa-user-times"></i> Deconnexion</a></li>
                                @else
                                    <li class="log_in"><a href="{{route('login')}}"><i class="fa fa-lock" aria-hidden="true"></i> Se Connecter </a></li>
                                    <li class="#"><a href="{{route('register')}}"><i class="fa fa-user-plus"></i>S'enregistrer</a></li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!--// Header Top Area -->

    <!-- Header Middle Area -->
    <div class="header-middle bg-violet">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-3 col-md-6 col-sm-6 order-1 order-lg-1">
                    <a href="{{route('shop',['store_slug' => $store->slug])}}" class="header-logo">
                        <img src="{{asset($store->logo)}}" alt="logo YanfoShop" width="100">
                    </a>
                </div>
                <div class="col-lg-6 col-12 order-3 order-lg-2">
                    <form action="{{route('results',['store_slug' => $store->slug])}}" class="header-searchbox" method="POST">
                        {{csrf_field()}}
                        <select class="select-searchcategory" name="category">
                            @foreach($categories as $category)
                                <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                        </select>
                        <input type="text" placeholder="Cherchez votre produit ici ..." name="query" required>
                        <button type="submit"><i class="lnr lnr-magnifier"></i></button>
                    </form>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 order-2 order-lg-3">
                    <div class="header-icons">
                        <div class="header-account">
                            <button class="header-accountbox-trigger"><span class="lnr lnr-user"></span> Mon Compte <i class="ion ion-ios-arrow-down"></i></button>
                            <ul class="header-accountbox dropdown-list">
                                <li>
                                    <a href="{{route('customer.welcome')}}">Mes Informations</a>
                                </li>
                                <li>
                                    <a href="{{route('store.cart.index',['store_slug' => $store->slug])}}">Mon Panier</a>
                                </li>
                                <li>
                                    <a href="{{ route('customer.commandeView')}}">Mes Commandes</a>
                                </li>
                                <li>
                                    <a href="{{route('customer.wishlist')}}">Mes Favoris</a>
                                </li>
                                @if(Auth::check())
                                    <li>
                                        <a href="{{route('logout')}}">Se Deconnecter</a>
                                    </li>
                                @else
                                    <li>
                                        <a href="{{route('login')}}">Se Connecter</a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                        <div class="header-cart">
                            <div class="header-carticon" href="{{route('store.cart.index',['store_slug' => $store->slug])}}"><i class="lnr lnr-cart"></i>
                                @if($cart->count())
                                    <span class="count">{{$cart->count()}}</span>
                                <!-- Minicart -->
                                    <div class="header-minicart minicart">
                                        <div class="minicart-header">
                                            @foreach($cart as $product)
                                                <div class="minicart-product">
                                                    <div class="minicart-productimage">
                                                        <a href="#">
                                                            <img src="{{asset($product->product->image_url)}}" alt="{{$product->name}}">
                                                        </a>
                                                        <span class="minicart-productquantity">{{$product->product_qty}}</span>
                                                    </div>
                                                    <div class="minicart-productcontent">
                                                        <h6><a href="#">{{$product->product->name}}</a></h6>
                                                        <span class="minicart-productprice">{{$product->product_total}} CFA</span>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                        <ul class="minicart-pricing">
                                            <li>Estimation <span>20~30 jours</span></li>
                                            <li>Total <span>{{getStoreTotal($store->id)}} CFA</span></li>
                                        </ul>
                                        <div class="minicart-footer">
                                            <a href="{{route('store.cart.checkout',['store_slug' => $store->slug])}}" class="ho-button ho-button-dark ho-button-fullwidth">
                                                <span>Passer la commande</span>
                                            </a>
                                        </div>
                                    </div>
                                    <!--// Minicart -->
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-bottom bg-theme">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-9 d-none d-lg-block">
                    <nav class="ho-navigation">
                        <ul>
                            @if($categories->count())
                                <div class="header-catmenu catmenu">
                                    <button class="catmenu-trigger">
                                        <span>Categories</span>
                                    </button>
                                    <nav class="catmenu-body">
                                        <ul>
                                            @foreach($categories as $category)
                                                <li><a href="{{route('viewCategory',['store_slug' => $store->slug, 'name' => $category->name ])}}">{{$category->name}}</a></li>
                                            @endforeach
                                        </ul>
                                    </nav>
                                </div>
                            @endif
                            <li class="{{\Request::is($store->slug) ?'active':''}} ">
                                <a href="{{route('shop',['store_slug' => $store->slug])}}" class="big"><i class="fa fa-home"></i> Boutique</a>
                            </li>
                            <li class="{{\Request::is($store->slug.'/qui-sommes-nous') ?'active':''}} ">
                                <a href="{{route('aboutUs',['store_slug' => $store->slug])}}" class="big"><i class="fa fa-info-circle"></i> A Propos</a>
                            </li>
                            <li class="{{\Request::is($store->slug.'/mon-panier') ?'active':''}} ">
                                <a href="{{route('store.cart.index',['store_slug' => $store->slug])}}" class="big"><i class="lnr lnr-cart"></i> Panier</a>
                            </li>
                            <li class="{{\Request::is($store->slug.'/contact') ?'active':''}} ">
                                <a href="{{route('contact',['store_slug' => $store->slug])}}" class="big">
                                    <i class="fa fa-envelope"></i> Contact
                                </a>
                            </li>
                            @if($store->id == 1)
                               <li class="{{\Request::is($store->slug.'/foire-aux-questions') ?'active':''}} " >
                                   <a href="{{route('faq',['store_slug' => $store->slug])}}" class="big"><i class="fa fa-question-circle"></i> FAQ</a>
                               </li>
                            @endif
                        </ul>
                    </nav>
                </div>
                <div class="col-lg-3">
                    <div class="header-contactinfo">
                        <i class="flaticon-support"></i>
                        <span>Nous Appeler:</span>
                        <b>{{$store->phone}}</b>
                    </div>
                </div>
                <div class="col-12 d-block d-lg-none">
                    <div class="mobile-menu clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</header>

