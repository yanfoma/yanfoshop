<div class="ho-section features-area bg-white ptb-30 bg-blue-orange4">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-12">
                <div class="featurebox">
                    <i class="flaticon-shipped"></i>
                    <h5>Livraison Gratuite</h5>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-12">
                <div class="featurebox">
                    <i class="ion-ios-thumbs-up"></i>
                    <h5>Qualité et Originalité</h5>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-12">
                <div class="featurebox">
                    <i class="fa fa-bolt"></i>
                    <h5>Comande Rapide</h5>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-12">
                <div class="featurebox">
                    <i class="flaticon-support-1"></i>
                    <h5>Support 24/7</h5>
                </div>
            </div>
        </div>
    </div>
</div>
<footer class="footer bg-welcome2">
    <div class="footer-toparea">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-4">
                    <div class="footer-widget widget-info">
                        <h5 class="footer-widget-title">Nous Contacter</h5>
                        <ul>
                            <li><i class="ion ion-ios-pin"></i> Ouagadougou - Burkina Faso</li>
                            <li><i class="ion ion-ios-pin"></i> Abidjan - Cote d'Ivoire</li>
                            <li><i class="ion ion-ios-pin"></i> Yaounde - Cameroun</li>
                            <li><i class="ion ion-ios-pin"></i> Kigali - Rwanda</li>
                            <li><i class="ion ion-ios-call"></i> +(226) 71 33 15 23</li>
                            <li><i class="ion ion-ios-call"></i> +(226) 74 33 42 77</li>
                            <li><i class="ion ion-ios-call"></i> +(886) 989 59 72 35</li>
                            <li><i class="ion ion-ios-mail"></i> Email: <a href="#">info@yanfoma.tech</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4">
                    <div class="footer-widget widget-links">
                        <h5 class="footer-widget-title">Yanfoma Soko</h5>
                        <ul>
                            <li><a href="{{route('yanfomaSoko.about')}}"> A Propos</a></li>
                            <li><a href="{{route('yanfomaSoko.vendre')}}"> Devenir un Vendeur</a></li>
                        </ul>
                    </div>
                    <div class="footer-widget widget-links">
                        <a href="{{route('nosBoutiques')}}" style="font-weight: bold;" class="footer-widget-title">Nos Boutiques</a>
{{--                        <ul>--}}
{{--                            @foreach($stores as $store)--}}
{{--                                <li><a href="{{route('shop',$store->slug)}}">{{$store->name}}</a></li>--}}
{{--                            @endforeach--}}
{{--                        </ul>--}}
                    </div>
                </div>

                <div class="col-lg-4 col-md-4">
                    <div class="footer-widget widget-links">
                        <h5 class="footer-widget-title">Connection</h5>
                        <ul>
                            @if(Auth::check())
                                <li>
                                    <a href="{{route('logout')}}">Se Deconnecter</a>
                                </li>
                            @else
                                <li>
                                    <a href="{{route('login')}}">Se Connecter</a>
                                </li>
                            @endif
                        </ul>
                    </div>
                    <div class="footer-widget widget-links">
                        <h5 class="footer-widget-title">Mon Compte</h5>
                        <ul>
                            <li><a href="{{route('customer.welcome')}}">Mes Informations</a></li>
                            <li><a href="{{ route('customer.commandeView')}}">Mes Commandes</a></li>
                            <li><a href="{{route('customer.wishlist')}}">Mes Favoris</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--// Footer Top Area -->
    <div class="footer-bottomarea bg-blue-orange4">
        <div class="container">
            <div class="footer-copyright2">
                <p class="copyright">© 2016-2019 Tous Droits Réservés<a href="https://yanfoma.tech"> Yanfoma</a></p>
            </div>
        </div>
    </div>
</footer>