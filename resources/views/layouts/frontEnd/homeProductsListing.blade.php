<style type="text/css">
    .ajax-load{
        background: #e1e1e1;
        padding: 10px;
        width: 100%;
    }
</style>


<div class="col-md-12" id="post-data">
	@include('layouts.frontEnd.data')
</div>
<div class="ajax-load text-center" style="display: none">
    <p><img src="http://demo.itsolutionstuff.com/plugin/loader.gif" height="40">Chargement de plus de produits</p>
</div>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script type="text/javascript">
    var page = 1;
    var end  = 0;
    $(window).scroll(function() {
        if($(window).scrollTop() + $(window).height() >= $(document).height() && end==0) {
            page++;
            console.log(page)
            loadMoreData(page);
        }
    });

    function loadMoreData(page){
        $.ajax(
            {
                url: '{{route("welcome")}}?page=' + page,
                type: "get",
                headers: {
                    'X-CSRF-Token': $('#_token').val(),
                },
                beforeSend: function()
                {
                    $('.ajax-load').show();
                }
            })
            .done(function(data)
            {
                //console.log("data.html is " + data.html + "data.html.length is " + data.html.length);
                if(data.html.length == 85){
                    $('.ajax-load').html("Aucun autre produit trouvé");
                    end++;
                    return
                }
                $('.ajax-load').hide();
                $("#post-data").append(data.html);
                console.log(end);
            })
            .fail(function(jqXHR, ajaxOptions, thrownError)
            {
                console.log('server not responding...');
            });
    }
</script>

{{--@if($products->count())--}}
{{--<div class="shop-page-products mt-30">--}}
{{--	<div class="row no-gutters infinite-scroll">--}}
{{--		@foreach($products as $product)--}}
{{--			<div class="col-lg-3 col-md-4 col-sm-6 col-12">--}}
{{--				<article class="hoproduct product2">--}}
{{--					<div class="hoproduct-image">--}}
{{--						<a class="hoproduct-thumb" href="{{route('shop.single',['store_slug' =>$product->store->slug,'slug' => $product->slug])}}">--}}
{{--							<img class="hoproduct-frontimage" src="{{$product->image_url}}" alt="{{$product->name}}">--}}
{{--						</a>--}}
{{--						<ul class="hoproduct-actionbox">--}}
{{--							<li><a href="{{route('store.cart.addDirect',['store_slug' => $product->store->slug,'id' => $product->id])}}"><i class="lnr lnr-cart"></i></a></li>--}}
{{--							<li><a href="{{route('shop.single',['store_slug' => $product->store->slug, 'id' => $product->slug])}}"><i class="lnr lnr-eye"></i></a></li>--}}
{{--							@if($product->is_wished_by_auth_user())--}}
{{--								<li>--}}
{{--									<a class="wished" href="{{route('product.unwishlist', ['store_slug' => $product->store->slug,  'id' => $product->id ])}}">--}}
{{--										<i class="lnr lnr-heart"></i>--}}
{{--									</a>--}}
{{--								</li>--}}
{{--							@else--}}
{{--								<li>--}}
{{--									<a class="" href="{{route('product.wishlist', ['store_slug' => $product->store->slug, 'id' => $product->id ])}}">--}}
{{--										<i class="lnr lnr-heart"></i>--}}
{{--									</a>--}}
{{--								</li>--}}
{{--							@endif--}}
{{--							@if($product->is_liked_by_auth_user())--}}
{{--								<li><a class="unlike" href="{{route('product.unlike', [ 'store_slug' => $product->store->slug, 'id' => $product->id ])}}"><i class="lnr lnr-thumbs-up"></i></a></li>--}}
{{--							@else--}}
{{--								<li><a class="" href="{{route('product.like', [ 'store_slug' => $product->store->slug, 'id' => $product->id ])}}"><i class="lnr lnr-thumbs-up"></i></a></li>--}}
{{--							@endif--}}
{{--						</ul>--}}
{{--						<ul class="hoproduct-flags">--}}
{{--							<li class="flag-pack">{{$product->store->name}}</li>--}}
{{--							@if($product->onSale && $product->saleType == 2)--}}
{{--								<li class="flag-discount">-{{ $product->salePercentage }} %</li>--}}
{{--							@endif--}}
{{--						</ul>--}}
{{--					</div>--}}
{{--					<div class="hoproduct-content">--}}
{{--						<h5 class="hoproduct-title product-title"><a href="{{route('shop.single',['store_slug' =>$product->store->slug,'slug' => $product->slug])}}">{{$product->name}}</a></h5>--}}
{{--						<div class="hoproduct-pricebox product-pricebox">--}}
{{--							<div class="pricebox">--}}
{{--								<div class="float_left">--}}
{{--									@if($product->onSale)--}}
{{--										<del class="oldprice">{{number_format($product->price, 0 , ',' , ' ')}} </del>--}}
{{--										<span class="price">{{number_format($product->sale_amount, 0 , ',' , ' ')}} CFA </span>--}}
{{--									@else--}}
{{--										<span class="price">{{number_format($product->price, 0 , ',' , ' ')}} CFA </span>--}}
{{--									@endif--}}
{{--								</div>--}}
{{--								<div class="float_right">--}}
{{--									<i class="fa fa-exclamation-triangle" style="color: yellow;"></i>--}}
{{--									Min: {{$product->minQty}}--}}
{{--								</div>--}}
{{--							</div>--}}
{{--						</div>--}}
{{--					</div>--}}
{{--				</article>--}}
{{--			</div>--}}
{{--		@endforeach--}}
{{--			<div class="ajax-load text-center" style="display:none">--}}
{{--				<p><img src="http://demo.itsolutionstuff.com/plugin/loader.gif">Loading More products</p>--}}
{{--			</div>--}}
{{--	</div>--}}
{{--</div>--}}
{{--	@else--}}
{{--	<div  style="height: 450px; padding: 200px;">--}}
{{--		<h1> Aucun Produit pour l'instant !!!</h1>--}}
{{--	</div>--}}
{{--@endif--}}