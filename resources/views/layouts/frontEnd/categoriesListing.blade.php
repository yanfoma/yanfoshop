<style>

</style>
@if($categories->count())
	<div class="ho-section categories-area mtb-30">
	<div class="container2">
		<div class="section-title">
			<h3>Nos Catégories</h3>
		</div>
		<div class="categories-slider-2 slider-navigation-2 slider-navigation-2-m0">
			@foreach($categories as $categorie)
			<div class="category-wrapper">
				<div class="category">
					<div class="category-content widgetCat">
						<div class="widget-header">
							<h5 class="category-title">{{$categorie->name}}</h5>
							<span class="category-productcounter">{{$categorie->products->count()}} Produits</span>
						</div>
						<div class="widget-body">
							<a href="{{route('viewCategory',['store_slug' => $store->slug,'name' => $categorie->name ])}}" class=" btn-category">Voir Produits<i class="fa fa-arrow-circle-o-right"></i></a>
						</div>
					</div>
				</div>
			</div>
			@endforeach
		</div>
	</div>
</div>
@endif