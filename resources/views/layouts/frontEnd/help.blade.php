<!-- Categories Area -->
<div class="ho-section categories-area mtb-30">
    <div class="container">
        <div class="section-title">
            <h3>CENTRE D'AIDE</h3>
        </div>
        <div class="categories-slider-2 slider-navigation-2 slider-navigation-2-m0">

            <div class="category-wrapper">
                <!-- Single Category -->
                <div class="category">
                    <a href="{{route('aboutUs',['store_slug' => $store->slug])}}#video" class="category-thumb">
                        <img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1553176978/Yanfoma/video.png" alt="yanfoshop en video">
                    </a>
                    <div class="category-content">
                        <a href="{{route('aboutUs',['store_slug' => $store->slug])}}#video" class="category-productlink">Visiter <i class="ion ion-md-arrow-dropright"></i></a>
                    </div>
                </div>
                <!--// Single Category -->
            </div>

            <div class="category-wrapper">
                <!-- Single Category -->
                <div class="category">
                    <a href="{{route('faq',['store_slug' => $store->slug])}}" class="category-thumb">
                        <img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1553177441/Yanfoma/faqHelp.png" alt="faq">
                    </a>
                    <div class="category-content">
                        <a href="{{route('faq',['store_slug' => $store->slug])}}" class="category-productlink">Visiter <i class="ion ion-md-arrow-dropright"></i></a>
                    </div>
                </div>
                <!--// Single Category -->
            </div>

            <div class="category-wrapper">
                <!-- Single Category -->
                <div class="category">
                    <a href="{{route('payments',['store_slug' => $store->slug])}}" class="category-thumb">
                        <img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1553177977/Yanfoma/paiements.png" alt="paiements">
                    </a>
                    <div class="category-content">
                        <a href="{{route('payments',['store_slug' => $store->slug])}}" class="category-productlink">Visiter<i class="ion ion-md-arrow-dropright"></i></a>
                    </div>
                </div>
                <!--// Single Category -->
            </div>

            <div class="category-wrapper">
                <!-- Single Category -->
                <div class="category">
                    <a href="{{route('testimonyViewAll',['store_slug' => $store->slug])}}" class="category-thumb">
                        <img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1553178795/Yanfoma/temoignages.png" alt="temoignages">
                    </a>
                    <div class="category-content">
                        <a href="{{route('testimonyViewAll',['store_slug' => $store->slug])}}" class="category-productlink">Visiter <i class="ion ion-md-arrow-dropright"></i></a>
                    </div>
                </div>
                <!--// Single Category -->
            </div>

            <div class="category-wrapper">
                <!-- Single Category -->
                <div class="category">
                    <a href="{{route('testimonyIndex',['store_slug' => $store->slug])}}" class="category-thumb">
                        <img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1553179141/Yanfoma/monavis.png" alt="je donne mon avis">
                    </a>
                    <div class="category-content">
                        <a href="{{route('testimonyIndex',['store_slug' => $store->slug])}}" class="category-productlink">Visiter <i class="ion ion-md-arrow-dropright"></i></a>
                    </div>
                </div>
                <!--// Single Category -->
            </div>

            <div class="category-wrapper">
                <!-- Single Category -->
                <div class="category">
                    <a href="{{route('suivre',['store_slug' => $store->slug])}}" class="category-thumb">
                        <img class="category-img" src="https://res.cloudinary.com/yanfomaweb/image/upload/v1553179818/Yanfoma/toutSurMaCommande.png" alt="Tout Savoir Sur Ma Commande">
                    </a>
                    <div class="category-content">
                        <a href="{{route('suivre',['store_slug' => $store->slug])}}" class="category-productlink">Visiter <i class="ion ion-md-arrow-dropright"></i></a>
                    </div>
                </div>
                <!--// Single Category -->
            </div>

        </div>
    </div>
</div>
<!--// Categories Area -->