<div class="banner-area">
	<div class="container3">
		<div class="imgbanner imgbanner-2 mt-30">
			<a href="#">
				<img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1563314523/Yanfoma/achat.png" alt="mode de paiements">
			</a>
		</div>
	</div>
</div>
<div class="countdown_product default_product mt-30">
	<div class="section_title">
		<h2> NOS SENSEURS</h2>
		<div class="icone_img">
			<img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1561555031/Yanfoma/sensorsIcon.png" alt="">
		</div>
	</div>
	<div class="row">
		@foreach($senseurs->products->take(6) as $product)
			<div class="col-lg-2 col-md-2 col-sm-2 col-12">
				<article class="hoproduct product">
						<div class="hoproduct-image">
							<div class="hoproduct-image">
								<a class="hoproduct-thumb" href="{{route('shop.single',['store_slug' =>$product->store->slug,'slug' => $product->slug])}}">
									<img class="hoproduct-frontimage" src="{{$product->image_url}}" alt="{{$product->name}}">
								</a>
							</div>
							<ul class="hoproduct-actionbox">
								<li><a href="{{route('store.cart.addDirect',['store_slug' => $product->store->slug,'id' => $product->id])}}"><i class="lnr lnr-cart"></i></a></li>
								<li><a href="{{route('shop.single',['store_slug' =>$product->store->slug,'slug' => $product->slug])}}"><i class="lnr lnr-eye"></i></a></li>
							</ul>
						</div>
						<div class="hoproduct-content">
							<h5 class="hoproduct-title product-title"><a href="{{route('shop.single',['store_slug' =>$product->store->slug,'slug' => $product->slug])}}">{{$product->name}} </a></h5>
							<div class="hoproduct-pricebox">
								<div class="pricebox">
									@if($product->onSale)
										<del class="oldprice">{{number_format($product->price, 0 , ',' , ' ')}} </del>
										<span class="price">{{number_format($product->sale_amount, 0 , ',' , ' ')}} CFA </span>
									@else
										<span class="price">{{number_format($product->price, 0 , ',' , ' ')}} CFA </span>
									@endif
								</div>
							</div>
							<p class="hoproduct-content-description"></p>
						</div>
					</article>
			</div>
		@endforeach
	</div>
</div>