<style>
	.newarrival-bestseller-featured-product{
		background: #18b7bf;
		border-radius: 15px!important;
		padding: 10px!important;
	}
	.section-title-2 .nav li a.active, .section-title-2 .nav li a:hover {
		color: #ffd15b;
	}
	.section-title-2 .nav li a{
		color: #000;
	}
	.hoproduct{
		background: #fff;
		margin: 10px;
		border-radius: 10px;
	}
</style>


@if($bonsDeals->count())
<div class="ho-section newarrival-bestseller-featured-product mtb-30">
	<div class="container2">
		<div class="section-title-2">
			<ul class="nav" id="bstab3" role="tablist">
				<li class="nav-item">
					<a class="nav-link active" id="bstab3-area1-tab" data-toggle="tab" href="#bstab3-area1" role="tab"
					   aria-controls="bstab3-area1" aria-selected="true">Nouveautés</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="bstab3-area2-tab" data-toggle="tab" href="#bstab3-area2" role="tab" aria-controls="bstab3-area2"
					   aria-selected="false">Bons Deals</a>
				</li>
			</ul>
		</div>
		<div class="tab-content" id="bstab3-ontent">
			<div class="tab-pane fade show active" id="bstab3-area1" role="tabpanel" aria-labelledby="bstab3-area1-tab">
				<div class="product-slider new-best-featured-slider slider-navigation-2">
					@foreach($nouveautes as $product)
						<div class="product-slider-col">
							<article class="hoproduct">
								<div class="hoproduct-image product-imageBondeals">
									<a class="hoproduct-thumb" href="{{route('shop.single',['store_slug' => $store->slug, 'slug' => $product->slug])}}">
										<img class="hoproduct-frontimage" src="{{$product->image_url}}" alt="{{$product->name}}">
									</a>
									<ul class="hoproduct-actionbox">
										<li><a href="{{route('store.cart.addDirect',['store_slug' => $store->slug, 'id' => $product->id])}}"><i class="lnr lnr-cart"></i></a></li>
										<li><a href="{{route('shop.single',['store_slug' => $store->slug, 'slug' => $product->slug])}}"><i class="lnr lnr-eye"></i></a></li>
									</ul>
									<ul class="hoproduct-flags">
										<li class="flag-pack">Nouveau</li>
										@if($product->onSale && $product->saleType == 2)
											<li class="flag-discount">-{{ $product->salePercentage }} %</li>
										@endif
									</ul>
								</div>
								<div class="hoproduct-content">
									<h5 class="hoproduct-title product-titleBondeals"><a href="{{route('shop.single',['store_slug' => $store->slug, 'slug' => $product->slug])}}">{{$product->name}}</a></h5>
									<div class="hoproduct-pricebox product-priceboxBondeals">
										<div class="pricebox">
											@if($product->onSale)
												<del class="oldprice">{{number_format($product->price, 0 , ',' , ' ')}} </del>
												<span class="price">{{number_format($product->sale_amount, 0 , ',' , ' ')}} CFA </span>
											@else
												<span class="price">{{number_format($product->price, 0 , ',' , ' ')}} CFA </span>
											@endif
										</div>
									</div>
								</div>
							</article>
						</div>
					@endforeach
				</div>
			</div>
			<div class="tab-pane fade" id="bstab3-area2" role="tabpanel" aria-labelledby="bstab3-area2-tab">
				<div class="product-slider new-best-featured-slider slider-navigation-2">
					@foreach($bonsDeals as $product)
						<div class="product-slider-col">
							<article class="hoproduct">
								<div class="hoproduct-image product-imageBondeals">
									<a class="hoproduct-thumb" href="{{route('shop.single',['store_slug' => $store->slug, 'slug' => $product->slug])}}">
										<img class="hoproduct-frontimage" src="{{$product->image_url}}" alt="{{$product->name}}">
									</a>
									<ul class="hoproduct-actionbox">
										<li><a href="{{route('store.cart.addDirect',['store_slug' => $store->slug, 'id' => $product->id])}}"><i class="lnr lnr-cart"></i></a></li>
										<li><a href="{{route('shop.single',['store_slug' => $store->slug, 'slug' => $product->slug])}}"><i class="lnr lnr-eye"></i></a></li>
									</ul>
									<ul class="hoproduct-flags">
										<li class="flag-pack">Eco</li>
										@if($product->onSale && $product->saleType == 2)
											<li class="flag-discount">-{{ $product->salePercentage }} %</li>
										@endif
									</ul>
								</div>
								<div class="hoproduct-content">
									<h5 class="hoproduct-title product-titleBondeals"><a href="{{route('shop.single',['store_slug' => $store->slug, 'slug' => $product->slug])}}">{{$product->name}}</a></h5>
									<div class="hoproduct-pricebox product-priceboxBondeals">
										<div class="pricebox">
											@if($product->onSale)
												<del class="oldprice">{{number_format($product->price, 0 , ',' , ' ')}} </del>
												<span class="price">{{number_format($product->sale_amount, 0 , ',' , ' ')}} CFA </span>
											@else
												<span class="price">{{number_format($product->price, 0 , ',' , ' ')}} CFA </span>
											@endif
										</div>
									</div>
								</div>
							</article>
						</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
</div>
@endif