<header class="header">
    <!-- Header Top Area -->
    <div class="header-top bg-welcome10 header-welcomemsg2">
        <div class="row align-items-center">
            <div class="col-lg-4 col-md-4 col-sm-4 col-4">
                <p class="header-welcomemsg ">Heureux de vous voir sur <span>Yanfoma Soko.</span></p>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-8 col-8">
                <div class="text-center">
                    <div class="header_right_info">
                        <ul>
                            @if(Auth::check())
                                <li>
                                    <a href="{{route('logout')}}"><i class="fa fa-sign-out"></i> Se Deconnecter</a>
                                </li>
                            @else
                                <li class="log_in"><a href="{{route('login')}}"><i class="fa fa-lock" aria-hidden="true"></i> Se Connecter </a></li>
                                <li class="#"><a href="{{route('register')}}"><i class="fa fa-user-plus"></i>S'enregistrer</a></li>
                            @endif

                            <li><a href="{{route('devis',['store_slug' => 'yanfoshop'])}}"><i class="fa fa-question-circle"></i>Demander un Devis</a></li>
                            <li class="#"><a href="#"><i class="fa fa-usd"></i>Devise: Francs CFA</a></li>
                            {{--<li class="#"><a href="#"><i class="fa fa-usd"></i>Paramètres</a></li>--}}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--// Header Top Area -->
    <div id="myHeader"><!-- Header Middle Area -->
    <div class="header-middle bg-welcome2">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-3 col-md-6 col-sm-6 order-1 order-lg-1">
                    <a href="{{route('welcome')}}" class="header-logoHome">
                        <img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1553270364/Yanfoma/VertDback.png" alt="logo" style="">
                    </a>
                </div>
                <div class="col-lg-6 col-12 order-3 order-lg-2">
                    <form action="{{route('yanfomaSoko.search')}}" class="header-searchbox" method="POST">
                        {{csrf_field()}}
                        <select class="select-searchcategory" name="store">
                            @foreach($stores as $store)
                                <option value="{{$store->id}}">{{$store->name}}</option>
                            @endforeach
                        </select>
                        <input type="text" placeholder="Cherchez votre produit ici ..." name="query" required>
                        <button type="submit"><i class="lnr lnr-magnifier"></i></button>
                    </form>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 order-2 order-lg-3">
                    <div class="header-icons">
                        <div class="header-account">
                            <button class="header-accountbox-trigger"><span class="lnr lnr-user"></span> Mon Compte <i class="ion ion-ios-arrow-down"></i></button>
                            <ul class="header-accountbox dropdown-list">
                                <li>
                                    <a href="{{route('customer.welcome')}}">Mes Informations</a>
                                </li>
                                <li>
                                    <a href="{{route('yanfomasoko.cart.index')}}">Mon Panier</a>
                                </li>
                                <li>
                                    <a href="{{ route('customer.commandeView')}}">Mes Commandes</a>
                                </li>
                                <li>
                                    <a href="{{route('customer.wishlist')}}">Mes Favoris</a>
                                </li>
                                @if(Auth::check())
                                    <li>
                                        <a href="{{route('logout')}}">Se Deconnecter</a>
                                    </li>
                                @else
                                    <li>
                                        <a href="{{route('login')}}">Se Connecter</a>
                                    </li>
                                @endif
                            </ul>
                        </div>

                        <div class="header-cart">
                            <div class="header-carticon" href="{{route('yanfomasoko.cart.index')}}"><i class="lnr lnr-cart"></i>
                                @if(Auth::check())
                                    @if($cart->count())
                                        <span class="count">{{$cart->count()}}</span>
                                        <!-- Minicart -->
                                        <div class="header-minicart minicart">
                                            <div class="minicart-header">
                                                @foreach($cart as $product)
                                                    <div class="minicart-product">
                                                        <div class="minicart-productimage">
                                                            <a href="#">
                                                                <img src="{{asset($product->product->image_url)}}" alt="{{$product->name}}">
                                                            </a>
                                                            <span class="minicart-productquantity">{{$product->product_qty}}</span>
                                                        </div>
                                                        <div class="minicart-productcontent">
                                                            <h6><a href="#">{{$product->product->name}}</a></h6>
                                                            <span class="minicart-productprice">{{$product->product_total}} CFA</span>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                            <ul class="minicart-pricing">
                                                <li>Estimation <span>20-30 jours</span></li>
                                                <li>Total <span>{{getTotal()}} CFA</span></li>
                                            </ul>
                                            <div class="minicart-footer">
                                                <a href="{{route('yanfomasoko.cart.checkout')}}" class="ho-button ho-button-fullwidth">
                                                    <span>Passer la commande</span>
                                                </a>
                                            </div>
                                        </div>
                                    @endif
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!--// Header Middle Area -->
    <!-- Header Bottom Area -->
    <div class="header-bottom bg-welcome2"  id="menu">
        <div class="#">
            <div class="row align-items-center">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <nav class="ho-navigation ho-navigation-1 nav2">
                        <ul>
                            <li class="{{\Request::is('/') ?'active':''}}">
                                <a href="{{route('welcome')}}"><i class="fa fa-home"></i> Accueil</a>
                            </li>
                            <li class="{{\Request::is('nos-boutiques') ?'active':''}}">
                                <a href="{{route('nosBoutiques')}}"><i class="fa fa-shopping-bag"></i> Nos Boutiques</a>
                            </li>
                            <li class="{{\Request::is('panier') ?'active':''}} {{\Request::is('transaction-reussie') ?'active':''}} {{\Request::is('passer-votre-commande') ?'active':''}}">
                                <a href="{{route('yanfomasoko.cart.index')}}">Mon Panier</a>
                            </li>
                            <li class="{{\Request::is('a-propos-de-nous') ?'active':''}}">
                                <a href="{{route('yanfomaSoko.about')}}"><i class="fa fa-info-circle"></i> A Propos</a>
                            </li>
{{--                            <li class="{{\Request::is('devenir-vendeur') ?'active':''}}">--}}
{{--                                <a href="{{route('yanfomaSoko.vendre')}}"><i class="fa fa-usd"></i> Devenir un Vendeur</a>--}}
{{--                            </li>--}}
                            <li class="{{\Request::is('nous-contacter') ?'active':''}}">
                                <a href="{{route('yanfomaSoko.contactUs')}}"><i class="fa fa-envelope"></i> Contact</a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-12 d-block d-lg-none">
                    <div class="mobile-menu clearfix"></div>
                </div>
            </div>
        </div>
    </div>

</header>