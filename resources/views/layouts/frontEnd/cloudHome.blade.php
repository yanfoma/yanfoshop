<div class="ho-section trending-our-product-area bg-grey">
	<div class="container2">
		<div class="row">
			<div class="col-lg-3 col-md-4">
				<div class="imgbanner imgbanner-2 mt-30">
					<a href="{{route('shop','yanfoshop')}}">
						<img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1563286759/Yanfoma/servers.png" alt="image banner">
					</a>
				</div>
			</div>
			<div class="col-lg-9 col-md-8">
				<div class="countdown_product default_product mt-30">
					<div class="section_title">
						<h2> NOS SERVEURS</h2>
						<div class="icone_img">
							<img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1561554188/Yanfoma/serversIcon.png" alt="">
						</div>
					</div>
					<div class="tab-content" id="bstab3-ontent">
						<div class="tab-pane fade show active" id="bstab3-area1" role="tabpanel" aria-labelledby="bstab3-area1-tab">
							<div class="product-slider trending-products-slider slider-navigation-2">
									<div class="product-slider-col">
										@foreach($cloud->products as $index => $product)
											@if($index == 0)
												<article class="hoproduct hoproduct-3">
													<div class="hoproduct-image">
														<a class="hoproduct-thumb" href="{{route('shop.single',['store_slug' => $product->store->slug, 'slug' => $product->slug])}}">
															<img class="hoproduct-frontimage" src="{{$product->image_url}}" alt="{{$product->name}}">
														</a>
														<ul class="hoproduct-actionbox">
															<li><a href="{{route('store.cart.addDirect',['store_slug' => $product->store->slug,'id' => $product->id])}}"><i class="lnr lnr-cart"></i></a></li>
															<li><a href="{{route('shop.single',['store_slug' => $product->store->slug, 'slug' => $product->slug])}}" ><i class="lnr lnr-eye"></i></a></li>
															@if($product->is_wished_by_auth_user())
																<li>
																	<a class="wished" href="{{route('product.unwishlist', ['store_slug' => $product->store->slug,  'id' => $product->id ])}}">
																		<i class="lnr lnr-heart"></i>
																	</a>
																</li>
															@else
																<li>
																	<a class="" href="{{route('product.wishlist', ['store_slug' => $product->store->slug, 'id' => $product->id ])}}">
																		<i class="lnr lnr-heart"></i>
																	</a>
																</li>
															@endif
															@if($product->is_liked_by_auth_user())
																<li><a class="unlike" href="{{route('product.unlike', [ 'store_slug' => $product->store->slug, 'id' => $product->id ])}}"><i class="lnr lnr-thumbs-up"></i></a></li>
															@else
																<li><a class="" href="{{route('product.like', [ 'store_slug' => $product->store->slug, 'id' => $product->id ])}}"><i class="lnr lnr-thumbs-up"></i></a></li>
															@endif
														</ul>
													</div>
													<div class="hoproduct-content">
														<h4 class="hoproduct-title"><a href="{{route('shop.single',['store_slug' => $product->store->slug, 'slug' => $product->slug])}}">{{$product->name}}</a></h4>
														<h6 class="hoproduct-title">{{str_limit(strip_tags($product->description),50)}}</h6>
														<div class="hoproduct-pricebox">
															<div class="pricebox">
																@if($product->onSale)
																	<del class="oldprice">{{number_format($product->price, 0 , ',' , ' ')}} </del>
																	<span class="price">{{number_format($product->sale_amount, 0 , ',' , ' ')}} CFA </span>
																@else
																	<span class="price">{{number_format($product->price, 0 , ',' , ' ')}} CFA </span>
																@endif
															</div>
														</div>
													</div>
												</article>
											@endif
											@if($index == 1)
												<article class="hoproduct hoproduct-3">
														<div class="hoproduct-image">
															<a class="hoproduct-thumb" href="{{route('shop.single',['store_slug' => $product->store->slug, 'slug' => $product->slug])}}">
																<img class="hoproduct-frontimage" src="{{$product->image_url}}" alt="{{$product->name}}">
															</a>
															<ul class="hoproduct-actionbox">
																<li><a href="{{route('store.cart.addDirect',['store_slug' => $product->store->slug,'id' => $product->id])}}"><i class="lnr lnr-cart"></i></a></li>
																<li><a href="{{route('shop.single',['store_slug' => $product->store->slug, 'slug' => $product->slug])}}" ><i class="lnr lnr-eye"></i></a></li>
																@if($product->is_wished_by_auth_user())
																	<li>
																		<a class="wished" href="{{route('product.unwishlist', ['store_slug' => $product->store->slug,  'id' => $product->id ])}}">
																			<i class="lnr lnr-heart"></i>
																		</a>
																	</li>
																@else
																	<li>
																		<a class="" href="{{route('product.wishlist', ['store_slug' => $product->store->slug, 'id' => $product->id ])}}">
																			<i class="lnr lnr-heart"></i>
																		</a>
																	</li>
																@endif
																@if($product->is_liked_by_auth_user())
																	<li><a class="unlike" href="{{route('product.unlike', [ 'store_slug' => $product->store->slug, 'id' => $product->id ])}}"><i class="lnr lnr-thumbs-up"></i></a></li>
																@else
																	<li><a class="" href="{{route('product.like', [ 'store_slug' => $product->store->slug, 'id' => $product->id ])}}"><i class="lnr lnr-thumbs-up"></i></a></li>
																@endif
															</ul>
														</div>
														<div class="hoproduct-content">
															<h4 class="hoproduct-title"><a href="{{route('shop.single',['store_slug' => $product->store->slug, 'slug' => $product->slug])}}">{{$product->name}}</a></h4>
															<h6 class="hoproduct-title">{{str_limit(strip_tags($product->description),50)}}</h6>
															<div class="hoproduct-pricebox">
																<div class="pricebox">
																	@if($product->onSale)
																		<del class="oldprice">{{number_format($product->price, 0 , ',' , ' ')}} </del>
																		<span class="price">{{number_format($product->sale_amount, 0 , ',' , ' ')}} CFA </span>
																	@else
																		<span class="price">{{number_format($product->price, 0 , ',' , ' ')}} CFA </span>
																	@endif
																</div>
															</div>
														</div>
													</article>
											@endif
										@endforeach
									</div>
									<div class="product-slider-col">
										@foreach($cloud->products as $index => $product)
											@if($index == 3)
												<article class="hoproduct hoproduct-3">
													<div class="hoproduct-image">
														<a class="hoproduct-thumb" href="{{route('shop.single',['store_slug' => $product->store->slug, 'slug' => $product->slug])}}">
															<img class="hoproduct-frontimage" src="{{$product->image_url}}" alt="{{$product->name}}">
														</a>
														<ul class="hoproduct-actionbox">
															<li><a href="{{route('store.cart.addDirect',['store_slug' => $product->store->slug,'id' => $product->id])}}"><i class="lnr lnr-cart"></i></a></li>
															<li><a href="{{route('shop.single',['store_slug' => $product->store->slug, 'slug' => $product->slug])}}" ><i class="lnr lnr-eye"></i></a></li>
															@if($product->is_wished_by_auth_user())
																<li>
																	<a class="wished" href="{{route('product.unwishlist', ['store_slug' => $product->store->slug,  'id' => $product->id ])}}">
																		<i class="lnr lnr-heart"></i>
																	</a>
																</li>
															@else
																<li>
																	<a class="" href="{{route('product.wishlist', ['store_slug' => $product->store->slug, 'id' => $product->id ])}}">
																		<i class="lnr lnr-heart"></i>
																	</a>
																</li>
															@endif
															@if($product->is_liked_by_auth_user())
																<li><a class="unlike" href="{{route('product.unlike', [ 'store_slug' => $product->store->slug, 'id' => $product->id ])}}"><i class="lnr lnr-thumbs-up"></i></a></li>
															@else
																<li><a class="" href="{{route('product.like', [ 'store_slug' => $product->store->slug, 'id' => $product->id ])}}"><i class="lnr lnr-thumbs-up"></i></a></li>
															@endif
														</ul>
													</div>
													<div class="hoproduct-content">
														<h4 class="hoproduct-title"><a href="{{route('shop.single',['store_slug' => $product->store->slug, 'slug' => $product->slug])}}">{{$product->name}}</a></h4>
														<h6 class="hoproduct-title">{{str_limit(strip_tags($product->description),50)}}</h6>
														<div class="hoproduct-pricebox">
															<div class="pricebox">
																@if($product->onSale)
																	<del class="oldprice">{{number_format($product->price, 0 , ',' , ' ')}} </del>
																	<span class="price">{{number_format($product->sale_amount, 0 , ',' , ' ')}} CFA </span>
																@else
																	<span class="price">{{number_format($product->price, 0 , ',' , ' ')}} CFA </span>
																@endif
															</div>
														</div>
													</div>
												</article>
											@endif
											@if($index == 2)
												<article class="hoproduct hoproduct-3">
														<div class="hoproduct-image">
															<a class="hoproduct-thumb" href="{{route('shop.single',['store_slug' => $product->store->slug, 'slug' => $product->slug])}}">
																<img class="hoproduct-frontimage" src="{{$product->image_url}}" alt="{{$product->name}}">
															</a>
															<ul class="hoproduct-actionbox">
																<li><a href="{{route('store.cart.addDirect',['store_slug' => $product->store->slug,'id' => $product->id])}}"><i class="lnr lnr-cart"></i></a></li>
																<li><a href="{{route('shop.single',['store_slug' => $product->store->slug, 'slug' => $product->slug])}}" ><i class="lnr lnr-eye"></i></a></li>
																@if($product->is_wished_by_auth_user())
																	<li>
																		<a class="wished" href="{{route('product.unwishlist', ['store_slug' => $product->store->slug,  'id' => $product->id ])}}">
																			<i class="lnr lnr-heart"></i>
																		</a>
																	</li>
																@else
																	<li>
																		<a class="" href="{{route('product.wishlist', ['store_slug' => $product->store->slug, 'id' => $product->id ])}}">
																			<i class="lnr lnr-heart"></i>
																		</a>
																	</li>
																@endif
																@if($product->is_liked_by_auth_user())
																	<li><a class="unlike" href="{{route('product.unlike', [ 'store_slug' => $product->store->slug, 'id' => $product->id ])}}"><i class="lnr lnr-thumbs-up"></i></a></li>
																@else
																	<li><a class="" href="{{route('product.like', [ 'store_slug' => $product->store->slug, 'id' => $product->id ])}}"><i class="lnr lnr-thumbs-up"></i></a></li>
																@endif
															</ul>
														</div>
														<div class="hoproduct-content">
															<h4 class="hoproduct-title"><a href="{{route('shop.single',['store_slug' => $product->store->slug, 'slug' => $product->slug])}}">{{$product->name}}</a></h4>
															<h6 class="hoproduct-title">{{str_limit(strip_tags($product->description),50)}}</h6>
															<div class="hoproduct-pricebox">
																<div class="pricebox">
																	@if($product->onSale)
																		<del class="oldprice">{{number_format($product->price, 0 , ',' , ' ')}} </del>
																		<span class="price">{{number_format($product->sale_amount, 0 , ',' , ' ')}} CFA </span>
																	@else
																		<span class="price">{{number_format($product->price, 0 , ',' , ' ')}} CFA </span>
																	@endif
																</div>
															</div>
														</div>
													</article>
											@endif
										@endforeach
									</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>