<div class="shop-filters-sortby">
	<div class="select-sortby">
		<button class="select-sortby-current">Filtrer Par:</button>
		<ul class="select-sortby-list dropdown-list">
			<li><a href="{{ route('shop',['query' => "newest"])}}">Dernières nouveautés</a></li>
			<li><a href="{{ route('shop',['query' => "nameAsc"])}}">Nom: A-Z</a></li>
			<li><a href="{{ route('shop',['query' => "nameDesc"])}}">Nom: Z-A</a></li>
			<li><a href="{{ route('shop',['query' => "priceAsc"])}}">Prix : Ordre croissant</a></li>
			<li><a href="{{ route('shop',['query' => "priceDesc"])}}">Prix : ordre décroissant</a></li>
		</ul>
	</div>
</div>