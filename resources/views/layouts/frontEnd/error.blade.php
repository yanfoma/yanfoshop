<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    {{--@include('tracking::gtm')--}}
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Expires" content="7" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>
    @yield('post_header')

    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">

    <style>
        body,
        html {
            padding: 0;
            margin: 0;
            width: 100%;
            height: 100%;
            overflow: hidden;
            background-color: #10455b;
            font-family: 'sans-serif';
            color: #ffaf20;
        }

        img {
            width: 30%!important;
        }
        .error {
            text-align: center;
            padding: 16px;
            position: relative;
            top: 50%;
            transform: translateY(-50%);
            -webkit-transform: translateY(-50%)
        }

        h1 {
            margin: -10px 0 -10px;
            font-size: calc(17vw + 40px);
            letter-spacing: 2px;
            opacity: 1;
        }

        p {
            opacity: 1;
            font-size: 20px;
            margin: 8px 0 38px 8px;
            font-weight: bold
        }

        a {
            color: #ffaf20!important;
            font-size: 20px;
            font-weight: bold;
            text-decoration:none;
        }

        .btn-lg {
            font-size: 15px;
            padding: 12px 16px;
        }
        .btn-outline-primary {
            background: #ffaf20!important;
            color: #fff!important;
            border-radius: 8px;
            font-weight: bold;
        }

        .btn-outline-primary:hover{
            background: #fff!important;
            color: #ffaf20!important;
        }

        .fa-arrow-left {
            position: fixed;
            top: 30px;
            left: 30px;
            font-size: 2em;
            color:white;
            text-decoration:none
        }

    </style>

    @include('layouts.frontEnd.header')
    @yield('style')
</head>
<body>
    <!--[if lte IE 9]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
    <![endif]-->
    @yield('content')
</body>
</html>
