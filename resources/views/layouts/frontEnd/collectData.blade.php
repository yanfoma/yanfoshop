<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('tracking::gtm')
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Expires" content="7" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    @yield('post_header')
    <link href="{{asset('css/styleYanfoma.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
    <link rel="stylesheet" href="{{ asset('css/plugins.css')}}">
    <link rel="stylesheet" href="{{ asset('css/user.css') }}"                 type="text/css" media="screen,projection">
    <link rel="stylesheet" href="{{ asset('css/intlTelInput.css') }}"         type="text/css" media="screen,projection">
    @include('layouts.frontEnd.header')
    @yield('style')
</head>
    <body @yield('color')>
        <div class="boxed_wrapper">
            @yield('content')
            <script src="{{asset('js/vendor/jquery-3.3.1.min.js')}}"></script>
            <script src="{{asset('js/popper.min.js')}}"></script>
            <script src="{{asset('js/bootstrap.min.js')}}"></script>
            <script src="{{asset('js/plugins.js')}}"></script>
            @yield('scripts')
        </div>
    </body>
</html>
