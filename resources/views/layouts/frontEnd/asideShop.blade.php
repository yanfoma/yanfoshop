<div class="col-md-3 col-sm-12 col-xs-12 sidebar_styleTwo">
	<div class="wrapper">
		<div class="sidebar_search">
			<form id="searchShop" action="{{route('shop')}}" enctype="application/x-www-form-urlencoded">
				<input type="text" id="query" name="query" placeholder="chercher un produit....">
				<button id="searchbutton" class="tran3s color1_bg"><i class="fa fa-search" aria-hidden="true"></i></button>
			</form>
		</div> <br><br><!-- End of .sidebar_styleOne -->
		<div class="best_sellers clear_fix wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
			<div class="theme_inner_title">
				<h3>Nos Récents Produits</h3>
			</div>
			<div class="best-selling-area">
				@foreach($latests as $latest)
					<div class="best_selling_item clear_fix border">
						<div class="img_holder float_left">
							<a href="{{route('shop.single',['slug' => $latest->slug])}}"><img src="{{asset($latest->image)}}" alt="{{$latest->name}}" width="70" height="70"></a>
						</div> <!-- End of .img_holder -->

						<div class="text float_left">
							<a href="{{route('shop.single',['slug' => $latest->slug])}}"><h4>{{$latest->name}}</h4></a>
							<span>{{$latest->price}} CFA</span>
						</div> <!-- End of .text -->
					</div> <!-- End of .best_selling_item -->
				@endforeach
			</div>

		</div> <!-- End of .best_sellers -->
		<div class="best_sellers clear_fix wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
			<div class="theme_inner_title">
				<h3>Nos Bons Deals</h3>
			</div>
			<div class="best-selling-area">
				@foreach($bondeals as $bondeal)
					<div class="best_selling_item clear_fix border">
						<div class="img_holder float_left">
							<a href="{{route('shop.single',['slug' => $bondeal->slug])}}"><img src="{{asset($bondeal->image)}}" alt="{{$bondeal->name}}" width="70" height="70"></a>
						</div> <!-- End of .img_holder -->

						<div class="text float_left">
							<a href="{{route('shop.single',['slug' => $bondeal->slug])}}"><h4>{{$bondeal->name}}</h4></a>
							<span>{{$bondeal->price}} CFA</span>
						</div> <!-- End of .text -->
					</div> <!-- End of .best_selling_item -->
				@endforeach
			</div>

		</div> <!-- End of .best_sellers -->
	</div> <!-- End of .wrapper -->
</div>

