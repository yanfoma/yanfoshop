<div class="banners-area pb-50 bg-blue-orange6">
	<div class="container2">
		<div class="row">
			<div class="col-lg-2">
				<!-- Category Menu -->
				<div class="catmenu catmenu-2 bg-blue-orange4">
					<button class="catmenu-trigger">
						<span>Categories</span>
					</button>
					<nav class="catmenu-body">
						<ul>
							@foreach($parentCategories as $parentCategory)
								<li class="catmenu-dropdown">
									<a href="#"><i class="ion ion-ios-power"></i>{{$parentCategory->name}}</a>
									<ul class="megamenu catmenu-megamenu">
										@foreach($parentCategory->subcategories as $subcategories)
											<li><a href="#">{{$subcategories->name}}</a>
											<ul>
												@foreach($subcategories->categories as $category)
													<li><a href="{{route('viewCategory',['store_slug' => $category->store->slug,'name' => $category->name ])}}">{{$category->name}}</a></li>
												@endforeach
											</ul>
										</li>
										@endforeach
									</ul>
								</li>
							@endforeach
						</ul>
					</nav>
				</div>
				<!--// Category Menu -->
			</div>
			<div class="col-lg-8">
				<!-- Hero Area -->
				<div class="herobanner herobanner-3 slider-navigation slider-dots">
				@foreach($slides as $slide)
					<!-- Herobanner Single -->
						<div class="herobanner-single">
							<img src="{{$slide->image}}" alt="{{$slide->image}}">
							<div class="herobanner-content">
								<div class="herobanner-box">
									<p></p>
								</div>
								@if($slide->order == 0)
									<div class="herobanner-box">
										<a href="{{route('shop',['store_slug'=> $slide->store->slug])}}" class="ho-button ho-button-white visiter">
											<i class="lnr lnr-cart" style="color: red!important;"></i>
											<span>Visiter</span>
										</a>
									</div>
								@else
									<div class="herobanner-box">
										<a href="{{route('shop','yanfoShop')}}" class="ho-button ho-button-white">
											<i class="lnr lnr-cart"></i>
											<span>Visiter</span>
										</a>
									</div>
								@endif
							</div>
						</div>
				@endforeach
				</div>
				<div class="header-tagsarea bg-blue-orange4 mt-lg-2" id="headerBoutique1">
					<div class="row">
						<div class="col-lg-4 col-md-6 col-12">
							<div class="deals">
								<a href="{{route('homeDeals')}}">Deals</a>
							</div>
						</div>
						<div class="col-lg-4 col-md-6 col-12">
							<div class="deals">
								<a href="{{route('homePromo')}}">Promo</a>
							</div>
						</div>
						<div class="col-lg-4 col-md-6 col-12">
							<div class="deals">
								<a href="{{route('homeNewest')}}">Nouveautes</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-2 access">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<div class="accessItem accessItem1">
								<img class="user-pic" src="//ae01.alicdn.com/kf/HTB1o19DcBKw3KVjSZTE763uRpXap.png" alt="user icon">
								<h5 class="newuser-welcome">Bienvenue sur yanfoma Soko</h5>

								@if(Auth::check())
									<a class="button-access" href="{{route('customer.welcome')}}">Compte</a>
									<a class="button-access" href="{{route('logout')}}">Deconnecter</a>
								@else
									<a class="button-access" href="{{route('login')}}">Connection</a>
									<a class="button-access" href="{{route('register')}}">Rejoindre</a>
								@endif

							</div>
						</div>
						<div class="col-lg-12">
							<div class="accessItem accessItem2">
								<img class="devis" src="https://res.cloudinary.com/yanfomaweb/image/upload/v1571078144/Yanfoma/store-icon.png" alt="devis icon">
								<a href="{{route('nosBoutiques')}}">Nos Boutiques</a>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="accessItem accessItem2">
								<img class="devis" src="https://res.cloudinary.com/yanfomaweb/image/upload/v1571078483/Yanfoma/devis.png" alt="devis icon">
								<a href="{{route('devis',['store_slug' => 'yanfoshop'])}}">Demander Un Devis</a>
							</div>
						</div>

						<div class="col-lg-12">
							<div class="accessItem accessItem2">
								<img class="devis" src="https://res.cloudinary.com/yanfomaweb/image/upload/v1571078589/Yanfoma/heart.png" alt="heart icon">
								<a href="{{route('customer.wishlist')}}">MES Favoris</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>