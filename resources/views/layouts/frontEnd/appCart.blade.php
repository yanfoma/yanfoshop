<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('tracking::gtm')
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Expires" content="7" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>
    @yield('post_header')

    @include('layouts.frontEnd.style')

    @include('layouts.frontEnd.header')
    @yield('style')
</head>
<body>
    <!--[if lte IE 9]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
    <![endif]-->
        <div id="wrapper" class="wrapper">
            @include('layouts.frontEnd.menuHome')
            <main class="page-content">
                @yield('content')
            </main>
            @include('layouts.frontEnd.footerHome')
        </div>
        @include('layouts.frontEnd.scripts')
        @yield('scripts')
</body>
</html>
