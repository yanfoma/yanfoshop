<div class="ho-section newsletter-area bg-blue-gradient ptb-50">
	<div class="container">
		<div class="newsletter">
			<div class="newsletter-title">
				<h2>Suivez-Nous Sur nos Réseaux Sociaux</h2>
			</div>
			<div class="newsletter-content">
			</div>
			<div class="newsletter-socialicons socialicons socialicons-radial">
				<ul>
					@if($store->facebook)
						<li><a href="{{$store->facebook}}"><i class="fa fa-fw fa-facebook"></i> <strong></strong></a></li>
					@endif
					@if($store->messenger)
						<li><a href="{{$store->messenger}}" target="_blank"><i class="fa fa-fw fa-comments"></i> <strong></strong></a></li>
					@endif
					@if($store->whatsapp)
						<li><a href="{{$store->whatsapp}}"><i class="fa fa-fw fa-whatsapp"></i> <strong></strong></a></li>
					@endif
					@if($store->linkedIn)
						<li><a href="{{$store->linkedIn}}"><i class="fa fa-fw fa-linkedin"></i> <strong></strong></a></li>
					@endif
				</ul>
			</div>
		</div>
	</div>
</div>