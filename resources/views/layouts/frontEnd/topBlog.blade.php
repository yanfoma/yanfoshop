

<div class="bg-white pt-30">
    <div class="row">
        <div class="col-lg-3 col-md-6 col-12 featurebox1">
            <div class="featurebox">
                <i class="flaticon-shipped"></i>
                <h5>Livraison Gratuite</h5>
                <p>Livraison Gratuite pour tous nos produits</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-12 featurebox1">
            <div class="featurebox">
                <i class="ion-ios-thumbs-up"></i>
                <h5>Qualité et Originalité</h5>
                <p>Produits de Produits de Qualité et d'Origine Unique</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-12 featurebox1">
            <div class="featurebox">
                <i class="fa fa-bolt"></i>
                <h5>COMMANDE RAPIDE</h5>
                <p>Commande Rapide et Personnalisée </p>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-12 featurebox1">
            <div class="featurebox">
                <i class="flaticon-support"></i>
                <h5>Support 24/7</h5>
                <p>Une Équipe Performante À Votre écoute 24H/24 7j/7</p>
            </div>
        </div>
        <!--// Single Feature -->
    </div>
</div>