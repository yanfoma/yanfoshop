<style>
	.product-title{
		height: 80px!important;
	}
	.product{
		margin-right:3px!important;
		border-radius: 20px;
		border: 5px solid #efefef;
		box-shadow: 0 0 20px rgba(0, 0, 0, 0.2);
		-webkit-box-shadow: 0 0 20px rgba(0, 0, 0, 0.2);
		-moz-box-shadow: 0 0 20px rgba(0, 0, 0, 0.2);
	}
</style>
<div class="ho-section related-product-area pb-30 ">
	<div class="container">
		<div class="section-title">
			<h3>Nos Récents Produits </h3>
		</div>
		<div class="product-slider new-best-featured-slider slider-navigation-2 product">
			@foreach($latests as $product)
				<div class="product-slider-col">
				<!-- Single Product -->
				<article class="hoproduct product">
					<div class="hoproduct-image">
						<a class="hoproduct-thumb" href="{{route('shop.single',['store_slug' => $store->slug, 'slug' => $product->slug])}}">
							<img class="hoproduct-frontimage" src="{{$product->image_url}}" alt="product image">
						</a>
						<ul class="hoproduct-actionbox">
							<li><a href="{{route('store.cart.addDirect',['store_slug' => $store->slug, 'id' => $product->id])}}"><i class="lnr lnr-cart"></i></a></li>
							<li><a href="{{route('shop.single',['store_slug' => $store->slug, 'slug' => $product->slug])}}" ><i class="lnr lnr-eye"></i></a></li>
						</ul>
						<ul class="hoproduct-flags">
							@if($product->onSale && $product->saleType == 2)
								<li class="flag-discount">-{{ $product->salePercentage }} %</li>
							@endif
						</ul>
					</div>
					<div class="hoproduct-content">
						<h5 class="hoproduct-title product-title"><a href="{{route('shop.single',['store_slug' => $store->slug, 'slug' => $product->slug])}}">{{$product->name}}</a></h5>
						<div class="hoproduct-pricebox product-pricebox">
							<div class="pricebox">
								<div class="float_left">
									@if($product->onSale)
										<del class="oldprice">{{number_format($product->price, 0 , ',' , ' ')}} </del>
										<span class="price">{{number_format($product->sale_amount, 0 , ',' , ' ')}} CFA </span>
									@else
										<span class="price">{{number_format($product->price, 0 , ',' , ' ')}} CFA </span>
									@endif
								</div>
								<div class="float_right">
									<i class="fa fa-exclamation-triangle" style="color: yellow;"></i>
									Min: {{$product->minQty}}
								</div>
							</div>
						</div>
					</div>
				</article>
			</div>
			@endforeach
		</div>
	</div>
</div>