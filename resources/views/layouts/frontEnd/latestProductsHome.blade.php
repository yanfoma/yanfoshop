<div class="countdown_product default_product bg-grey pt-2 pb-2 pl-2 pr-2">
	<div class="section_title">
		<h2> BOUTIQUES SPECIALISÉES</h2>
		<div class="icone_img">
			<img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1563057116/Yanfoma/newIconstore.png" alt="">
		</div>
	</div>
	<div class="banner-area">
		<div class="container3">
			<div class="row">
				<div class="col-md-6">
					<div class="imgbanner imgbanner-2 mt-30">
						<a href="{{route('shop',"yanfoshop")}}">
							<img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1563299545/Yanfoma/shop1.jpg" alt="yanfoshop">
						</a>
					</div>
				</div>

				<div class="col-md-6">
					<div class="imgbanner imgbanner-2 mt-30">
						<a href="{{route('shop',"emprise")}}">
							<img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1563403752/Yanfoma/emprise.png" alt="emprise">
						</a>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
<div class="countdown_product default_product bg-grey pt-2 pb-2 pl-2 pr-2">
	<div class="section_title" style="background: #ffeed3;!important;">
		<h2> Nouveautés</h2>
		<div class="icone_img">
			<img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1561554569/Yanfoma/newIcon.png" alt="">
		</div>
	</div>
	<div class="product-slider new-best-featured-slider slider-navigation-2" style="background: #ffeed3;!important;" >
		@foreach($nouveautes as $product)
			<div class="product-slider-col">
				<article class="hoproduct product2">
					<div class="hoproduct-image">
						<a class="hoproduct-thumb" href="{{route('shop.single',['store_slug' =>$product->store->slug,'slug' => $product->slug])}}">
							<img class="hoproduct-frontimage" src="{{$product->image_url}}" alt="{{$product->name}}">
						</a>
						<ul class="hoproduct-actionbox">
							<li><a href="{{route('store.cart.addDirect',['store_slug' => $product->store->slug,'id' => $product->id])}}"><i class="lnr lnr-cart"></i></a></li>
							<li><a href="{{route('shop.single',['store_slug' => $product->store->slug, 'id' => $product->slug])}}"><i class="lnr lnr-eye"></i></a></li>
							@if($product->is_wished_by_auth_user())
								<li>
									<a class="wished" href="{{route('product.unwishlist', ['store_slug' => $product->store->slug,  'id' => $product->id ])}}">
										<i class="lnr lnr-heart"></i>
									</a>
								</li>
							@else
								<li>
									<a class="" href="{{route('product.wishlist', ['store_slug' => $product->store->slug, 'id' => $product->id ])}}">
										<i class="lnr lnr-heart"></i>
									</a>
								</li>
							@endif
							@if($product->is_liked_by_auth_user())
								<li><a class="unlike" href="{{route('product.unlike', [ 'store_slug' => $product->store->slug, 'id' => $product->id ])}}"><i class="lnr lnr-thumbs-up"></i></a></li>
							@else
								<li><a class="" href="{{route('product.like', [ 'store_slug' => $product->store->slug, 'id' => $product->id ])}}"><i class="lnr lnr-thumbs-up"></i></a></li>
							@endif
						</ul>
						<ul class="hoproduct-flags">
							<li class="flag-pack">{{$product->store->name}}</li>
							@if($product->onSale && $product->saleType == 2)
								<li class="flag-discount">-{{ $product->salePercentage }} %</li>
							@endif
						</ul>
					</div>
					<div class="hoproduct-content">
						<h5 class="hoproduct-title product-title"><a href="{{route('shop.single',['store_slug' =>$product->store->slug,'slug' => $product->slug])}}">{{$product->name}}</a></h5>
						<div class="hoproduct-pricebox product-pricebox">
							<div class="pricebox">
								<div class="float_left">
									@if($product->onSale)
										<del class="oldprice">{{number_format($product->price, 0 , ',' , ' ')}} </del>
										<span class="price">{{number_format($product->sale_amount, 0 , ',' , ' ')}} CFA </span>
									@else
										<span class="price">{{number_format($product->price, 0 , ',' , ' ')}} CFA </span>
									@endif
								</div>
								<div class="float_right">
									<i class="fa fa-exclamation-triangle" style="color: yellow;"></i>
									Min: {{$product->minQty}}
								</div>
							</div>
						</div>
					</div>
				</article>
			</div>
		@endforeach
	</div>
</div>