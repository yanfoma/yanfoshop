<!-- Hero Area -->
@if($slides->count())
<div class="herobanner herobanner-2 slider-navigation slider-dots slider-dots-center">
	@foreach($slides as $slide)
	<!-- Herobanner Single -->
	<div class="herobanner-single">
		<img src="{{$slide->image}}" alt="hero image">
		{{--<div class="herobanner-content">--}}
			{{--<div class="herobanner-box">--}}
				{{--<h4>BLUETOOTH SPEAKER</h4>--}}
			{{--</div>--}}
			{{--<div class="herobanner-box">--}}
				{{--<h1>Double Housing <span>Wireless</span></h1>--}}
			{{--</div>--}}
			{{--<div class="herobanner-box">--}}
				{{--<p>Play your music from any Bluetooth enable device.</p>--}}
			{{--</div>--}}
			{{--<div class="herobanner-box">--}}
				{{--<a href="shop-rightsidebar.html" class="ho-button ho-button-white">--}}
					{{--<i class="lnr lnr-cart"></i>--}}
					{{--<span>Shop Now</span>--}}
				{{--</a>--}}
			{{--</div>--}}
		{{--</div>--}}
		<span class="herobanner-progress"></span>
	</div>
	<!--// Herobanner Single -->
	@endforeach
</div>
@endif
<!--// Hero Area -->
<style>
	.herobanner-2 {
		opacity: 0;
		visibility: hidden;
		transition: opacity 1s ease;
		-webkit-transition: opacity 1s ease;
	}
	.herobanner-2.slick-initialized {
		visibility: visible;
		opacity: 1;
	}
</style>

