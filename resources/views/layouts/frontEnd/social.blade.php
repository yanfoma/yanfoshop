<div class="ho-section newsletter-area bg-blue-orange ptb-40 col-md-12 mt-30">
    <div class="container">
        <div class="newsletter">
            <div class="newsletter-title">
                <h2>Suivez-Nous Sur nos Réseaux Sociaux</h2>
            </div>
            <div class="newsletter-content">
            </div>
            <div class="newsletter-socialicons2 socialicons socialicons-radial">
                <ul>
                    <li><a href="https://www.facebook.com/yanfoma/"><i class="fa fa-fw fa-facebook"></i> <strong></strong></a></li>
                    <li><a href="https://m.me/yanfoma" target="_blank"><i class="fa fa-fw fa-comments"></i> <strong></strong></a></li>
                    <li><a href="https://chat.whatsapp.com/invite/EsCjgPWHuPf39ujQKANX5C"><i class="fa fa-fw fa-whatsapp"></i> <strong></strong></a></li>
                    <li><a href="https://www.linkedin.com/company-beta/22333242"><i class="fa fa-fw fa-linkedin"></i> <strong></strong></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
