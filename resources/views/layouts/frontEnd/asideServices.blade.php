<style>
	.service-catergory .active a{
		color: white!important;
	}
</style>
<div class="col-lg-3 col-md-4 col-sm-12">
	<div class="service-sidebar">
		<ul class="service-catergory">
			<li class="{{\Request::is('en/service/partnership' ,'fr/service/partenariat')	?'active':''}}"> <a href="{{route('partnership')}}">Strategic Business PartnerShip</a></li>
			<li class="{{\Request::is('en/service/web'		  ,'fr/service/web')    		?'active':''}}"> <a href="{{route('web')}}">YanfoWeb</a></li>
			<li class="{{\Request::is('en/service/medias'	  ,'fr/service/medias') 		?'active':''}}"> <a href="{{route('medias')}}">YanfoMedias</a></li>
			<li class="{{\Request::is('en/service/analytics'   ,'fr/service/analytics')		?'active':''}}"> <a href="{{route('analytics')}}">YanfoAnalytics</a></li>
			<li class="{{\Request::is('en/service/referral'    ,'fr/service/referral')   	?'active':''}}"> <a href="{{route('referral')}}">Referral</a></li>
		</ul>
		<br><br>
		{{--<div class="inner-title">--}}
			{{--<h3>{{trans('app.Notre Brochure')}}</h3>--}}
		{{--</div>--}}
		{{--<div class="brochures">--}}

			{{--<ul class="brochures-lists">--}}
				{{--<li><a href="#"><span class="fa fa-file-pdf-o"></span>{{trans('app.Our Presentation')}}</a></li>--}}
			{{--</ul>--}}
		{{--</div>--}}
		<div class="contact-info2">
			<h4>{{trans('app.contactUs')}}</h4>
			<div class="text">
				<p>{{trans('app.PleaseContactUs')}}</p>
			</div>
			<ul>
				<li><i class="fa fa-phone"></i>+886 9895-97235</li><br>
				<li><i class="fa fa-envelope"></i><a href="#">info@yanfoma.tech</a></li>
			</ul>
			<a href="{{route('contact')}}" class="thm-btn">{{trans('app.nousContacter')}}</a>
		</div>

	</div>
</div>