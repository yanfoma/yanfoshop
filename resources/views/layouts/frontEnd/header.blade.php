<meta charset="UTF-8">
<meta name="viewport"              content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description"           content="Yanfoma The Hotpot Of Technologies">
<meta name="keywords"              content="Yanfoma Shop">
<meta name="author"                content="Yanfoma">
<meta property="fb:app_id"         content="1530728007250371">
<meta property="og:site_name"      content="yanfoma">
<meta property="og:url"            content="https://yanfoma.com">
<meta property="og:type"           content="website">

{{--<meta http-equiv="Content-Security-Policy" content="default-src *; style-src 'self' http://* 'unsafe-inline'; script-src 'self' http://* 'unsafe-inline' 'unsafe-eval'" />--}}

<meta http-equiv="Content-Security-Policy" content="default-src gap://ready file://* *; style-src 'self' http://* https://* 'unsafe-inline'; script-src 'self' http://* https://* 'unsafe-inline' 'unsafe-eval'">

<link rel="apple-touch-icon" sizes="180x180" href="{{asset('favicon/apple-touch-icon.png')}}">
<link rel="icon" type="image/png" sizes="32x32" href="{{asset('favicon/favicon-32x32.png')}}">
<link rel="icon" type="image/png" sizes="16x16" href="{{asset('favicon/favicon-16x16.png')}}">
<link rel="manifest" href="{{asset('favicon/site.webmanifest')}}">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="theme-color" content="#ffffff">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<meta name="msapplication-TileColor"    content="#ffffff">
<meta name="msapplication-TileImage"    content="{{asset('favicon/favicon.ico')}}">
<meta name="theme-color"                content="#ffffff">
<meta property="al:web:should_fallback" content="true">