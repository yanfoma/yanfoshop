<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('tracking::gtm')
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Security-Policy" content="default-src *; style-src 'self' http://* 'unsafe-inline'; script-src 'self' http://* 'unsafe-inline' 'unsafe-eval'" />
    <meta http-equiv="Expires" content="7" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    @yield('post_header')
    @include('layouts.frontEnd.styleCustommer')
    <link href="{{asset('css/styleYanfoma.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="{{asset('css/custom.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
    <link rel="stylesheet" href="{{ asset('css/user.css') }}"                 type="text/css" media="screen,projection">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/css/bootstrap-select.min.css"                 type="text/css" media="screen,projection">
    <link rel="stylesheet" href="{{ asset('css/intlTelInput.css') }}"         type="text/css" media="screen,projection">
    @include('layouts.frontEnd.header')
    @yield('style')
</head>
<style>
    .badge ,.badge1{
        background-color: red;
        border-radius: 30px;
        position: relative;
        top: -10px;
        color: white;
        display: inline-block;
        font-size: 10px;
        line-height: 1.4;
        padding: 3px 7px;
        text-align: center;
        width: 20px;
        height: 20px;
        vertical-align: middle;
        white-space: nowrap;
    }
    .badge {
        left: -14px;
    }
    .badge1{
        left: -10px;
    }
</style>
    <body @yield('color')>
        <div class="boxed_wrapper">
            @include('layouts.frontEnd.menuHome')
            @yield('content')
            @include('layouts.frontEnd.footerHome')
            @include('layouts.frontEnd.scripts')
            @yield('scripts')
        </div>
    </body>
</html>
