<div class="countdown_product default_product mt-30">
	<div class="section_title">
		<h2> NOS VINS</h2>
		<div class="icone_img">
			<img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1561554092/Yanfoma/wineIcon.png" alt="">
		</div>
	</div>
	<div class="tab-content" id="bstab3-ontent">
			<div class="tab-pane fade show active" id="bstab3-area1" role="tabpanel" aria-labelledby="bstab3-area1-tab">
				<div class="ourproduct-2">
					<div class="row no-gutters">
						<div class="col-lg-5">
							<div class="ourproduct-2-banner imgbanner imgbanner-2">
								<a href="{{route('shop','emprise')}}" target="_blank">
									<img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1561756696/Yanfoma/nosvins.png" alt="EMPRISE">
								</a>
							</div>
						</div>
						<div class="col-lg-7">
							<article class="hoproduct hoproduct-5 ourproduct-2-product">
								<div class="hoproduct-image">
									<a class="hoproduct-thumb" href="{{route('shop.single',['store_slug' => $firstVin->store->slug, 'slug' => $firstVin->slug])}}">
										<img class="hoproduct-frontimage" src="{{$firstVin->image_url}}" alt="{{$firstVin->name}}">
									</a>
									<ul class="hoproduct-flags">
										@if($firstVin->onSale && $firstVin->saleType == 2)
											<li class="flag-discount">-{{ $firstVin->salePercentage }} %</li>
										@endif
									</ul>
								</div>
								<div class="hoproduct-content">
									<h5 class="hoproduct-title"><a href="{{route('shop.single',['store_slug' => $firstVin->store->slug, 'slug' => $firstVin->slug])}}">{{$firstVin->name}}</a></h5>
									<div class="hoproduct-pricebox">
										<div class="pricebox">
											@if($firstVin->onSale)
												<del class="oldprice">{{number_format($firstVin->price, 0 , ',' , ' ')}} </del>
												<span class="price">{{number_format($firstVin->sale_amount, 0 , ',' , ' ')}} CFA </span>
											@else
												<span class="price">{{number_format($firstVin->price, 0 , ',' , ' ')}} CFA </span>
											@endif
										</div>
									</div>
									<a href="{{route('store.cart.addDirect',['store_slug' => $firstVin->store->slug,'id' => $firstVin->id])}}" class="ho-button ">
										<span>Ajouter Au Panier</span>
									</a>
								</div>
							</article>
						</div>
					</div>
					{{--<div class="product-slider ourproduct-2-slider slider-navigation-3 slider-navigation-3-side vps">--}}
						{{--@foreach($vins->products as $product)--}}
							{{--<div class="product-slider-col">--}}
								{{--<article class="hoproduct hoproduct-4">--}}
									{{--<div class="hoproduct-image">--}}
										{{--<a class="hoproduct-thumb" href="{{route('shop.single',['store_slug' => $product->store->slug, 'slug' => $product->slug])}}">--}}
											{{--<img class="hoproduct-frontimage" src="{{$product->image_url}}" alt="{{$product->name}}">--}}
										{{--</a>--}}
									{{--</div>--}}
									{{--<div class="hoproduct-content">--}}
										{{--<h5 class="hoproduct-title"><a href="{{route('shop.single',['store_slug' => $product->store->slug, 'slug' => $product->slug])}}">{{$product->name}}</a></h5>--}}
										{{--<div class="hoproduct-pricebox">--}}
											{{--<div class="pricebox">--}}
												{{--@if($product->onSale)--}}
													{{--<del class="oldprice">{{number_format($product->price, 0 , ',' , ' ')}} </del>--}}
													{{--<span class="price">{{number_format($product->sale_amount, 0 , ',' , ' ')}} CFA </span>--}}
												{{--@else--}}
													{{--<span class="price">{{number_format($product->price, 0 , ',' , ' ')}} CFA </span>--}}
												{{--@endif--}}
											{{--</div>--}}
										{{--</div>--}}
									{{--</div>--}}
								{{--</article>--}}
							{{--</div>--}}
						{{--@endforeach--}}
					{{--</div>--}}
				</div>
			</div>
		</div>
</div>