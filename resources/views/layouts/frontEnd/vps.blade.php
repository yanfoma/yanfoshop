<!-- Our Products Area -->
<div class="ho-section ourproducts-area-2 bg-grey ptb-50">
	<div class="container2">
		<div class="section-title">
			<h3>NOS SERVEURS </h3>
		</div>
		<div class="tab-content" id="bstab3-ontent">
			<div class="tab-pane fade show active" id="bstab3-area1" role="tabpanel" aria-labelledby="bstab3-area1-tab">
				<div class="ourproduct-2">
					<div class="row no-gutters">
						<div class="col-lg-5">
							<div class="ourproduct-2-banner imgbanner imgbanner-2">
								<a href="https://yacs.yanfoma.tech/" target="_blank">
									<img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1552614344/Yanfoma/vpsPub.png" alt="VPS AND YACS PUB">
								</a>
							</div>
						</div>
						<div class="col-lg-7">
							<article class="hoproduct hoproduct-5 ourproduct-2-product">
								<div class="hoproduct-image">
									<a class="hoproduct-thumb" href="{{route('shop.single',['store_slug' => $store->slug, 'slug' => $firstVps->slug])}}">
										<img class="hoproduct-frontimage" src="{{$firstVps->image_url}}" alt="{{$firstVps->name}}">
									</a>
									<ul class="hoproduct-flags">
										@if($firstVps->onSale && $firstVps->saleType == 2)
											<li class="flag-discount">-{{ $firstVps->salePercentage }} %</li>
										@endif
									</ul>
								</div>
								<div class="hoproduct-content">
									<h5 class="hoproduct-title"><a href="{{route('shop.single',['store_slug' => $store->slug, 'slug' => $firstVps->slug])}}">{{$firstVps->name}}</a></h5>
									<div class="hoproduct-pricebox">
										<div class="pricebox">
											@if($firstVps->onSale)
												<del class="oldprice">{{number_format($firstVps->price, 0 , ',' , ' ')}} </del>
												<span class="price">{{number_format($firstVps->sale_amount, 0 , ',' , ' ')}} CFA </span>
											@else
												<span class="price">{{number_format($firstVps->price, 0 , ',' , ' ')}} CFA </span>
											@endif
										</div>
									</div>
									<a href="{{route('store.cart.addDirect',['store_slug' => $store->slug,'id' => $firstVps->id])}}" class="ho-button">
										<span>Ajouter Au Panier</span>
									</a>
								</div>
							</article>
						</div>
					</div>

					<div class="product-slider ourproduct-2-slider slider-navigation-3 slider-navigation-3-side vps">
						@foreach($cloud->products as $product)
							<div class="product-slider-col">
							<article class="hoproduct hoproduct-4">
								<div class="hoproduct-image">
									<a class="hoproduct-thumb" href="{{route('shop.single',['store_slug' => $store->slug, 'slug' => $product->slug])}}">
										<img class="hoproduct-frontimage" src="{{$product->image_url}}" alt="{{$product->name}}">
									</a>
								</div>
								<div class="hoproduct-content">
									<h5 class="hoproduct-title"><a href="{{route('shop.single',['store_slug' => $store->slug, 'slug' => $product->slug])}}">{{$product->name}}</a></h5>
									<div class="hoproduct-pricebox">
										<div class="pricebox">
											@if($product->onSale)
												<del class="oldprice">{{number_format($product->price, 0 , ',' , ' ')}} </del>
												<span class="price">{{number_format($product->sale_amount, 0 , ',' , ' ')}} CFA </span>
											@else
												<span class="price">{{number_format($product->price, 0 , ',' , ' ')}} CFA </span>
											@endif
										</div>
									</div>
								</div>
							</article>
						</div>
						@endforeach
					</div>
				</div>
			</div>
		</div>
	</div>
</div>