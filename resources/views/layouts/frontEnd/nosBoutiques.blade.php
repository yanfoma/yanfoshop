{{--<div class="ho-section categories-area mt-30 bg-welcome4 pt-4 pb-4 pl-2 pr-2" >--}}
{{--    <div class="container">--}}
{{--        <div class="section-title white-text">--}}
{{--            <h3>Nos Boutiques</h3>--}}
{{--        </div>--}}
{{--        <div class="categories-slider-2 ">--}}
{{--            @foreach($stores as $store)--}}
{{--                <div class="category-wrapper">--}}
{{--                <!-- Single Category -->--}}
{{--                <div class="category">--}}
{{--                    <a href="{{route('shop',$store->slug)}}" class="category-thumb">--}}
{{--                        <img src="{{$store->image}}" alt="category image">--}}
{{--                    </a>--}}
{{--                    <div class="category-content">--}}
{{--                        <h6 class="category-title white-text">{{$store->name}}</h6>--}}
{{--                        <span class="category-productcounter">{{$store->products->count()}} Produits</span>--}}
{{--                        <a href="{{route('shop',$store->slug)}}" class="category-productlink">Visiter <i class="ion ion-md-arrow-dropright"></i></a>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <!--// Single Category -->--}}
{{--            </div>--}}
{{--            @endforeach--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}

<div class="mt-30 nosBoutiques pb-4" >
    <div class="container">
        <div class="section-title">
            <h3>Nos Boutiques</h3>
        </div>
        <div class="row">
            @foreach($stores as $store)
            <div class="col-lg-3 store">
                <a href="{{route('shop',$store->slug)}}">
                    <div class="cover">
                        <img src="{{$store->image}}" alt="store image">
                    </div>
                </a>
            </div>
            @endforeach
        </div>
    </div>
</div>