<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10&appId=400025927061215";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
	<div class="blog-sidebar">


		<div class="popular_news">
			<div class="inner-title">
				<h4>{{trans('app.latestPosts')}}</h4>
			</div>

			<div class="popular-post">
				@foreach($posts as $post)
					<div class="item">
						<div class="post-thumb">
							<a href="{{route('post.single',['slug' => $post->slug ])}}">
								<img src="{{asset($post->featuredImage)}}" alt="{{asset($post->title)}}"></a>
						</div>
							<a href="{{route('post.single',['slug' => $post->slug ])}}">
								<h5>{{$post->title}}</h5></a>
						<div class="post-info"><i class="fa fa-calendar"></i>{{$post->created_at}} </div>
					</div>
				@endforeach
			</div>
		</div>
		<br>

		<div class="category-style-one">
			<div class="inner-title">
				<h4>{{trans('app.categories')}}</h4>
			</div>
			<ul class="list">
				@foreach($categories as $category)
					<li><a href="#" class="clearfix"><span class="float_left">{{$category->name}}</span>
							<span class="float_right">({{$category->postTranslations->count()}})</span></a></li>
				@endforeach
			</ul>
		</div><br> <!-- End of .sidebar_categories -->

		<div class="sidebar_tags wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
			<div class="inner-title">
				<h4>Tags</h4>
			</div>
			<ul>
				@foreach($tags as $tag)
					<li><a href="#" class="tran3s">{{$tag->tag}}</a></li>
				@endforeach

			</ul>
		</div> <!-- End of .sidebar_tags -->
		<div class="sb-it archive">
			<h4 class="sb-title">Facebook</h4><br>
			<div class="fb-page" data-href="https://www.facebook.com/yanfoma/" data-tabs="timeline" data-width="300" data-height="400" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/afrikeveil.org/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/afrikeveil.org/">AFRIK EVEIL</a></blockquote></div>
		</div>

	</div> <!-- End of .wrapper -->
</div>