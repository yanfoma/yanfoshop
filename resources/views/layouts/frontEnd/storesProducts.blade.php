<div class="ho-section trending-our-product-area">
	<div class="#">
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<div class="countdown_product default_product mt-30">
					<div class="section_title">
						<h2> Produits Dans Nos Boutiques</h2>
						<div class="icone_img">
							<img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1561554844/Yanfoma/storeIcon.png" alt="">
						</div>
					</div>
					<div class="section-title">
						<ul class="nav" id="bstab2" role="tablist">
							@foreach($stores as $store)
								<li class="nav-item">
									<a class="nav-link @if($store->id == 1) active @endif" id="bstab2-area{{$store->id}}-tab" data-toggle="tab" href="#bstab2-area{{$store->id}}" role="tab"
									   aria-controls="bstab2-area{{$store->id}}" aria-selected="true">{{$store->name}}</a>
								</li>
							@endforeach
						</ul>
					</div>
					<div class="tab-content" id="bstab2-ontent">
						@foreach($stores as $store)
							<div class="tab-pane fade @if($store->id == 1)show  active @endif" id="bstab2-area{{$store->id}}" role="tabpanel" aria-labelledby="bstab2-area{{$store->id}}-tab">
							<div class="product-slider our-products-slider slider-navigation-2">
								@foreach($store->products->sortByDesc('created_at')->take(12) as $product)
									<div class="product-slider-col">
										<article class="hoproduct product">
											<div class="hoproduct-image">
												<a class="hoproduct-thumb" href="{{route('shop.single',['store_slug' =>$product->store->slug,'slug' => $product->slug])}}">
													<img class="hoproduct-frontimage" src="{{$product->image_url}}" alt="{{$product->name}}">
												</a>
												<ul class="hoproduct-actionbox">
													<li><a href="{{route('store.cart.addDirect',['store_slug' => $product->store->slug,'id' => $product->id])}}"><i class="lnr lnr-cart"></i></a></li>
													<li><a href="{{route('shop.single',['store_slug' => $product->store->slug, 'id' => $product->slug])}}"><i class="lnr lnr-eye"></i></a></li>
													@if($product->is_wished_by_auth_user())
														<li>
															<a class="wished" href="{{route('product.unwishlist', ['store_slug' => $product->store->slug,  'id' => $product->id ])}}">
																<i class="lnr lnr-heart"></i>
															</a>
														</li>
													@else
														<li>
															<a class="" href="{{route('product.wishlist', ['store_slug' => $product->store->slug, 'id' => $product->id ])}}">
																<i class="lnr lnr-heart"></i>
															</a>
														</li>
													@endif
													@if($product->is_liked_by_auth_user())
														<li><a class="unlike" href="{{route('product.unlike', [ 'store_slug' => $product->store->slug, 'id' => $product->id ])}}"><i class="lnr lnr-thumbs-up"></i></a></li>
													@else
														<li><a class="" href="{{route('product.like', [ 'store_slug' => $product->store->slug, 'id' => $product->id ])}}"><i class="lnr lnr-thumbs-up"></i></a></li>
													@endif
												</ul>
												<ul class="hoproduct-flags">
													<li class="flag-pack">{{$product->store->name}}</li>
													@if($product->onSale && $product->saleType == 2)
														<li class="flag-discount">-{{ $product->salePercentage }} %</li>
													@endif
												</ul>
											</div>
											<div class="hoproduct-content">
												<h5 class="hoproduct-title product-title"><a href="{{route('shop.single',['store_slug' =>$product->store->slug,'slug' => $product->slug])}}">{{$product->name}}</a></h5>
												<div class="hoproduct-pricebox product-pricebox bg-blue-orange4">
													<div class="pricebox">
														<div class="float_left">
															@if($product->onSale)
																<del class="oldprice">{{number_format($product->price, 0 , ',' , ' ')}} </del>
																<span class="price">{{number_format($product->sale_amount, 0 , ',' , ' ')}} CFA </span>
															@else
																<span class="price">{{number_format($product->price, 0 , ',' , ' ')}} CFA </span>
															@endif
														</div>
														<div class="float_right">
															<i class="fa fa-exclamation-triangle" style="color: yellow;"></i>
															Min: {{$product->minQty}}
														</div>
													</div>
												</div>
											</div>
										</article>
									</div>
								@endforeach
							</div>
						</div>
						@endforeach
					</div>
				</div>
			</div>
			{{--<div class="col-lg-3 col-md-4">--}}
				{{--<div class="categories-area mt-30">--}}
					{{--<div class="section-title">--}}
						{{--<h3>NOS CATÉGORIES</h3>--}}
					{{--</div>--}}
					{{--<div class="categories-slider slider-navigation-2 slider-navigation-2-m0">--}}
						{{--@foreach($categories->chunk(3) as $chunk)--}}
							{{--<div class="category-wrapper">--}}
								{{--@foreach($chunk as $categorie)--}}
									{{--<div class="category">--}}
										{{--<a href="{{route('viewCategory',['store_slug' => $categorie->store->slug,'name' => $categorie->name ])}}" class="category-thumbBlock">--}}
											{{--<span class="thumbnail-img">--}}
                      							{{--<img class="center" src="http://demo.hasthemes.com/urani-v4/urani/assets/img/product/thumbnail/1.png" alt="Urani's Product Thumbnail">--}}
                    						{{--</span>--}}
											{{--<span class="thumbnail-name mt-5">{{$categorie->name}}</span>--}}
										{{--</a>--}}
										{{--<div class="category-content">--}}
											{{--<span class="category-productcounter">Dans: {{$categorie->store->name}} </span>--}}
											{{--<span class="category-productcounter">{{$categorie->products->Count()}} Produit(s)</span>--}}
											{{--<a href="{{route('viewCategory',['store_slug' => $categorie->store->slug,'name' => $categorie->name ])}}" class="category-productlink2">Voir Categorie <i class="ion ion-md-arrow-dropright"></i></a>--}}
										{{--</div>--}}
									{{--</div>--}}
								{{--@endforeach--}}
							{{--</div>--}}
						{{--@endforeach--}}
					{{--</div>--}}
				{{--</div>--}}
			{{--</div>--}}
		</div>
	</div>
</div>