<div class="col-lg-3 col-md-6 col-sm-6 order-2 order-lg-3">
	<div class="header-icons">
		<div class="header-account">
			<button class="header-accountbox-trigger"><span class="lnr lnr-user"></span> Mon Compte <i class="ion ion-ios-arrow-down"></i></button>
			<ul class="header-accountbox dropdown-list">
				<li>
					<a href="{{route('customer.welcome')}}">Mes Informations</a>
				</li>
				<li>
					<a href="{{route('cart.index')}}">Mon Panier</a>
				</li>
				<li>
					<a href="{{ route('customer.commandeView')}}">Mes Commandes</a>
				</li>
				<li>
					<a href="{{route('customer.wishlist')}}">Mes Favoris</a>
				</li>
				<li>
					<a href="{{route('logout')}}">Se Deconnecter</a>
				</li>
			</ul>
		</div>
		<div class="header-cart">
			<div class="header-carticon" href="{{route('cart.index')}}"><i class="lnr lnr-cart"></i>
				@if(Cart::content()->count())
					<span class="count">{{Cart::content()->count()}}</span>
				@endif
				@if(Cart::content()->count())
				<!-- Minicart -->
					<div class="header-minicart minicart">
						<div class="minicart-header">
							@foreach(Cart::content() as $product)
								<div class="minicart-product">
									<div class="minicart-productimage">
										<a href="#">
											<img src="{{asset($product->model->image_url)}}" alt="{{$product->name}}">
										</a>
										<span class="minicart-productquantity">{{$product->qty}}</span>
									</div>
									<div class="minicart-productcontent">
										<h6><a href="#">{{$product->name}}</a></h6>
										<span class="minicart-productprice">{{$product->price}} CFA</span>
									</div>
								</div>
							@endforeach
						</div>
						<ul class="minicart-pricing">
							<li>Estimation <span>20-50 jours</span></li>
							<li>Total <span>{{$product->total()}} CFA</span></li>
						</ul>
						<div class="minicart-footer">
							<a href="{{route('cart.index')}}" class="ho-button ho-button-fullwidth">
								<span>Panier</span>
							</a>
							<a href="{{route('cart.checkout')}}" class="ho-button ho-button-dark ho-button-fullwidth">
								<span>Passer la commande</span>
							</a>
						</div>
					</div>
					<!--// Minicart -->
				@endif
			</div>
		</div>
	</div>
</div>