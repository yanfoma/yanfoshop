<!-- Styles -->
<!-- CSS files -->
{{--<link rel="stylesheet" href="https://yanfoma.net/css/frontEnd/style.css"        type="text/css" media="screen,projection">--}}
{{--<link rel="stylesheet" href="https://yanfoma.tech/css/frontEnd/responsive.css"  type="text/css" media="screen,projection">--}}

<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('css/plugins.css')}}">
<link rel="stylesheet" href="{{ asset('css/style.css') }}">
<!-- Custom Styles -->
<link rel="stylesheet" href="{{ asset('css/customCustommer.css') }}">
<link rel="stylesheet" href="{{ asset('css/toastr.min.css') }}"                 type="text/css" media="screen,projection">
<link href="{{ asset('css/dropify.min.css') }}"                                 type="text/css" rel="stylesheet" media="screen,projection">
{{--<link href="{{ asset('css/frontEnd/style.css') }}"                              type="text/css" rel="stylesheet" media="screen,projection">--}}

<link rel="stylesheet" href="{{asset('css/owl.carousel.css')}}">
<link rel="stylesheet" href="{{asset('css/owl.theme.default.min.css')}}">

{{--<!-- Latest compiled and minified CSS -->--}}
{{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/css/bootstrap-select.min.css">--}}