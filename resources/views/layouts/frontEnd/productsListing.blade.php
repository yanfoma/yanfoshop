@if($products->count())
<div class="shop-filters mt-30">
	<div class="shop-filters-viewmode"></div>
	<span class="shop-filters-viewitemcount"></span>
	<div class="shop-filters-sortby">
		<div class="select-sortby">
			<button class="select-sortby-current">Filtrer Par:</button>
			<ul class="select-sortby-list dropdown-list">
				<li><a href="{{ route('shop',['store_slug' =>$store->slug,'query' => "newest"])}}">Dernières nouveautés</a></li>
				<li><a href="{{ route('shop',['store_slug' =>$store->slug,'query' => "nameAsc"])}}">Nom: A-Z</a></li>
				<li><a href="{{ route('shop',['store_slug' =>$store->slug,'query' => "nameDesc"])}}">Nom: Z-A</a></li>
				<li><a href="{{ route('shop',['store_slug' =>$store->slug,'query' => "priceAsc"])}}">Prix : Ordre croissant</a></li>
				<li><a href="{{ route('shop',['store_slug' =>$store->slug,'query' => "priceDesc"])}}">Prix : ordre décroissant</a></li>
			</ul>
		</div>
	</div>
</div>

<div class="shop-page-products mt-30">
	<div class="row no-gutters">
		@foreach($products as $product)
			<div class="col-lg-3 col-md-4 col-sm-6 col-12">
			<!-- Single Product -->
			<article class="hoproduct product">
				<div class="hoproduct-image product-image">
					<a class="hoproduct-thumb" href="{{route('shop.single',['store_slug' =>$store->slug,'slug' => $product->slug])}}">
						<img class="hoproduct-frontimage" src="{{$product->image_url}}" alt="{{$product->name}}">
					</a>
					<ul class="hoproduct-actionbox">
						<li><a href="{{route('store.cart.addDirect',['store_slug' => $store->slug,'id' => $product->id])}}"><i class="lnr lnr-cart"></i></a></li>
						<li><a href="{{route('shop.single',['store_slug' => $store->slug, 'id' => $product->slug])}}"><i class="lnr lnr-eye"></i></a></li>
						@if($product->is_wished_by_auth_user())
							<li>
								<a class="wished" href="{{route('product.unwishlist', ['store_slug' => $store->slug,  'id' => $product->id ])}}">
									<i class="lnr lnr-heart"></i>
								</a>
							</li>
						@else
							<li>
								<a class="" href="{{route('product.wishlist', ['store_slug' => $store->slug, 'id' => $product->id ])}}">
									<i class="lnr lnr-heart"></i>
								</a>
							</li>
						@endif
						@if($product->is_liked_by_auth_user())
							<li><a class="unlike" href="{{route('product.unlike', [ 'store_slug' => $store->slug, 'id' => $product->id ])}}"><i class="lnr lnr-thumbs-up"></i></a></li>
						@else
							<li><a class="" href="{{route('product.like', [ 'store_slug' => $store->slug, 'id' => $product->id ])}}"><i class="lnr lnr-thumbs-up"></i></a></li>
						@endif
					</ul>
					<ul class="hoproduct-flags">
						@if($product->onSale && $product->saleType == 2)
							<li class="flag-discount">-{{ $product->salePercentage }} %</li>
						@endif
					</ul>
				</div>
				<div class="hoproduct-content">
					<h5 class="hoproduct-title product-title"><a href="{{route('shop.single',['store_slug' =>$store->slug,'slug' => $product->slug])}}">{{$product->name}}</a></h5>
					<div class="hoproduct-pricebox product-pricebox">
						<div class="pricebox">
								<div class="float_left">
									@if($product->onSale)
										<del class="oldprice">{{number_format($product->price, 0 , ',' , ' ')}} </del>
										<span class="price">{{number_format($product->sale_amount, 0 , ',' , ' ')}} CFA </span>
									@else
										<span class="price">{{number_format($product->price, 0 , ',' , ' ')}} CFA </span>
									@endif
								</div>
								<div class="float_right">
									<i class="fa fa-exclamation-triangle" style="color: yellow;"></i>
									Min: {{$product->minQty}}
								</div>
						</div>
					</div>
				</div>
			</article>
			</div>
		@endforeach
	</div>
</div>
	@else
	<div  style="height: 450px; padding: 200px;">
		<h1> Aucun Produit pour l'instant !!!</h1>
	</div>
@endif
