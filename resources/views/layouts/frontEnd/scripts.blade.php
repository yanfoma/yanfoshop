<!-- jQuery js -->
{{--<script src="{{asset('js/jquery.js')}}"></script>--}}
{{--<!-- bootstrap js -->--}}
{{--<script src="{{asset('js/bootstrap.min.js')}}"></script>--}}
<!-- jQuery ui js -->

{{--<!-- jQuery validation -->--}}
{{--<script src="{{asset('js/frontEnd/jquery.validate.min.js')}}"></script>--}}

{{--<!--dropify-->--}}
{{--<script type="text/javascript" src="{{asset('js/dropify.min.js')}}"></script>--}}

{{--<!-- Latest compiled and minified JavaScript -->--}}
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/js/bootstrap-select.min.js"></script>--}}


{{--<!-- mixit up -->--}}
{{--<script src="{{asset('js/frontEnd/wow.js')}}"></script>--}}
{{--<script src="{{asset('js/frontEnd/jquery.mixitup.min.js')}}"></script>--}}
{{--<script src="{{asset('js/frontEnd/jquery.fitvids.js')}}"></script>--}}
{{--<script src="{{asset('js/frontEnd/bootstrap-select.min.js')}}"></script>--}}
{{--<script src="{{asset('js/frontEnd/menuzord.js')}}"></script>--}}

{{--<!-- revolution slider js -->--}}
{{--<script src="{{asset('js/frontEnd/jquery.themepunch.tools.min.js')}}"></script>--}}
{{--<script src="{{asset('js/frontEnd/jquery.themepunch.revolution.min.js')}}"></script>--}}
{{--<script src="{{asset('js/frontEnd/revolution.extension.actions.min.js')}}"></script>--}}
{{--<script src="{{asset('js/frontEnd/revolution.extension.carousel.min.js')}}"></script>--}}
{{--<script src="{{asset('js/frontEnd/revolution.extension.kenburn.min.js')}}"></script>--}}
{{--<script src="{{asset('js/frontEnd/revolution.extension.layeranimation.min.js')}}"></script>--}}
{{--<script src="{{asset('js/frontEnd/revolution.extension.migration.min.js')}}"></script>--}}
{{--<script src="{{asset('js/frontEnd/revolution.extension.navigation.min.js')}}"></script>--}}
{{--<script src="{{asset('js/frontEnd/revolution.extension.parallax.min.js')}}"></script>--}}
{{--<script src="{{asset('js/frontEnd/revolution.extension.slideanims.min.js')}}"></script>--}}
{{--<script src="{{asset('js/frontEnd/revolution.extension.video.min.js')}}"></script>--}}

{{--<!-- fancy box -->--}}
{{--<script src="{{asset('js/frontEnd/jquery.fancybox.pack.js')}}"></script>--}}
{{--<script src="{{asset('js/frontEnd/jquery.polyglot.language.switcher.js')}}"></script>--}}
{{--<script src="{{asset('js/frontEnd/nouislider.js')}}"></script>--}}
{{--<script src="{{asset('js/frontEnd/jquery.bootstrap-touchspin.js')}}"></script>--}}
{{--<script src="{{asset('js/frontEnd/SmoothScroll.js')}}"></script>--}}
{{--<script src="{{asset('js/frontEnd/jquery.appear.js')}}"></script>--}}
{{--<script src="{{asset('js/frontEnd/jquery.countTo.js')}}"></script>--}}
{{--<script src="{{asset('js/frontEnd/jquery.flexslider.js')}}"></script>--}}
{{--<script src="{{asset('js/frontEnd/imagezoom.js')}}"></script>--}}
{{--<script src="{{asset('js/frontEnd/default-map.js')}}" id="map-script" ></script>--}}
{{--<script src="{{asset('js/frontEnd/custom.js')}}"></script>--}}

<!-- Js Files -->
<script src="{{asset('js/vendor/modernizr-3.6.0.min.js')}}"></script>
<script src="{{asset('js/vendor/jquery-3.3.1.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/jquery-ui.js')}}"></script>
<script src="{{asset('js/plugins.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>
<!-- owl carousel js -->
<script src="{{asset('js/owl.carousel.min.js')}}"></script>
<!-- owl carousel js -->
<script type="text/javascript" src="{{asset('js/owl.carousel.min.js')}}"></script>
<script src="{{asset('js/toastr.min.js')}}"  type="text/javascript" ></script>
<!--scrollPress JS BACK TO TOP-->
<script src="">
    $(document).ready(function(){
        $(".owl-carousel").owlCarousel({
            margin:10,
            responsiveClass:true,
            loop:true,
            lazyLoad: false,
            nav:true,
            navText: [" ", " "],
            navClass: ["owl-prev triangle", "owl-next triangle right"],
        });
    });
</script>

<script type="text/javascript">
    if ('serviceWorker' in navigator) {
        console.log("Will the service worker register?");
        navigator.serviceWorker.register('../sw.js')
            .then(function(reg){
                console.log("Yes, it did.");
            }).catch(function(err) {
            console.log("No it didn't. This happened: ", err)
        });
    }

</script>


<script type="text/javascript">
    @if(Session::has('success'))
        toastr.success("{{ Session::get('success') }}");
    @endif
    @if(Session::has('info'))
        toastr.info("{{ Session::get('info') }}");
    @endif
    @if (Session::has('sweet_alert.alert'))
        swal({
        text: "{!! Session::get('sweet_alert.text') !!}",
        title: "{!! Session::get('sweet_alert.title') !!}",
        timer: "{!! Session::get('sweet_alert.timer') !!}",
        type: "{!! Session::get('sweet_alert.type') !!}",
        showConfirmButton: "{!! Session::get('sweet_alert.showConfirmButton') !!}",
        confirmButtonText: "{!! Session::get('sweet_alert.confirmButtonText') !!}",
        confirmButtonColor: "#AEDEF4"});
    @endif
</script>
