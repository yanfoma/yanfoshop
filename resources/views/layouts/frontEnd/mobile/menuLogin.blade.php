<div id="menu-hider"></div>
<div id="menu-1" data-selected="menu-components"  class="menu-box menu-sidebar-left-over">
    <div class="menu-socials">
        <a href="https://www.facebook.com/yanfoma/" class="font-12"><i class="fab facebook-color fa-facebook-f"></i></a>
        <a href="https://m.me/yanfoma" class="font-12"><i class="fa twitter-color fa-comments-o"></i></a>
        <a href="https://chat.whatsapp.com/invite/EsCjgPWHuPf39ujQKANX5C" class="font-12"><i class="fab color-green-dark fa-whatsapp"></i></a>
        <a href="https://www.linkedin.com/company-beta/22333242" class="font-13"><i class="fa  twitter-color fa-linkedin"></i></a>
        <a href="#" class="font-14 close-menu"><i class="fa color-red-dark fa-times"></i></a>
    </div>
    <a href="{{route('welcome')}}" class="menu-logo"></a>
    {{--<em class="menu-sub-logo">The Ultimate Mobile Solution</em>--}}
    <div class="menu">
        <a class="menu-item active-item" href="{{route('welcome')}}"><i class="font-15 fa color-primary-soko fa-home"></i><strong>Acceuil</strong><i class="fa fa-circle"></i></a>
        <a class="menu-item" href="{{route('mobile.boutiques')}}"><i class="font-14 fa color-primary-soko fa-institution"></i><strong>Nos Boutiques</strong><i class="fa fa-circle"></i></a>
        <a class="menu-item" href="{{route('yanfomasoko.cart.index')}}"><i class="font-14 fas color-primary-soko fa-shopping-bag"></i><strong>Panier</strong><i class="fa fa-circle"></i></a>
        <a class="menu-item {{\Request::is('auth-center/login') ? 'menu-active':''}} " href="{{route('login')}}"><i class="font-15 fa color-primary-soko fa-sign-in"></i><strong>Se Connecter</strong><i class="fa fa-circle"></i></a>
        <a class="menu-item {{\Request::is('auth-center/register') ? 'menu-active':''}}" href="{{route('register')}}"><i class="font-15 fa color-primary-soko fa-user-plus"></i><strong>S'enregistrer</strong><i class="fa fa-circle"></i></a>
        <a class="menu-item" href="{{route('yanfomaSoko.contactUs')}}"><i class="font-14 fa color-primary-soko fa-envelope"></i><strong>Contact</strong><i class="fa fa-circle"></i></a>
        <em class="menu-divider"> Tous Droits Réservés <span class="copyright-year"></span><i class="fa fa-copyright"></i></em>
    </div>
</div>
<div id="menu-find" data-load="menu-find.html" data-height="420" class="menu-box menu-load menu-bottom"></div>

<div id="footer-menu" class="footer-menu-5-icons bg-yellow1-dark" style="margin-top: 50px;!important;">
    <a href="{{route('mobile.boutiques')}}" class="{{\Request::is('mobile/nos-boutiques') ?'active-menu':''}}"><i class="fa fa-institution"></i><span>Boutiques</span></a>
    @if(Auth::check())
        <a href="#" data-menu="menu-cart" class="{{\Request::is('panier') ?'active-menu':''}}"><i class="fas fa-shopping-bag"></i><span>Panier</span></a>
    @else
        <a href="{{route('yanfomasoko.cart.index')}}" class="{{\Request::is('panier') ?'active-menu':''}}"><i class="fas fa-shopping-bag"></i><span>Panier</span></a>
    @endif
    <a href="{{route('welcome')}}" class="{{\Request::is('/') | \Request::is('mobile') ?'active-menu':''}}"><i class="fa fa-home"></i><span>Acceuil</span></a>
    <a href="{{route('yanfomaSoko.about')}}" class="{{\Request::is('a-propos-de-nous') ?'active-menu':''}}" ><i class="fa fa-info-circle"></i><span>A Propos</span></a>
    <a href="{{route('yanfomaSoko.contactUs')}}" class="{{\Request::is('nous-contacter') ?'active-menu':''}}" ><i class="fa fa-envelope"></i><span>Contact</span></a>
    <div class="clear"></div>
</div>

<div class="header header-scroll-effect">
    <div class="header-line-1 header-hidden header-logo-app">
        <a href="#" class="back-button header-logo-title">Retour</a>
        <a href="#" class="back-button header-icon header-icon-1"><i class="fa fa-angle-left"></i></a>
        {{--<a href="#" data-menu="menu-find" class="header-icon header-icon-3"><i class="fa fa-search"></i></a>--}}
        <a href="#" data-menu="menu-1" class="header-icon header-icon-4"><i class="fa fa-bars"></i></a>
    </div>
    <div class="header-line-2 header-scroll-effect">
        <a href="#" class="header-pretitle header-date2 color-highlight"><br></a>
        <a href="{{route('welcome')}}" class="header-title font-23 center-text">Yanfoma <span class="soko">Soko</span></a>
        <a href="#" data-menu="menu-1" class="header-icon header-icon-1"><i class="fa fa-bars"></i></a>
        {{--<a href="#" data-menu="menu-find" class="header-icon header-icon-2"><i class="fa fa-search"></i></a>--}}
    </div>
</div>
