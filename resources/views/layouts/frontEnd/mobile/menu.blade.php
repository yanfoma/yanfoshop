<div id="menu-hider"></div>
<div id="menu-1" data-selected="menu-components"  class="menu-box menu-sidebar-left-parallax">
    <div class="menu-socials">
        <a href="https://www.facebook.com/yanfoma/" class="font-12"><i class="fab facebook-color fa-facebook-f"></i></a>
        <a href="https://m.me/yanfoma" class="font-12"><i class="fa twitter-color fa-comments-o"></i></a>
        <a href="https://chat.whatsapp.com/invite/EsCjgPWHuPf39ujQKANX5C" class="font-12"><i class="fab color-green-dark fa-whatsapp"></i></a>
        <a href="https://www.linkedin.com/company-beta/22333242" class="font-13"><i class="fa  twitter-color fa-linkedin"></i></a>
        <a href="#" class="font-14 close-menu"><i class="fa color-red-dark fa-times"></i></a>
    </div>
    <a href="{{route('welcome')}}" class="menu-logo"></a>
    {{--<em class="menu-sub-logo">The Ultimate Mobile Solution</em>--}}
    <div class="menu">
        <a class="menu-item {{\Request::is('boutiques/'.$store->slug) ?'menu-active':''}}" href="{{route('shop',['store_slug' => $store->slug])}}"><i class="font-15 fa color-blue-dark fa-home"></i><strong>Acceuil</strong><i class="fa fa-circle"></i></a>
        <a class="menu-item {{\Request::is('categories/'.$store->slug) ?'menu-active':''}}" href="{{route('categories',['store_slug' => $store->slug])}}"><i class="font-14 fa color-blue-dark fa-table"></i><strong>Categories</strong><i class="fa fa-circle"></i></a>
        <a class="menu-item {{\Request::is($store->slug.'/mon-panier') ?'menu-active':''}}" href="{{route('store.cart.index',['store_slug' => $store->slug])}}"><i class="font-14 fa color-blue-dark fa-shopping-bag"></i><strong>Panier</strong><i class="fa fa-circle"></i></a>
        <a class="menu-item {{\Request::is($store->slug.'/ils-parlent-de-nous') ?'menu-active':''}}" href="{{route('testimonyViewAll',['store_slug' => $store->slug])}}"><i class="font-17 fa color-blue-dark fa-star"></i><strong>Temoignages</strong><i class="fa fa-circle"></i></a>
        <a class="menu-item {{\Request::is($store->slug.'/partagez-nous-votre-experience') ?'menu-active':''}}" href="{{route('testimonyIndex',['store_slug' => $store->slug])}}"><i class="font-17 fa color-blue-dark fa-smile"></i><strong>Votre Avis Compte </strong><i class="fa fa-circle"></i></a>
        @if($store->id == 1)
          <a class="menu-item {{\Request::is($store->slug.'/faq') ?'menu-active':''}}" href="{{route('faq',['store_slug' => $store->slug])}}"><i class="font-14 fa color-blue-dark fa-tasks"></i><strong>Foire aux Questions</strong><i class="fa fa-circle"></i></a>
        @endif
        <a class="menu-item {{\Request::is('centre-d-aide/'.$store->slug) ?'menu-active':''}} " href="{{route('mobile.help',['store_slug' => $store->slug])}}"><i class="font-17 fa color-blue-dark fa-question-circle"></i><strong>Centre D'aide </strong><i class="fa fa-circle"></i></a>
        @if(Auth::check())
            <a class="menu-item  {{\Request::is('customer/aide','customer/deconnection') ?'menu-active':''}}" href="{{route('logout')}}">                <i class="font-17 fa  color-blue-dark fa fa-sign-out"></i><strong>Deconnection <i class="fa fa-circle"></i></strong></a>
        @else
            <a class="menu-item" href="{{route('login')}}"><i class="font-15 fa color-blue-dark fa-lock"></i><strong>Se Connecter</strong><i class="fa fa-circle"></i></a>
            <a class="menu-item" href="{{route('register')}}"><i class="font-15 fa color-blue-dark fa-user-plus"></i><strong>S'enregistrer</strong><i class="fa fa-circle"></i></a>
        @endif
        <em class="menu-divider"> Tous Droits Réservés <span class="copyright-year"></span><i class="fa fa-copyright"></i></em>
    </div>
</div>
<div id="menu-list" data-selected="menu-components"  class="menu-box menu-sidebar-left-over">
    {{--<div class="menu-title">--}}
        {{--<span class="color-highlight">Check out our pages</span>--}}
        {{--<h1>Navigation</h1>--}}
        {{--<a href="#" class="menu-hide"><i class="fa fa-times"></i></a>--}}
    {{--</div>--}}
    {{--<div class="menu-page">--}}
        {{--<ul class="menu-list">--}}
            {{--<li id="menu-index">--}}
                {{--<a href="index.html">--}}
                    {{--<i class='fa fa-home color-green-dark'></i>--}}
                    {{--<span>Homepage</span>--}}
                    {{--<em>This is where it all Begins</em>--}}
                    {{--<i class="fa fa-angle-right"></i>--}}
                {{--</a>--}}
            {{--</li>--}}
            {{--<li id="menu-components">--}}
                {{--<a href="components.html">--}}
                    {{--<i class='fa fa-cog color-yellow-dark'></i>--}}
                    {{--<span>Components</span>--}}
                    {{--<em>Just a Copy and Paste Away</em>--}}
                    {{--<i class="fa fa-angle-right"></i>--}}
                {{--</a>--}}
            {{--</li>--}}
            {{--<li id="menu-pages">--}}
                {{--<a href="pages-list.html">--}}
                    {{--<i class='fa fa-heart color-red-dark'></i>--}}
                    {{--<span>Site Pages</span>--}}
                    {{--<em>Easy to Customize and Use</em>--}}
                    {{--<i class="fa fa-angle-right"></i>--}}
                {{--</a>--}}
            {{--</li>--}}
            {{--<li id="menu-media">--}}
                {{--<a href="media.html">--}}
                    {{--<i class='fa fa-camera color-brown-light'></i>--}}
                    {{--<span>Media</span>--}}
                    {{--<em>Showcase your Projects with Style</em>--}}
                    {{--<i class="fa fa-angle-right"></i>--}}
                {{--</a>--}}
            {{--</li>--}}
            {{--<li id="menu-contact">--}}
                {{--<a href="page-contact.html">--}}
                    {{--<i class='fa fa-envelope color-blue-dark'></i>--}}
                    {{--<span>Get in Touch</span>--}}
                    {{--<em>Let's get in Touch or Just Say Hello</em>--}}
                    {{--<i class="fa fa-angle-right"></i>--}}
                {{--</a>--}}
            {{--</li>--}}
        {{--</ul>--}}
    {{--</div>--}}
</div>
<div id="menu-cart" data-height="420" class="menu-box menu-bottom" style="transition: all 300ms ease; display: block; height: 420px; top: auto;">
    @if(Auth::check())
        @if($cart->count())
            <div class="content content-boxed content-boxed-padding">
                <div class="menu-title">
                    <h4 class="primary-heading center-text">Panier: {{$cart->count()}} Produit(s)</h4>
                    <a href="#" class="menu-hide"><i class="fa fa-times"></i></a>
                </div>
                @foreach($cart as $product)
                    <div class="store-cart-2">
                        <img class="preload-image" src="{{asset($product->product->image_url)}}" data-src="{{asset($product->product->image_url)}}" alt="{{$product->product->name}}">
                        <strong>{{$product->product->name}}</strong>
                        <span>Boutique: {{getStoreName($product->store_id)}}</span>
                        <em>{{$product->product_total}}<del>{{$product->product_price}}</del></em>
                        <input class="quantity" type="text" value="{{$product->product_qty}}" name="quantity" readonly>
                    </div>
                @endforeach
                <div class="decoration"></div>
                <div class="store-cart-total top-20 bottom-30">
                    <strong class="font-16 uppercase ultrabold">Total</strong>
                    <span class="font-16 uppercase ultrabold">{{getTotal()}} CFA</span>
                    <div class="clear"></div>
                </div>
                <div class="decoration"></div>
                <a href="{{route('store.cart.checkout',['store_slug' => $store->slug])}}" class="button bg-primary button-rounded button-full button-sm ultrabold uppercase shadow-small">Passer la commande</a>
            </div>
        @else
            <div class="menu-title">
                <h4 class="center-text primary-heading">Panier</h4>
                <a href="#" class="menu-hide"><i class="fa fa-times"></i></a>
            </div>
            <div class="above-overlay">
                <img class="" src="{{asset('images/empty-cart.png')}}" width="100" style="left: 30%;margin: 20px;">
                <p class="color-primary opacity-100 center-text bottom-10">
                    Ajouter des produits pour commencer
                </p>
                <a href="{{route('shop',['store_slug' => $store->slug])}}" class="button button-s button-center bg-primary button-rounded top-10 bottom-10 uppercase ultrabold ">Acceuil</a>
            </div>
        @endif
    @endif
</div>
<div id="menu-find" data-load="menu-find.html" data-height="420" class="menu-box menu-load menu-bottom"></div>

<div id="footer-menu" class="footer-menu-5-icons page-content-light-gray bg-primary" style="margin-top: 50px;!important;">
    <a href="{{route('welcome')}}" class="{{\Request::is('mobile/nos-boutiques') ?'active-menu2':''}}"><i class="fa fa-institution"></i><span>Yanfoma Soko</span></a>
    @if(Auth::check())
        <a href="#" data-menu="menu-cart" class="{{\Request::is($store->slug.'/mon-panier') ?'active-menu2':''}} "><i class="fas fa-shopping-bag"></i><span>Panier</span></a>
    @else
        <a href="{{route('store.cart.index',['store_slug' => $store->slug])}}" class="{{\Request::is($store->slug.'/mon-panier') ?'active-menu2':''}}"><i class="fas fa-shopping-bag"></i><span>Panier</span></a>
    @endif
    <a href="{{route('shop',['store_slug' => $store->slug])}}" class="{{\Request::is('boutiques/'.$store->slug) ?'active-menu2':''}}"><i class="fa fa-home"></i><span>Acceuil</span></a>
    @if(Auth::check())
        <a href="{{route('customer.welcome')}}" class="{{\Request::is('customer/mes-informations') ? 'active-menu':''}}"><i class="fas fa-user"></i><span>Mon Compte</span></a>
    @else
        <a href="{{route('login')}}" class="{{\Request::is('auth-center/login') ?'active-menu':''}}"><i class="fas fa-lock"></i><span>Se Connecter</span></a>
    @endif
    <a href="{{route('contact',['store_slug' => $store->slug])}}" class="{{\Request::is($store->slug.'/contact') ?'active-menu2':''}} " ><i class="fa fa-envelope"></i><span>Contact</span></a>
    <div class="clear"></div>
</div>
<div class="header header-scroll-effect">
    <div class="header-line-1 header-hidden header-logo-app">
        <a href="#" class="back-button header-logo-title">Retour</a>
        <a href="#" class="back-button header-icon header-icon-1"><i class="fa fa-angle-left"></i></a>
        {{--<a href="#" data-menu="menu-find" class="header-icon header-icon-3"><i class="fa fa-search"></i></a>--}}
        <a href="#" data-menu="menu-1" class="header-icon header-icon-4"><i class="fa fa-bars"></i></a>
    </div>
    <div class="header-line-2 header-scroll-effect">
        <a href="#" class="header-pretitle header-date2 color-highlight"><br></a>
        <a href="#" class="header-title font-23">Yanfoma <span class="soko">Soko</span></a>
        <a href="#" data-menu="menu-1" class="header-icon header-icon-1"><i class="fa fa-bars"></i></a>
{{--        <a href="#" data-menu="menu-find" class="header-icon header-icon-2"><i class="fa fa-search"></i></a>--}}
    </div>
</div>
