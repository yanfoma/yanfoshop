<link rel="stylesheet" type="text/css" href="{{ asset('css/mobile/style.css') }}">
<link rel="manifest " href="{{ asset('manifest.json') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('fonts/css/fontawesome-all.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/mobile/framework.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/mobile/framework-store.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/mobile/framework-news.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/mobile/framework-blog.css') }}">