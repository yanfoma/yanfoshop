<div id="menu-hider"></div>
<div id="menu-1" data-selected="menu-components"  class="menu-box menu-sidebar-left-over">
    <div class="menu-socials">
        <a href="https://www.facebook.com/yanfoma/" class="font-12"><i class="fab facebook-color fa-facebook-f"></i></a>
        <a href="https://m.me/yanfoma" class="font-12"><i class="fa twitter-color fa-comments-o"></i></a>
        <a href="https://chat.whatsapp.com/invite/EsCjgPWHuPf39ujQKANX5C" class="font-12"><i class="fab color-green-dark fa-whatsapp"></i></a>
        <a href="https://www.linkedin.com/company-beta/22333242" class="font-13"><i class="fa  twitter-color fa-linkedin"></i></a>
        <a href="#" class="font-14 close-menu"><i class="fa color-red-dark fa-times"></i></a>
    </div>
    <a href="{{route('welcome')}}" class="menu-logo"></a>
    {{--<em class="menu-sub-logo">The Ultimate Mobile Solution</em>--}}
    <div class="menu">
        <a class="menu-item {{\Request::is('/') ? 'menu-active':''}}" href="{{route('welcome')}}"><i class="font-15 fa color-primary-soko fa-home"></i><strong>Acceuil</strong><i class="fa fa-circle"></i></a>
        <a class="menu-item {{\Request::is('a-propos-de-nous') ? 'menu-active':''}}" href="{{route('yanfomaSoko.about')}}"><i class="font-15 fa color-primary-soko fa-info-circle"></i><strong>A Propos</strong><i class="fa fa-circle"></i></a>
        <a class="menu-item {{\Request::is('mobile/nos-boutiques') ? 'menu-active':''}} " href="{{route('mobile.boutiques')}}"><i class="font-14 fa color-primary-soko fa-institution"></i><strong>Nos Boutiques</strong><i class="fa fa-circle"></i></a>
        <a class="menu-item {{\Request::is('panier') ? 'menu-active':''}}" href="{{route('yanfomasoko.cart.index')}}"><i class="font-14 fas color-primary-soko fa-shopping-bag"></i><strong>Panier</strong><i class="fa fa-circle"></i></a>
        <a class="menu-item {{\Request::is('nous-contacter') ? 'menu-active':''}}" href="{{route('yanfomaSoko.contactUs')}}"><i class="font-14 fa color-primary-soko fa-envelope"></i><strong>Contact</strong><i class="fa fa-circle"></i></a>
        <em class="menu-divider">ESPACE CLIENT<i class="fa fa-help color-blue-dark"></i></em>
        @if(Auth::check())
            <a class="menu-item {{\Request::is('customer/mes-informations') ?'menu-active':''}}"                 href="{{route('customer.welcome')}}">      <i class=" font-17 fa color-primary-soko fa fa-user"></i>       <strong>MES INFORMATIONS</strong> <i class="fa fa-circle"></i></a>
            <a class="menu-item {{\Request::is('customer/mon-adresse') ?'menu-active':''}}"                      href="{{route('customer.adresse')}}">      <i class=" font-17 fa color-primary-soko fa fa-map-marker"></i> <strong>ADDRESSES</strong> <i class="fa fa-circle"></i></a>
            <a class="menu-item {{\Request::is('customer/mon-mot-de-passe') ?'menu-active':''}}"                 href="{{route('customer.password')}}">     <i class=" font-17 fa color-primary-soko fa fa-lock"></i>       <strong>Changer De Mot De Passe </strong> <i class="fa fa-circle"></i></a>
            <a class="menu-item {{\Request::is('customer/mes-commandes') ?'menu-active':''}}"                    href="{{route('customer.commandeView')}}"> <i class="font-14 fa color-primary-soko fa-bars"></i> <strong>Mes Commandes </strong><i class="fa fa-circle"></i></a>
            <a class="menu-item {{\Request::is('customer/ma-liste-de-souhaits') ?'menu-active':''}}"             href="{{route('customer.wishlist')}}">     <i class="font-17 fa color-primary-soko fa fa-heart"></i><strong>Mes Favoris </strong><i class="fa fa-circle"></i></a>
            <a class="menu-item  {{\Request::is('en/customer/aide','customer/aide') ?'menu-active':''}}"         href="{{route('customer.aide')}}">         <i class="font-17 fa color-primary-soko fa fa-question-circle"></i><strong>Aide</strong><i class="fa fa-circle"></i></a>
            <a class="menu-item  {{\Request::is('en/customer/aide','customer/deconnection') ?'menu-active':''}}" href="{{route('logout')}}">                <i class="font-17 fa color-primary-soko fa fa-sign-out"></i><strong>Deconnection <i class="fa fa-circle"></i></strong></a>
        @else
            <a class="menu-item {{\Request::is('auth-center/login') ? 'menu-active':''}} " href="{{route('login')}}"><i class="font-15 fa color-primary-soko fa-sign-in"></i><strong>Se Connecter</strong><i class="fa fa-circle"></i></a>
            <a class="menu-item {{\Request::is('auth-center/register') ? 'menu-active':''}}" href="{{route('register')}}"><i class="font-15 fa color-primary-soko fa-user-plus"></i><strong>S'enregistrer</strong><i class="fa fa-circle"></i></a>
        @endif
        <em class="menu-divider"> Tous Droits Réservés <span class="copyright-year"></span><i class="fa fa-copyright"></i></em>
    </div>
</div>
<div id="menu-cart" data-height="420" class="menu-box menu-bottom " style="transition: all 300ms ease; display: block; height: 420px; top: auto;">
    @if(Auth::check())
        @if($cart->count())
            <div class="content content-boxed content-boxed-padding">
                <div class="menu-title">
                    <h4 class="primary-heading-soko center-text">Panier: {{$cart->count()}} Produit(s)</h4>
                    <a href="#" class="menu-hide"><i class="fa fa-times"></i></a>
                </div>
                @foreach($cart as $product)
                    <div class="store-cart-2">
                        <img class="preload-image" src="{{asset($product->product->image_url)}}" data-src="{{asset($product->product->image_url)}}" alt="{{$product->product->name}}">
                        <strong>{{$product->product->name}}</strong>
                        <span>Boutique: {{getStoreName($product->store_id)}}</span>
                        <em>{{$product->product_total}}<del>{{$product->product_price}}</del></em>
                        <input class="quantity" type="text" value="{{$product->product_qty}}" name="quantity" readonly>
                    </div>
                @endforeach
                <div class="decoration"></div>
                <div class="store-cart-total top-20 bottom-30">
                    <strong class="font-16 uppercase ultrabold">Total</strong>
                    <span class="font-16 uppercase ultrabold">{{getTotal()}} CFA</span>
                    <div class="clear"></div>
                </div>
                <div class="decoration"></div>
                <a href="{{route('yanfomasoko.cart.checkout')}}" class="button bg-yellow-dark button-rounded button-full button-sm ultrabold uppercase shadow-small">Passer la commande</a>
            </div>
        @else
            <div class="content bottom-0">
                <div class="menu-title">
                    <h4 class="primary-heading-soko center-text top-5">Votre Panier est Vide</h4>
                    <a href="#" class="menu-hide"><i class="fa fa-times"></i></a>
                </div>
                <div class="above-overlay">
                    <img class="" src="{{asset('images/empty-cart.png')}}" width="100" style="left: 30%;margin: 20px;">
                    <p class="color-primary-soko opacity-80 center-text bottom-20">
                        Ajouter des produits pour commencer
                    </p>
                    <a href="{{route('mobile.boutiques')}}" class="button button-s button-center bg-yellow-dark button-rounded top-20 bottom-10 uppercase ultrabold ">Nos Boutiques</a>
                </div>
            </div>
        @endif
    @endif
</div>
<div id="footer-menu" class="footer-menu-5-icons bg-yellow1-dark" style="margin-top: 50px;!important;">
    <a href="{{route('mobile.boutiques')}}" class="{{\Request::is('mobile/nos-boutiques') ? 'active-menu':''}}"><i class="fa fa-institution"></i><span>Boutiques</span></a>
    @if(Auth::check())
        <a href="#" data-menu="menu-cart" class="{{\Request::is('panier') ? 'active-menu':''}}"><i class="fas fa-shopping-bag"></i><span>Panier</span></a>
    @else
        <a href="{{route('yanfomasoko.cart.index')}}" class="{{\Request::is('panier') ?'active-menu':''}}"><i class="fas fa-shopping-bag"></i><span>Panier</span></a>
    @endif
    <a href="{{route('welcome')}}" class="{{\Request::is('/') | \Request::is('/') ? 'active-menu':''}}"><i class="fa fa-home"></i><span>Acceuil</span></a>
    @if(Auth::check())
        <a href="{{route('customer.welcome')}}" class="{{\Request::is('customer/mes-informations') ? 'active-menu':''}}"><i class="fas fa-user"></i><span>Mon Compte</span></a>
    @else
        <a href="{{route('login')}}" class="{{\Request::is('auth-center/login') ?'active-menu':''}}"><i class="fas fa-lock"></i><span>Se Connecter</span></a>
    @endif
    <a href="{{route('yanfomaSoko.contactUs')}}" class="{{\Request::is('nous-contacter') ? 'active-menu':''}}" ><i class="fa fa-envelope"></i><span>Contact</span></a>
    <div class="clear"></div>
</div>

<div class="header header-scroll-effect">
    <div class="header-line-1 header-hidden header-logo-app">
        <a href="#" class="back-button header-logo-title">Retour</a>
        <a href="#" class="back-button header-icon header-icon-1"><i class="fa fa-angle-left"></i></a>
        {{--<a href="#" data-menu="menu-find" class="header-icon header-icon-3"><i class="fa fa-search"></i></a>--}}
        <a href="#" data-menu="menu-1" class="header-icon header-icon-4"><i class="fa fa-bars"></i></a>
    </div>
    <div class="header-line-2 header-scroll-effect">
        <a href="{{route('welcome')}}" class="header-pretitle header-date2 color-highlight"><br></a>
        <a href="{{route('welcome')}}" class="header-title font-18 center-text">Yanfoma <span class="soko">Soko</span></a>
        <a href="#" data-menu="menu-1" class="header-icon header-icon-1"><i class="fa fa-bars"></i></a>
        {{--<a href="#" data-menu="menu-find" class="header-icon header-icon-2"><i class="fa fa-search"></i></a>--}}
    </div>
</div>
