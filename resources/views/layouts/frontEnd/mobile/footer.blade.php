<!-- Footer -->
<footer class="footer bg-violet">
    <!-- Footer Top Area -->
    @if($store->id == 1)
        <div class="footer-toparea">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-12">
                    <div class="footer-widget widget-info">
                        <h5 class="footer-widget-title">A Propos de YanfoShop</h5>
                        <p style="text-align: justify">yanfoShop est un shopping mall en ligne qui se veut être une plateforme d'accès, de partages et d'échanges de technologie numérique, la première destination en ligne des makers, développeurs, entrepreneurs...</p>
                        <a href="{{route('aboutUs',['store_slug' => $store->slug])}}" class="default_link">En Savoir Plus <i class="fa fa-angle-right"></i></a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-3">
                    <div class="footer-widget widget-links">
                        <h5 class="footer-widget-title">METHODES DE PAIEMENT</h5>
                        <ul>
                            <li><a href="{{route('payments',['store_slug' => $store->slug])}}#Bitcoin">Bitcoin</a></li>
                            <li><a href="{{route('payments',['store_slug' => $store->slug])}}#EcobankPay">Ecobank Pay</a></li>
                            <li><a href="{{route('payments',['store_slug' => $store->slug])}}#OrangeMoney">Orange Money</a></li>
                            <li><a href="{{route('payments',['store_slug' => $store->slug])}}#CCK">Cash Chez Kaanu (CCK)</a></li>
                            <li><a href="{{route('payments',['store_slug' => $store->slug])}}#DepotBancaire">Dépôt Bancaire</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3">
                    <div class="footer-widget widget-links">
                        <h5 class="footer-widget-title">CENTRE D'AIDE</h5>
                        <ul>
                            <li><a href="{{route('devis',['store_slug' => $store->slug])}}">Demander Un Devis</a></li>
                            <li><a href="{{route('aboutUs',['store_slug' => $store->slug])}}#video">YanfoShop En Video</a></li>
                            <li><a href="{{route('faq',['store_slug' => $store->slug])}}">Foire Aux Questions</a></li>
                            <li><a href="{{route('suivre',['store_slug' => $store->slug])}}">Tout Savoir Sur Ma Commande</a></li>
                            <li><a href="{{route('testimonyViewAll',['store_slug' => $store->slug])}}"> Témoignages</a></li>
                            <li><a href="{{route('testimonyIndex',['store_slug' => $store->slug])}}">Votre Avis Compte</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4">
                    <div class="footer-widget widget-info">
                        <h5 class="footer-widget-title">NOUS CONTACTER</h5>
                        <ul>
                            <li><i class="ion ion-ios-pin"></i> Ouagadougou, Burkina Faso</li>
                            <li><i class="ion ion-ios-pin"></i> Abidjan, Cote d'Ivoire</li>
                            <li><i class="ion ion-ios-pin"></i> Yaounde, Cameroun</li>
                            <li><i class="ion ion-ios-pin"></i> Kigali, Rwanda</li>
                            <li><i class="ion ion-ios-call"></i> +(226) 71 33 15 23 </li>
                            <li><i class="ion ion-ios-call"></i> +(226) 74 33 42 77  </li>
                            <li><i class="ion ion-ios-call"></i> +(886) 989 59 72 35  </li>
                            <li><i class="ion ion-ios-mail"></i> <a href="#">info@yanfoma.tech</a></li>
                        </ul>
                    </div>
                </div>
                {{--<div class="col-lg-3 col-12">--}}
                    {{--<div class="footer-widget widget-customerservice">--}}
                        {{--<div class="info">--}}
                            {{--<h5 class="footer-widget-title">CUSTOMER SERVICE</h5>--}}
                            {{--<h5>SEND AN EMAIL</h5>--}}
                            {{--<h5>HOTLINE: : <a href="#">+88.2345.6789</a></h5>--}}
                            {{--<h6>8:00AM–5.30PM AEST MON–FRI</h6>--}}
                        {{--</div>--}}
                        {{--<div class="payment">--}}
                            {{--<h6>SECURE PAYMENT VIA</h6>--}}
                            {{--<img src="images/icons/payment.png" alt="footer payment">--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>
    @endif

    <!-- Footer Bottom -->
    <div class="footer-bottomarea bg-theme">
        <div class="container">
            <div class="footer-copyright">
                <p class="copyright">© 2016-2019 Tous Droits Réservés<a href="https://yanfoma.tech"> Yanfoma</a></p>
            </div>
        </div>
    </div>
    <!--// Footer Bottom -->

</footer>
<!--// Footer -->

{{--<footer class="main-footer">--}}
    {{--<!--Widgets Section-->--}}
    {{--<div class="widgets-section">--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<!--Big Column-->--}}
                {{--<div class="big-column col-md-6 col-sm-12 col-xs-12">--}}
                    {{--<div class="row clearfix">--}}
                        {{--<!--Footer Column-->--}}
                        {{--<div class="footer-column col-md-6 col-sm-6 col-xs-12">--}}
                            {{--<div class="footer-widget about-widget">--}}
                                {{--<h3 class="footer-title">A Propos de YanfoShop</h3>--}}

                                {{--<div class="widget-content">--}}
                                    {{--<div class="text-justify"><p>yanfoShop est un shopping mall en ligne qui se veut être une plateforme d'accès,--}}
                                            {{--de partages et d'échanges de technologie numérique, la première destination en ligne des makers,--}}
                                            {{--développeurs, entrepreneurs...</p> </div>--}}
                                    {{--<div class="link">--}}
                                        {{--<a href="{{route('aboutUs')}}" class="default_link">En Savoir Plus <i class="fa fa-angle-right"></i></a>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<!--Footer Column-->--}}
                        {{--<div class="footer-column col-md-6 col-sm-6 col-xs-12">--}}
                            {{--<div class="footer-widget links-widget">--}}
                                {{--<h3 class="footer-title">METHODES DE PAIEMENT</h3>--}}
                                {{--<div class="widget-content">--}}
                                    {{--<ul class="list">--}}
                                        {{--<li><a href="{{route('payments')}}#Bitcoin">Bitcoin</a></li>--}}
                                        {{--<li><a href="{{route('payments')}}#EcobankPay">Ecobank Pay</a></li>--}}
                                        {{--<li><a href="{{route('payments')}}#OrangeMoney">Orange Money</a></li>--}}
                                        {{--<li><a href="{{route('payments')}}#CCK">Cash Chez Kaanu (CCK)</a></li>--}}
                                        {{--<li><a href="{{route('payments')}}#DepotBancaire">Dépôt Bancaire</a></li>--}}
                                    {{--</ul>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}

                {{--<!--Big Column-->--}}
                {{--<div class="big-column col-md-6 col-sm-12 col-xs-12">--}}
                    {{--<div class="row clearfix">--}}
                        {{--<!--Footer Column-->--}}
                        {{--<div class="footer-column col-md-6 col-sm-6 col-xs-12">--}}
                            {{--<div class="footer-widget links-widget">--}}
                                {{--<h3 class="footer-title">CENTRE D'AIDE</h3>--}}
                                {{--<div class="widget-content">--}}
                                    {{--<ul class="list">--}}
                                        {{--<li><a href="{{route('devis')}}">Demander Un Devis</a></li>--}}
                                        {{--<li><a href="{{route('aboutUs')}}#video">YanfoShop En Video</a></li>--}}
                                        {{--<li><a href="{{route('faq')}}">Foire Aux Questions</a></li>--}}
                                        {{--<li><a href="{{route('suivre')}}">Tout Savoir Sur Ma Commande</a></li>--}}
                                        {{--<li><a href="{{route('testimonyViewAll')}}"> Témoignages</a> / <a href="{{route('testimonyIndex')}}">Votre Avis Compte</a></li>--}}
                                    {{--</ul>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<!--Footer Column-->--}}
                        {{--<div class="footer-column col-md-6 col-sm-6 col-xs-12">--}}
                            {{--<div class="footer-widget contact-widget">--}}
                                {{--<h3 class="footer-title">NOUS CONTACTER</h3>--}}
                                {{--<div class="widget-content">--}}
                                    {{--<ul class="contact-info">--}}
                                        {{--<li><span class="fa fa-map-marker"></span>Ouagadougou Burkina Faso</li>--}}
                                        {{--<li><span class="fa fa-phone"></span>+(226) 71 33 15 23  <br>+(226) 74 33 42 77</li>--}}
                                        {{--<li><span class="fa fa-envelope"></span>info@yanfoma.tech</li>--}}
                                    {{--</ul>--}}
                                {{--</div>--}}
                                {{--<ul class="social">--}}
                                    {{--<li><a href="https://www.facebook.com/yanfoma/"                         target="_blank"><i class="fa fa-facebook"></i></a></li>--}}
                                    {{--<li><a href="https://chat.whatsapp.com/invite/EsCjgPWHuPf39ujQKANX5C"  target="_blank"><i class="fa fa-whatsapp"></i></a></li>--}}
                                    {{--<li><a href="https://m.me/yanfoma"  target="_blank"><i class="fa fa-comments"></i></a></li>--}}
                                    {{--<li><a href="https://www.linkedin.com/company-beta/22333242"            target="_blank"><i class="fa fa-linkedin"></i></a></li>--}}
                                {{--</ul>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}

            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<!--Footer Bottom-->--}}
    {{--<section class="footer-bottom">--}}
        {{--<div class="container">--}}
            {{--<div class=" copy-text center">--}}
                {{--<p>{{trans('app.copyRigths')}}<a href="https://yanfoma.tech"> Yanfoma.</a></p>--}}

            {{--</div><!-- /.pull-right -->--}}
        {{--</div><!-- /.container -->--}}
    {{--</section>--}}
{{--</footer>--}}

{{--<!-- Scroll Top Button -->--}}
{{--<button class="scroll-top tran3s color1_bg">--}}
    {{--<span class="fa fa-angle-up"></span>--}}
{{--</button>--}}
{{--<!--Start of Tawk.to Script-->--}}
{{--<script type="text/javascript">--}}
    {{--var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();--}}
    {{--(function(){--}}
        {{--var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];--}}
        {{--s1.async=true;--}}
        {{--s1.src='https://embed.tawk.to/5a9f8a4b4b401e45400d79ae/default';--}}
        {{--s1.charset='UTF-8';--}}
        {{--s1.setAttribute('crossorigin','*');--}}
        {{--s0.parentNode.insertBefore(s1,s0);--}}
    {{--})();--}}
{{--</script>--}}
{{--<!--End of Tawk.to Script-->--}}