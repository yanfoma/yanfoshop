<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('tracking::gtm')
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Expires" content="7" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>
    @yield('post_header')

    @include('layouts.frontEnd.mobile.style')

    @include('layouts.frontEnd.mobile.header')
    @yield('style')
</head>
<body>
    <div id="page-transitions" class="page-build light-skin highlight-blue">
        @include('layouts.frontEnd.mobile.menuLogin')
        <div style="margin-bottom: 50px;!important;">
            @yield('content')
        </div>
    </div>
    @include('layouts.frontEnd.mobile.scripts')
    @yield('scripts')
</body>
</html>
