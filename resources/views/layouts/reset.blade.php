<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Yanfoma Soko Changer de Mot De Passe</title>

    <!-- Styles -->
    <link href="{{ asset('css/materialize.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="{{ asset('css/style.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
    <!-- Custome CSS-->
    <link href="{{ asset('css/custom.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
<!--  <link href="{{ asset('css/page-center.css') }}" type="text/css" rel="stylesheet" media="screen,projection"> -->
    {{--<link href="{{ asset('frontEnd/bootstrap.css') }}" type="text/css" rel="stylesheet">--}}
    {{--<link href="{{ asset('frontEnd/bootstrap.min.css') }}" type="text/css" rel="stylesheet" media="screen,projection">--}}

</head>

<body>
@yield('style')
@yield('content')
@include('layouts.scripts')
@yield('scripts')
</body>
</html>
